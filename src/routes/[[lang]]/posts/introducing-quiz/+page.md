<script context="module" lang="ts">
	import type { Metadata } from '../lib';
	import BlogLayout from '$lib/BlogLayout.svelte';
	import image from './image.png';
	import studentA from './studentA.png';
	import studentB from './studentB.png';

	export let metadata: Metadata = {
		title: 'Phụ nữ phường Phú Hậu',
		description: 'Phụ nữ phường Phú Hậu.',
		date: new Date('2024-01-30T15:00:00-04:00'),
		image,
		imageAlt: "Quiz: Phụ nữ phường Phú Hậu",
		published: true
	};
</script>

<BlogLayout {metadata}>
Hội Phụ Nữ Phường Phú Hậu - Tấm gương sáng về tinh thần tương thân tương ái

Hội Phụ Nữ phường Phú Hậu là tổ chức chính trị - xã hội vững mạnh, đóng vai trò quan trọng trong công cuộc xây dựng và phát triển cộng đồng địa phương. Trong năm qua, dù gặp không ít khó khăn, thách thức, nhưng với tinh thần đoàn kết, nhiệt huyết và trách nhiệm, các thành viên Hội đã triển khai nhiều hoạt động thiết thực, mang lại lợi ích thiết thực cho phụ nữ và cộng đồng.

Một trong những hoạt động nổi bật là chương trình "Vững bước phụ nữ Phú Hậu" nhằm hỗ trợ phụ nữ có hoàn cảnh khó khăn, bị ảnh hưởng bởi đại dịch COVID-19. Không những cung cấp kịp thời các nhu yếu phẩm thiết yếu, mà chương trình còn trao tặng nhiều suất học bổng, hỗ trợ phụ nữ khởi nghiệp, tạo việc làm, ổn định cuộc sống. Đây là minh chứng sống động về tinh thần tương thân tương ái, sẻ chia, đùm bọc của chị em phụ nữ phường Phú Hậu.

Bên cạnh đó, Hội còn tích cực tham gia các hoạt động bảo vệ môi trường, như thực hiện phong trào "Ngày chủ nhật xanh", thu gom rác thải, trồng cây xanh tại các điểm công cộng. Các chị em còn tích cực tham gia các lớp tập huấn, nâng cao kiến thức pháp luật, kỹ năng sống, chăm sóc sức khỏe, góp phần xây dựng gia đình no ấm, bình đẳng, tiến bộ, hạnh phúc.

Với những đóng góp tích cực, Hội Phụ Nữ phường Phú Hậu đã được công nhận là tổ chức Hội xuất sắc cấp quận, xứng đáng là tấm gương sáng về tinh thần tương thân tương ái, vì cuộc sống ngày càng tốt đẹp hơn của phụ nữ và cộng đồng.
<figure>
	<img src={studentA} alt="Student A" />
	<img src={studentB} alt="Student B" />
	<figcaption>Figure: Screens of two students in one team</figcaption>
</figure>

</BlogLayout>

<style>
	figure {
		margin: 0;
		display: flex;
		flex-wrap: wrap;
		gap: 0.5em;
	}

	figure img {
		flex: 1;
		width: 100%;
		min-width: 15ch;
		border: 0.15em solid;
		border-radius: 0.7em;
	}

	figcaption {
		width: 100%;
		text-align: center;
	}
</style>
