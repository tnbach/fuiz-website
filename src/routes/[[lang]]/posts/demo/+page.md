<script context="module" lang="ts">
	import type { Metadata } from '../lib';
	import BlogLayout from '$lib/BlogLayout.svelte';

	export let metadata: Metadata = {
		title: 'Demo',
		description: 'A free alternative to Kahoot with enhanced collaboration.',
		date: new Date('2024-01-30T15:00:00-04:00'),
		published: true
	};
</script>

<BlogLayout {metadata}>
Lorem sit amet, consectetuer adip

</BlogLayout>

<style>
	figure {
		margin: 0;
		display: flex;
		flex-wrap: wrap;
		gap: 0.5em;
	}

	figure img {
		flex: 1;
		width: 100%;
		min-width: 15ch;
		border: 0.15em solid;
		border-radius: 0.7em;
	}

	figcaption {
		width: 100%;
		text-align: center;
	}
</style>
