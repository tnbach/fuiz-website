-- Drop tables if they exist
DROP TABLE IF EXISTS denied_submissions;
DROP TABLE IF EXISTS approved_submissions;
DROP TABLE IF EXISTS pending_submissions;

-- Recreate the tables
CREATE TABLE denied_submissions (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    r2_key TEXT,
    denied_by TEXT,
    reason TEXT,
    created_at INTEGER
);

CREATE TABLE approved_submissions (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT,
    author TEXT,
    published INTEGER,
    public_url TEXT,
    r2_key TEXT,
    approved_by TEXT,
    tags TEXT,
    slides_count INTEGER,
    thumbnail TEXT,
    alt TEXT,
    played_count INTEGER,
    last_updated INTEGER,
    language TEXT
);

CREATE TABLE pending_submissions (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    assigned BOOLEAN,
    desired_id TEXT,
    r2_key TEXT
);
