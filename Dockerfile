# Use the official Bun image if available
FROM oven/bun:1.0

# If an official Bun image isn't available, start with a base image
# FROM ubuntu:latest

# Install Bun manually if using a base image (skip if using the official Bun image)
# RUN apt-get update && apt-get install -y curl
# RUN curl -fsSL https://bun.sh/install | bash

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the project files to the container
COPY . .

# Install dependencies (if any)
RUN bun install
RUN bun add vite

# Expose the port your app runs on
EXPOSE 5173

# Command to run your app using Bun
CMD ["bun", "run", "dev"]