var __require = /* @__PURE__ */ ((x3) => typeof require !== "undefined" ? require : typeof Proxy !== "undefined" ? new Proxy(x3, {
  get: (a, b) => (typeof require !== "undefined" ? require : a)[b]
}) : x3)(function(x3) {
  if (typeof require !== "undefined")
    return require.apply(this, arguments);
  throw new Error('Dynamic require of "' + x3 + '" is not supported');
});

// .wrangler/tmp/bundle-F0QOhY/checked-fetch.js
var urls = /* @__PURE__ */ new Set();
function checkURL(request, init2) {
  const url = request instanceof URL ? request : new URL(
    (typeof request === "string" ? new Request(request, init2) : request).url
  );
  if (url.port && url.port !== "443" && url.protocol === "https:") {
    if (!urls.has(url.toString())) {
      urls.add(url.toString());
      console.warn(
        `WARNING: known issue with \`fetch()\` requests to custom HTTPS ports in published Workers:
 - ${url.toString()} - the custom port will be ignored when the Worker is published using the \`wrangler deploy\` command.
`
      );
    }
  }
}
globalThis.fetch = new Proxy(globalThis.fetch, {
  apply(target, thisArg, argArray) {
    const [request, init2] = argArray;
    checkURL(request, init2);
    return Reflect.apply(target, thisArg, argArray);
  }
});

// .wrangler/tmp/pages-ebF2m8/bundledWorker-0.003586811502288345.mjs
var __require2 = /* @__PURE__ */ ((x3) => typeof __require !== "undefined" ? __require : typeof Proxy !== "undefined" ? new Proxy(x3, {
  get: (a, b) => (typeof __require !== "undefined" ? __require : a)[b]
}) : x3)(function(x3) {
  if (typeof __require !== "undefined")
    return __require.apply(this, arguments);
  throw new Error('Dynamic require of "' + x3 + '" is not supported');
});
var urls2 = /* @__PURE__ */ new Set();
function checkURL2(request, init2) {
  const url = request instanceof URL ? request : new URL(
    (typeof request === "string" ? new Request(request, init2) : request).url
  );
  if (url.port && url.port !== "443" && url.protocol === "https:") {
    if (!urls2.has(url.toString())) {
      urls2.add(url.toString());
      console.warn(
        `WARNING: known issue with \`fetch()\` requests to custom HTTPS ports in published Workers:
 - ${url.toString()} - the custom port will be ignored when the Worker is published using the \`wrangler deploy\` command.
`
      );
    }
  }
}
globalThis.fetch = new Proxy(globalThis.fetch, {
  apply(target, thisArg, argArray) {
    const [request, init2] = argArray;
    checkURL2(request, init2);
    return Reflect.apply(target, thisArg, argArray);
  }
});
var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __require22 = /* @__PURE__ */ ((x3) => typeof __require2 !== "undefined" ? __require2 : typeof Proxy !== "undefined" ? new Proxy(x3, {
  get: (a, b) => (typeof __require2 !== "undefined" ? __require2 : a)[b]
}) : x3)(function(x3) {
  if (typeof __require2 !== "undefined")
    return __require2.apply(this, arguments);
  throw Error('Dynamic require of "' + x3 + '" is not supported');
});
var __esm = (fn, res) => function __init() {
  return fn && (res = (0, fn[__getOwnPropNames(fn)[0]])(fn = 0)), res;
};
var __commonJS = (cb, mod) => function __require222() {
  return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key2 of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key2) && key2 !== except)
        __defProp(to, key2, { get: () => from[key2], enumerable: !(desc = __getOwnPropDesc(from, key2)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));
var DEV;
var init_prod_ssr = __esm({
  ".svelte-kit/output/server/chunks/prod-ssr.js"() {
    DEV = false;
  }
});
function noop() {
}
function is_promise(value) {
  return !!value && (typeof value === "object" || typeof value === "function") && typeof /** @type {any} */
  value.then === "function";
}
function run(fn) {
  return fn();
}
function blank_object() {
  return /* @__PURE__ */ Object.create(null);
}
function run_all(fns) {
  fns.forEach(run);
}
function safe_not_equal(a, b) {
  return a != a ? b == b : a !== b || a && typeof a === "object" || typeof a === "function";
}
function subscribe(store, ...callbacks) {
  if (store == null) {
    for (const callback of callbacks) {
      callback(void 0);
    }
    return noop;
  }
  const unsub = store.subscribe(...callbacks);
  return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
}
function null_to_empty(value) {
  return value == null ? "" : value;
}
function set_current_component(component10) {
  current_component = component10;
}
function get_current_component() {
  if (!current_component)
    throw new Error("Function called outside component initialization");
  return current_component;
}
function setContext(key2, context) {
  get_current_component().$$.context.set(key2, context);
  return context;
}
function getContext(key2) {
  return get_current_component().$$.context.get(key2);
}
function ensure_array_like(array_like_or_iterator) {
  return array_like_or_iterator?.length !== void 0 ? array_like_or_iterator : Array.from(array_like_or_iterator);
}
function escape(value, is_attr = false) {
  const str = String(value);
  const pattern2 = is_attr ? ATTR_REGEX : CONTENT_REGEX;
  pattern2.lastIndex = 0;
  let escaped2 = "";
  let last = 0;
  while (pattern2.test(str)) {
    const i = pattern2.lastIndex - 1;
    const ch = str[i];
    escaped2 += str.substring(last, i) + (ch === "&" ? "&amp;" : ch === '"' ? "&quot;" : "&lt;");
    last = i + 1;
  }
  return escaped2 + str.substring(last);
}
function escape_attribute_value(value) {
  const should_escape = typeof value === "string" || value && typeof value === "object";
  return should_escape ? escape(value, true) : value;
}
function each(items, fn) {
  items = ensure_array_like(items);
  let str = "";
  for (let i = 0; i < items.length; i += 1) {
    str += fn(items[i], i);
  }
  return str;
}
function validate_component(component10, name) {
  if (!component10 || !component10.$$render) {
    if (name === "svelte:component")
      name += " this={...}";
    throw new Error(
      `<${name}> is not a valid SSR component. You may need to review your build config to ensure that dependencies are compiled, rather than imported as pre-compiled modules. Otherwise you may need to fix a <${name}>.`
    );
  }
  return component10;
}
function create_ssr_component(fn) {
  function $$render(result, props, bindings, slots, context) {
    const parent_component = current_component;
    const $$ = {
      on_destroy,
      context: new Map(context || (parent_component ? parent_component.$$.context : [])),
      // these will be immediately discarded
      on_mount: [],
      before_update: [],
      after_update: [],
      callbacks: blank_object()
    };
    set_current_component({ $$ });
    const html = fn(result, props, bindings, slots);
    set_current_component(parent_component);
    return html;
  }
  return {
    render: (props = {}, { $$slots = {}, context = /* @__PURE__ */ new Map() } = {}) => {
      on_destroy = [];
      const result = { title: "", head: "", css: /* @__PURE__ */ new Set() };
      const html = $$render(result, props, {}, $$slots, context);
      run_all(on_destroy);
      return {
        html,
        css: {
          code: Array.from(result.css).map((css12) => css12.code).join("\n"),
          map: null
          // TODO
        },
        head: result.title + result.head
      };
    },
    $$render
  };
}
function add_attribute(name, value, boolean) {
  if (value == null || boolean && !value)
    return "";
  const assignment = boolean && value === true ? "" : `="${escape(value, true)}"`;
  return ` ${name}${assignment}`;
}
function style_object_to_string(style_object) {
  return Object.keys(style_object).filter((key2) => style_object[key2]).map((key2) => `${key2}: ${escape_attribute_value(style_object[key2])};`).join(" ");
}
function add_styles(style_object) {
  const styles = style_object_to_string(style_object);
  return styles ? ` style="${styles}"` : "";
}
var current_component;
var ATTR_REGEX;
var CONTENT_REGEX;
var missing_component;
var on_destroy;
var init_ssr = __esm({
  ".svelte-kit/output/server/chunks/ssr.js"() {
    ATTR_REGEX = /[&"]/g;
    CONTENT_REGEX = /[&<]/g;
    missing_component = {
      $$render: () => ""
    };
  }
});
function set_private_env(environment) {
  private_env = environment;
}
function set_public_env(environment) {
  public_env = environment;
}
function set_safe_public_env(environment) {
  safe_public_env = environment;
}
var private_env;
var public_env;
var safe_public_env;
var init_shared_server = __esm({
  ".svelte-kit/output/server/chunks/shared-server.js"() {
    private_env = {};
    public_env = {};
    safe_public_env = {};
  }
});
function enforceLanguageTag(unsafeLanguageTag) {
  return () => {
    const tag2 = unsafeLanguageTag();
    if (!isAvailableLanguageTag(tag2)) {
      throw new Error(`languageTag() didn't return a valid language tag. Check your setLanguageTag call`);
    }
    return tag2;
  };
}
function isAvailableLanguageTag(thing) {
  return availableLanguageTags.includes(thing);
}
var sourceLanguageTag;
var availableLanguageTags;
var languageTag;
var setLanguageTag;
var init_runtime = __esm({
  ".svelte-kit/output/server/chunks/runtime.js"() {
    sourceLanguageTag = "vi";
    availableLanguageTags = /** @type {const} */
    ["en", "vi"];
    languageTag = () => sourceLanguageTag;
    setLanguageTag = (tag2) => {
      if (typeof tag2 === "function") {
        languageTag = enforceLanguageTag(tag2);
      } else {
        languageTag = enforceLanguageTag(() => tag2);
      }
    };
  }
});
var dev;
var init_index2 = __esm({
  ".svelte-kit/output/server/chunks/index2.js"() {
    init_prod_ssr();
    dev = DEV;
  }
});
async function bring(input, init2) {
  try {
    return await fetch(input, init2);
  } catch (e3) {
    return void 0;
  }
}
function isNotUndefined(a) {
  return a !== void 0;
}
var init_util = __esm({
  ".svelte-kit/output/server/chunks/util.js"() {
  }
});
function error(status, body2) {
  if (isNaN(status) || status < 400 || status > 599) {
    throw new Error(`HTTP error status codes must be between 400 and 599 \u2014 ${status} is invalid`);
  }
  throw new HttpError(status, body2);
}
function redirect(status, location) {
  if (isNaN(status) || status < 300 || status > 308) {
    throw new Error("Invalid status code");
  }
  throw new Redirect(status, location.toString());
}
function json(data, init2) {
  const body2 = JSON.stringify(data);
  const headers2 = new Headers(init2?.headers);
  if (!headers2.has("content-length")) {
    headers2.set("content-length", encoder.encode(body2).byteLength.toString());
  }
  if (!headers2.has("content-type")) {
    headers2.set("content-type", "application/json");
  }
  return new Response(body2, {
    ...init2,
    headers: headers2
  });
}
function text(body2, init2) {
  const headers2 = new Headers(init2?.headers);
  if (!headers2.has("content-length")) {
    const encoded = encoder.encode(body2);
    headers2.set("content-length", encoded.byteLength.toString());
    return new Response(encoded, {
      ...init2,
      headers: headers2
    });
  }
  return new Response(body2, {
    ...init2,
    headers: headers2
  });
}
function fail(status, data) {
  return new ActionFailure(status, data);
}
var HttpError;
var Redirect;
var SvelteKitError;
var ActionFailure;
var encoder;
var init_chunks = __esm({
  ".svelte-kit/output/server/chunks/index.js"() {
    HttpError = class {
      /**
       * @param {number} status
       * @param {{message: string} extends App.Error ? (App.Error | string | undefined) : App.Error} body
       */
      constructor(status, body2) {
        this.status = status;
        if (typeof body2 === "string") {
          this.body = { message: body2 };
        } else if (body2) {
          this.body = body2;
        } else {
          this.body = { message: `Error: ${status}` };
        }
      }
      toString() {
        return JSON.stringify(this.body);
      }
    };
    Redirect = class {
      /**
       * @param {300 | 301 | 302 | 303 | 304 | 305 | 306 | 307 | 308} status
       * @param {string} location
       */
      constructor(status, location) {
        this.status = status;
        this.location = location;
      }
    };
    SvelteKitError = class extends Error {
      /**
       * @param {number} status
       * @param {string} text
       * @param {string} message
       */
      constructor(status, text2, message) {
        super(message);
        this.status = status;
        this.text = text2;
      }
    };
    ActionFailure = class {
      /**
       * @param {number} status
       * @param {T} data
       */
      constructor(status, data) {
        this.status = status;
        this.data = data;
      }
    };
    encoder = new TextEncoder();
  }
});
function getToken(cookies) {
  const credintials = cookies.get("google");
  if (!credintials)
    error(401, "no google cookie");
  return JSON.parse(credintials);
}
async function refreshToken(tokens) {
  const { clientId, clientSecret } = options();
  const response = await bring("https://oauth2.googleapis.com/token", {
    method: "POST",
    headers: {
      "content-type": "application/json",
      accept: "application/json"
    },
    body: JSON.stringify({
      client_id: clientId,
      client_secret: clientSecret,
      refreshToken: tokens.refresh_token,
      grant_type: "refresh_token"
    })
  });
  if (!response?.ok)
    return void 0;
  return {
    ...tokens,
    ...await response.json()
  };
}
function createMultipartBody(metadata, mediaData, boundary) {
  const metadataPart = `--${boundary}\r
Content-Type: application/json; charset=UTF-8\r
\r
${JSON.stringify(
    metadata
  )}\r
`;
  const mediaPart = `--${boundary}\r
Content-Type: ${mediaData.type}\r
\r
${mediaData.data}\r
`;
  const closingBoundary = `--${boundary}--\r
`;
  return metadataPart + mediaPart + closingBoundary;
}
function generateBoundary() {
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const boundaryLength = 16;
  return Array.from(
    { length: boundaryLength },
    () => characters.charAt(Math.floor(Math.random() * characters.length))
  ).join("");
}
function getDrive(cookies) {
  return new Drive(getToken(cookies));
}
async function getFileIdFromName(service, name) {
  return await service.file(["id"], { name });
}
async function sequential(values) {
  const results = [];
  for (const value of values) {
    results.push(await value);
  }
  return results;
}
function delay(ms) {
  return new Promise((resolve2) => setTimeout(resolve2, ms));
}
async function getCreations(service, f) {
  return await service.list(["name", "properties"], { mimeType: "application/json" }, f);
}
async function getImages(service, f) {
  return await service.list(["name"], { mimeType: "text/plain" }, f);
}
var options;
var scope;
var Drive;
var init_googleUtil = __esm({
  ".svelte-kit/output/server/chunks/googleUtil.js"() {
    init_shared_server();
    init_util();
    init_chunks();
    options = () => ({
      clientId: private_env.CLIENT_ID,
      clientSecret: private_env.CLIENT_SECRET,
      redirectUri: private_env.REDIRECT_URI
    });
    scope = "https://www.googleapis.com/auth/drive.appdata";
    Drive = class {
      constructor(tokens) {
        this.tokens = tokens;
      }
      headers() {
        return {
          Authorization: "Bearer " + this.tokens.access_token,
          "Content-Type": "application/json",
          Accept: "application/json"
        };
      }
      async deleteFile(file) {
        await fetch(`https://www.googleapis.com/drive/v3/files/${file.id}`, {
          method: "DELETE",
          headers: this.headers()
        });
      }
      async file(fields, search) {
        const url = new URL("https://www.googleapis.com/drive/v3/files");
        url.search = new URLSearchParams({
          q: Object.keys(search).map((k2) => `${k2} = '${search[k2]}'`).join(""),
          fields: `files(${fields.join(", ")})`,
          spaces: "appDataFolder"
        }).toString();
        const res = await fetch(url, {
          headers: this.headers()
        });
        const { files } = await res.json();
        return files.at(0);
      }
      async content(file) {
        const url = new URL(`https://www.googleapis.com/drive/v3/files/${file.id}`);
        url.search = new URLSearchParams({
          spaces: "appDataFolder",
          alt: "media"
        }).toString();
        const res = await fetch(url, {
          headers: {
            ...this.headers(),
            Accept: "text/plain"
          }
        });
        if (!res.ok)
          return void 0;
        return await res.text();
      }
      async update(file, data) {
        const url = new URL("https://www.googleapis.com/upload/drive/v3/files");
        url.search = new URLSearchParams({
          fieldId: file.id,
          spaces: "appDataFolder",
          uploadType: "multipart"
        }).toString();
        const { id, ...metadata } = file;
        const boundary = generateBoundary();
        const requestBody = createMultipartBody(
          {
            fileId: id,
            ...metadata
          },
          data,
          boundary
        );
        await fetch(url, {
          method: "PATCH",
          headers: {
            ...this.headers(),
            "Content-Type": `multipart/related; boundary=${boundary}`,
            "Content-Length": requestBody.length.toString()
          },
          body: requestBody
        });
      }
      async create(fileProperties, data) {
        const url = new URL("https://www.googleapis.com/upload/drive/v3/files");
        url.search = new URLSearchParams({
          uploadType: "multipart"
        }).toString();
        const boundary = generateBoundary();
        const requestBody = createMultipartBody(
          { ...fileProperties, parents: ["appDataFolder"] },
          data,
          boundary
        );
        await fetch(url, {
          method: "POST",
          headers: {
            ...this.headers(),
            "Content-Type": `multipart/related; boundary=${boundary}`,
            "Content-Length": requestBody.length.toString()
          },
          body: requestBody
        });
      }
      async list(fields, search, transform, pageToken) {
        const url = new URL("https://www.googleapis.com/drive/v3/files");
        url.search = new URLSearchParams({
          ...pageToken && { pageToken },
          q: Object.keys(search).map((k2) => `${k2} = '${search[k2]}'`).join(""),
          fields: `nextPageToken, files(id, ${fields.join(", ")})`,
          spaces: "appDataFolder"
        }).toString();
        const res = await fetch(url, {
          headers: this.headers()
        });
        const { files, nextPageToken } = await res.json();
        const transformedFiles = await sequential(files.map(transform));
        return nextPageToken ? transformedFiles.concat(await this.list(fields, search, transform, nextPageToken)) : transformedFiles;
      }
    };
  }
});
var hooks_server_exports = {};
__export(hooks_server_exports, {
  handle: () => handle
});
var handle;
var init_hooks_server = __esm({
  ".svelte-kit/output/server/chunks/hooks.server.js"() {
    init_runtime();
    init_index2();
    init_googleUtil();
    handle = async ({ event, resolve: resolve2 }) => {
      const lang = event.params.lang ?? sourceLanguageTag;
      const session = event.cookies.get("google");
      const google = session ? await (async () => {
        try {
          const tokens = await refreshToken(JSON.parse(session));
          if (tokens === void 0) {
            event.cookies.delete("google", { path: "/" });
            return false;
          }
          event.cookies.set("google", JSON.stringify(tokens), {
            path: "/",
            httpOnly: true,
            sameSite: "strict",
            secure: !dev,
            maxAge: 60 * 60 * 24 * 30
          });
          return true;
        } catch (e3) {
          return false;
        }
      })() : false;
      event.locals.google = google;
      return await resolve2(event, {
        transformPageChunk({ done: done2, html }) {
          if (done2) {
            return html.replace("%lang%", lang);
          }
        }
      });
    };
  }
});
function resolve(base2, path) {
  if (path[0] === "/" && path[1] === "/")
    return path;
  let url = new URL(base2, internal);
  url = new URL(path, url);
  return url.protocol === internal.protocol ? url.pathname + url.search + url.hash : url.href;
}
function normalize_path(path, trailing_slash) {
  if (path === "/" || trailing_slash === "ignore")
    return path;
  if (trailing_slash === "never") {
    return path.endsWith("/") ? path.slice(0, -1) : path;
  } else if (trailing_slash === "always" && !path.endsWith("/")) {
    return path + "/";
  }
  return path;
}
function decode_pathname(pathname) {
  return pathname.split("%25").map(decodeURI).join("%25");
}
function decode_params(params) {
  for (const key2 in params) {
    params[key2] = decodeURIComponent(params[key2]);
  }
  return params;
}
function make_trackable(url, callback, search_params_callback) {
  const tracked = new URL(url);
  Object.defineProperty(tracked, "searchParams", {
    value: new Proxy(tracked.searchParams, {
      get(obj, key2) {
        if (key2 === "get" || key2 === "getAll" || key2 === "has") {
          return (param) => {
            search_params_callback(param);
            return obj[key2](param);
          };
        }
        callback();
        const value = Reflect.get(obj, key2);
        return typeof value === "function" ? value.bind(obj) : value;
      }
    }),
    enumerable: true,
    configurable: true
  });
  for (const property of tracked_url_properties) {
    Object.defineProperty(tracked, property, {
      get() {
        callback();
        return url[property];
      },
      enumerable: true,
      configurable: true
    });
  }
  {
    tracked[Symbol.for("nodejs.util.inspect.custom")] = (depth, opts, inspect) => {
      return inspect(url, opts);
    };
  }
  {
    disable_hash(tracked);
  }
  return tracked;
}
function disable_hash(url) {
  allow_nodejs_console_log(url);
  Object.defineProperty(url, "hash", {
    get() {
      throw new Error(
        "Cannot access event.url.hash. Consider using `$page.url.hash` inside a component instead"
      );
    }
  });
}
function disable_search(url) {
  allow_nodejs_console_log(url);
  for (const property of ["search", "searchParams"]) {
    Object.defineProperty(url, property, {
      get() {
        throw new Error(`Cannot access url.${property} on a page with prerendering enabled`);
      }
    });
  }
}
function allow_nodejs_console_log(url) {
  {
    url[Symbol.for("nodejs.util.inspect.custom")] = (depth, opts, inspect) => {
      return inspect(new URL(url), opts);
    };
  }
}
function has_data_suffix(pathname) {
  return pathname.endsWith(DATA_SUFFIX) || pathname.endsWith(HTML_DATA_SUFFIX);
}
function add_data_suffix(pathname) {
  if (pathname.endsWith(".html"))
    return pathname.replace(/\.html$/, HTML_DATA_SUFFIX);
  return pathname.replace(/\/$/, "") + DATA_SUFFIX;
}
function strip_data_suffix(pathname) {
  if (pathname.endsWith(HTML_DATA_SUFFIX)) {
    return pathname.slice(0, -HTML_DATA_SUFFIX.length) + ".html";
  }
  return pathname.slice(0, -DATA_SUFFIX.length);
}
function validator(expected) {
  function validate(module, file) {
    if (!module)
      return;
    for (const key2 in module) {
      if (key2[0] === "_" || expected.has(key2))
        continue;
      const values = [...expected.values()];
      const hint = hint_for_supported_files(key2, file?.slice(file.lastIndexOf("."))) ?? `valid exports are ${values.join(", ")}, or anything with a '_' prefix`;
      throw new Error(`Invalid export '${key2}'${file ? ` in ${file}` : ""} (${hint})`);
    }
  }
  return validate;
}
function hint_for_supported_files(key2, ext = ".js") {
  const supported_files = [];
  if (valid_layout_exports.has(key2)) {
    supported_files.push(`+layout${ext}`);
  }
  if (valid_page_exports.has(key2)) {
    supported_files.push(`+page${ext}`);
  }
  if (valid_layout_server_exports.has(key2)) {
    supported_files.push(`+layout.server${ext}`);
  }
  if (valid_page_server_exports.has(key2)) {
    supported_files.push(`+page.server${ext}`);
  }
  if (valid_server_exports.has(key2)) {
    supported_files.push(`+server${ext}`);
  }
  if (supported_files.length > 0) {
    return `'${key2}' is a valid export in ${supported_files.slice(0, -1).join(", ")}${supported_files.length > 1 ? " or " : ""}${supported_files.at(-1)}`;
  }
}
var internal;
var tracked_url_properties;
var DATA_SUFFIX;
var HTML_DATA_SUFFIX;
var valid_layout_exports;
var valid_page_exports;
var valid_layout_server_exports;
var valid_page_server_exports;
var valid_server_exports;
var validate_layout_exports;
var validate_page_exports;
var validate_layout_server_exports;
var validate_page_server_exports;
var validate_server_exports;
var init_exports = __esm({
  ".svelte-kit/output/server/chunks/exports.js"() {
    internal = new URL("sveltekit-internal://");
    tracked_url_properties = /** @type {const} */
    [
      "href",
      "pathname",
      "search",
      "toString",
      "toJSON"
    ];
    DATA_SUFFIX = "/__data.json";
    HTML_DATA_SUFFIX = ".html__data.json";
    valid_layout_exports = /* @__PURE__ */ new Set([
      "load",
      "prerender",
      "csr",
      "ssr",
      "trailingSlash",
      "config"
    ]);
    valid_page_exports = /* @__PURE__ */ new Set([...valid_layout_exports, "entries"]);
    valid_layout_server_exports = /* @__PURE__ */ new Set([...valid_layout_exports]);
    valid_page_server_exports = /* @__PURE__ */ new Set([...valid_layout_server_exports, "actions", "entries"]);
    valid_server_exports = /* @__PURE__ */ new Set([
      "GET",
      "POST",
      "PATCH",
      "PUT",
      "DELETE",
      "OPTIONS",
      "HEAD",
      "fallback",
      "prerender",
      "trailingSlash",
      "config",
      "entries"
    ]);
    validate_layout_exports = validator(valid_layout_exports);
    validate_page_exports = validator(valid_page_exports);
    validate_layout_server_exports = validator(valid_layout_server_exports);
    validate_page_server_exports = validator(valid_page_server_exports);
    validate_server_exports = validator(valid_server_exports);
  }
});
function is_primitive(thing) {
  return Object(thing) !== thing;
}
function is_plain_object(thing) {
  const proto = Object.getPrototypeOf(thing);
  return proto === Object.prototype || proto === null || Object.getOwnPropertyNames(proto).sort().join("\0") === object_proto_names;
}
function get_type(thing) {
  return Object.prototype.toString.call(thing).slice(8, -1);
}
function get_escaped_char(char) {
  switch (char) {
    case '"':
      return '\\"';
    case "<":
      return "\\u003C";
    case "\\":
      return "\\\\";
    case "\n":
      return "\\n";
    case "\r":
      return "\\r";
    case "	":
      return "\\t";
    case "\b":
      return "\\b";
    case "\f":
      return "\\f";
    case "\u2028":
      return "\\u2028";
    case "\u2029":
      return "\\u2029";
    default:
      return char < " " ? `\\u${char.charCodeAt(0).toString(16).padStart(4, "0")}` : "";
  }
}
function stringify_string(str) {
  let result = "";
  let last_pos = 0;
  const len = str.length;
  for (let i = 0; i < len; i += 1) {
    const char = str[i];
    const replacement = get_escaped_char(char);
    if (replacement) {
      result += str.slice(last_pos, i) + replacement;
      last_pos = i + 1;
    }
  }
  return `"${last_pos === 0 ? str : result + str.slice(last_pos)}"`;
}
var escaped;
var DevalueError;
var object_proto_names;
var init_utils = __esm({
  "node_modules/devalue/src/utils.js"() {
    escaped = {
      "<": "\\u003C",
      "\\": "\\\\",
      "\b": "\\b",
      "\f": "\\f",
      "\n": "\\n",
      "\r": "\\r",
      "	": "\\t",
      "\u2028": "\\u2028",
      "\u2029": "\\u2029"
    };
    DevalueError = class extends Error {
      /**
       * @param {string} message
       * @param {string[]} keys
       */
      constructor(message, keys2) {
        super(message);
        this.name = "DevalueError";
        this.path = keys2.join("");
      }
    };
    object_proto_names = /* @__PURE__ */ Object.getOwnPropertyNames(
      Object.prototype
    ).sort().join("\0");
  }
});
function uneval(value, replacer) {
  const counts = /* @__PURE__ */ new Map();
  const keys2 = [];
  const custom = /* @__PURE__ */ new Map();
  function walk(thing) {
    if (typeof thing === "function") {
      throw new DevalueError(`Cannot stringify a function`, keys2);
    }
    if (!is_primitive(thing)) {
      if (counts.has(thing)) {
        counts.set(thing, counts.get(thing) + 1);
        return;
      }
      counts.set(thing, 1);
      if (replacer) {
        const str2 = replacer(thing);
        if (typeof str2 === "string") {
          custom.set(thing, str2);
          return;
        }
      }
      const type = get_type(thing);
      switch (type) {
        case "Number":
        case "BigInt":
        case "String":
        case "Boolean":
        case "Date":
        case "RegExp":
          return;
        case "Array":
          thing.forEach((value2, i) => {
            keys2.push(`[${i}]`);
            walk(value2);
            keys2.pop();
          });
          break;
        case "Set":
          Array.from(thing).forEach(walk);
          break;
        case "Map":
          for (const [key2, value2] of thing) {
            keys2.push(
              `.get(${is_primitive(key2) ? stringify_primitive(key2) : "..."})`
            );
            walk(value2);
            keys2.pop();
          }
          break;
        default:
          if (!is_plain_object(thing)) {
            throw new DevalueError(
              `Cannot stringify arbitrary non-POJOs`,
              keys2
            );
          }
          if (Object.getOwnPropertySymbols(thing).length > 0) {
            throw new DevalueError(
              `Cannot stringify POJOs with symbolic keys`,
              keys2
            );
          }
          for (const key2 in thing) {
            keys2.push(`.${key2}`);
            walk(thing[key2]);
            keys2.pop();
          }
      }
    }
  }
  walk(value);
  const names = /* @__PURE__ */ new Map();
  Array.from(counts).filter((entry) => entry[1] > 1).sort((a, b) => b[1] - a[1]).forEach((entry, i) => {
    names.set(entry[0], get_name(i));
  });
  function stringify3(thing) {
    if (names.has(thing)) {
      return names.get(thing);
    }
    if (is_primitive(thing)) {
      return stringify_primitive(thing);
    }
    if (custom.has(thing)) {
      return custom.get(thing);
    }
    const type = get_type(thing);
    switch (type) {
      case "Number":
      case "String":
      case "Boolean":
        return `Object(${stringify3(thing.valueOf())})`;
      case "RegExp":
        return `new RegExp(${stringify_string(thing.source)}, "${thing.flags}")`;
      case "Date":
        return `new Date(${thing.getTime()})`;
      case "Array":
        const members = (
          /** @type {any[]} */
          thing.map(
            (v, i) => i in thing ? stringify3(v) : ""
          )
        );
        const tail = thing.length === 0 || thing.length - 1 in thing ? "" : ",";
        return `[${members.join(",")}${tail}]`;
      case "Set":
      case "Map":
        return `new ${type}([${Array.from(thing).map(stringify3).join(",")}])`;
      default:
        const obj = `{${Object.keys(thing).map((key2) => `${safe_key(key2)}:${stringify3(thing[key2])}`).join(",")}}`;
        const proto = Object.getPrototypeOf(thing);
        if (proto === null) {
          return Object.keys(thing).length > 0 ? `Object.assign(Object.create(null),${obj})` : `Object.create(null)`;
        }
        return obj;
    }
  }
  const str = stringify3(value);
  if (names.size) {
    const params = [];
    const statements = [];
    const values = [];
    names.forEach((name, thing) => {
      params.push(name);
      if (custom.has(thing)) {
        values.push(
          /** @type {string} */
          custom.get(thing)
        );
        return;
      }
      if (is_primitive(thing)) {
        values.push(stringify_primitive(thing));
        return;
      }
      const type = get_type(thing);
      switch (type) {
        case "Number":
        case "String":
        case "Boolean":
          values.push(`Object(${stringify3(thing.valueOf())})`);
          break;
        case "RegExp":
          values.push(thing.toString());
          break;
        case "Date":
          values.push(`new Date(${thing.getTime()})`);
          break;
        case "Array":
          values.push(`Array(${thing.length})`);
          thing.forEach((v, i) => {
            statements.push(`${name}[${i}]=${stringify3(v)}`);
          });
          break;
        case "Set":
          values.push(`new Set`);
          statements.push(
            `${name}.${Array.from(thing).map((v) => `add(${stringify3(v)})`).join(".")}`
          );
          break;
        case "Map":
          values.push(`new Map`);
          statements.push(
            `${name}.${Array.from(thing).map(([k2, v]) => `set(${stringify3(k2)}, ${stringify3(v)})`).join(".")}`
          );
          break;
        default:
          values.push(
            Object.getPrototypeOf(thing) === null ? "Object.create(null)" : "{}"
          );
          Object.keys(thing).forEach((key2) => {
            statements.push(
              `${name}${safe_prop(key2)}=${stringify3(thing[key2])}`
            );
          });
      }
    });
    statements.push(`return ${str}`);
    return `(function(${params.join(",")}){${statements.join(
      ";"
    )}}(${values.join(",")}))`;
  } else {
    return str;
  }
}
function get_name(num) {
  let name = "";
  do {
    name = chars[num % chars.length] + name;
    num = ~~(num / chars.length) - 1;
  } while (num >= 0);
  return reserved.test(name) ? `${name}0` : name;
}
function escape_unsafe_char(c2) {
  return escaped[c2] || c2;
}
function escape_unsafe_chars(str) {
  return str.replace(unsafe_chars, escape_unsafe_char);
}
function safe_key(key2) {
  return /^[_$a-zA-Z][_$a-zA-Z0-9]*$/.test(key2) ? key2 : escape_unsafe_chars(JSON.stringify(key2));
}
function safe_prop(key2) {
  return /^[_$a-zA-Z][_$a-zA-Z0-9]*$/.test(key2) ? `.${key2}` : `[${escape_unsafe_chars(JSON.stringify(key2))}]`;
}
function stringify_primitive(thing) {
  if (typeof thing === "string")
    return stringify_string(thing);
  if (thing === void 0)
    return "void 0";
  if (thing === 0 && 1 / thing < 0)
    return "-0";
  const str = String(thing);
  if (typeof thing === "number")
    return str.replace(/^(-)?0\./, "$1.");
  if (typeof thing === "bigint")
    return thing + "n";
  return str;
}
var chars;
var unsafe_chars;
var reserved;
var init_uneval = __esm({
  "node_modules/devalue/src/uneval.js"() {
    init_utils();
    chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_$";
    unsafe_chars = /[<\b\f\n\r\t\0\u2028\u2029]/g;
    reserved = /^(?:do|if|in|for|int|let|new|try|var|byte|case|char|else|enum|goto|long|this|void|with|await|break|catch|class|const|final|float|short|super|throw|while|yield|delete|double|export|import|native|return|switch|throws|typeof|boolean|default|extends|finally|package|private|abstract|continue|debugger|function|volatile|interface|protected|transient|implements|instanceof|synchronized)$/;
  }
});
var UNDEFINED;
var HOLE;
var NAN;
var POSITIVE_INFINITY;
var NEGATIVE_INFINITY;
var NEGATIVE_ZERO;
var init_constants = __esm({
  "node_modules/devalue/src/constants.js"() {
    UNDEFINED = -1;
    HOLE = -2;
    NAN = -3;
    POSITIVE_INFINITY = -4;
    NEGATIVE_INFINITY = -5;
    NEGATIVE_ZERO = -6;
  }
});
function parse(serialized, revivers) {
  return unflatten(JSON.parse(serialized), revivers);
}
function unflatten(parsed, revivers) {
  if (typeof parsed === "number")
    return hydrate(parsed, true);
  if (!Array.isArray(parsed) || parsed.length === 0) {
    throw new Error("Invalid input");
  }
  const values = (
    /** @type {any[]} */
    parsed
  );
  const hydrated = Array(values.length);
  function hydrate(index10, standalone = false) {
    if (index10 === UNDEFINED)
      return void 0;
    if (index10 === NAN)
      return NaN;
    if (index10 === POSITIVE_INFINITY)
      return Infinity;
    if (index10 === NEGATIVE_INFINITY)
      return -Infinity;
    if (index10 === NEGATIVE_ZERO)
      return -0;
    if (standalone)
      throw new Error(`Invalid input`);
    if (index10 in hydrated)
      return hydrated[index10];
    const value = values[index10];
    if (!value || typeof value !== "object") {
      hydrated[index10] = value;
    } else if (Array.isArray(value)) {
      if (typeof value[0] === "string") {
        const type = value[0];
        const reviver = revivers?.[type];
        if (reviver) {
          return hydrated[index10] = reviver(hydrate(value[1]));
        }
        switch (type) {
          case "Date":
            hydrated[index10] = new Date(value[1]);
            break;
          case "Set":
            const set2 = /* @__PURE__ */ new Set();
            hydrated[index10] = set2;
            for (let i = 1; i < value.length; i += 1) {
              set2.add(hydrate(value[i]));
            }
            break;
          case "Map":
            const map2 = /* @__PURE__ */ new Map();
            hydrated[index10] = map2;
            for (let i = 1; i < value.length; i += 2) {
              map2.set(hydrate(value[i]), hydrate(value[i + 1]));
            }
            break;
          case "RegExp":
            hydrated[index10] = new RegExp(value[1], value[2]);
            break;
          case "Object":
            hydrated[index10] = Object(value[1]);
            break;
          case "BigInt":
            hydrated[index10] = BigInt(value[1]);
            break;
          case "null":
            const obj = /* @__PURE__ */ Object.create(null);
            hydrated[index10] = obj;
            for (let i = 1; i < value.length; i += 2) {
              obj[value[i]] = hydrate(value[i + 1]);
            }
            break;
          default:
            throw new Error(`Unknown type ${type}`);
        }
      } else {
        const array2 = new Array(value.length);
        hydrated[index10] = array2;
        for (let i = 0; i < value.length; i += 1) {
          const n2 = value[i];
          if (n2 === HOLE)
            continue;
          array2[i] = hydrate(n2);
        }
      }
    } else {
      const object = {};
      hydrated[index10] = object;
      for (const key2 in value) {
        const n2 = value[key2];
        object[key2] = hydrate(n2);
      }
    }
    return hydrated[index10];
  }
  return hydrate(0);
}
var init_parse = __esm({
  "node_modules/devalue/src/parse.js"() {
    init_constants();
  }
});
function stringify(value, reducers) {
  const stringified = [];
  const indexes = /* @__PURE__ */ new Map();
  const custom = [];
  for (const key2 in reducers) {
    custom.push({ key: key2, fn: reducers[key2] });
  }
  const keys2 = [];
  let p = 0;
  function flatten(thing) {
    if (typeof thing === "function") {
      throw new DevalueError(`Cannot stringify a function`, keys2);
    }
    if (indexes.has(thing))
      return indexes.get(thing);
    if (thing === void 0)
      return UNDEFINED;
    if (Number.isNaN(thing))
      return NAN;
    if (thing === Infinity)
      return POSITIVE_INFINITY;
    if (thing === -Infinity)
      return NEGATIVE_INFINITY;
    if (thing === 0 && 1 / thing < 0)
      return NEGATIVE_ZERO;
    const index11 = p++;
    indexes.set(thing, index11);
    for (const { key: key2, fn } of custom) {
      const value2 = fn(thing);
      if (value2) {
        stringified[index11] = `["${key2}",${flatten(value2)}]`;
        return index11;
      }
    }
    let str = "";
    if (is_primitive(thing)) {
      str = stringify_primitive2(thing);
    } else {
      const type = get_type(thing);
      switch (type) {
        case "Number":
        case "String":
        case "Boolean":
          str = `["Object",${stringify_primitive2(thing)}]`;
          break;
        case "BigInt":
          str = `["BigInt",${thing}]`;
          break;
        case "Date":
          str = `["Date","${thing.toISOString()}"]`;
          break;
        case "RegExp":
          const { source, flags } = thing;
          str = flags ? `["RegExp",${stringify_string(source)},"${flags}"]` : `["RegExp",${stringify_string(source)}]`;
          break;
        case "Array":
          str = "[";
          for (let i = 0; i < thing.length; i += 1) {
            if (i > 0)
              str += ",";
            if (i in thing) {
              keys2.push(`[${i}]`);
              str += flatten(thing[i]);
              keys2.pop();
            } else {
              str += HOLE;
            }
          }
          str += "]";
          break;
        case "Set":
          str = '["Set"';
          for (const value2 of thing) {
            str += `,${flatten(value2)}`;
          }
          str += "]";
          break;
        case "Map":
          str = '["Map"';
          for (const [key2, value2] of thing) {
            keys2.push(
              `.get(${is_primitive(key2) ? stringify_primitive2(key2) : "..."})`
            );
            str += `,${flatten(key2)},${flatten(value2)}`;
          }
          str += "]";
          break;
        default:
          if (!is_plain_object(thing)) {
            throw new DevalueError(
              `Cannot stringify arbitrary non-POJOs`,
              keys2
            );
          }
          if (Object.getOwnPropertySymbols(thing).length > 0) {
            throw new DevalueError(
              `Cannot stringify POJOs with symbolic keys`,
              keys2
            );
          }
          if (Object.getPrototypeOf(thing) === null) {
            str = '["null"';
            for (const key2 in thing) {
              keys2.push(`.${key2}`);
              str += `,${stringify_string(key2)},${flatten(thing[key2])}`;
              keys2.pop();
            }
            str += "]";
          } else {
            str = "{";
            let started = false;
            for (const key2 in thing) {
              if (started)
                str += ",";
              started = true;
              keys2.push(`.${key2}`);
              str += `${stringify_string(key2)}:${flatten(thing[key2])}`;
              keys2.pop();
            }
            str += "}";
          }
      }
    }
    stringified[index11] = str;
    return index11;
  }
  const index10 = flatten(value);
  if (index10 < 0)
    return `${index10}`;
  return `[${stringified.join(",")}]`;
}
function stringify_primitive2(thing) {
  const type = typeof thing;
  if (type === "string")
    return stringify_string(thing);
  if (thing instanceof String)
    return stringify_string(thing.toString());
  if (thing === void 0)
    return UNDEFINED.toString();
  if (thing === 0 && 1 / thing < 0)
    return NEGATIVE_ZERO.toString();
  if (type === "bigint")
    return `["BigInt","${thing}"]`;
  return String(thing);
}
var init_stringify = __esm({
  "node_modules/devalue/src/stringify.js"() {
    init_utils();
    init_constants();
  }
});
var init_devalue = __esm({
  "node_modules/devalue/index.js"() {
    init_uneval();
    init_parse();
    init_stringify();
  }
});
var require_cookie = __commonJS({
  "node_modules/cookie/index.js"(exports) {
    "use strict";
    exports.parse = parse5;
    exports.serialize = serialize2;
    var __toString = Object.prototype.toString;
    var fieldContentRegExp = /^[\u0009\u0020-\u007e\u0080-\u00ff]+$/;
    function parse5(str, options3) {
      if (typeof str !== "string") {
        throw new TypeError("argument str must be a string");
      }
      var obj = {};
      var opt = options3 || {};
      var dec = opt.decode || decode3;
      var index10 = 0;
      while (index10 < str.length) {
        var eqIdx = str.indexOf("=", index10);
        if (eqIdx === -1) {
          break;
        }
        var endIdx = str.indexOf(";", index10);
        if (endIdx === -1) {
          endIdx = str.length;
        } else if (endIdx < eqIdx) {
          index10 = str.lastIndexOf(";", eqIdx - 1) + 1;
          continue;
        }
        var key2 = str.slice(index10, eqIdx).trim();
        if (void 0 === obj[key2]) {
          var val = str.slice(eqIdx + 1, endIdx).trim();
          if (val.charCodeAt(0) === 34) {
            val = val.slice(1, -1);
          }
          obj[key2] = tryDecode(val, dec);
        }
        index10 = endIdx + 1;
      }
      return obj;
    }
    function serialize2(name, val, options3) {
      var opt = options3 || {};
      var enc = opt.encode || encode3;
      if (typeof enc !== "function") {
        throw new TypeError("option encode is invalid");
      }
      if (!fieldContentRegExp.test(name)) {
        throw new TypeError("argument name is invalid");
      }
      var value = enc(val);
      if (value && !fieldContentRegExp.test(value)) {
        throw new TypeError("argument val is invalid");
      }
      var str = name + "=" + value;
      if (null != opt.maxAge) {
        var maxAge = opt.maxAge - 0;
        if (isNaN(maxAge) || !isFinite(maxAge)) {
          throw new TypeError("option maxAge is invalid");
        }
        str += "; Max-Age=" + Math.floor(maxAge);
      }
      if (opt.domain) {
        if (!fieldContentRegExp.test(opt.domain)) {
          throw new TypeError("option domain is invalid");
        }
        str += "; Domain=" + opt.domain;
      }
      if (opt.path) {
        if (!fieldContentRegExp.test(opt.path)) {
          throw new TypeError("option path is invalid");
        }
        str += "; Path=" + opt.path;
      }
      if (opt.expires) {
        var expires = opt.expires;
        if (!isDate2(expires) || isNaN(expires.valueOf())) {
          throw new TypeError("option expires is invalid");
        }
        str += "; Expires=" + expires.toUTCString();
      }
      if (opt.httpOnly) {
        str += "; HttpOnly";
      }
      if (opt.secure) {
        str += "; Secure";
      }
      if (opt.partitioned) {
        str += "; Partitioned";
      }
      if (opt.priority) {
        var priority = typeof opt.priority === "string" ? opt.priority.toLowerCase() : opt.priority;
        switch (priority) {
          case "low":
            str += "; Priority=Low";
            break;
          case "medium":
            str += "; Priority=Medium";
            break;
          case "high":
            str += "; Priority=High";
            break;
          default:
            throw new TypeError("option priority is invalid");
        }
      }
      if (opt.sameSite) {
        var sameSite = typeof opt.sameSite === "string" ? opt.sameSite.toLowerCase() : opt.sameSite;
        switch (sameSite) {
          case true:
            str += "; SameSite=Strict";
            break;
          case "lax":
            str += "; SameSite=Lax";
            break;
          case "strict":
            str += "; SameSite=Strict";
            break;
          case "none":
            str += "; SameSite=None";
            break;
          default:
            throw new TypeError("option sameSite is invalid");
        }
      }
      return str;
    }
    function decode3(str) {
      return str.indexOf("%") !== -1 ? decodeURIComponent(str) : str;
    }
    function encode3(val) {
      return encodeURIComponent(val);
    }
    function isDate2(val) {
      return __toString.call(val) === "[object Date]" || val instanceof Date;
    }
    function tryDecode(str, decode4) {
      try {
        return decode4(str);
      } catch (e3) {
        return str;
      }
    }
  }
});
var require_set_cookie = __commonJS({
  "node_modules/set-cookie-parser/lib/set-cookie.js"(exports, module) {
    "use strict";
    var defaultParseOptions = {
      decodeValues: true,
      map: false,
      silent: false
    };
    function isNonEmptyString(str) {
      return typeof str === "string" && !!str.trim();
    }
    function parseString2(setCookieValue, options3) {
      var parts = setCookieValue.split(";").filter(isNonEmptyString);
      var nameValuePairStr = parts.shift();
      var parsed = parseNameValuePair(nameValuePairStr);
      var name = parsed.name;
      var value = parsed.value;
      options3 = options3 ? Object.assign({}, defaultParseOptions, options3) : defaultParseOptions;
      try {
        value = options3.decodeValues ? decodeURIComponent(value) : value;
      } catch (e3) {
        console.error(
          "set-cookie-parser encountered an error while decoding a cookie with value '" + value + "'. Set options.decodeValues to false to disable this feature.",
          e3
        );
      }
      var cookie = {
        name,
        value
      };
      parts.forEach(function(part) {
        var sides = part.split("=");
        var key2 = sides.shift().trimLeft().toLowerCase();
        var value2 = sides.join("=");
        if (key2 === "expires") {
          cookie.expires = new Date(value2);
        } else if (key2 === "max-age") {
          cookie.maxAge = parseInt(value2, 10);
        } else if (key2 === "secure") {
          cookie.secure = true;
        } else if (key2 === "httponly") {
          cookie.httpOnly = true;
        } else if (key2 === "samesite") {
          cookie.sameSite = value2;
        } else {
          cookie[key2] = value2;
        }
      });
      return cookie;
    }
    function parseNameValuePair(nameValuePairStr) {
      var name = "";
      var value = "";
      var nameValueArr = nameValuePairStr.split("=");
      if (nameValueArr.length > 1) {
        name = nameValueArr.shift();
        value = nameValueArr.join("=");
      } else {
        value = nameValuePairStr;
      }
      return { name, value };
    }
    function parse5(input, options3) {
      options3 = options3 ? Object.assign({}, defaultParseOptions, options3) : defaultParseOptions;
      if (!input) {
        if (!options3.map) {
          return [];
        } else {
          return {};
        }
      }
      if (input.headers) {
        if (typeof input.headers.getSetCookie === "function") {
          input = input.headers.getSetCookie();
        } else if (input.headers["set-cookie"]) {
          input = input.headers["set-cookie"];
        } else {
          var sch = input.headers[Object.keys(input.headers).find(function(key2) {
            return key2.toLowerCase() === "set-cookie";
          })];
          if (!sch && input.headers.cookie && !options3.silent) {
            console.warn(
              "Warning: set-cookie-parser appears to have been called on a request object. It is designed to parse Set-Cookie headers from responses, not Cookie headers from requests. Set the option {silent: true} to suppress this warning."
            );
          }
          input = sch;
        }
      }
      if (!Array.isArray(input)) {
        input = [input];
      }
      options3 = options3 ? Object.assign({}, defaultParseOptions, options3) : defaultParseOptions;
      if (!options3.map) {
        return input.filter(isNonEmptyString).map(function(str) {
          return parseString2(str, options3);
        });
      } else {
        var cookies = {};
        return input.filter(isNonEmptyString).reduce(function(cookies2, str) {
          var cookie = parseString2(str, options3);
          cookies2[cookie.name] = cookie;
          return cookies2;
        }, cookies);
      }
    }
    function splitCookiesString2(cookiesString) {
      if (Array.isArray(cookiesString)) {
        return cookiesString;
      }
      if (typeof cookiesString !== "string") {
        return [];
      }
      var cookiesStrings = [];
      var pos = 0;
      var start;
      var ch;
      var lastComma;
      var nextStart;
      var cookiesSeparatorFound;
      function skipWhitespace() {
        while (pos < cookiesString.length && /\s/.test(cookiesString.charAt(pos))) {
          pos += 1;
        }
        return pos < cookiesString.length;
      }
      function notSpecialChar() {
        ch = cookiesString.charAt(pos);
        return ch !== "=" && ch !== ";" && ch !== ",";
      }
      while (pos < cookiesString.length) {
        start = pos;
        cookiesSeparatorFound = false;
        while (skipWhitespace()) {
          ch = cookiesString.charAt(pos);
          if (ch === ",") {
            lastComma = pos;
            pos += 1;
            skipWhitespace();
            nextStart = pos;
            while (pos < cookiesString.length && notSpecialChar()) {
              pos += 1;
            }
            if (pos < cookiesString.length && cookiesString.charAt(pos) === "=") {
              cookiesSeparatorFound = true;
              pos = nextStart;
              cookiesStrings.push(cookiesString.substring(start, lastComma));
              start = pos;
            } else {
              pos = lastComma + 1;
            }
          } else {
            pos += 1;
          }
        }
        if (!cookiesSeparatorFound || pos >= cookiesString.length) {
          cookiesStrings.push(cookiesString.substring(start, cookiesString.length));
        }
      }
      return cookiesStrings;
    }
    module.exports = parse5;
    module.exports.parse = parse5;
    module.exports.parseString = parseString2;
    module.exports.splitCookiesString = splitCookiesString2;
  }
});
var layout_ts_exports = {};
__export(layout_ts_exports, {
  prerender: () => prerender
});
var prerender;
var init_layout_ts = __esm({
  ".svelte-kit/output/server/entries/pages/_layout.ts.js"() {
    prerender = true;
  }
});
var layout_server_ts_exports = {};
__export(layout_server_ts_exports, {
  load: () => load
});
var load;
var init_layout_server_ts = __esm({
  ".svelte-kit/output/server/entries/pages/_layout.server.ts.js"() {
    load = async ({ locals }) => {
      return {
        google: locals.google
      };
    };
  }
});
function get(key2, parse5 = JSON.parse) {
  try {
    return parse5(sessionStorage[key2]);
  } catch {
  }
}
var SNAPSHOT_KEY;
var SCROLL_KEY;
var getStores;
var page;
var init_stores = __esm({
  ".svelte-kit/output/server/chunks/stores.js"() {
    init_ssr();
    init_exports();
    init_devalue();
    SNAPSHOT_KEY = "sveltekit:snapshot";
    SCROLL_KEY = "sveltekit:scroll";
    get(SCROLL_KEY) ?? {};
    get(SNAPSHOT_KEY) ?? {};
    getStores = () => {
      const stores = getContext("__svelte__");
      return {
        /** @type {typeof page} */
        page: {
          subscribe: stores.page.subscribe
        },
        /** @type {typeof navigating} */
        navigating: {
          subscribe: stores.navigating.subscribe
        },
        /** @type {typeof updated} */
        updated: stores.updated
      };
    };
    page = {
      subscribe(fn) {
        const store = getStores().page;
        return store.subscribe(fn);
      }
    };
  }
});
function route(path, lang) {
  path = withoutLanguageTag(path);
  if (lang === sourceLanguageTag)
    return path;
  return `/${lang}${path}`;
}
function withoutLanguageTag(path) {
  const [, maybeLang, ...rest2] = path.split("/");
  if (availableLanguageTags.includes(maybeLang)) {
    return `/${rest2.join("/")}`;
  }
  return path;
}
var init_i18n_routing = __esm({
  ".svelte-kit/output/server/chunks/i18n-routing.js"() {
    init_runtime();
  }
});
var PUBLIC_PLAY_URL;
var PUBLIC_CORKBOARD_URL;
var init_public = __esm({
  ".svelte-kit/output/server/chunks/public.js"() {
    PUBLIC_PLAY_URL = "https://localhost:5173";
    PUBLIC_CORKBOARD_URL = "http://localhost:5040";
  }
});
var layout_svelte_exports = {};
__export(layout_svelte_exports, {
  default: () => Layout
});
var I18NHeader;
var css;
var Layout;
var init_layout_svelte = __esm({
  ".svelte-kit/output/server/entries/pages/_layout.svelte.js"() {
    init_ssr();
    init_stores();
    init_runtime();
    init_i18n_routing();
    init_public();
    I18NHeader = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let $page, $$unsubscribe_page;
      $$unsubscribe_page = subscribe(page, (value) => $page = value);
      $$unsubscribe_page();
      return `${$$result.head += `<!-- HEAD_svelte-47uq8b_START -->${each(availableLanguageTags, (lang) => {
        return `<link rel="alternate"${add_attribute("hreflang", lang, 0)}${add_attribute("href", PUBLIC_PLAY_URL + route($page.url.pathname, lang), 0)}>`;
      })}<!-- HEAD_svelte-47uq8b_END -->`, ""}`;
    });
    css = {
      code: ":root{--background-color:#fffbf5;--color:#241f31;--palette-light:#fffbf5;--palette-dark:#241f31;--accent-color:#d4131b;@media (prefers-color-scheme: dark) {\n			--background-color: #241f31;\n			--color: #fffbf5;\n		}}html.light{--background-color:#fffbf5;--color:#241f31}html.dark{--background-color:#241f31;--color:#fffbf5}body{font-family:'Atkinson Hyperlegible', sans-serif;font-size:32px;color:var(--color)}html{background:var(--background-color)}",
      map: null
    };
    Layout = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let $$unsubscribe_page;
      $$unsubscribe_page = subscribe(page, (value) => value);
      let lang = sourceLanguageTag;
      $$result.css.add(css);
      {
        setLanguageTag(lang);
      }
      $$unsubscribe_page();
      return `${validate_component(I18NHeader, "I18NHeader").$$render($$result, {}, {}, {})} ${slots.default ? slots.default({}) : ``}`;
    });
  }
});
var __exports = {};
__export(__exports, {
  component: () => component,
  fonts: () => fonts,
  imports: () => imports,
  index: () => index,
  server: () => layout_server_ts_exports,
  server_id: () => server_id,
  stylesheets: () => stylesheets,
  universal: () => layout_ts_exports,
  universal_id: () => universal_id
});
var index;
var component_cache;
var component;
var universal_id;
var server_id;
var imports;
var stylesheets;
var fonts;
var init__ = __esm({
  ".svelte-kit/output/server/nodes/0.js"() {
    init_layout_ts();
    init_layout_server_ts();
    index = 0;
    component = async () => component_cache ??= (await Promise.resolve().then(() => (init_layout_svelte(), layout_svelte_exports))).default;
    universal_id = "src/routes/+layout.ts";
    server_id = "src/routes/+layout.server.ts";
    imports = ["_app/immutable/nodes/0.Cjnx_n-Y.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js", "_app/immutable/chunks/stores.C6La04jU.js", "_app/immutable/chunks/entry.Dv6jx6u8.js", "_app/immutable/chunks/runtime.CM0qr2RF.js", "_app/immutable/chunks/i18n-routing.GhjsszfS.js", "_app/immutable/chunks/public.Bp7fU829.js"];
    stylesheets = ["_app/immutable/assets/0.Ch9dOGs1.css"];
    fonts = ["_app/immutable/assets/poppins-devanagari-800-normal.vytqx0aS.woff2", "_app/immutable/assets/poppins-devanagari-800-normal.D1WZdwpY.woff", "_app/immutable/assets/poppins-latin-ext-800-normal.B0fQqkW3.woff2", "_app/immutable/assets/poppins-latin-ext-800-normal.Bq7hlA9q.woff", "_app/immutable/assets/poppins-latin-800-normal.Bd8-pIP1.woff2", "_app/immutable/assets/poppins-latin-800-normal.DelpWfYX.woff", "_app/immutable/assets/atkinson-hyperlegible-latin-ext-400-normal.Dwzd0TKx.woff2", "_app/immutable/assets/atkinson-hyperlegible-latin-ext-400-normal.-EtKVqC7.woff", "_app/immutable/assets/atkinson-hyperlegible-latin-400-normal.BKTgBNmI.woff2", "_app/immutable/assets/atkinson-hyperlegible-latin-400-normal.DDbeQdWO.woff"];
  }
});
var error_svelte_exports = {};
__export(error_svelte_exports, {
  default: () => Error2
});
var Error2;
var init_error_svelte = __esm({
  ".svelte-kit/output/server/entries/fallbacks/error.svelte.js"() {
    init_ssr();
    init_stores();
    Error2 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let $page, $$unsubscribe_page;
      $$unsubscribe_page = subscribe(page, (value) => $page = value);
      $$unsubscribe_page();
      return `<h1>${escape($page.status)}</h1> <p>${escape($page.error?.message)}</p>`;
    });
  }
});
var __exports2 = {};
__export(__exports2, {
  component: () => component2,
  fonts: () => fonts2,
  imports: () => imports2,
  index: () => index2,
  stylesheets: () => stylesheets2
});
var index2;
var component_cache2;
var component2;
var imports2;
var stylesheets2;
var fonts2;
var init__2 = __esm({
  ".svelte-kit/output/server/nodes/1.js"() {
    index2 = 1;
    component2 = async () => component_cache2 ??= (await Promise.resolve().then(() => (init_error_svelte(), error_svelte_exports))).default;
    imports2 = ["_app/immutable/nodes/1.CqfnJylo.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js", "_app/immutable/chunks/stores.C6La04jU.js", "_app/immutable/chunks/entry.Dv6jx6u8.js"];
    stylesheets2 = [];
    fonts2 = [];
  }
});
var layout_ts_exports2 = {};
__export(layout_ts_exports2, {
  prerender: () => prerender2
});
var prerender2;
var init_layout_ts2 = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/library/_layout.ts.js"() {
    prerender2 = false;
  }
});
var layout_svelte_exports2 = {};
__export(layout_svelte_exports2, {
  default: () => Layout2
});
var Layout2;
var init_layout_svelte2 = __esm({
  ".svelte-kit/output/server/entries/fallbacks/layout.svelte.js"() {
    init_ssr();
    Layout2 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      return `${slots.default ? slots.default({}) : ``}`;
    });
  }
});
var __exports3 = {};
__export(__exports3, {
  component: () => component3,
  fonts: () => fonts3,
  imports: () => imports3,
  index: () => index3,
  stylesheets: () => stylesheets3,
  universal: () => layout_ts_exports2,
  universal_id: () => universal_id2
});
var index3;
var component_cache3;
var component3;
var universal_id2;
var imports3;
var stylesheets3;
var fonts3;
var init__3 = __esm({
  ".svelte-kit/output/server/nodes/2.js"() {
    init_layout_ts2();
    index3 = 2;
    component3 = async () => component_cache3 ??= (await Promise.resolve().then(() => (init_layout_svelte2(), layout_svelte_exports2))).default;
    universal_id2 = "src/routes/[[lang]]/library/+layout.ts";
    imports3 = ["_app/immutable/nodes/2.Kv4YKyzi.js", "_app/immutable/chunks/layout.lwRKNrU8.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js"];
    stylesheets3 = [];
    fonts3 = [];
  }
});
var layout_ts_exports3 = {};
__export(layout_ts_exports3, {
  prerender: () => prerender3
});
var prerender3;
var init_layout_ts3 = __esm({
  ".svelte-kit/output/server/entries/pages/google/_layout.ts.js"() {
    prerender3 = false;
  }
});
var __exports4 = {};
__export(__exports4, {
  component: () => component4,
  fonts: () => fonts4,
  imports: () => imports4,
  index: () => index4,
  stylesheets: () => stylesheets4,
  universal: () => layout_ts_exports3,
  universal_id: () => universal_id3
});
var index4;
var component_cache4;
var component4;
var universal_id3;
var imports4;
var stylesheets4;
var fonts4;
var init__4 = __esm({
  ".svelte-kit/output/server/nodes/4.js"() {
    init_layout_ts3();
    index4 = 4;
    component4 = async () => component_cache4 ??= (await Promise.resolve().then(() => (init_layout_svelte2(), layout_svelte_exports2))).default;
    universal_id3 = "src/routes/google/+layout.ts";
    imports4 = ["_app/immutable/nodes/4.Kv4YKyzi.js", "_app/immutable/chunks/layout.lwRKNrU8.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js"];
    stylesheets4 = [];
    fonts4 = [];
  }
});
var page_ts_exports = {};
var init_page_ts = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/admin/_page.ts.js"() {
  }
});
function Descriptor(source) {
  var target = create(NULL);
  if (hasOwn(source, "value")) {
    target.value = source.value;
  }
  if (hasOwn(source, "writable")) {
    target.writable = source.writable;
  }
  if (hasOwn(source, "get")) {
    target.get = source.get;
  }
  if (hasOwn(source, "set")) {
    target.set = source.set;
  }
  if (hasOwn(source, "enumerable")) {
    target.enumerable = source.enumerable;
  }
  if (hasOwn(source, "configurable")) {
    target.configurable = source.configurable;
  }
  return target;
}
function __PURE__(re2) {
  var test2 = re2.test = Test(re2);
  var exec3 = re2.exec = Exec(re2);
  var source = test2.source = exec3.source = re2.source;
  test2.unicode = exec3.unicode = re2.unicode;
  test2.ignoreCase = exec3.ignoreCase = re2.ignoreCase;
  test2.multiline = exec3.multiline = source.indexOf("^") < 0 && source.indexOf("$") < 0 ? null : re2.multiline;
  test2.dotAll = exec3.dotAll = source.indexOf(".") < 0 ? null : re2.dotAll;
  return re2;
}
function theRegExp(re2) {
  return /* @__PURE__ */ __PURE__(re2);
}
function graveAccentReplacer($$) {
  return $$ === "\\`" ? "`" : $$;
}
function RE(template) {
  var U = this.U;
  var I = this.I;
  var M = this.M;
  var S = this.S;
  var raw = template.raw;
  var source = raw[0].replace(NT, "");
  var index10 = 1;
  var length = arguments.length;
  while (index10 !== length) {
    var value = arguments[index10];
    if (typeof value === "string") {
      source += value;
    } else {
      var value_source = value.source;
      if (typeof value_source !== "string") {
        throw TypeError$1("source");
      }
      if (value.unicode === U) {
        throw SyntaxError$1("unicode");
      }
      if (value.ignoreCase === I) {
        throw SyntaxError$1("ignoreCase");
      }
      if (value.multiline === M && (includes(value_source, "^") || includes(value_source, "$"))) {
        throw SyntaxError$1("multiline");
      }
      if (value.dotAll === S && includes(value_source, ".")) {
        throw SyntaxError$1("dotAll");
      }
      source += value_source;
    }
    source += raw[index10++].replace(NT, "");
  }
  var re2 = RegExp$1(U ? source = source.replace(ESCAPE, graveAccentReplacer) : source, this.flags);
  var test2 = re2.test = Test(re2);
  var exec3 = re2.exec = Exec(re2);
  test2.source = exec3.source = source;
  test2.unicode = exec3.unicode = !U;
  test2.ignoreCase = exec3.ignoreCase = !I;
  test2.multiline = exec3.multiline = includes(source, "^") || includes(source, "$") ? !M : null;
  test2.dotAll = exec3.dotAll = includes(source, ".") ? !S : null;
  return re2;
}
function Context(flags) {
  return {
    U: !includes(flags, "u"),
    I: !includes(flags, "i"),
    M: !includes(flags, "m"),
    S: !includes(flags, "s"),
    flags
  };
}
function groupify(branches, uFlag, noEscape) {
  var group = create$1(NULL);
  var appendBranch = uFlag ? appendPointBranch : appendCodeBranch;
  for (var length = branches.length, index10 = 0; index10 < length; ++index10) {
    appendBranch(group, branches[index10]);
  }
  return sourcify(group, !noEscape);
}
function appendPointBranch(group, branch) {
  if (branch) {
    var character = SURROGATE_PAIR.test(branch) ? branch.slice(0, 2) : branch.charAt(0);
    appendPointBranch(group[character] || (group[character] = create$1(NULL)), branch.slice(character.length));
  } else {
    group[""] = GROUP;
  }
}
function appendCodeBranch(group, branch) {
  if (branch) {
    var character = branch.charAt(0);
    appendCodeBranch(group[character] || (group[character] = create$1(NULL)), branch.slice(1));
  } else {
    group[""] = GROUP;
  }
}
function sourcify(group, needEscape) {
  var branches = [];
  var singleCharactersBranch = [];
  var noEmptyBranch = true;
  for (var character in group) {
    if (character) {
      var sub_branches = sourcify(group[character], needEscape);
      if (needEscape && NEED_TO_ESCAPE_IN_REGEXP.test(character)) {
        character = "\\" + character;
      }
      sub_branches ? branches.push(character + sub_branches) : singleCharactersBranch.push(character);
    } else {
      noEmptyBranch = false;
    }
  }
  singleCharactersBranch.length && branches.unshift(singleCharactersBranch.length === 1 ? singleCharactersBranch[0] : "[" + singleCharactersBranch.join("") + "]");
  return branches.length === 0 ? "" : (branches.length === 1 && (singleCharactersBranch.length || noEmptyBranch) ? branches[0] : "(?:" + branches.join("|") + ")") + (noEmptyBranch ? "" : "?");
}
var SyntaxError$1;
var RangeError$1;
var TypeError$1;
var Error$1;
var undefined$1;
var BigInt$1;
var RegExp$1;
var WeakMap$1;
var get2;
var set;
var create$1;
var isSafeInteger;
var getOwnPropertyNames;
var freeze;
var isPrototypeOf;
var NULL;
var bind;
var test;
var exec2;
var apply$1;
var Proxy$1;
var Object_defineProperty;
var assign$1;
var Object$1;
var floor;
var isArray$1;
var Infinity2;
var fromCharCode;
var Array$1;
var hasOwnProperty;
var propertyIsEnumerable;
var apply;
var isEnum;
var hasOwn;
var create;
var Test;
var Exec;
var NT;
var ESCAPE;
var includes;
var RE_bind;
var CONTEXT;
var newRegExp;
var clearRegExp;
var clearRegExp$1;
var NEED_TO_ESCAPE_IN_REGEXP;
var SURROGATE_PAIR;
var GROUP;
var WeakSet$1;
var has;
var add;
var del;
var keys;
var getOwnPropertySymbols;
var Null$1;
var is;
var Object_defineProperties;
var fromEntries;
var Reflect_construct;
var Reflect_defineProperty;
var Reflect_deleteProperty;
var ownKeys;
var Keeper;
var newWeakMap;
var target2keeper;
var proxy2target;
var target2proxy;
var handlers;
var newProxy;
var orderify;
var Null;
var map_has;
var map_del;
var INLINES;
var SECTIONS;
var isInline;
var ofInline;
var beInline;
var isSection;
var beSection;
var INLINE;
var tables;
var tables_add;
var isTable;
var implicitTables;
var implicitTables_add;
var implicitTables_del;
var directlyIfNot;
var DIRECTLY;
var IMPLICITLY;
var pairs;
var pairs_add;
var fromPair;
var PAIR;
var PlainTable;
var OrderedTable;
var NONE;
var sourcePath;
var sourceLines;
var lastLineIndex;
var lineIndex;
var throws;
var EOL;
var todo;
var next;
var rest;
var mark;
var where;
var done;
var Whitespace;
var PRE_WHITESPACE;
var VALUE_REST_exec;
var LITERAL_STRING_exec;
var MULTI_LINE_LITERAL_STRING_0_1_2;
var MULTI_LINE_LITERAL_STRING_0;
var __MULTI_LINE_LITERAL_STRING_exec;
var SYM_WHITESPACE;
var Tag;
var KEY_VALUE_PAIR_exec;
var _VALUE_PAIR_exec;
var TAG_REST_exec;
var MULTI_LINE_BASIC_STRING;
var MULTI_LINE_BASIC_STRING_exec_0_length;
var ESCAPED_EXCLUDE_CONTROL_CHARACTER_TAB______;
var ESCAPED_EXCLUDE_CONTROL_CHARACTER__________;
var ESCAPED_EXCLUDE_CONTROL_CHARACTER_DEL______;
var ESCAPED_EXCLUDE_CONTROL_CHARACTER_DEL_SLASH;
var __ESCAPED_EXCLUDE_CONTROL_CHARACTER;
var ESCAPED_EXCLUDE_CONTROL_CHARACTER_test;
var BASIC_STRING_TAB______;
var BASIC_STRING__________;
var BASIC_STRING_DEL______;
var BASIC_STRING_DEL_SLASH;
var __BASIC_STRING;
var BASIC_STRING_exec_1_endIndex;
var IS_DOT_KEY;
var DOT_KEY;
var BARE_KEY_STRICT;
var BARE_KEY_FREE;
var __BARE_KEY_exec;
var LITERAL_KEY____;
var LITERAL_KEY_DEL;
var __LITERAL_KEY_exec;
var supportArrayOfTables;
var TABLE_DEFINITION_exec_groups;
var KEY_VALUE_PAIR_exec_groups;
var CONTROL_CHARACTER_EXCLUDE_TAB____;
var CONTROL_CHARACTER_EXCLUDE_TAB_DEL;
var __CONTROL_CHARACTER_EXCLUDE_test;
var switchRegExp;
var NUM;
var IS_AMAZING;
var BAD_DXOB;
var isAmazing;
var mustScalar;
var ARGS_MODE;
var useWhatToJoinMultilineString;
var usingBigInt;
var IntegerMinNumber;
var IntegerMaxNumber;
var ANY;
var Keys;
var isKeys;
var KEYS$1;
var preserveLiteral;
var zeroDatetime;
var inlineTable;
var moreDatetime;
var disallowEmptyKey;
var sError;
var sFloat;
var Table3;
var allowLonger;
var enableNull;
var allowInlineTableMultilineAndTrailingCommaEvenNoComma;
var preserveComment;
var disableDigit;
var arrayTypes;
var arrayTypes_get;
var arrayTypes_set;
var As;
var AS_TYPED;
var asMixed;
var asNulls;
var asStrings;
var asTables;
var asArrays;
var asBooleans;
var asFloats;
var asIntegers;
var asOffsetDateTimes;
var asLocalDateTimes;
var asLocalDates;
var asLocalTimes;
var processor;
var each2;
var collect_on;
var collect_off;
var collect;
var Process;
var clear;
var use;
var isView;
var isArrayBuffer;
var TextDecoder$1;
var Symbol$1;
var previous;
var x;
var _literal;
var LiteralObject;
var arrays;
var arrays_add;
var isArray;
var OF_TABLES;
var STATICALLY;
var staticalArrays;
var staticalArrays_add;
var isStatic;
var newArray;
var NativeDate;
var parse$2;
var preventExtensions;
var getOwnPropertyDescriptors;
var defineProperties;
var fpc;
var _29_;
var _30_;
var _31_;
var _23_;
var _59_;
var YMD;
var HMS;
var OFFSET$;
var Z_exec;
var OFFSET_DATETIME_exec;
var OFFSET_DATETIME_ZERO_exec;
var IS_LOCAL_DATETIME;
var IS_LOCAL_DATE;
var IS_LOCAL_TIME;
var T;
var DELIMITER_DOT;
var DOT_ZERO;
var ZERO;
var zeroReplacer;
var Datetime;
var Value;
var d;
var d2u;
var ValueOFFSET;
var validateLeap;
var VALIDATE_LEAP;
var DATE$1;
var OffsetDateTime_ISOString;
var OffsetDateTime_value;
var OffsetDateTime_use;
var OffsetDateTime;
var LocalDateTime_ISOString;
var LocalDateTime_value;
var LocalDateTime_get;
var LocalDateTime_set;
var LocalDateTime;
var LocalDate_ISOString;
var LocalDate_value;
var LocalDate_get;
var LocalDate_set;
var LocalDate;
var LocalTime_ISOString;
var LocalTime_value;
var LocalTime_get;
var LocalTime_set;
var LocalTime;
var parseInt$1;
var fromCodePoint;
var ESCAPED_IN_SINGLE_LINE;
var ESCAPED_IN_MULTI_LINE;
var BasicString;
var MultilineBasicString;
var INTEGER_D;
var BAD_D;
var IS_D_INTEGER;
var IS_XOB_INTEGER;
var BAD_XOB;
var UNDERSCORES$1;
var UNDERSCORES_SIGN;
var IS_INTEGER;
var MIN;
var MAX;
var BigIntInteger;
var NumberInteger;
var Integer;
var isFinite$1;
var NaN$1;
var _NaN;
var _Infinity$1;
var IS_FLOAT;
var UNDERSCORES;
var IS_ZERO;
var NORMALIZED;
var ORIGINAL;
var Float;
var prepareTable;
var appendTable;
var prepareInlineTable;
var checkLiteralString;
var assignLiteralString;
var assignBasicString;
var KEYS;
var commentFor;
var commentForThis;
var includesNewline;
var getCOMMENT;
var getComment;
var IS_OFFSET$;
var IS_EMPTY;
var parseKeys;
var push;
var equalStaticArray;
var equalInlineTable;
var ForComment;
var assign;
var Root2;
var MAX_SAFE_INTEGER;
var DATE;
var valueOf$2;
var isString;
var valueOf$1;
var isNumber;
var isBigInt;
var valueOf;
var isBoolean;
var ESCAPED;
var NEED_BASIC;
var BY_ESCAPE;
var NEED_ESCAPE;
var singlelineString;
var NEED_MULTILINE_BASIC;
var multilineNeedBasic;
var REAL_MULTILINE_ESCAPE;
var NEED_MULTILINE_ESCAPE;
var Float64Array$1;
var Uint8Array$1;
var _Infinity;
var INTEGER_LIKE;
var ensureFloat;
var float64Array;
var uint8Array;
var NaN_7;
var float;
var isDate;
var BARE;
var $Key$;
var FIRST;
var literalString;
var $Keys;
var TOMLSection;
var IS_INDENT;
var linesFromStringify;
var isLinesFromStringify;
var textDecoder;
var binary2string;
var isBinaryLike;
var includesNonScalar;
var assertFulScalar;
var holding;
var parse3;
var parse$1;
var init_j_toml = __esm({
  "node_modules/@ltd/j-toml/index.mjs"() {
    SyntaxError$1 = SyntaxError;
    RangeError$1 = RangeError;
    TypeError$1 = TypeError;
    Error$1 = { if: Error }.if;
    undefined$1 = void 0;
    BigInt$1 = typeof BigInt === "undefined" ? undefined$1 : BigInt;
    RegExp$1 = RegExp;
    WeakMap$1 = WeakMap;
    get2 = WeakMap.prototype.get;
    set = WeakMap.prototype.set;
    create$1 = Object.create;
    isSafeInteger = Number.isSafeInteger;
    getOwnPropertyNames = Object.getOwnPropertyNames;
    freeze = Object.freeze;
    isPrototypeOf = Object.prototype.isPrototypeOf;
    NULL = /* j-globals: null.prototype (internal) */
    Object.seal ? /* @__PURE__ */ Object.preventExtensions(/* @__PURE__ */ Object.create(null)) : null;
    bind = Function.prototype.bind;
    test = RegExp.prototype.test;
    exec2 = RegExp.prototype.exec;
    apply$1 = Reflect.apply;
    Proxy$1 = Proxy;
    Object_defineProperty = Object.defineProperty;
    assign$1 = Object.assign;
    Object$1 = Object;
    floor = Math.floor;
    isArray$1 = Array.isArray;
    Infinity2 = 1 / 0;
    fromCharCode = String.fromCharCode;
    Array$1 = Array;
    hasOwnProperty = Object.prototype.hasOwnProperty;
    propertyIsEnumerable = Object.prototype.propertyIsEnumerable;
    apply = Function.prototype.apply;
    isEnum = /* @__PURE__ */ propertyIsEnumerable.call.bind(propertyIsEnumerable);
    hasOwn = /* j-globals: Object.hasOwn (polyfill) */
    Object$1.hasOwn || /* @__PURE__ */ function() {
      return hasOwnProperty.bind ? hasOwnProperty.call.bind(hasOwnProperty) : function hasOwn2(object, key2) {
        return hasOwnProperty.call(object, key2);
      };
    }();
    create = Object$1.create;
    Test = bind ? /* @__PURE__ */ bind.bind(test) : function(re2) {
      return function(string) {
        return test.call(re2, string);
      };
    };
    Exec = bind ? /* @__PURE__ */ bind.bind(exec2) : function(re2) {
      return function(string) {
        return exec2.call(re2, string);
      };
    };
    NT = /[\n\t]+/g;
    ESCAPE = /\\./g;
    includes = "".includes ? function(that, searchString) {
      return that.includes(searchString);
    } : function(that, searchString) {
      return that.indexOf(searchString) > -1;
    };
    RE_bind = bind && /* @__PURE__ */ bind.bind(RE);
    CONTEXT = /* @__PURE__ */ Context("");
    newRegExp = Proxy$1 ? /* @__PURE__ */ new Proxy$1(RE, {
      apply: function(RE2, thisArg, args) {
        return apply$1(RE2, CONTEXT, args);
      },
      get: function(RE2, flags) {
        return RE_bind(Context(flags));
      },
      defineProperty: function() {
        return false;
      },
      preventExtensions: function() {
        return false;
      }
    }) : /* @__PURE__ */ function() {
      RE.apply = RE.apply;
      var newRegExp2 = function() {
        return RE.apply(CONTEXT, arguments);
      };
      var d2 = 1;
      var g2 = d2 * 2;
      var i = g2 * 2;
      var m = i * 2;
      var s3 = i * 2;
      var u2 = s3 * 2;
      var y = u2 * 2;
      var flags = y * 2 - 1;
      while (flags--) {
        (function(context) {
          newRegExp2[context.flags] = function() {
            return RE.apply(context, arguments);
          };
        })(Context(
          (flags & d2 ? "" : "d") + (flags & g2 ? "" : "g") + (flags & i ? "" : "i") + (flags & m ? "" : "m") + (flags & s3 ? "" : "s") + (flags & u2 ? "" : "u") + (flags & y ? "" : "y")
        ));
      }
      return freeze ? freeze(newRegExp2) : newRegExp2;
    }();
    clearRegExp = "$_" in RegExp$1 ? /* @__PURE__ */ function() {
      var REGEXP = /^/;
      REGEXP.test = REGEXP.test;
      return function clearRegExp3(value) {
        REGEXP.test("");
        return value;
      };
    }() : function clearRegExp2(value) {
      return value;
    };
    clearRegExp$1 = clearRegExp;
    NEED_TO_ESCAPE_IN_REGEXP = /^[$()*+\-.?[\\\]^{|]/;
    SURROGATE_PAIR = /^[\uD800-\uDBFF][\uDC00-\uDFFF]/;
    GROUP = /* @__PURE__ */ create$1(NULL);
    WeakSet$1 = WeakSet;
    has = WeakSet.prototype.has;
    add = WeakSet.prototype.add;
    del = WeakSet.prototype["delete"];
    keys = Object.keys;
    getOwnPropertySymbols = Object.getOwnPropertySymbols;
    Null$1 = /* j-globals: null (internal) */
    /* @__PURE__ */ function() {
      var assign2 = Object.assign || function assign3(target, source) {
        var keys$1, index10, key2;
        for (keys$1 = keys(source), index10 = 0; index10 < keys$1.length; ++index10) {
          key2 = keys$1[index10];
          target[key2] = source[key2];
        }
        if (getOwnPropertySymbols) {
          for (keys$1 = getOwnPropertySymbols(source), index10 = 0; index10 < keys$1.length; ++index10) {
            key2 = keys$1[index10];
            if (isEnum(source, key2)) {
              target[key2] = source[key2];
            }
          }
        }
        return target;
      };
      function Nullify(constructor) {
        delete constructor.prototype.constructor;
        freeze(constructor.prototype);
        return constructor;
      }
      function Null2(origin) {
        return origin === undefined$1 ? this : typeof origin === "function" ? /* @__PURE__ */ Nullify(origin) : /* @__PURE__ */ assign2(/* @__PURE__ */ create(NULL), origin);
      }
      delete Null2.name;
      Null2.prototype = null;
      freeze(Null2);
      return Null2;
    }();
    is = Object.is;
    Object_defineProperties = Object.defineProperties;
    fromEntries = Object.fromEntries;
    Reflect_construct = Reflect.construct;
    Reflect_defineProperty = Reflect.defineProperty;
    Reflect_deleteProperty = Reflect.deleteProperty;
    ownKeys = Reflect.ownKeys;
    Keeper = () => [];
    newWeakMap = () => {
      const weakMap = new WeakMap$1();
      weakMap.has = weakMap.has;
      weakMap.get = weakMap.get;
      weakMap.set = weakMap.set;
      return weakMap;
    };
    target2keeper = /* @__PURE__ */ newWeakMap();
    proxy2target = /* @__PURE__ */ newWeakMap();
    target2proxy = /* @__PURE__ */ newWeakMap();
    handlers = /* @__PURE__ */ assign$1(create$1(NULL), {
      defineProperty: (target, key2, descriptor) => {
        if (hasOwn(target, key2)) {
          return Reflect_defineProperty(target, key2, assign$1(create$1(NULL), descriptor));
        }
        if (Reflect_defineProperty(target, key2, assign$1(create$1(NULL), descriptor))) {
          const keeper = target2keeper.get(target);
          keeper[keeper.length] = key2;
          return true;
        }
        return false;
      },
      deleteProperty: (target, key2) => {
        if (Reflect_deleteProperty(target, key2)) {
          const keeper = target2keeper.get(target);
          const index10 = keeper.indexOf(key2);
          index10 < 0 || --keeper.copyWithin(index10, index10 + 1).length;
          return true;
        }
        return false;
      },
      ownKeys: (target) => target2keeper.get(target),
      construct: (target, args, newTarget) => orderify(Reflect_construct(target, args, newTarget)),
      apply: (target, thisArg, args) => orderify(apply$1(target, thisArg, args))
    });
    newProxy = (target, keeper) => {
      target2keeper.set(target, keeper);
      const proxy = new Proxy$1(target, handlers);
      proxy2target.set(proxy, target);
      return proxy;
    };
    orderify = (object) => {
      if (proxy2target.has(object)) {
        return object;
      }
      let proxy = target2proxy.get(object);
      if (proxy) {
        return proxy;
      }
      proxy = newProxy(object, assign$1(Keeper(), ownKeys(object)));
      target2proxy.set(object, proxy);
      return proxy;
    };
    Null = /* @__PURE__ */ function() {
      function throwConstructing() {
        throw TypeError$1(`Super constructor Null cannot be invoked with 'new'`);
      }
      function throwApplying() {
        throw TypeError$1(`Super constructor Null cannot be invoked without 'new'`);
      }
      const Nullify = (constructor) => {
        delete constructor.prototype.constructor;
        freeze(constructor.prototype);
        return constructor;
      };
      function Null2(constructor) {
        return new.target ? new.target === Null2 ? /* @__PURE__ */ throwConstructing() : /* @__PURE__ */ newProxy(this, Keeper()) : typeof constructor === "function" ? /* @__PURE__ */ Nullify(constructor) : /* @__PURE__ */ throwApplying();
      }
      Null2.prototype = null;
      Object_defineProperty(Null2, "name", assign$1(create$1(NULL), { value: "", configurable: false }));
      freeze(Null2);
      return Null2;
    }();
    map_has = WeakMap.prototype.has;
    map_del = WeakMap.prototype["delete"];
    INLINES = new WeakMap$1();
    SECTIONS = new WeakSet$1();
    isInline = /* @__PURE__ */ map_has.bind(INLINES);
    ofInline = /* @__PURE__ */ get2.bind(INLINES);
    beInline = /* @__PURE__ */ set.bind(INLINES);
    isSection = /* @__PURE__ */ has.bind(SECTIONS);
    beSection = /* @__PURE__ */ add.bind(SECTIONS);
    INLINE = true;
    tables = new WeakSet$1();
    tables_add = /* @__PURE__ */ add.bind(tables);
    isTable = /* @__PURE__ */ has.bind(tables);
    implicitTables = new WeakSet$1();
    implicitTables_add = /* @__PURE__ */ add.bind(implicitTables);
    implicitTables_del = /* @__PURE__ */ del.bind(implicitTables);
    directlyIfNot = (table) => {
      if (implicitTables_del(table)) {
        beSection(table);
        return true;
      }
      return false;
    };
    DIRECTLY = true;
    IMPLICITLY = false;
    pairs = new WeakSet$1();
    pairs_add = /* @__PURE__ */ add.bind(pairs);
    fromPair = /* @__PURE__ */ has.bind(pairs);
    PAIR = true;
    PlainTable = /* @__PURE__ */ Null$1(class Table extends Null$1 {
      constructor(isDirect, isInline$fromPair) {
        super();
        tables_add(this);
        isDirect ? isInline$fromPair ? beInline(this, true) : beSection(this) : (isInline$fromPair ? pairs_add : implicitTables_add)(this);
        return this;
      }
    });
    OrderedTable = /* @__PURE__ */ Null$1(class Table2 extends Null {
      constructor(isDirect, isInline$fromPair) {
        super();
        tables_add(this);
        isDirect ? isInline$fromPair ? beInline(this, true) : beSection(this) : (isInline$fromPair ? pairs_add : implicitTables_add)(this);
        return this;
      }
    });
    NONE = [];
    sourcePath = "";
    sourceLines = NONE;
    lastLineIndex = -1;
    lineIndex = -1;
    throws = (error2) => {
      throw error2;
    };
    EOL = /\r?\n/;
    todo = (source, path) => {
      if (typeof path !== "string") {
        throw TypeError$1(`TOML.parse({ path })`);
      }
      sourcePath = path;
      sourceLines = source.split(EOL);
      lastLineIndex = sourceLines.length - 1;
      lineIndex = -1;
    };
    next = () => sourceLines[++lineIndex];
    rest = () => lineIndex !== lastLineIndex;
    mark = class {
      lineIndex = lineIndex;
      type;
      restColumn;
      constructor(type, restColumn) {
        this.type = type;
        this.restColumn = restColumn;
        return this;
      }
      must() {
        lineIndex === lastLineIndex && throws(SyntaxError$1(`${this.type} is not close until the end of the file` + where(", which started from ", this.lineIndex, sourceLines[this.lineIndex].length - this.restColumn + 1)));
        return sourceLines[++lineIndex];
      }
      nowrap(argsMode) {
        throw throws(Error$1(`TOML.parse(${argsMode ? `${argsMode}multilineStringJoiner` : `,{ joiner }`}) must be passed, while the source including multi-line string` + where(", which started from ", this.lineIndex, sourceLines[this.lineIndex].length - this.restColumn + 1)));
      }
    };
    where = (pre, rowIndex = lineIndex, columnNumber = 0) => sourceLines === NONE ? "" : sourcePath ? `
    at (${sourcePath}:${rowIndex + 1}:${columnNumber})` : `${pre}line ${rowIndex + 1}: ${sourceLines[rowIndex]}`;
    done = () => {
      sourcePath = "";
      sourceLines = NONE;
    };
    Whitespace = /[ \t]/;
    PRE_WHITESPACE = /* @__PURE__ */ newRegExp`
	^${Whitespace}+`.valueOf();
    ({ exec: VALUE_REST_exec } = /* @__PURE__ */ newRegExp.s`
	^
	(
		(?:\d\d\d\d-\d\d-\d\d \d)?
		[\w\-+.:]+
	)
	${Whitespace}*
	(.*)
	$`.valueOf());
    ({ exec: LITERAL_STRING_exec } = /* @__PURE__ */ newRegExp.s`
	^
	'([^']*)'
	${Whitespace}*
	(.*)`.valueOf());
    ({ exec: MULTI_LINE_LITERAL_STRING_0_1_2 } = /* @__PURE__ */ newRegExp.s`
	^
	(.*?)
	'''('{0,2})
	${Whitespace}*
	(.*)`.valueOf());
    ({ exec: MULTI_LINE_LITERAL_STRING_0 } = /* @__PURE__ */ newRegExp.s`
	^
	(.*?)
	'''()
	${Whitespace}*
	(.*)`.valueOf());
    __MULTI_LINE_LITERAL_STRING_exec = MULTI_LINE_LITERAL_STRING_0;
    SYM_WHITESPACE = /* @__PURE__ */ newRegExp.s`
	^
	.
	${Whitespace}*`.valueOf();
    Tag = /[^\x00-\x1F"#'()<>[\\\]`{}\x7F]+/;
    ({ exec: KEY_VALUE_PAIR_exec } = /* @__PURE__ */ newRegExp.s`
	^
	${Whitespace}*
	=
	${Whitespace}*
	(?:
		<(${Tag})>
		${Whitespace}*
	)?
	(.*)
	$`.valueOf());
    ({ exec: _VALUE_PAIR_exec } = /* @__PURE__ */ newRegExp.s`
	^
	<(${Tag})>
	${Whitespace}*
	(.*)
	$`.valueOf());
    ({ exec: TAG_REST_exec } = /* @__PURE__ */ newRegExp.s`
	^
	<(${Tag})>
	${Whitespace}*
	(.*)
	$`.valueOf());
    MULTI_LINE_BASIC_STRING = theRegExp(/[^\\"]+|\\.?|"(?!"")"?/sy);
    MULTI_LINE_BASIC_STRING_exec_0_length = (_2) => {
      let lastIndex = (
        /*MULTI_LINE_BASIC_STRING.lastIndex = */
        0
      );
      while (MULTI_LINE_BASIC_STRING.test(_2)) {
        lastIndex = MULTI_LINE_BASIC_STRING.lastIndex;
      }
      return lastIndex;
    };
    ESCAPED_EXCLUDE_CONTROL_CHARACTER_TAB______ = /[^\\\x00-\x08\x0B-\x1F\x7F]+|\\(?:[btnfr"\\]|[\t ]*\n[\t\n ]*|u[\dA-Fa-f]{4}|U[\dA-Fa-f]{8})/g;
    ESCAPED_EXCLUDE_CONTROL_CHARACTER__________ = /[^\\\x00-\x09\x0B-\x1F\x7F]+|\\(?:[btnfr"\\]|[\t ]*\n[\t\n ]*|u[\dA-Fa-f]{4}|U[\dA-Fa-f]{8})/g;
    ESCAPED_EXCLUDE_CONTROL_CHARACTER_DEL______ = /[^\\\x00-\x09\x0B-\x1F]+|\\(?:[btnfr"\\]|[\t ]*\n[\t\n ]*|u[\dA-Fa-f]{4}|U[\dA-Fa-f]{8})/g;
    ESCAPED_EXCLUDE_CONTROL_CHARACTER_DEL_SLASH = /[^\\\x00-\x09\x0B-\x1F]+|\\(?:[btnfr"\\/]|[\t ]*\n[\t\n ]*|u[\dA-Fa-f]{4}|U[\dA-Fa-f]{8})/g;
    __ESCAPED_EXCLUDE_CONTROL_CHARACTER = ESCAPED_EXCLUDE_CONTROL_CHARACTER_TAB______;
    ESCAPED_EXCLUDE_CONTROL_CHARACTER_test = (_2) => !_2.replace(__ESCAPED_EXCLUDE_CONTROL_CHARACTER, "");
    BASIC_STRING_TAB______ = theRegExp(/[^\\"\x00-\x08\x0B-\x1F\x7F]+|\\(?:[btnfr"\\]|u[\dA-Fa-f]{4}|U[\dA-Fa-f]{8})/y);
    BASIC_STRING__________ = theRegExp(/[^\\"\x00-\x08\x0B-\x1F\x7F]+|\\(?:[btnfr"\\]|u[\dA-Fa-f]{4}|U[\dA-Fa-f]{8})/y);
    BASIC_STRING_DEL______ = theRegExp(/[^\\"\x00-\x08\x0B-\x1F]+|\\(?:[btnfr"\\]|u[\dA-Fa-f]{4}|U[\dA-Fa-f]{8})/y);
    BASIC_STRING_DEL_SLASH = theRegExp(/[^\\"\x00-\x08\x0B-\x1F]+|\\(?:[btnfr"\\/]|u[\dA-Fa-f]{4}|U[\dA-Fa-f]{8})/y);
    __BASIC_STRING = BASIC_STRING_DEL_SLASH;
    BASIC_STRING_exec_1_endIndex = (line) => {
      let lastIndex = __BASIC_STRING.lastIndex = 1;
      while (__BASIC_STRING.test(line)) {
        lastIndex = __BASIC_STRING.lastIndex;
      }
      lastIndex !== line.length && line[lastIndex] === '"' || throws(SyntaxError$1(`Bad basic string` + where(" at ")));
      return lastIndex;
    };
    ({ test: IS_DOT_KEY } = theRegExp(/^[ \t]*\./));
    DOT_KEY = /^[ \t]*\.[ \t]*/;
    ({ exec: BARE_KEY_STRICT } = theRegExp(/^[\w-]+/));
    ({ exec: BARE_KEY_FREE } = theRegExp(/^[^ \t#=[\]'".]+(?:[ \t]+[^ \t#=[\]'".]+)*/));
    __BARE_KEY_exec = BARE_KEY_FREE;
    ({ exec: LITERAL_KEY____ } = theRegExp(/^'[^'\x00-\x08\x0B-\x1F\x7F]*'/));
    ({ exec: LITERAL_KEY_DEL } = theRegExp(/^'[^'\x00-\x08\x0B-\x1F]*'/));
    __LITERAL_KEY_exec = LITERAL_KEY_DEL;
    supportArrayOfTables = true;
    TABLE_DEFINITION_exec_groups = (lineRest, parseKeys2) => {
      const asArrayItem = lineRest[1] === "[";
      if (asArrayItem) {
        supportArrayOfTables || throws(SyntaxError$1(`Array of Tables is not allowed before TOML v0.2` + where(", which at ")));
        lineRest = lineRest.slice(2);
      } else {
        lineRest = lineRest.slice(1);
      }
      lineRest = lineRest.replace(PRE_WHITESPACE, "");
      const { leadingKeys, finalKey } = { lineRest } = parseKeys2(lineRest);
      lineRest = lineRest.replace(PRE_WHITESPACE, "");
      lineRest && lineRest[0] === "]" || throws(SyntaxError$1(`Table header is not closed` + where(", which is found at ")));
      (lineRest.length > 1 ? lineRest[1] === "]" === asArrayItem : !asArrayItem) || throws(SyntaxError$1(`Square brackets of Table definition statement not match` + where(" at ")));
      lineRest = lineRest.slice(asArrayItem ? 2 : 1).replace(PRE_WHITESPACE, "");
      let tag2;
      if (lineRest && lineRest[0] === "<") {
        ({ 1: tag2, 2: lineRest } = TAG_REST_exec(lineRest) || throws(SyntaxError$1(`Bad tag` + where(" at "))));
      } else {
        tag2 = "";
      }
      return { leadingKeys, finalKey, asArrayItem, tag: tag2, lineRest };
    };
    KEY_VALUE_PAIR_exec_groups = ({ leadingKeys, finalKey, lineRest }) => {
      const { 1: tag2 = "" } = { 2: lineRest } = KEY_VALUE_PAIR_exec(lineRest) || throws(SyntaxError$1(`Keys must equal something` + where(", but missing at ")));
      tag2 || lineRest && lineRest[0] !== "#" || throws(SyntaxError$1(`Value can not be missing after euqal sign` + where(", which is found at ")));
      return { leadingKeys, finalKey, tag: tag2, lineRest };
    };
    ({ test: CONTROL_CHARACTER_EXCLUDE_TAB____ } = theRegExp(/[\x00-\x08\x0B-\x1F\x7F]/));
    ({ test: CONTROL_CHARACTER_EXCLUDE_TAB_DEL } = theRegExp(/[\x00-\x08\x0B-\x1F]/));
    __CONTROL_CHARACTER_EXCLUDE_test = CONTROL_CHARACTER_EXCLUDE_TAB____;
    switchRegExp = (specificationVersion) => {
      switch (specificationVersion) {
        case 1:
          __MULTI_LINE_LITERAL_STRING_exec = MULTI_LINE_LITERAL_STRING_0_1_2;
          __LITERAL_KEY_exec = LITERAL_KEY____;
          __CONTROL_CHARACTER_EXCLUDE_test = CONTROL_CHARACTER_EXCLUDE_TAB____;
          __ESCAPED_EXCLUDE_CONTROL_CHARACTER = ESCAPED_EXCLUDE_CONTROL_CHARACTER_TAB______;
          __BASIC_STRING = BASIC_STRING_TAB______;
          __BARE_KEY_exec = BARE_KEY_STRICT;
          supportArrayOfTables = true;
          break;
        case 0.5:
          __MULTI_LINE_LITERAL_STRING_exec = MULTI_LINE_LITERAL_STRING_0;
          __LITERAL_KEY_exec = LITERAL_KEY____;
          __CONTROL_CHARACTER_EXCLUDE_test = CONTROL_CHARACTER_EXCLUDE_TAB____;
          __ESCAPED_EXCLUDE_CONTROL_CHARACTER = ESCAPED_EXCLUDE_CONTROL_CHARACTER__________;
          __BASIC_STRING = BASIC_STRING__________;
          __BARE_KEY_exec = BARE_KEY_STRICT;
          supportArrayOfTables = true;
          break;
        case 0.4:
          __MULTI_LINE_LITERAL_STRING_exec = MULTI_LINE_LITERAL_STRING_0;
          __LITERAL_KEY_exec = LITERAL_KEY_DEL;
          __CONTROL_CHARACTER_EXCLUDE_test = CONTROL_CHARACTER_EXCLUDE_TAB_DEL;
          __ESCAPED_EXCLUDE_CONTROL_CHARACTER = ESCAPED_EXCLUDE_CONTROL_CHARACTER_DEL______;
          __BASIC_STRING = BASIC_STRING_DEL______;
          __BARE_KEY_exec = BARE_KEY_STRICT;
          supportArrayOfTables = true;
          break;
        default:
          __MULTI_LINE_LITERAL_STRING_exec = MULTI_LINE_LITERAL_STRING_0;
          __LITERAL_KEY_exec = LITERAL_KEY_DEL;
          __CONTROL_CHARACTER_EXCLUDE_test = CONTROL_CHARACTER_EXCLUDE_TAB_DEL;
          __ESCAPED_EXCLUDE_CONTROL_CHARACTER = ESCAPED_EXCLUDE_CONTROL_CHARACTER_DEL_SLASH;
          __BASIC_STRING = BASIC_STRING_DEL_SLASH;
          __BARE_KEY_exec = BARE_KEY_FREE;
          supportArrayOfTables = false;
      }
    };
    NUM = /* @__PURE__ */ newRegExp`
	(?:
		0
		(?:
			b[01][_01]*
		|
			o[0-7][_0-7]*
		|
			x[\dA-Fa-f][_\dA-Fa-f]*
		|
			(?:\.\d[_\d]*)?(?:[Ee]-?\d[_\d]*)?
		)
	|
		[1-9][_\d]*
		(?:\.\d[_\d]*)?(?:[Ee]-?\d[_\d]*)?
	|
		inf
	|
		nan
	)
`.valueOf();
    ({ test: IS_AMAZING } = /* @__PURE__ */ newRegExp`
	^(?:
		-?${NUM}
		(?:-${NUM})*
	|
		true
	|
		false
	)$
`.valueOf());
    ({ test: BAD_DXOB } = /* @__PURE__ */ newRegExp`_(?![\dA-Fa-f])`.valueOf());
    isAmazing = (keys2) => IS_AMAZING(keys2) && !BAD_DXOB(keys2);
    mustScalar = true;
    ARGS_MODE = "";
    useWhatToJoinMultilineString = null;
    usingBigInt = true;
    IntegerMinNumber = 0;
    IntegerMaxNumber = 0;
    ANY = {
      test: () => true
    };
    Keys = class KeysRegExp extends RegExp$1 {
      constructor(keys2) {
        super(`^${groupify(keys2)}$`);
        let maxLength = -1;
        for (let index10 = keys2.length; index10; ) {
          const { length } = keys2[--index10];
          if (length > maxLength) {
            maxLength = length;
          }
        }
        this.lastIndex = maxLength + 1;
        return this;
      }
      test(key2) {
        return key2.length < this.lastIndex && super.test(key2);
      }
    };
    isKeys = /* @__PURE__ */ isPrototypeOf.bind(/* @__PURE__ */ freeze(Keys.prototype));
    KEYS$1 = ANY;
    arrayTypes = new WeakMap$1();
    arrayTypes_get = /* @__PURE__ */ get2.bind(arrayTypes);
    arrayTypes_set = /* @__PURE__ */ set.bind(arrayTypes);
    As = () => {
      const as = (array2) => {
        const got = arrayTypes_get(array2);
        got ? got === as || throws(TypeError$1(`Types in Array must be same` + where(". Check "))) : arrayTypes_set(array2, as);
        return array2;
      };
      return as;
    };
    AS_TYPED = {
      asNulls: As(),
      asStrings: As(),
      asTables: As(),
      asArrays: As(),
      asBooleans: As(),
      asFloats: As(),
      asIntegers: As(),
      asOffsetDateTimes: As(),
      asLocalDateTimes: As(),
      asLocalDates: As(),
      asLocalTimes: As()
    };
    asMixed = (array2) => array2;
    processor = null;
    each2 = null;
    collect_on = (tag2, array2, table, key2) => {
      const _each = create$1(NULL);
      _each._linked = each2;
      _each.tag = tag2;
      if (table) {
        _each.table = table;
        _each.key = key2;
      }
      if (array2) {
        _each.array = array2;
        _each.index = array2.length;
      }
      each2 = _each;
    };
    collect_off = () => {
      throw throws(SyntaxError$1(`xOptions.tag is not enabled, but found tag syntax` + where(" at ")));
    };
    collect = collect_off;
    Process = () => {
      if (each2) {
        const _processor = processor;
        let _each = each2;
        each2 = null;
        return () => {
          const processor2 = _processor;
          let each3 = _each;
          _each = null;
          do {
            processor2(each3);
          } while (each3 = each3._linked);
        };
      }
      return null;
    };
    clear = () => {
      KEYS$1 = ANY;
      useWhatToJoinMultilineString = processor = each2 = null;
      zeroDatetime = false;
    };
    use = (specificationVersion, multilineStringJoiner, useBigInt, keys2, xOptions, argsMode) => {
      ARGS_MODE = argsMode;
      let mixed;
      switch (specificationVersion) {
        case 1:
          mustScalar = mixed = moreDatetime = sFloat = inlineTable = true;
          zeroDatetime = disallowEmptyKey = false;
          break;
        case 0.5:
          mustScalar = moreDatetime = sFloat = inlineTable = true;
          mixed = zeroDatetime = disallowEmptyKey = false;
          break;
        case 0.4:
          mustScalar = disallowEmptyKey = inlineTable = true;
          mixed = zeroDatetime = moreDatetime = sFloat = false;
          break;
        case 0.3:
          mustScalar = disallowEmptyKey = true;
          mixed = zeroDatetime = moreDatetime = sFloat = inlineTable = false;
          break;
        case 0.2:
          zeroDatetime = disallowEmptyKey = true;
          mustScalar = mixed = moreDatetime = sFloat = inlineTable = false;
          break;
        case 0.1:
          zeroDatetime = disallowEmptyKey = true;
          mustScalar = mixed = moreDatetime = sFloat = inlineTable = false;
          break;
        default:
          throw RangeError$1(`TOML.parse(,specificationVersion)`);
      }
      switchRegExp(specificationVersion);
      if (typeof multilineStringJoiner === "string") {
        useWhatToJoinMultilineString = multilineStringJoiner;
      } else if (multilineStringJoiner === undefined$1) {
        useWhatToJoinMultilineString = null;
      } else {
        throw TypeError$1(`TOML.parse(${ARGS_MODE ? `${ARGS_MODE}multilineStringJoiner` : `,{ joiner }`})`);
      }
      if (useBigInt === undefined$1 || useBigInt === true) {
        usingBigInt = true;
      } else if (useBigInt === false) {
        usingBigInt = false;
      } else {
        if (typeof useBigInt !== "number") {
          throw TypeError$1(`TOML.parse(${ARGS_MODE ? `${ARGS_MODE},useBigInt` : `,{ bigint }`})`);
        }
        if (!isSafeInteger(useBigInt)) {
          throw RangeError$1(`TOML.parse(${ARGS_MODE ? `${ARGS_MODE},useBigInt` : `,{ bigint }`})`);
        }
        usingBigInt = null;
        useBigInt >= 0 ? IntegerMinNumber = -(IntegerMaxNumber = useBigInt) : IntegerMaxNumber = -(IntegerMinNumber = useBigInt) - 1;
      }
      if (!BigInt$1 && usingBigInt !== false) {
        throw Error$1(`Can't work without TOML.parse(${ARGS_MODE ? `${ARGS_MODE},useBigInt` : `,{ bigint }`}) being set to false, because the host doesn't have BigInt support`);
      }
      if (keys2 == null) {
        KEYS$1 = ANY;
      } else {
        if (!isKeys(keys2)) {
          throw TypeError$1(`TOML.parse(,{ keys })`);
        }
        KEYS$1 = keys2;
      }
      if (xOptions == null) {
        Table3 = PlainTable;
        sError = allowLonger = enableNull = allowInlineTableMultilineAndTrailingCommaEvenNoComma = false;
        collect = collect_off;
      } else if (typeof xOptions !== "object") {
        throw TypeError$1(`TOML.parse(${ARGS_MODE ? `${ARGS_MODE},,xOptions` : `,{ x }`})`);
      } else {
        const { order, longer, exact, null: _null, multi, comment, string, literal, tag: tag2, ...unknown2 } = xOptions;
        const unknownNames = getOwnPropertyNames(unknown2);
        if (unknownNames.length) {
          throw TypeError$1(`TOML.parse(${ARGS_MODE ? `${ARGS_MODE},,{ ${unknownNames.join(", ")} }` : `,{ x: { ${unknownNames.join(", ")} } }`})`);
        }
        Table3 = order ? OrderedTable : PlainTable;
        allowLonger = !longer;
        sError = !!exact;
        enableNull = !!_null;
        allowInlineTableMultilineAndTrailingCommaEvenNoComma = !!multi;
        preserveComment = !!comment;
        disableDigit = !!string;
        preserveLiteral = !!literal;
        if (tag2) {
          if (typeof tag2 !== "function") {
            throw TypeError$1(`TOML.parse(${ARGS_MODE ? `${ARGS_MODE},,{ tag }` : `,{ x: { tag } }`})`);
          }
          if (!mixed) {
            throw TypeError$1(`TOML.parse(${ARGS_MODE ? `${ARGS_MODE},,xOptions` : `,{ x }`}) xOptions.tag needs at least TOML 1.0 to support mixed type array`);
          }
          processor = tag2;
          collect = collect_on;
        } else {
          collect = collect_off;
        }
      }
      mixed ? asNulls = asStrings = asTables = asArrays = asBooleans = asFloats = asIntegers = asOffsetDateTimes = asLocalDateTimes = asLocalDates = asLocalTimes = asMixed : { asNulls, asStrings, asTables, asArrays, asBooleans, asFloats, asIntegers, asOffsetDateTimes, asLocalDateTimes, asLocalDates, asLocalTimes } = AS_TYPED;
    };
    isView = ArrayBuffer.isView;
    isArrayBuffer = /* j-globals: class.isArrayBuffer (internal) */
    /* @__PURE__ */ function() {
      if (typeof ArrayBuffer === "function") {
        var byteLength_apply = apply.bind(Object.getOwnPropertyDescriptor(ArrayBuffer.prototype, "byteLength").get);
        return function isArrayBuffer2(value) {
          try {
            byteLength_apply(value);
          } catch (error2) {
            return false;
          }
          return true;
        };
      }
      return function isArrayBuffer2() {
        return false;
      };
    }();
    TextDecoder$1 = TextDecoder;
    Symbol$1 = Symbol;
    previous = Symbol$1("previous");
    x = (rootStack) => {
      let stack = rootStack;
      let result = stack.next();
      if (!result.done) {
        result.value[previous] = stack;
        result = (stack = result.value).next();
        for (; ; ) {
          if (result.done) {
            if (stack === rootStack) {
              break;
            }
            stack = stack[previous];
            result = stack.next(result.value);
          } else {
            result.value[previous] = stack;
            result = (stack = result.value).next();
          }
        }
      }
      return result.value;
    };
    _literal = Symbol$1("_literal");
    LiteralObject = (literal, value) => {
      const object = Object$1(value);
      object[_literal] = literal;
      return object;
    };
    arrays = new WeakSet$1();
    arrays_add = /* @__PURE__ */ add.bind(arrays);
    isArray = /* @__PURE__ */ has.bind(arrays);
    OF_TABLES = false;
    STATICALLY = true;
    staticalArrays = new WeakSet$1();
    staticalArrays_add = /* @__PURE__ */ add.bind(staticalArrays);
    isStatic = /* @__PURE__ */ has.bind(staticalArrays);
    newArray = (isStatic2) => {
      const array2 = [];
      arrays_add(array2);
      isStatic2 && staticalArrays_add(array2);
      return array2;
    };
    NativeDate = Date;
    parse$2 = Date.parse;
    preventExtensions = Object.preventExtensions;
    getOwnPropertyDescriptors = Object.getOwnPropertyDescriptors;
    defineProperties = /* j-globals: null.defineProperties (internal) */
    function defineProperties2(object, descriptorMap) {
      var created = create$1(NULL);
      var names = keys(descriptorMap);
      for (var length = names.length, index10 = 0; index10 < length; ++index10) {
        var name = names[index10];
        created[name] = Descriptor(descriptorMap[name]);
      }
      if (getOwnPropertySymbols) {
        var symbols = getOwnPropertySymbols(descriptorMap);
        for (length = symbols.length, index10 = 0; index10 < length; ++index10) {
          var symbol = symbols[index10];
          if (isEnum(descriptorMap, symbol)) {
            created[symbol] = Descriptor(descriptorMap[symbol]);
          }
        }
      }
      return Object_defineProperties(object, created);
    };
    fpc = (c2) => {
      freeze(freeze(c2).prototype);
      return c2;
    };
    _29_ = /(?:0[1-9]|1\d|2\d)/;
    _30_ = /(?:0[1-9]|[12]\d|30)/;
    _31_ = /(?:0[1-9]|[12]\d|3[01])/;
    _23_ = /(?:[01]\d|2[0-3])/;
    _59_ = /[0-5]\d/;
    YMD = /* @__PURE__ */ newRegExp`
	\d\d\d\d-
	(?:
		0
		(?:
			[13578]-${_31_}
			|
			[469]-${_30_}
			|
			2-${_29_}
		)
		|
		1
		(?:
			[02]-${_31_}
			|
			1-${_30_}
		)
	)
`.valueOf();
    HMS = /* @__PURE__ */ newRegExp`
	${_23_}:${_59_}:${_59_}
`.valueOf();
    OFFSET$ = /(?:[Zz]|[+-]\d\d:\d\d)$/;
    ({ exec: Z_exec } = theRegExp(/(([+-])\d\d):(\d\d)$/));
    ({ exec: OFFSET_DATETIME_exec } = /* @__PURE__ */ newRegExp`
	^
	${YMD}
	[Tt ]
	${HMS}
	(?:\.\d{1,3}(\d*?)0*)?
	(?:[Zz]|[+-]${_23_}:${_59_})
	$`.valueOf());
    ({ exec: OFFSET_DATETIME_ZERO_exec } = /* @__PURE__ */ newRegExp`
	^
	${YMD}
	[Tt ]
	${HMS}
	()
	[Zz]
	$`.valueOf());
    ({ test: IS_LOCAL_DATETIME } = /* @__PURE__ */ newRegExp`
	^
	${YMD}
	[Tt ]
	${HMS}
	(?:\.\d+)?
	$`.valueOf());
    ({ test: IS_LOCAL_DATE } = /* @__PURE__ */ newRegExp`
	^
	${YMD}
	$`.valueOf());
    ({ test: IS_LOCAL_TIME } = /* @__PURE__ */ newRegExp`
	^
	${HMS}
	(?:\.\d+)?
	$`.valueOf());
    T = /[ t]/;
    DELIMITER_DOT = /[-T:.]/g;
    DOT_ZERO = /\.?0+$/;
    ZERO = /\.(\d*?)0+$/;
    zeroReplacer = (match, p1) => p1;
    Datetime = /* @__PURE__ */ (() => {
      const Datetime2 = function() {
        return this;
      };
      const descriptors = Null$1(null);
      {
        const descriptor = Null$1(null);
        for (const key2 of ownKeys(NativeDate.prototype)) {
          key2 === "constructor" || key2 === "toJSON" || (descriptors[key2] = descriptor);
        }
      }
      Datetime2.prototype = preventExtensions(create$1(NativeDate.prototype, descriptors));
      return freeze(Datetime2);
    })();
    Value = (ISOString) => ISOString.replace(ZERO, zeroReplacer).replace(DELIMITER_DOT, "");
    d = /./gs;
    d2u = (d2) => "\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009"[d2];
    ValueOFFSET = (time, more) => time < 0 ? ("" + (time + 6216730554e4)).replace(d, d2u).padStart(14, "\u2000") + more.replace(d, d2u) + time : more ? (time + ".").padStart(16, "0") + more : ("" + time).padStart(15, "0");
    validateLeap = (literal) => {
      if (literal.startsWith("02-29", 5)) {
        const year = +literal.slice(0, 4);
        return year & 3 ? false : year % 100 ? true : year % 400 ? false : year % 3200 ? true : false;
      }
      return true;
    };
    ({ test: VALIDATE_LEAP } = /* @__PURE__ */ newRegExp.s`^.....(?:06.30|12.31).23:59:59`.valueOf());
    DATE$1 = /* @__PURE__ */ defineProperties(new NativeDate(0), /* @__PURE__ */ getOwnPropertyDescriptors(NativeDate.prototype));
    OffsetDateTime_ISOString = Symbol$1("OffsetDateTime_ISOString");
    OffsetDateTime_value = Symbol$1("OffsetDateTime_value");
    OffsetDateTime_use = (that, $ = 0) => {
      DATE$1.setTime(+that[OffsetDateTime_value] + $);
      return DATE$1;
    };
    OffsetDateTime = /* @__PURE__ */ fpc(class OffsetDateTime2 extends Datetime {
      [OffsetDateTime_ISOString];
      [OffsetDateTime_value];
      get [Symbol$1.toStringTag]() {
        return "OffsetDateTime";
      }
      valueOf() {
        return this[OffsetDateTime_value];
      }
      toISOString() {
        return this[OffsetDateTime_ISOString];
      }
      constructor(literal) {
        validateLeap(literal) || throws(SyntaxError$1(`Invalid Offset Date-Time ${literal}` + where(" at ")));
        const with60 = literal.startsWith("60", 17);
        let without60 = with60 ? literal.slice(0, 17) + "59" + literal.slice(19) : literal;
        const { 1: more = "" } = (zeroDatetime ? OFFSET_DATETIME_ZERO_exec(without60) : OFFSET_DATETIME_exec(without60)) || throws(SyntaxError$1(`Invalid Offset Date-Time ${literal}` + where(" at ")));
        const time = parse$2(without60 = without60.replace(T, "T").replace("z", "Z"));
        if (with60) {
          DATE$1.setTime(time);
          VALIDATE_LEAP(DATE$1.toISOString()) || throws(SyntaxError$1(`Invalid Offset Date-Time ${literal}` + where(" at ")));
        }
        super();
        this[OffsetDateTime_ISOString] = without60;
        this[OffsetDateTime_value] = ValueOFFSET(time, more);
        return this;
      }
      getUTCFullYear() {
        return OffsetDateTime_use(this).getUTCFullYear();
      }
      ///get year () :FullYear { return OffsetDateTime_get(this, 0, 4); }
      ///set year (value :FullYear) { OffsetDateTime_set(this, 0, 4, value, true); }
      getUTCMonth() {
        return OffsetDateTime_use(this).getUTCMonth();
      }
      ///get month () { return OffsetDateTime_get(this, 5, 7); }
      ///set month (value) { OffsetDateTime_set(this, 5, 7, value, true); }
      getUTCDate() {
        return OffsetDateTime_use(this).getUTCDate();
      }
      ///get day () :Date { return OffsetDateTime_get(this, 8, 10); }
      ///set day (value :Date) { OffsetDateTime_set(this, 8, 10, value, true); }
      getUTCHours() {
        return OffsetDateTime_use(this).getUTCHours();
      }
      ///get hour () :Hours { return OffsetDateTime_get(this, 11, 13); }
      ///set hour (value :Hours) { OffsetDateTime_set(this, 11, 13, value, true); }
      getUTCMinutes() {
        return OffsetDateTime_use(this).getUTCMinutes();
      }
      ///get minute () :Minutes { return OffsetDateTime_get(this, 14, 16); }
      ///set minute (value :Minutes) { OffsetDateTime_set(this, 14, 16, value, true); }
      getUTCSeconds() {
        return OffsetDateTime_use(this).getUTCSeconds();
      }
      ///get second () :Seconds { return OffsetDateTime_get(this, 17, 19); }
      ///set second (value :Seconds) { OffsetDateTime_set(this, 17, 19, value, true); }
      getUTCMilliseconds() {
        return OffsetDateTime_use(this).getUTCMilliseconds();
      }
      ///
      ///get millisecond () :Milliseconds { return this[OffsetDateTime_value]%1000; }///
      /*set millisecond (value :Milliseconds) {
      	this[OffsetDateTime_ISOString] = this[OffsetDateTime_ISOString].slice(0, 19) + ( value ? ( '.' + ( '' + value ).padStart(3, '0') ).replace(DOT_ZERO, '') : '' ) + this[OffsetDateTime_ISOString].slice(this[OffsetDateTime_ISOString].search(OFFSET$));
      	OffsetDateTime_set(this, 0, 0, 0, false);
      }*/
      //
      ///get microsecond () :Milliseconds
      ///set microsecond (value :Milliseconds)
      ///get nanosecond () :Milliseconds
      ///set nanosecond (value :Milliseconds)
      getUTCDay() {
        return OffsetDateTime_use(this).getUTCDay();
      }
      ///get dayOfWeek () { return OffsetDateTime_use(this, this.getTimezoneOffset()*60000).getUTCDay() || 7; }
      getTimezoneOffset() {
        const z = Z_exec(this[OffsetDateTime_ISOString]);
        return z ? +z[1] * 60 + +(z[2] + z[3]) : 0;
      }
      ///get offset () { return this[OffsetDateTime_ISOString].endsWith('Z') ? 'Z' : this[OffsetDateTime_ISOString].slice(-6); }
      /*set offset (value) {
      	this[OffsetDateTime_ISOString] = this[OffsetDateTime_ISOString].slice(0, this[OffsetDateTime_ISOString].endsWith('Z') ? -1 : -6) + value;
      	OffsetDateTime_set(this, 0, 0, 0, true);
      }*/
      //
      getTime() {
        return floor(+this[OffsetDateTime_value]);
      }
      ///
      /*setTime (this :OffsetDateTime, value :Time) :void {
      	value = DATE.setTime(value);
      	const z = Z_exec(this[OffsetDateTime_ISOString]);
      	DATE.setTime(value + ( z ? +z[1]*60 + +( z[2] + z[3] ) : 0 )*60000);
      	this[OffsetDateTime_ISOString] = z ? DATE.toISOString().slice(0, -1) + z[0] : DATE.toISOString();
      	this[OffsetDateTime_value] = ValueOFFSET(value, '');
      	///return value;
      }*/
    });
    LocalDateTime_ISOString = Symbol$1("LocalDateTime_ISOString");
    LocalDateTime_value = Symbol$1("LocalDateTime_value");
    LocalDateTime_get = (that, start, end) => +that[LocalDateTime_ISOString].slice(start, end);
    LocalDateTime_set = (that, start, end, value) => {
      const string = "" + value;
      const size = end - start;
      if (string.length > size) {
        throw RangeError$1();
      }
      that[LocalDateTime_value] = Value(
        that[LocalDateTime_ISOString] = that[LocalDateTime_ISOString].slice(0, start) + string.padStart(size, "0") + that[LocalDateTime_ISOString].slice(end)
      );
    };
    LocalDateTime = /* @__PURE__ */ fpc(class LocalDateTime2 extends Datetime {
      [LocalDateTime_ISOString];
      [LocalDateTime_value];
      get [Symbol$1.toStringTag]() {
        return "LocalDateTime";
      }
      valueOf() {
        return this[LocalDateTime_value];
      }
      toISOString() {
        return this[LocalDateTime_ISOString];
      }
      constructor(literal) {
        IS_LOCAL_DATETIME(literal) && validateLeap(literal) || throws(SyntaxError$1(`Invalid Local Date-Time ${literal}` + where(" at ")));
        super();
        this[LocalDateTime_value] = Value(
          this[LocalDateTime_ISOString] = literal.replace(T, "T")
        );
        return this;
      }
      getFullYear() {
        return LocalDateTime_get(this, 0, 4);
      }
      setFullYear(value) {
        LocalDateTime_set(this, 0, 4, value);
      }
      getMonth() {
        return LocalDateTime_get(this, 5, 7) - 1;
      }
      setMonth(value) {
        LocalDateTime_set(this, 5, 7, value + 1);
      }
      getDate() {
        return LocalDateTime_get(this, 8, 10);
      }
      setDate(value) {
        LocalDateTime_set(this, 8, 10, value);
      }
      getHours() {
        return LocalDateTime_get(this, 11, 13);
      }
      setHours(value) {
        LocalDateTime_set(this, 11, 13, value);
      }
      getMinutes() {
        return LocalDateTime_get(this, 14, 16);
      }
      setMinutes(value) {
        LocalDateTime_set(this, 14, 16, value);
      }
      getSeconds() {
        return LocalDateTime_get(this, 17, 19);
      }
      setSeconds(value) {
        LocalDateTime_set(this, 17, 19, value);
      }
      getMilliseconds() {
        return +this[LocalDateTime_value].slice(14, 17).padEnd(3, "0");
      }
      ///
      setMilliseconds(value) {
        this[LocalDateTime_value] = Value(
          this[LocalDateTime_ISOString] = this[LocalDateTime_ISOString].slice(0, 19) + (value ? ("." + ("" + value).padStart(3, "0")).replace(DOT_ZERO, "") : "")
        );
      }
    });
    LocalDate_ISOString = Symbol$1("LocalDate_ISOString");
    LocalDate_value = Symbol$1("LocalDate_value");
    LocalDate_get = (that, start, end) => +that[LocalDate_ISOString].slice(start, end);
    LocalDate_set = (that, start, end, value) => {
      const string = "" + value;
      const size = end - start;
      if (string.length > size) {
        throw RangeError$1();
      }
      that[LocalDate_value] = Value(
        that[LocalDate_ISOString] = that[LocalDate_ISOString].slice(0, start) + string.padStart(size, "0") + that[LocalDate_ISOString].slice(end)
      );
    };
    LocalDate = /* @__PURE__ */ fpc(class LocalDate2 extends Datetime {
      [LocalDate_ISOString];
      [LocalDate_value];
      get [Symbol$1.toStringTag]() {
        return "LocalDate";
      }
      valueOf() {
        return this[LocalDate_value];
      }
      toISOString() {
        return this[LocalDate_ISOString];
      }
      constructor(literal) {
        IS_LOCAL_DATE(literal) && validateLeap(literal) || throws(SyntaxError$1(`Invalid Local Date ${literal}` + where(" at ")));
        super();
        this[LocalDate_value] = Value(
          this[LocalDate_ISOString] = literal
        );
        return this;
      }
      getFullYear() {
        return LocalDate_get(this, 0, 4);
      }
      setFullYear(value) {
        LocalDate_set(this, 0, 4, value);
      }
      getMonth() {
        return LocalDate_get(this, 5, 7) - 1;
      }
      setMonth(value) {
        LocalDate_set(this, 5, 7, value + 1);
      }
      getDate() {
        return LocalDate_get(this, 8, 10);
      }
      setDate(value) {
        LocalDate_set(this, 8, 10, value);
      }
    });
    LocalTime_ISOString = Symbol$1("LocalTime_ISOString");
    LocalTime_value = Symbol$1("LocalTime_value");
    LocalTime_get = (that, start, end) => +that[LocalTime_ISOString].slice(start, end);
    LocalTime_set = (that, start, end, value) => {
      const string = "" + value;
      const size = end - start;
      if (string.length > size) {
        throw RangeError$1();
      }
      that[LocalTime_value] = Value(
        that[LocalTime_ISOString] = that[LocalTime_ISOString].slice(0, start) + string.padStart(2, "0") + that[LocalTime_ISOString].slice(end)
      );
    };
    LocalTime = /* @__PURE__ */ fpc(class LocalTime2 extends Datetime {
      [LocalTime_ISOString];
      [LocalTime_value];
      get [Symbol$1.toStringTag]() {
        return "LocalTime";
      }
      valueOf() {
        return this[LocalTime_value];
      }
      toISOString() {
        return this[LocalTime_ISOString];
      }
      constructor(literal) {
        IS_LOCAL_TIME(literal) || throws(SyntaxError$1(`Invalid Local Time ${literal}` + where(" at ")));
        super();
        this[LocalTime_value] = Value(
          this[LocalTime_ISOString] = literal
        );
        return this;
      }
      getHours() {
        return LocalTime_get(this, 0, 2);
      }
      setHours(value) {
        LocalTime_set(this, 0, 2, value);
      }
      getMinutes() {
        return LocalTime_get(this, 3, 5);
      }
      setMinutes(value) {
        LocalTime_set(this, 3, 5, value);
      }
      getSeconds() {
        return LocalTime_get(this, 6, 8);
      }
      setSeconds(value) {
        LocalTime_set(this, 6, 8, value);
      }
      getMilliseconds() {
        return +this[LocalTime_value].slice(6, 9).padEnd(3, "0");
      }
      ///
      setMilliseconds(value) {
        this[LocalTime_value] = Value(
          this[LocalTime_ISOString] = this[LocalTime_ISOString].slice(0, 8) + (value ? ("." + ("" + value).padStart(3, "0")).replace(DOT_ZERO, "") : "")
        );
      }
    });
    parseInt$1 = parseInt;
    fromCodePoint = String.fromCodePoint;
    ESCAPED_IN_SINGLE_LINE = /[^\\]+|\\(?:[\\"btnfr/]|u.{4}|U.{8})/gs;
    ESCAPED_IN_MULTI_LINE = /[^\n\\]+|\n|\\(?:[\t ]*\n[\t\n ]*|[\\"btnfr/]|u.{4}|U.{8})/gs;
    BasicString = (literal) => {
      if (!literal) {
        return "";
      }
      const parts = literal.match(ESCAPED_IN_SINGLE_LINE);
      const { length } = parts;
      let index10 = 0;
      do {
        const part = parts[index10];
        if (part[0] === "\\") {
          switch (part[1]) {
            case "\\":
              parts[index10] = "\\";
              break;
            case '"':
              parts[index10] = '"';
              break;
            case "b":
              parts[index10] = "\b";
              break;
            case "t":
              parts[index10] = "	";
              break;
            case "n":
              parts[index10] = "\n";
              break;
            case "f":
              parts[index10] = "\f";
              break;
            case "r":
              parts[index10] = "\r";
              break;
            case "u":
              const charCode = parseInt$1(part.slice(2), 16);
              mustScalar && 55295 < charCode && charCode < 57344 && throws(RangeError$1(`Invalid Unicode Scalar ${part}` + where(" at ")));
              parts[index10] = fromCharCode(charCode);
              break;
            case "U":
              const codePoint = parseInt$1(part.slice(2), 16);
              (mustScalar && 55295 < codePoint && codePoint < 57344 || 1114111 < codePoint) && throws(RangeError$1(`Invalid Unicode Scalar ${part}` + where(" at ")));
              parts[index10] = fromCodePoint(codePoint);
              break;
            case "/":
              parts[index10] = "/";
              break;
          }
        }
      } while (++index10 !== length);
      return parts.join("");
    };
    MultilineBasicString = (literal, useWhatToJoinMultilineString2, n2) => {
      if (!literal) {
        return "";
      }
      const parts = literal.match(ESCAPED_IN_MULTI_LINE);
      const { length } = parts;
      let index10 = 0;
      do {
        const part = parts[index10];
        if (part === "\n") {
          ++n2;
          parts[index10] = useWhatToJoinMultilineString2;
        } else if (part[0] === "\\") {
          switch (part[1]) {
            case "\n":
            case " ":
            case "	":
              for (let i = 0; i = part.indexOf("\n", i) + 1; ) {
                ++n2;
              }
              parts[index10] = "";
              break;
            case "\\":
              parts[index10] = "\\";
              break;
            case '"':
              parts[index10] = '"';
              break;
            case "b":
              parts[index10] = "\b";
              break;
            case "t":
              parts[index10] = "	";
              break;
            case "n":
              parts[index10] = "\n";
              break;
            case "f":
              parts[index10] = "\f";
              break;
            case "r":
              parts[index10] = "\r";
              break;
            case "u":
              const charCode = parseInt$1(part.slice(2), 16);
              mustScalar && 55295 < charCode && charCode < 57344 && throws(RangeError$1(`Invalid Unicode Scalar ${part}` + where(" at ", lineIndex + n2)));
              parts[index10] = fromCharCode(charCode);
              break;
            case "U":
              const codePoint = parseInt$1(part.slice(2), 16);
              (mustScalar && 55295 < codePoint && codePoint < 57344 || 1114111 < codePoint) && throws(RangeError$1(`Invalid Unicode Scalar ${part}` + where(" at ", lineIndex + n2)));
              parts[index10] = fromCodePoint(codePoint);
              break;
            case "/":
              parts[index10] = "/";
              break;
          }
        }
      } while (++index10 !== length);
      return parts.join("");
    };
    INTEGER_D = /[-+]?(?:0|[1-9][_\d]*)/;
    ({ test: BAD_D } = /* @__PURE__ */ newRegExp`_(?!\d)`.valueOf());
    ({ test: IS_D_INTEGER } = /* @__PURE__ */ newRegExp`^${INTEGER_D}$`.valueOf());
    ({ test: IS_XOB_INTEGER } = theRegExp(/^0(?:x[\dA-Fa-f][_\dA-Fa-f]*|o[0-7][_0-7]*|b[01][_01]*)$/));
    ({ test: BAD_XOB } = /* @__PURE__ */ newRegExp`_(?![\dA-Fa-f])`.valueOf());
    UNDERSCORES$1 = /_/g;
    UNDERSCORES_SIGN = /_|^[-+]/g;
    IS_INTEGER = (literal) => (IS_D_INTEGER(literal) || /*options.xob && */
    IS_XOB_INTEGER(literal)) && !BAD_XOB(literal);
    MIN = BigInt$1 && -/* @__PURE__ */ BigInt$1("0x8000000000000000");
    MAX = BigInt$1 && /* @__PURE__ */ BigInt$1("0x7FFFFFFFFFFFFFFF");
    BigIntInteger = (literal) => {
      IS_INTEGER(literal) || throws(SyntaxError$1(`Invalid Integer ${literal}` + where(" at ")));
      const bigInt = literal[0] === "-" ? -BigInt$1(literal.replace(UNDERSCORES_SIGN, "")) : BigInt$1(literal.replace(UNDERSCORES_SIGN, ""));
      allowLonger || MIN <= bigInt && bigInt <= MAX || throws(RangeError$1(`Integer expect 64 bit range (-9,223,372,036,854,775,808 to 9,223,372,036,854,775,807), not includes ${literal}` + where(" meet at ")));
      return bigInt;
    };
    NumberInteger = (literal) => {
      IS_INTEGER(literal) || throws(SyntaxError$1(`Invalid Integer ${literal}` + where(" at ")));
      const number = parseInt$1(literal.replace(UNDERSCORES$1, ""));
      isSafeInteger(number) || throws(RangeError$1(`Integer did not use BitInt must fit Number.isSafeInteger, not includes ${literal}` + where(" meet at ")));
      return number;
    };
    Integer = (literal) => {
      if (usingBigInt === true) {
        return BigIntInteger(literal);
      }
      if (usingBigInt === false) {
        return NumberInteger(literal);
      }
      IS_INTEGER(literal) || throws(SyntaxError$1(`Invalid Integer ${literal}` + where(" at ")));
      const number = parseInt$1(literal.replace(UNDERSCORES$1, ""));
      if (IntegerMinNumber <= number && number <= IntegerMaxNumber) {
        return number;
      }
      const bigInt = literal[0] === "-" ? -BigInt$1(literal.replace(UNDERSCORES_SIGN, "")) : BigInt$1(literal.replace(UNDERSCORES_SIGN, ""));
      allowLonger || MIN <= bigInt && bigInt <= MAX || throws(RangeError$1(`Integer expect 64 bit range (-9,223,372,036,854,775,808 to 9,223,372,036,854,775,807), not includes ${literal}` + where(" meet at ")));
      return bigInt;
    };
    isFinite$1 = isFinite;
    NaN$1 = 0 / 0;
    _NaN = -NaN$1;
    _Infinity$1 = -Infinity2;
    ({ test: IS_FLOAT } = /* @__PURE__ */ newRegExp`
	^
	${INTEGER_D}
	(?:
		\.\d[_\d]*
		(?:[eE][-+]?\d[_\d]*)?
	|
		[eE][-+]?\d[_\d]*
	)
	$`.valueOf());
    UNDERSCORES = /_/g;
    ({ test: IS_ZERO } = theRegExp(/^[-+]?0(?:\.0+)?(?:[eE][-+]?0+)?$/));
    ({ exec: NORMALIZED } = theRegExp(/^[-0]?(\d*)(?:\.(\d+))?(?:e\+?(-?\d+))?$/));
    ({ exec: ORIGINAL } = theRegExp(/^[-+]?0?(\d*)(?:\.(\d*?)0*)?(?:[eE]\+?(-?\d+))?$/));
    Float = (literal) => {
      if (!IS_FLOAT(literal) || BAD_D(literal)) {
        if (sFloat) {
          if (literal === "inf" || literal === "+inf") {
            return Infinity2;
          }
          if (literal === "-inf") {
            return _Infinity$1;
          }
          if (literal === "nan" || literal === "+nan") {
            return NaN$1;
          }
          if (literal === "-nan") {
            return _NaN;
          }
        } else if (!sError) {
          if (literal === "inf" || literal === "+inf") {
            return Infinity2;
          }
          if (literal === "-inf") {
            return _Infinity$1;
          }
        }
        throw throws(SyntaxError$1(`Invalid Float ${literal}` + where(" at ")));
      }
      const withoutUnderscores = literal.replace(UNDERSCORES, "");
      const number = +withoutUnderscores;
      if (sError) {
        isFinite$1(number) || throws(RangeError$1(`Float ${literal} has been as big as inf` + where(" at ")));
        number || IS_ZERO(withoutUnderscores) || throws(RangeError$1(`Float ${literal} has been as little as ${literal[0] === "-" ? "-" : ""}0` + where(" at ")));
        const { 1: normalized_integer, 2: normalized_fractional = "", 3: normalized_exponent = "" } = NORMALIZED(number);
        const { 1: original_integer, 2: original_fractional = "", 3: original_exponent = "" } = ORIGINAL(withoutUnderscores);
        original_integer + original_fractional === normalized_integer + normalized_fractional && original_exponent - original_fractional.length === normalized_exponent - normalized_fractional.length || throws(RangeError$1(`Float ${literal} has lost its exact and been ${number}` + where(" at ")));
      }
      return number;
    };
    prepareTable = (table, keys2) => {
      const { length } = keys2;
      let index10 = 0;
      while (index10 < length) {
        const key2 = keys2[index10++];
        if (key2 in table) {
          table = table[key2];
          if (isTable(table)) {
            isInline(table) && throws(Error$1(`Trying to define Table under Inline Table` + where(" at ")));
          } else if (isArray(table)) {
            isStatic(table) && throws(Error$1(`Trying to append value to Static Array` + where(" at ")));
            table = table[table.length - 1];
          } else {
            throw throws(Error$1(`Trying to define Table under non-Table value` + where(" at ")));
          }
        } else {
          table = table[key2] = new Table3(IMPLICITLY);
          while (index10 < length) {
            table = table[keys2[index10++]] = new Table3(IMPLICITLY);
          }
          return table;
        }
      }
      return table;
    };
    appendTable = (table, finalKey, asArrayItem, tag2) => {
      let lastTable;
      if (asArrayItem) {
        let arrayOfTables;
        if (finalKey in table) {
          isArray(arrayOfTables = table[finalKey]) && !isStatic(arrayOfTables) || throws(Error$1(`Trying to push Table to non-ArrayOfTables value` + where(" at ")));
        } else {
          arrayOfTables = table[finalKey] = newArray(OF_TABLES);
        }
        tag2 && collect(tag2, arrayOfTables, table, finalKey);
        arrayOfTables[arrayOfTables.length] = lastTable = new Table3(DIRECTLY);
      } else {
        if (finalKey in table) {
          lastTable = table[finalKey];
          fromPair(lastTable) && throws(Error$1(`A table defined implicitly via key/value pair can not be accessed to via []` + where(", which at ")));
          directlyIfNot(lastTable) || throws(Error$1(`Duplicate Table definition` + where(" at ")));
        } else {
          table[finalKey] = lastTable = new Table3(DIRECTLY);
        }
        tag2 && collect(tag2, null, table, finalKey);
      }
      return lastTable;
    };
    prepareInlineTable = (table, keys2) => {
      const { length } = keys2;
      let index10 = 0;
      while (index10 < length) {
        const key2 = keys2[index10++];
        if (key2 in table) {
          table = table[key2];
          isTable(table) || throws(Error$1(`Trying to assign property through non-Table value` + where(" at ")));
          isInline(table) && throws(Error$1(`Trying to assign property through static Inline Table` + where(" at ")));
          fromPair(table) || throws(Error$1(`A table defined implicitly via [] can not be accessed to via key/value pair` + where(", which at ")));
        } else {
          table = table[key2] = new Table3(IMPLICITLY, PAIR);
          while (index10 < length) {
            table = table[keys2[index10++]] = new Table3(IMPLICITLY, PAIR);
          }
          return table;
        }
      }
      return table;
    };
    checkLiteralString = (literal) => {
      __CONTROL_CHARACTER_EXCLUDE_test(literal) && throws(SyntaxError$1(`Control characters other than Tab are not permitted in a Literal String` + where(", which was found at ")));
      return literal;
    };
    assignLiteralString = (table, finalKey, literal) => {
      if (!literal.startsWith(`'''`)) {
        const $2 = LITERAL_STRING_exec(literal) || throws(SyntaxError$1(`Bad literal string` + where(" at ")));
        const value = checkLiteralString($2[1]);
        table[finalKey] = preserveLiteral ? LiteralObject(literal.slice(0, value.length + 2), value) : value;
        return $2[2];
      }
      const $ = __MULTI_LINE_LITERAL_STRING_exec(literal.slice(3));
      if ($) {
        const value = checkLiteralString($[1]) + $[2];
        table[finalKey] = preserveLiteral ? LiteralObject(literal.slice(0, value.length + 6), value) : value;
        return $[3];
      }
      const start = new mark("Multi-line Literal String", literal.length);
      const leadingNewline = !(literal = literal.slice(3));
      if (leadingNewline) {
        literal = start.must();
        const $2 = __MULTI_LINE_LITERAL_STRING_exec(literal);
        if ($2) {
          const value = checkLiteralString($2[1]) + $2[2];
          table[finalKey] = preserveLiteral ? LiteralObject([`'''`, literal.slice(0, value.length + 3)], value) : value;
          return $2[3];
        }
      }
      useWhatToJoinMultilineString === null && start.nowrap(ARGS_MODE);
      for (const lines = [checkLiteralString(literal)]; ; ) {
        const line = start.must();
        const $2 = __MULTI_LINE_LITERAL_STRING_exec(line);
        if ($2) {
          lines[lines.length] = checkLiteralString($2[1]) + $2[2];
          const value = lines.join(useWhatToJoinMultilineString);
          if (preserveLiteral) {
            lines[lines.length - 1] += `'''`;
            leadingNewline ? lines.unshift(`'''`) : lines[0] = `'''${literal}`;
            table[finalKey] = LiteralObject(lines, value);
          } else {
            table[finalKey] = value;
          }
          return $2[3];
        }
        lines[lines.length] = checkLiteralString(line);
      }
    };
    assignBasicString = (table, finalKey, literal) => {
      if (!literal.startsWith('"""')) {
        const index10 = BASIC_STRING_exec_1_endIndex(literal);
        const value = BasicString(literal.slice(1, index10));
        table[finalKey] = preserveLiteral ? LiteralObject(literal.slice(0, index10 + 1), value) : value;
        return literal.slice(index10 + 1).replace(PRE_WHITESPACE, "");
      }
      let length = 3 + MULTI_LINE_BASIC_STRING_exec_0_length(literal.slice(3));
      if (literal.length !== length) {
        const $ = literal.slice(3, length);
        ESCAPED_EXCLUDE_CONTROL_CHARACTER_test($) || throws(SyntaxError$1(`Bad multi-line basic string` + where(" at ")));
        const value = BasicString($) + (literal.startsWith('"', length += 3) ? literal.startsWith('"', ++length) ? (++length, '""') : '"' : "");
        table[finalKey] = preserveLiteral ? LiteralObject(literal.slice(0, length), value) : value;
        return literal.slice(length).replace(PRE_WHITESPACE, "");
      }
      const start = new mark("Multi-line Basic String", length);
      const skipped = (literal = literal.slice(3)) ? 0 : 1;
      if (skipped) {
        literal = start.must();
        let length2 = MULTI_LINE_BASIC_STRING_exec_0_length(literal);
        if (literal.length !== length2) {
          const $ = literal.slice(0, length2);
          ESCAPED_EXCLUDE_CONTROL_CHARACTER_test($) || throws(SyntaxError$1(`Bad multi-line basic string` + where(" at ")));
          const value = MultilineBasicString($, useWhatToJoinMultilineString, skipped) + (literal.startsWith('"', length2 += 3) ? literal.startsWith('"', ++length2) ? (++length2, '""') : '"' : "");
          table[finalKey] = preserveLiteral ? LiteralObject(['"""', literal.slice(0, length2)], value) : value;
          return literal.slice(length2).replace(PRE_WHITESPACE, "");
        }
      }
      useWhatToJoinMultilineString === null && start.nowrap(ARGS_MODE);
      ESCAPED_EXCLUDE_CONTROL_CHARACTER_test(literal + "\n") || throws(SyntaxError$1(`Bad multi-line basic string` + where(" at ")));
      for (const lines = [literal]; ; ) {
        const line = start.must();
        let length2 = MULTI_LINE_BASIC_STRING_exec_0_length(line);
        if (line.length !== length2) {
          const $ = line.slice(0, length2);
          ESCAPED_EXCLUDE_CONTROL_CHARACTER_test($) || throws(SyntaxError$1(`Bad multi-line basic string` + where(" at ")));
          const value = MultilineBasicString(lines.join("\n") + "\n" + $, useWhatToJoinMultilineString, skipped) + (line.startsWith('"', length2 += 3) ? line.startsWith('"', ++length2) ? (++length2, '""') : '"' : "");
          if (preserveLiteral) {
            skipped ? lines.unshift('"""') : lines[0] = `"""${literal}`;
            lines[lines.length] = `${$}"""`;
            table[finalKey] = LiteralObject(lines, value);
          } else {
            table[finalKey] = value;
          }
          return line.slice(length2).replace(PRE_WHITESPACE, "");
        }
        ESCAPED_EXCLUDE_CONTROL_CHARACTER_test(line + "\n") || throws(SyntaxError$1(`Bad multi-line basic string` + where(" at ")));
        lines[lines.length] = line;
      }
    };
    KEYS = /* @__PURE__ */ Null$1(null);
    commentFor = (key2) => KEYS[key2] || (KEYS[key2] = Symbol$1(key2));
    commentForThis = Symbol$1("this");
    ({ test: includesNewline } = theRegExp(/\r?\n/g));
    getCOMMENT = (table, keyComment) => {
      if (keyComment in table) {
        const comment = table[keyComment];
        if (typeof comment !== "string") {
          throw TypeError$1(`the value of comment must be a string, while "${comment === null ? "null" : typeof comment}" type is found`);
        }
        if (includesNewline(comment)) {
          throw SyntaxError$1(`the value of comment must be a string and can not include newline`);
        }
        return ` #${comment}`;
      }
      return "";
    };
    getComment = (table, key2) => key2 in KEYS ? getCOMMENT(table, KEYS[key2]) : "";
    ({ test: IS_OFFSET$ } = theRegExp(OFFSET$));
    ({ test: IS_EMPTY } = theRegExp(/^\[[\t ]*]/));
    parseKeys = (rest2) => {
      let lineRest = rest2;
      const leadingKeys = [];
      let lastIndex = -1;
      for (; ; ) {
        lineRest || throws(SyntaxError$1(`Empty bare key` + where(" at ")));
        if (lineRest[0] === '"') {
          const index10 = BASIC_STRING_exec_1_endIndex(lineRest);
          KEYS$1.test(leadingKeys[++lastIndex] = BasicString(lineRest.slice(1, index10))) || throws(Error$1(`Key not allowed` + where(" at ")));
          lineRest = lineRest.slice(index10 + 1);
        } else {
          const isQuoted = lineRest[0] === "'";
          const key2 = ((isQuoted ? __LITERAL_KEY_exec : __BARE_KEY_exec)(lineRest) || throws(SyntaxError$1(`Bad ${isQuoted ? "literal string" : "bare"} key` + where(" at "))))[0];
          lineRest = lineRest.slice(key2.length);
          KEYS$1.test(leadingKeys[++lastIndex] = isQuoted ? key2.slice(1, -1) : key2) || throws(Error$1(`Key not allowed` + where(" at ")));
        }
        if (IS_DOT_KEY(lineRest)) {
          lineRest = lineRest.replace(DOT_KEY, "");
        } else {
          break;
        }
      }
      if (disableDigit) {
        const keys2 = rest2.slice(0, -lineRest.length);
        (isAmazing(keys2) || enableNull && keys2 === "null") && throws(SyntaxError$1(`Bad bare key disabled by xOptions.string` + where(" at ")));
      }
      if (disallowEmptyKey) {
        let index10 = lastIndex;
        do {
          leadingKeys[index10] || throws(SyntaxError$1(`Empty key is not allowed before TOML v0.5` + where(", which at ")));
        } while (index10--);
      }
      const finalKey = leadingKeys[lastIndex];
      leadingKeys.length = lastIndex;
      return { leadingKeys, finalKey, lineRest };
    };
    push = (lastArray, lineRest) => {
      if (lineRest[0] === "<") {
        const { 1: tag2 } = { 2: lineRest } = _VALUE_PAIR_exec(lineRest) || throws(SyntaxError$1(`Bad tag ` + where(" at ")));
        collect(tag2, lastArray, null);
        switch (lineRest && lineRest[0]) {
          case ",":
          case "]":
          case "":
          case "#":
            lastArray[lastArray.length] = undefined$1;
            return lineRest;
        }
      }
      switch (lineRest[0]) {
        case "'":
          return assignLiteralString(asStrings(lastArray), lastArray.length, lineRest);
        case '"':
          return assignBasicString(asStrings(lastArray), lastArray.length, lineRest);
        case "{":
          inlineTable || throws(SyntaxError$1(`Inline Table is not allowed before TOML v0.4` + where(", which at ")));
          return equalInlineTable(asTables(lastArray), lastArray.length, lineRest);
        case "[":
          return equalStaticArray(asArrays(lastArray), lastArray.length, lineRest);
      }
      const { 1: literal } = { 2: lineRest } = VALUE_REST_exec(lineRest) || throws(SyntaxError$1(`Bad atom value` + where(" at ")));
      if (literal === "true") {
        asBooleans(lastArray)[lastArray.length] = true;
      } else if (literal === "false") {
        asBooleans(lastArray)[lastArray.length] = false;
      } else if (enableNull && literal === "null") {
        asNulls(lastArray)[lastArray.length] = null;
      } else if (literal.includes(":")) {
        if (literal.includes("-")) {
          if (IS_OFFSET$(literal)) {
            asOffsetDateTimes(lastArray)[lastArray.length] = new OffsetDateTime(literal);
          } else {
            moreDatetime || throws(SyntaxError$1(`Local Date-Time is not allowed before TOML v0.5` + where(", which at ")));
            asLocalDateTimes(lastArray)[lastArray.length] = new LocalDateTime(literal);
          }
        } else {
          moreDatetime || throws(SyntaxError$1(`Local Time is not allowed before TOML v0.5` + where(", which at ")));
          asLocalTimes(lastArray)[lastArray.length] = new LocalTime(literal);
        }
      } else if (literal.indexOf("-") !== literal.lastIndexOf("-") && literal[0] !== "-") {
        moreDatetime || throws(SyntaxError$1(`Local Date is not allowed before TOML v0.5` + where(", which at ")));
        asLocalDates(lastArray)[lastArray.length] = new LocalDate(literal);
      } else {
        literal.includes(".") || literal.includes("n") || (literal.includes("e") || literal.includes("E")) && !literal.startsWith("0x") ? asFloats(lastArray)[lastArray.length] = preserveLiteral ? LiteralObject(literal, Float(literal)) : Float(literal) : asIntegers(lastArray)[lastArray.length] = preserveLiteral ? LiteralObject(literal, Integer(literal)) : Integer(literal);
      }
      return lineRest;
    };
    equalStaticArray = function* (table, finalKey, lineRest) {
      const staticArray = table[finalKey] = newArray(STATICALLY);
      if (IS_EMPTY(lineRest)) {
        beInline(staticArray, lineRest[1] === "]" ? 0 : 3);
        return lineRest.slice(lineRest.indexOf("]")).replace(SYM_WHITESPACE, "");
      }
      const start = new mark("Static Array", lineRest.length);
      let inline = lineRest.startsWith("[ ") || lineRest.startsWith("[	") ? 3 : 0;
      lineRest = lineRest.replace(SYM_WHITESPACE, "");
      while (!lineRest || lineRest[0] === "#") {
        inline = null;
        lineRest = start.must().replace(PRE_WHITESPACE, "");
      }
      if (lineRest[0] === "]") {
        inline === null || beInline(staticArray, inline);
        return lineRest.replace(SYM_WHITESPACE, "");
      }
      for (; ; ) {
        const rest2 = push(staticArray, lineRest);
        lineRest = typeof rest2 === "string" ? rest2 : yield rest2;
        while (!lineRest || lineRest[0] === "#") {
          inline = null;
          lineRest = start.must().replace(PRE_WHITESPACE, "");
        }
        if (lineRest[0] === ",") {
          lineRest = lineRest.replace(SYM_WHITESPACE, "");
          while (!lineRest || lineRest[0] === "#") {
            inline = null;
            lineRest = start.must().replace(PRE_WHITESPACE, "");
          }
          if (lineRest[0] === "]") {
            break;
          }
        } else {
          if (lineRest[0] === "]") {
            break;
          }
          throw throws(SyntaxError$1(`Unexpect character in static array item value` + where(", which is found at ")));
        }
      }
      inline === null || beInline(staticArray, inline);
      return lineRest.replace(SYM_WHITESPACE, "");
    };
    equalInlineTable = function* (table, finalKey, lineRest) {
      const inlineTable2 = table[finalKey] = new Table3(DIRECTLY, INLINE);
      if (allowInlineTableMultilineAndTrailingCommaEvenNoComma) {
        const start = new mark("Inline Table", lineRest.length);
        lineRest = lineRest.replace(SYM_WHITESPACE, "");
        let inline = true;
        for (; ; ) {
          while (!lineRest || lineRest[0] === "#") {
            inline = false;
            lineRest = start.must().replace(PRE_WHITESPACE, "");
          }
          if (lineRest[0] === "}") {
            break;
          }
          const forComment = ForComment(inlineTable2, lineRest);
          const rest2 = assign(forComment);
          lineRest = typeof rest2 === "string" ? rest2 : yield rest2;
          if (lineRest) {
            if (lineRest[0] === "#") {
              if (preserveComment) {
                forComment.table[commentFor(forComment.finalKey)] = lineRest.slice(1);
              }
              inline = false;
              do {
                lineRest = start.must().replace(PRE_WHITESPACE, "");
              } while (!lineRest || lineRest[0] === "#");
            }
          } else {
            inline = false;
            do {
              lineRest = start.must().replace(PRE_WHITESPACE, "");
            } while (!lineRest || lineRest[0] === "#");
          }
          if (lineRest[0] === ",") {
            lineRest = lineRest.replace(SYM_WHITESPACE, "");
          }
        }
        inline || beInline(inlineTable2, false);
      } else {
        lineRest = lineRest.replace(SYM_WHITESPACE, "") || throws(SyntaxError$1(`Inline Table is intended to appear on a single line` + where(", which broken at ")));
        if (lineRest[0] !== "}") {
          for (; ; ) {
            lineRest[0] === "#" && throws(SyntaxError$1(`Inline Table is intended to appear on a single line` + where(", which broken at ")));
            const rest2 = assign(ForComment(inlineTable2, lineRest));
            lineRest = (typeof rest2 === "string" ? rest2 : yield rest2) || throws(SyntaxError$1(`Inline Table is intended to appear on a single line` + where(", which broken at ")));
            if (lineRest[0] === "}") {
              break;
            }
            if (lineRest[0] === ",") {
              lineRest = lineRest.replace(SYM_WHITESPACE, "") || throws(SyntaxError$1(`Inline Table is intended to appear on a single line` + where(", which broken at ")));
              lineRest[0] === "}" && throws(SyntaxError$1(`The last property of an Inline Table can not have a trailing comma` + where(", which was found at ")));
            }
          }
        }
      }
      return lineRest.replace(SYM_WHITESPACE, "");
    };
    ForComment = (lastInlineTable, lineRest) => {
      const { leadingKeys, finalKey, tag: tag2 } = { lineRest } = KEY_VALUE_PAIR_exec_groups(parseKeys(lineRest));
      return { table: prepareInlineTable(lastInlineTable, leadingKeys), finalKey, tag: tag2, lineRest };
    };
    assign = ({ finalKey, tag: tag2, lineRest, table }) => {
      finalKey in table && throws(Error$1(`Duplicate property definition` + where(" at ")));
      if (tag2) {
        collect(tag2, null, table, finalKey);
        switch (lineRest && lineRest[0]) {
          case ",":
          case "}":
          case "":
          case "#":
            table[finalKey] = undefined$1;
            return lineRest;
        }
      }
      switch (lineRest && lineRest[0]) {
        case "'":
          return assignLiteralString(table, finalKey, lineRest);
        case '"':
          return assignBasicString(table, finalKey, lineRest);
        case "{":
          inlineTable || throws(SyntaxError$1(`Inline Table is not allowed before TOML v0.4` + where(", which at ")));
          return equalInlineTable(table, finalKey, lineRest);
        case "[":
          return equalStaticArray(table, finalKey, lineRest);
      }
      const { 1: literal } = { 2: lineRest } = VALUE_REST_exec(lineRest) || throws(SyntaxError$1(`Bad atom value` + where(" at ")));
      if (literal === "true") {
        table[finalKey] = true;
      } else if (literal === "false") {
        table[finalKey] = false;
      } else if (enableNull && literal === "null") {
        table[finalKey] = null;
      } else if (literal.includes(":")) {
        if (literal.includes("-")) {
          if (IS_OFFSET$(literal)) {
            table[finalKey] = new OffsetDateTime(literal);
          } else {
            moreDatetime || throws(SyntaxError$1(`Local Date-Time is not allowed before TOML v0.5` + where(", which at ")));
            table[finalKey] = new LocalDateTime(literal);
          }
        } else {
          moreDatetime || throws(SyntaxError$1(`Local Time is not allowed before TOML v0.5` + where(", which at ")));
          table[finalKey] = new LocalTime(literal);
        }
      } else if (literal.indexOf("-") !== literal.lastIndexOf("-") && literal[0] !== "-") {
        moreDatetime || throws(SyntaxError$1(`Local Date is not allowed before TOML v0.5` + where(", which at ")));
        table[finalKey] = new LocalDate(literal);
      } else {
        table[finalKey] = literal.includes(".") || literal.includes("n") || (literal.includes("e") || literal.includes("E")) && !literal.startsWith("0x") ? preserveLiteral ? LiteralObject(literal, Float(literal)) : Float(literal) : preserveLiteral ? LiteralObject(literal, Integer(literal)) : Integer(literal);
      }
      return lineRest;
    };
    Root2 = () => {
      const rootTable = new Table3();
      let lastSectionTable = rootTable;
      while (rest()) {
        const line = next().replace(PRE_WHITESPACE, "");
        if (line) {
          if (line[0] === "[") {
            const { leadingKeys, finalKey, asArrayItem, tag: tag2, lineRest } = TABLE_DEFINITION_exec_groups(line, parseKeys);
            const table = prepareTable(rootTable, leadingKeys);
            if (lineRest) {
              lineRest[0] === "#" || throws(SyntaxError$1(`Unexpect charachtor after table header` + where(" at ")));
            }
            lastSectionTable = appendTable(table, finalKey, asArrayItem, tag2);
            preserveComment && lineRest && (lastSectionTable[commentForThis] = asArrayItem ? lineRest.slice(1) : table[commentFor(finalKey)] = lineRest.slice(1));
          } else if (line[0] === "#") {
            __CONTROL_CHARACTER_EXCLUDE_test(line) && throws(SyntaxError$1(`Control characters other than Tab are not permitted in comments` + where(", which was found at ")));
          } else {
            const forComment = ForComment(lastSectionTable, line);
            let rest2 = assign(forComment);
            typeof rest2 === "string" || (rest2 = x(rest2));
            if (rest2) {
              rest2[0] === "#" || throws(SyntaxError$1(`Unexpect charachtor after key/value pair` + where(" at ")));
              if (preserveComment) {
                forComment.table[commentFor(forComment.finalKey)] = rest2.slice(1);
              }
            }
          }
        }
      }
      return rootTable;
    };
    MAX_SAFE_INTEGER = Number.MAX_SAFE_INTEGER;
    DATE = Date.prototype;
    valueOf$2 = String.prototype.valueOf;
    isString = /* j-globals: class.isString (internal) */
    /* @__PURE__ */ function() {
      if (apply.bind) {
        var valueOf_apply = apply.bind(valueOf$2);
        return function isString2(value) {
          try {
            valueOf_apply(value);
          } catch (error2) {
            return false;
          }
          return true;
        };
      }
      return function isString2(value) {
        try {
          valueOf$2.apply(value);
        } catch (error2) {
          return false;
        }
        return true;
      };
    }();
    valueOf$1 = Number.prototype.valueOf;
    isNumber = /* j-globals: class.isNumber (internal) */
    /* @__PURE__ */ function() {
      if (apply.bind) {
        var valueOf_apply = apply.bind(valueOf$1);
        return function isNumber2(value) {
          try {
            valueOf_apply(value);
          } catch (error2) {
            return false;
          }
          return true;
        };
      }
      return function isNumber2(value) {
        try {
          valueOf$1.apply(value);
        } catch (error2) {
          return false;
        }
        return true;
      };
    }();
    isBigInt = /* j-globals: class.isBigInt (internal) */
    /* @__PURE__ */ function() {
      if (typeof BigInt === "function") {
        var valueOf_apply = apply.bind(BigInt.prototype.valueOf);
        return function isBigInt2(value) {
          try {
            valueOf_apply(value);
          } catch (error2) {
            return false;
          }
          return true;
        };
      }
      return function isBigInt2() {
        return false;
      };
    }();
    valueOf = BigInt.prototype.valueOf;
    isBoolean = /* j-globals: class.isBoolean (internal) */
    /* @__PURE__ */ function() {
      if (apply.bind) {
        var valueOf_apply = apply.bind(valueOf);
        return function isBoolean2(value) {
          try {
            valueOf_apply(value);
          } catch (error2) {
            return false;
          }
          return true;
        };
      }
      return function isBoolean2(value) {
        try {
          valueOf.apply(value);
        } catch (error2) {
          return false;
        }
        return true;
      };
    }();
    ESCAPED = /* @__PURE__ */ Null$1({
      .../* @__PURE__ */ fromEntries(/* @__PURE__ */ [...Array$1(32)].map((_2, charCode) => [fromCharCode(charCode), "\\u" + charCode.toString(16).toUpperCase().padStart(4, "0")])),
      "\b": "\\b",
      "	": "\\t",
      "\n": "\\n",
      "\f": "\\f",
      "\r": "\\r",
      '"': '\\"',
      '"""': '""\\"',
      "\\": "\\\\",
      "\x7F": "\\u007F"
    });
    ({ test: NEED_BASIC } = theRegExp(/[\x00-\x08\x0A-\x1F'\x7F]/));
    BY_ESCAPE = /[^\x00-\x08\x0A-\x1F"\\\x7F]+|./gs;
    ({ test: NEED_ESCAPE } = theRegExp(/^[\x00-\x08\x0A-\x1F"\\\x7F]/));
    singlelineString = (value) => {
      if (NEED_BASIC(value)) {
        const parts = value.match(BY_ESCAPE);
        let index10 = parts.length;
        do {
          if (NEED_ESCAPE(parts[--index10])) {
            parts[index10] = ESCAPED[parts[index10]];
          }
        } while (index10);
        return `"${parts.join("")}"`;
      }
      return `'${value}'`;
    };
    ({ test: NEED_MULTILINE_BASIC } = theRegExp(/[\x00-\x08\x0A-\x1F\x7F]|'''/));
    ({ test: multilineNeedBasic } = theRegExp(/[\x00-\x08\x0B-\x1F\x7F]|'''/));
    ({ test: REAL_MULTILINE_ESCAPE } = theRegExp(/[\x00-\x08\x0A-\x1F\\\x7F]|"""/));
    ({ test: NEED_MULTILINE_ESCAPE } = theRegExp(/^(?:[\x00-\x08\x0A-\x1F\\\x7F]|""")/));
    Float64Array$1 = Float64Array;
    Uint8Array$1 = Uint8Array;
    _Infinity = -Infinity2;
    ({ test: INTEGER_LIKE } = theRegExp(/^-?\d+$/));
    ensureFloat = (literal) => INTEGER_LIKE(literal) ? literal + ".0" : literal;
    float64Array = new Float64Array$1([NaN$1]);
    uint8Array = new Uint8Array$1(float64Array.buffer);
    NaN_7 = uint8Array[7];
    float = NaN_7 === new Uint8Array$1(new Float64Array$1([-NaN$1]).buffer)[7] ? (value) => value ? value === Infinity2 ? "inf" : value === _Infinity ? "-inf" : ensureFloat("" + value) : value === value ? is(value, 0) ? "0.0" : "-0.0" : "nan" : (value) => value ? value === Infinity2 ? "inf" : value === _Infinity ? "-inf" : ensureFloat("" + value) : value === value ? is(value, 0) ? "0.0" : "-0.0" : (float64Array[0] = value, uint8Array[7]) === NaN_7 ? "nan" : "-nan";
    isDate = /* @__PURE__ */ isPrototypeOf.bind(DATE);
    ({ test: BARE } = theRegExp(/^[\w-]+$/));
    $Key$ = (key2) => BARE(key2) ? key2 : singlelineString(key2);
    FIRST = /[^.]+/;
    literalString = (value) => `'${value}'`;
    $Keys = (keys2) => isAmazing(keys2) ? keys2.replace(FIRST, literalString) : keys2 === "null" ? `'null'` : keys2;
    TOMLSection = class extends Array$1 {
      document;
      constructor(document2) {
        super();
        this.document = document2;
        return this;
      }
      [Symbol$1.toPrimitive]() {
        return this.join(this.document.newline);
      }
      appendNewline() {
        this[this.length] = "";
      }
      set appendLine(source) {
        this[this.length] = source;
      }
      set appendInline(source) {
        this[this.length - 1] += source;
      }
      set appendInlineIf(source) {
        source && (this[this.length - 1] += source);
      }
      ///
      *assignBlock(documentKeys_, sectionKeys_, table, tableKeys) {
        const { document: document2 } = this;
        const { newlineUnderHeader, newlineUnderSectionButPair } = document2;
        const newlineAfterDotted = sectionKeys_ ? document2.newlineUnderPairButDotted : false;
        const newlineAfterPair = sectionKeys_ ? document2.newlineUnderDotted : document2.newlineUnderPair;
        for (const tableKey of tableKeys) {
          const value = table[tableKey];
          const $key$ = $Key$(tableKey);
          const documentKeys = documentKeys_ + $key$;
          if (isArray$1(value)) {
            const { length } = value;
            if (length) {
              let firstItem = value[0];
              if (isSection(firstItem)) {
                const tableHeader = `[[${documentKeys}]]`;
                const documentKeys_2 = documentKeys + ".";
                let index10 = 0;
                let table2 = firstItem;
                for (; ; ) {
                  const section = document2.appendSection();
                  section[0] = tableHeader + getCOMMENT(table2, commentForThis);
                  if (newlineUnderHeader) {
                    section[1] = "";
                    yield section.assignBlock(documentKeys_2, ``, table2, getOwnPropertyNames(table2));
                    newlineUnderSectionButPair && section.length !== 2 && section.appendNewline();
                  } else {
                    yield section.assignBlock(documentKeys_2, ``, table2, getOwnPropertyNames(table2));
                    newlineUnderSectionButPair && section.appendNewline();
                  }
                  if (++index10 === length) {
                    break;
                  }
                  table2 = value[index10];
                  if (!isSection(table2)) {
                    throw TypeError$1(`the first table item marked by Section() means the parent array is an array of tables, which can not include other types or table not marked by Section() any more in the rest items`);
                  }
                }
                continue;
              } else {
                let index10 = 1;
                while (index10 !== length) {
                  if (isSection(value[index10++])) {
                    throw TypeError$1(`if an array is not array of tables, it can not include any table that marked by Section()`);
                  }
                }
              }
            }
          } else {
            if (isSection(value)) {
              const section = document2.appendSection();
              section[0] = `[${documentKeys}]${document2.preferCommentForThis ? getCOMMENT(value, commentForThis) || getComment(table, tableKey) : getComment(table, tableKey) || getCOMMENT(value, commentForThis)}`;
              if (newlineUnderHeader) {
                section[1] = "";
                yield section.assignBlock(documentKeys + ".", ``, value, getOwnPropertyNames(value));
                newlineUnderSectionButPair && section.length !== 2 && section.appendNewline();
              } else {
                yield section.assignBlock(documentKeys + ".", ``, value, getOwnPropertyNames(value));
                newlineUnderSectionButPair && section.appendNewline();
              }
              continue;
            }
          }
          const sectionKeys = sectionKeys_ + $key$;
          this.appendLine = $Keys(sectionKeys) + " = ";
          const valueKeysIfValueIsDottedTable = this.value("", value, true);
          if (valueKeysIfValueIsDottedTable) {
            --this.length;
            yield this.assignBlock(documentKeys + ".", sectionKeys + ".", value, valueKeysIfValueIsDottedTable);
            newlineAfterDotted && this.appendNewline();
          } else {
            this.appendInlineIf = getComment(table, tableKey);
            newlineAfterPair && this.appendNewline();
          }
        }
      }
      value(indent, value, returnValueKeysIfValueIsDottedTable) {
        switch (typeof value) {
          case "object":
            if (value === null) {
              if (this.document.nullDisabled) {
                throw TypeError$1(`toml can not stringify "null" type value without truthy options.xNull`);
              }
              this.appendInline = "null";
              break;
            }
            const inlineMode = ofInline(value);
            if (isArray$1(value)) {
              if (inlineMode === undefined$1) {
                this.staticArray(indent, value);
              } else {
                const { $singlelineArray = inlineMode } = this.document;
                this.singlelineArray(indent, value, $singlelineArray);
              }
              break;
            }
            if (inlineMode !== undefined$1) {
              inlineMode || this.document.multilineTableDisabled ? this.inlineTable(indent, value) : this.multilineTable(indent, value, this.document.multilineTableComma);
              break;
            }
            if (isDate(value)) {
              this.appendInline = value.toISOString().replace("T", this.document.T).replace("Z", this.document.Z);
              break;
            }
            if (_literal in value) {
              const literal = value[_literal];
              if (typeof literal === "string") {
                this.appendInline = literal;
              } else if (isArray$1(literal)) {
                const { length } = literal;
                if (length) {
                  this.appendInline = literal[0];
                  let index10 = 1;
                  while (index10 !== length) {
                    this.appendLine = literal[index10++];
                  }
                } else {
                  throw TypeError$1(`literal value is broken`);
                }
              } else {
                throw TypeError$1(`literal value is broken`);
              }
              break;
            }
            if (isString(value)) {
              throw TypeError$1(`TOML.stringify refuse to handle [object String]`);
            }
            if (isNumber(value)) {
              throw TypeError$1(`TOML.stringify refuse to handle [object Number]`);
            }
            if (isBigInt(value)) {
              throw TypeError$1(`TOML.stringify refuse to handle [object BigInt]`);
            }
            if (isBoolean(value)) {
              throw TypeError$1(`TOML.stringify refuse to handle [object Boolean]`);
            }
            if (returnValueKeysIfValueIsDottedTable) {
              const keys2 = getOwnPropertyNames(value);
              if (keys2.length) {
                return keys2;
              }
              this.appendInline = "{ }";
            } else {
              this.inlineTable(indent, value);
            }
            break;
          case "bigint":
            this.appendInline = "" + value;
            break;
          case "number":
            this.appendInline = this.document.asInteger(value) ? is(value, -0) ? "-0" : "" + value : float(value);
            break;
          case "string":
            this.appendInline = singlelineString(value);
            break;
          case "boolean":
            this.appendInline = value ? "true" : "false";
            break;
          default:
            throw TypeError$1(`toml can not stringify "${typeof value}" type value`);
        }
        return null;
      }
      singlelineArray(indent, staticArray, inlineMode) {
        const { length } = staticArray;
        if (length) {
          this.appendInline = inlineMode & 2 ? "[ " : "[";
          this.value(indent, staticArray[0], false);
          let index10 = 1;
          while (index10 !== length) {
            this.appendInline = ", ";
            this.value(indent, staticArray[index10++], false);
          }
          this.appendInline = inlineMode & 2 ? " ]" : "]";
        } else {
          this.appendInline = inlineMode & 1 ? "[ ]" : "[]";
        }
      }
      staticArray(indent, staticArray) {
        this.appendInline = "[";
        const indent_ = indent + this.document.indent;
        const { length } = staticArray;
        let index10 = 0;
        while (index10 !== length) {
          this.appendLine = indent_;
          this.value(indent_, staticArray[index10++], false);
          this.appendInline = ",";
        }
        this.appendLine = indent + "]";
      }
      inlineTable(indent, inlineTable2) {
        const keys2 = getOwnPropertyNames(inlineTable2);
        if (keys2.length) {
          this.appendInline = "{ ";
          this.assignInline(indent, inlineTable2, ``, keys2);
          this[this.length - 1] = this[this.length - 1].slice(0, -2) + " }";
        } else {
          this.appendInline = "{ }";
        }
      }
      multilineTable(indent, inlineTable2, comma) {
        this.appendInline = "{";
        this.assignMultiline(indent, inlineTable2, ``, getOwnPropertyNames(inlineTable2), comma);
        this.appendLine = indent + "}";
      }
      assignInline(indent, inlineTable2, keys_, keys2) {
        for (const key2 of keys2) {
          const value = inlineTable2[key2];
          const keys3 = keys_ + $Key$(key2);
          const before_value = this.appendInline = $Keys(keys3) + " = ";
          const valueKeysIfValueIsDottedTable = this.value(indent, value, true);
          if (valueKeysIfValueIsDottedTable) {
            this[this.length - 1] = this[this.length - 1].slice(0, -before_value.length);
            this.assignInline(indent, value, keys3 + ".", valueKeysIfValueIsDottedTable);
          } else {
            this.appendInline = ", ";
          }
        }
      }
      assignMultiline(indent, inlineTable2, keys_, keys2, comma) {
        const indent_ = indent + this.document.indent;
        for (const key2 of keys2) {
          const value = inlineTable2[key2];
          const keys3 = keys_ + $Key$(key2);
          this.appendLine = indent_ + $Keys(keys3) + " = ";
          const valueKeysIfValueIsDottedTable = this.value(indent_, value, true);
          if (valueKeysIfValueIsDottedTable) {
            --this.length;
            this.assignMultiline(indent, value, keys3 + ".", valueKeysIfValueIsDottedTable, comma);
          } else {
            comma ? this.appendInline = "," + getComment(inlineTable2, key2) : this.appendInlineIf = getComment(inlineTable2, key2);
          }
        }
      }
    };
    ({ test: IS_INDENT } = theRegExp(/^[\t ]*$/));
    linesFromStringify = new WeakSet$1();
    isLinesFromStringify = /* @__PURE__ */ has.bind(linesFromStringify);
    textDecoder = /* @__PURE__ */ new TextDecoder$1("utf-8", Null$1({ fatal: true, ignoreBOM: false }));
    binary2string = (arrayBufferLike) => {
      if (isView(arrayBufferLike) ? arrayBufferLike.length !== arrayBufferLike.byteLength : !isArrayBuffer(arrayBufferLike)) {
        throw TypeError$1(`only Uint8Array or ArrayBuffer is acceptable`);
      }
      try {
        return textDecoder.decode(arrayBufferLike);
      } catch {
        throw Error$1(`A TOML doc must be a (ful-scalar) valid UTF-8 file, without any unknown code point.`);
      }
    };
    isBinaryLike = (value) => "byteLength" in value;
    ({ test: includesNonScalar } = theRegExp(/[\uD800-\uDFFF]/u));
    assertFulScalar = (string) => {
      if (clearRegExp$1(includesNonScalar(string))) {
        throw Error$1(`A TOML doc must be a (ful-scalar) valid UTF-8 file, without any uncoupled UCS-4 character code.`);
      }
    };
    holding = false;
    parse3 = (source, specificationVersion, multilineStringJoiner, bigint, x3, argsMode) => {
      let sourcePath2 = "";
      if (typeof source === "object" && source) {
        if (isArray$1(source)) {
          throw TypeError$1(isLinesFromStringify(source) ? `TOML.parse(array from TOML.stringify(,{newline?}))` : `TOML.parse(array)`);
        } else if (isBinaryLike(source)) {
          source = binary2string(source);
        } else {
          sourcePath2 = source.path;
          if (typeof sourcePath2 !== "string") {
            throw TypeError$1(`TOML.parse(source.path)`);
          }
          const { data, require: req = typeof __require22 === "function" ? __require22 : undefined$1 } = source;
          if (req) {
            const { resolve: resolve2 } = req;
            if (resolve2 != null) {
              const { paths } = resolve2;
              if (paths != null) {
                const ret = apply$1(paths, resolve2, [""]);
                if (ret != null) {
                  const val = ret[0];
                  if (val != null) {
                    const dirname_ = val.replace(/node_modules$/, "");
                    if (dirname_) {
                      sourcePath2 = req("path").resolve(dirname_, sourcePath2);
                      if (typeof sourcePath2 !== "string") {
                        throw TypeError$1(`TOML.parse(source.require('path').resolve)`);
                      }
                    }
                  }
                }
              }
            }
            if (data === undefined$1) {
              const data2 = req("fs").readFileSync(sourcePath2);
              if (typeof data2 === "object" && data2 && isBinaryLike(data2)) {
                source = binary2string(data2);
              } else {
                throw TypeError$1(`TOML.parse(source.require('fs').readFileSync)`);
              }
            } else if (typeof data === "string") {
              assertFulScalar(source = data);
            } else if (typeof data === "object" && data && isBinaryLike(data)) {
              source = binary2string(data);
            } else {
              throw TypeError$1(`TOML.parse(source.data)`);
            }
          } else {
            if (data === undefined$1) {
              throw TypeError$1(`TOML.parse(source.data|source.require)`);
            } else if (typeof data === "string") {
              assertFulScalar(source = data);
            } else if (typeof data === "object" && data && isBinaryLike(data)) {
              source = binary2string(data);
            } else {
              throw TypeError$1(`TOML.parse(source.data)`);
            }
          }
        }
      } else if (typeof source === "string") {
        assertFulScalar(source);
      } else {
        throw TypeError$1(`TOML.parse(source)`);
      }
      let joiner;
      let keys2;
      if (typeof multilineStringJoiner === "object" && multilineStringJoiner) {
        if (bigint !== undefined$1 || x3 !== undefined$1) {
          throw TypeError$1(`options mode ? args mode`);
        }
        joiner = multilineStringJoiner.joiner;
        bigint = multilineStringJoiner.bigint;
        keys2 = multilineStringJoiner.keys;
        x3 = multilineStringJoiner.x;
        argsMode = "";
      } else {
        joiner = multilineStringJoiner;
      }
      let rootTable;
      let process;
      if (holding) {
        throw Error$1(`parsing during parsing.`);
      }
      holding = true;
      try {
        use(specificationVersion, joiner, bigint, keys2, x3, argsMode);
        todo(source, sourcePath2);
        source && source[0] === "\uFEFF" && throws(TypeError$1(`TOML content (string) should not start with BOM (U+FEFF)` + where(" at ")));
        rootTable = Root2();
        process = Process();
      } finally {
        done();
        clear();
        holding = false;
        clearRegExp$1();
      }
      process && process();
      return rootTable;
    };
    parse$1 = /* @__PURE__ */ assign$1(
      (source, specificationVersion, multilineStringJoiner, useBigInt, xOptions) => typeof specificationVersion === "number" ? parse3(source, specificationVersion, multilineStringJoiner, useBigInt, xOptions, ",,") : parse3(source, 1, specificationVersion, multilineStringJoiner, useBigInt, ","),
      {
        "1.0": (source, multilineStringJoiner, useBigInt, xOptions) => parse3(source, 0.1, multilineStringJoiner, useBigInt, xOptions, ","),
        1: (source, multilineStringJoiner, useBigInt, xOptions) => parse3(source, 1, multilineStringJoiner, useBigInt, xOptions, ","),
        0.5: (source, multilineStringJoiner, useBigInt, xOptions) => parse3(source, 0.5, multilineStringJoiner, useBigInt, xOptions, ","),
        0.4: (source, multilineStringJoiner, useBigInt, xOptions) => parse3(source, 0.4, multilineStringJoiner, useBigInt, xOptions, ","),
        0.3: (source, multilineStringJoiner, useBigInt, xOptions) => parse3(source, 0.3, multilineStringJoiner, useBigInt, xOptions, ","),
        0.2: (source, multilineStringJoiner, useBigInt, xOptions) => parse3(source, 0.2, multilineStringJoiner, useBigInt, xOptions, ","),
        0.1: (source, multilineStringJoiner, useBigInt, xOptions) => parse3(source, 0.1, multilineStringJoiner, useBigInt, xOptions, ",")
      }
    );
  }
});
function dataURIToBlob(dataURI) {
  const [info, data] = dataURI.split(",");
  const mimeString = info.split(";")[0].split(":")[1];
  return new Blob([Uint8Array.from(atob(data), (c2) => c2.charCodeAt(0))], {
    type: mimeString
  });
}
async function getThumbnail(fuiz) {
  return await fuiz.slides.reduce(
    async (m, s3) => {
      const prev = await m;
      if (prev)
        return prev;
      const media = s3.MultipleChoice.media;
      if (!media)
        return void 0;
      if ("Corkboard" in media.Image) {
        const image = await bring(PUBLIC_CORKBOARD_URL + "/get/" + media.Image.Corkboard.id, {
          method: "GET"
        });
        if (!image?.ok)
          return void 0;
        const blob = await image.blob();
        const formData = new FormData();
        formData.append("image", blob);
        const thumbnail = await bring(PUBLIC_CORKBOARD_URL + "/thumbnail", {
          method: "POST",
          body: formData
        });
        if (!thumbnail?.ok)
          return void 0;
        return { thumbnail: await thumbnail.arrayBuffer(), alt: media.Image.Corkboard.alt };
      } else if ("Base64" in media.Image) {
        const blob = dataURIToBlob(media.Image.Base64.data);
        const formData = new FormData();
        formData.append("image", blob);
        const thumbnail = await bring(PUBLIC_CORKBOARD_URL + "/thumbnail", {
          method: "POST",
          body: formData
        });
        if (!thumbnail?.ok)
          return void 0;
        return { thumbnail: await thumbnail.arrayBuffer(), alt: media.Image.Base64.alt };
      } else {
        return void 0;
      }
    },
    (async () => void 0)()
  );
}
function encodeAsDataURL(array2) {
  let binary = "";
  for (const byte of new Uint8Array(array2)) {
    binary += String.fromCharCode(byte);
  }
  return "data:image/png;base64," + btoa(binary);
}
function fixPublish(p) {
  return {
    ...p,
    thumbnail: p.thumbnail ? encodeAsDataURL(p.thumbnail) : null,
    tags: p.tags.split(" ~ "),
    published: new Date(p.published),
    last_updated: new Date(p.last_updated),
    language: p.language
  };
}
var init_serverOnlyUtils = __esm({
  ".svelte-kit/output/server/chunks/serverOnlyUtils.js"() {
    init_util();
    init_public();
  }
});
var page_server_ts_exports = {};
__export(page_server_ts_exports, {
  actions: () => actions,
  load: () => load2,
  prerender: () => prerender4
});
async function createFileInGit(filePath, fileContent) {
  await fetch(
    `https://gitlab.com/api/v4/projects/${private_env.GITLAB_PROJECT_ID}/repository/files/${encodeURIComponent(filePath)}`,
    {
      method: "POST",
      headers: {
        "PRIVATE-TOKEN": private_env.GITLAB_PERSONAL_API,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        branch: "main",
        content: fileContent,
        commit_message: `add ${filePath}`
      })
    }
  );
  return `${private_env.GITLAB_PUBLIC_LINK}/-/raw/main/${filePath}`;
}
async function updateFileInGit(filePath, fileContent) {
  await fetch(
    `https://gitlab.com/api/v4/projects/${private_env.GITLAB_PROJECT_ID}/repository/files/${encodeURIComponent(filePath)}`,
    {
      method: "PUT",
      headers: {
        "PRIVATE-TOKEN": private_env.GITLAB_PERSONAL_API,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        branch: "main",
        content: fileContent,
        commit_message: `update ${filePath}`
      })
    }
  );
  return `${private_env.GITLAB_PUBLIC_LINK}/-/raw/main/${filePath}`;
}
async function denyWithReason(r2Key, reason2, by, platform) {
  await platform?.env.DATABASE.prepare(`DELETE FROM pending_submissions WHERE r2_key = ?1`).bind(r2Key).run();
  await platform?.env.DATABASE.prepare(
    `INSERT INTO denied_submissions (r2_key, denied_by, reason) VALUES (?1, ?2, ?3)`
  ).bind(r2Key, by, reason2).run();
}
async function loadStorage(key2, platform) {
  return await platform?.env.BUCKET.get(key2) || void 0;
}
var prerender4;
var load2;
var actions;
var init_page_server_ts = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/admin/_page.server.ts.js"() {
    init_chunks();
    init_j_toml();
    init_shared_server();
    init_serverOnlyUtils();
    prerender4 = false;
    load2 = async ({ request, platform }) => {
      const email = request.headers.get("Cf-Access-Authenticated-User-Email");
      if (!email) {
        return error(400, "not allowed");
      }
      for (; ; ) {
        await platform?.env.DATABASE.prepare(
          `UPDATE pending_submissions SET assigned = ?1 WHERE assigned IS NULL OR assigned = ?1 ORDER BY assigned NULLS LAST LIMIT 1`
        ).bind(email).run();
        const {
          r2_key: r2Key,
          desired_id: desiredId
        } = await platform?.env.DATABASE.prepare(
          `SELECT * FROM pending_submissions WHERE assigned = ?1 LIMIT 1`
        ).bind(email).first() || { r2_key: void 0, desired_id: void 0 };
        if (!r2Key || !desiredId)
          return { fuiz: void 0 };
        try {
          const fuizBody = await loadStorage(r2Key, platform);
          if (!fuizBody) {
            throw new Error("Fuiz not found in R2 storage");
          }
          const fuizBuf = await fuizBody.arrayBuffer();
          const { author: author2, tags: tags2, config, language: language2 } = await parse$1(fuizBuf, {
            bigint: false
          });
          const previousFuiz = desiredId === r2Key ? void 0 : await (async () => {
            const fuizBody2 = await loadStorage(desiredId, platform);
            if (!fuizBody2) {
              throw new Error("Fuiz not found in R2 storage");
            }
            const fuizBuf2 = await fuizBody2.arrayBuffer();
            const { author: author22, tags: tags22, config: config2, language: language22 } = await parse$1(fuizBuf2, {
              bigint: false
            });
            return { author: author22, tags: tags22, config: structuredClone(config2), language: language22 };
          })();
          return {
            previous_fuiz: previousFuiz,
            fuiz: {
              author: author2,
              tags: tags2,
              config: structuredClone(config),
              language: language2
            }
          };
        } catch (err) {
          let message = "Unknown Error";
          if (err instanceof Error)
            message = err.message;
          await denyWithReason(r2Key, message, "System", platform);
          continue;
        }
      }
    };
    actions = {
      approve: async ({ request, platform }) => {
        const email = request.headers.get("Cf-Access-Authenticated-User-Email");
        if (!email)
          return fail(400, { missing: true });
        const {
          r2_key: r2Key,
          desired_id: desiredId
        } = await platform?.env.DATABASE.prepare(
          `DELETE FROM pending_submissions WHERE assigned = ?1 RETURNING r2_key, desired_id`
        ).bind(email).first() || {
          r2_key: void 0,
          desired_id: void 0
        };
        if (!r2Key || !desiredId)
          return fail(400, { missing: true });
        const fuizBody = await loadStorage(r2Key, platform);
        if (!fuizBody)
          return fail(400, { missing: true });
        const fuizText = await fuizBody.text();
        const { author: author2, tags: tags2, config, language: language2 } = parse$1(fuizText, { bigint: false });
        if (desiredId != r2Key) {
          await platform?.env.BUCKET.delete(desiredId);
          await platform?.env.BUCKET.put(desiredId, fuizText);
          await platform?.env.BUCKET.delete(r2Key);
          const gitUrl = await updateFileInGit(desiredId + ".toml", fuizText);
          const { thumbnail, alt } = await getThumbnail(config) || {
            thumbnail: null,
            alt: null
          };
          await platform?.env.DATABASE.prepare(
            `UPDATE approved_submissions
					SET title = ?1,
						author = ?2,
						public_url = ?3,
						approved_by = ?4,
						tags = ?5,
						slides_count = ?6,
						thumbnail = ?7,
						alt = ?8,
						last_updated = ?9,
						language = ?10
					WHERE
						r2_key = ?11`
          ).bind(
            config.title,
            author2,
            gitUrl,
            email,
            tags2.join(" ~ "),
            config.slides.length,
            thumbnail,
            alt,
            Date.now(),
            language2,
            desiredId
          ).run();
        } else {
          const gitUrl = await createFileInGit(r2Key + ".toml", fuizText);
          const { thumbnail, alt } = await getThumbnail(config) || {
            thumbnail: null,
            alt: null
          };
          await platform?.env.DATABASE.prepare(
            `INSERT INTO approved_submissions
						(title, author, published, public_url, r2_key, approved_by, tags, slides_count, thumbnail, alt, played_count, last_updated, language)
				VALUES	(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13)`
          ).bind(
            config.title,
            author2,
            Date.now(),
            gitUrl,
            r2Key,
            email,
            tags2.join(" ~ "),
            config.slides.length,
            thumbnail,
            alt,
            0,
            Date.now(),
            language2
          ).run();
        }
        return { success: true };
      },
      reject: async ({ request, platform }) => {
        const email = request.headers.get("Cf-Access-Authenticated-User-Email");
        if (!email)
          return fail(400, { missing: true });
        const reason2 = (await request.formData()).get("reason")?.toString() || null;
        const { r2_key: r2Key } = await platform?.env.DATABASE.prepare(
          `DELETE FROM pending_submissions WHERE assigned = ?1 RETURNING r2_key`
        ).bind(email).first() || {
          r2_key: void 0
        };
        if (!r2Key)
          return fail(400, { missing: true });
        await platform?.env.DATABASE.prepare(
          `INSERT INTO denied_submissions (r2_key, denied_by, reason) VALUES (?1, ?2, ?3)`
        ).bind(r2Key, email, reason2).run();
        return { success: true };
      }
    };
  }
});
var create$2;
var open_source$2;
var missing_fuiz$2;
var nothing$2;
var slides_count$2;
var image_alt$2;
var question_text$2;
var time_limit$2;
var fuiz_title$2;
var choose_local$2;
var correct$2;
var wrong$2;
var error_alt$2;
var community$2;
var heart$2;
var diamond$2;
var spade$2;
var club$2;
var length_too_short$2;
var language$2;
var this_replaces$2;
var approve$2;
var reason$2;
var reject$2;
var author$2;
var tags$2;
var multiple_choice$2;
var answers$2;
var time_before_answers$2;
var publish_title$2;
var publish_desc$2;
var request_approved$2;
var request_denied$2;
var request_publish$2;
var request_update$2;
var add_tag$2;
var tag$2;
var unknown$2;
var author_played_times$2;
var fallback$2;
var played_times$2;
var import_fuiz$2;
var recently_published$2;
var most_published$2;
var create$12;
var open_source$1;
var missing_fuiz$1;
var nothing$1;
var slides_count$1;
var image_alt$1;
var question_text$1;
var time_limit$1;
var fuiz_title$1;
var choose_local$1;
var correct$1;
var wrong$1;
var error_alt$1;
var community$1;
var heart$1;
var diamond$1;
var spade$1;
var club$1;
var length_too_short$1;
var language$1;
var this_replaces$1;
var approve$1;
var reason$1;
var reject$1;
var author$1;
var tags$1;
var multiple_choice$1;
var answers$1;
var time_before_answers$1;
var publish_title$1;
var publish_desc$1;
var request_approved$1;
var request_denied$1;
var request_publish$1;
var request_update$1;
var add_tag$1;
var tag$1;
var unknown$1;
var author_played_times$1;
var fallback$1;
var played_times$1;
var import_fuiz$1;
var recently_published$1;
var most_published$1;
var create2;
var open_source;
var missing_fuiz;
var nothing;
var slides_count;
var image_alt;
var question_text;
var time_limit;
var fuiz_title;
var choose_local;
var correct;
var wrong;
var error_alt;
var community;
var heart;
var diamond;
var spade;
var club;
var length_too_short;
var language;
var this_replaces;
var approve;
var reason;
var reject;
var author;
var tags;
var multiple_choice;
var answers;
var time_before_answers;
var publish_title;
var publish_desc;
var request_approved;
var request_denied;
var request_publish;
var request_update;
var add_tag;
var tag;
var unknown;
var author_played_times;
var fallback;
var played_times;
var import_fuiz;
var recently_published;
var most_published;
var init_LanguageSwitcher_svelte_svelte_type_style_lang = __esm({
  ".svelte-kit/output/server/chunks/LanguageSwitcher.svelte_svelte_type_style_lang.js"() {
    init_runtime();
    create$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Create`;
    open_source$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Open Source`;
    missing_fuiz$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `o no! cannot find this fuiz!`;
    nothing$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Nothing`;
    slides_count$2 = /* @__NO_SIDE_EFFECTS__ */
    (params) => `${params.count} slides`;
    image_alt$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Image Alt`;
    question_text$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Question Text`;
    time_limit$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Time Limit`;
    fuiz_title$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Fuiz Title`;
    choose_local$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Choose from Local Fuizzes:`;
    correct$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Correct`;
    wrong$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Wrong`;
    error_alt$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Error`;
    community$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Community Made`;
    heart$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `heart`;
    diamond$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `diamond`;
    spade$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `spade`;
    club$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `club`;
    length_too_short$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `length is too short`;
    language$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Language`;
    this_replaces$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `This replaces:`;
    approve$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Approve`;
    reason$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Reason`;
    reject$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Reject`;
    author$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Author`;
    tags$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Tags`;
    multiple_choice$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Multiple Choice`;
    answers$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Answers`;
    time_before_answers$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Time Before Answers`;
    publish_title$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Publish your Fuiz`;
    publish_desc$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Share your created Fuiz with the rest of the world`;
    request_approved$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Your request has been approved. You can request to update it below.`;
    request_denied$2 = /* @__NO_SIDE_EFFECTS__ */
    (params) => `Your request has been denied with reason \`${params.reasonState}\`. You can try again below.`;
    request_publish$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Request Publish`;
    request_update$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Request Update`;
    add_tag$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Add Tag`;
    tag$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Tag`;
    unknown$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Unknown`;
    author_played_times$2 = /* @__NO_SIDE_EFFECTS__ */
    (params) => `${params.author} \u2022 played ${params.times} times`;
    fallback$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Fallback`;
    played_times$2 = /* @__NO_SIDE_EFFECTS__ */
    (params) => `Played ${params.times} times`;
    import_fuiz$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Import`;
    recently_published$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Recently Published`;
    most_published$2 = /* @__NO_SIDE_EFFECTS__ */
    () => `Most Played`;
    create$12 = /* @__NO_SIDE_EFFECTS__ */
    () => `T\u1EA1o`;
    open_source$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `M\xE3 ngu\u1ED3n m\u1EDF`;
    missing_fuiz$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `o no! Kh\xF4ng th\u1EC3 t\xECm th\u1EA5y Quiz!!`;
    nothing$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Khong l\xE0m g\xEC`;
    slides_count$1 = /* @__NO_SIDE_EFFECTS__ */
    (params) => `${params.count} slides`;
    image_alt$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Image Alt`;
    question_text$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `C\xE2u h\u1ECFi`;
    time_limit$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Gi\u1EDBi h\u1EA1n th\u1EDDi gian`;
    fuiz_title$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Ti\xEAu \u0111\u1EC1 Quiz`;
    choose_local$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Choose from Local Fuizzes:`;
    correct$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Ch\xEDnh x\xE1c`;
    wrong$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Sai`;
    error_alt$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Error`;
    community$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Community Made`;
    heart$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `heart`;
    diamond$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `diamond`;
    spade$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `spade`;
    club$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `club`;
    length_too_short$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `\u0111\u1ED9 d\xE0i qu\xE1 ng\u1EAFn`;
    language$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Ng\xF4n ng\u1EEF`;
    this_replaces$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `This replaces:`;
    approve$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Ch\u1EA5p nh\u1EADn`;
    reason$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `L\xFD do`;
    reject$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `T\u1EEB ch\u1ED1i`;
    author$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `T\xE1c gi\u1EA3`;
    tags$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Tags`;
    multiple_choice$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Multiple Choice`;
    answers$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Answers`;
    time_before_answers$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Time Before Answers`;
    publish_title$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Publish your Fuiz`;
    publish_desc$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Share your created Fuiz with the rest of the world`;
    request_approved$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Your request has been approved. You can request to update it below.`;
    request_denied$1 = /* @__NO_SIDE_EFFECTS__ */
    (params) => `Your request has been denied with reason \`${params.reasonState}\`. You can try again below.`;
    request_publish$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Request Publish`;
    request_update$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Request Update`;
    add_tag$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Add Tag`;
    tag$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Tag`;
    unknown$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Unknown`;
    author_played_times$1 = /* @__NO_SIDE_EFFECTS__ */
    (params) => `${params.author} \u2022 ch\u01A1i ${params.times} l\u1EA7n`;
    fallback$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Fallback`;
    played_times$1 = /* @__NO_SIDE_EFFECTS__ */
    (params) => `Ch\u01A1i ${params.times} l\u1EA7n`;
    import_fuiz$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Import`;
    recently_published$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Recently Published`;
    most_published$1 = /* @__NO_SIDE_EFFECTS__ */
    () => `Most Played`;
    create2 = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: create$2,
        vi: create$12
      }[options3.languageTag ?? languageTag()]();
    };
    open_source = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: open_source$2,
        vi: open_source$1
      }[options3.languageTag ?? languageTag()]();
    };
    missing_fuiz = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: missing_fuiz$2,
        vi: missing_fuiz$1
      }[options3.languageTag ?? languageTag()]();
    };
    nothing = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: nothing$2,
        vi: nothing$1
      }[options3.languageTag ?? languageTag()]();
    };
    slides_count = /* @__NO_SIDE_EFFECTS__ */
    (params, options3 = {}) => {
      return {
        en: slides_count$2,
        vi: slides_count$1
      }[options3.languageTag ?? languageTag()](params);
    };
    image_alt = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: image_alt$2,
        vi: image_alt$1
      }[options3.languageTag ?? languageTag()]();
    };
    question_text = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: question_text$2,
        vi: question_text$1
      }[options3.languageTag ?? languageTag()]();
    };
    time_limit = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: time_limit$2,
        vi: time_limit$1
      }[options3.languageTag ?? languageTag()]();
    };
    fuiz_title = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: fuiz_title$2,
        vi: fuiz_title$1
      }[options3.languageTag ?? languageTag()]();
    };
    choose_local = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: choose_local$2,
        vi: choose_local$1
      }[options3.languageTag ?? languageTag()]();
    };
    correct = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: correct$2,
        vi: correct$1
      }[options3.languageTag ?? languageTag()]();
    };
    wrong = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: wrong$2,
        vi: wrong$1
      }[options3.languageTag ?? languageTag()]();
    };
    error_alt = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: error_alt$2,
        vi: error_alt$1
      }[options3.languageTag ?? languageTag()]();
    };
    community = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: community$2,
        vi: community$1
      }[options3.languageTag ?? languageTag()]();
    };
    heart = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: heart$2,
        vi: heart$1
      }[options3.languageTag ?? languageTag()]();
    };
    diamond = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: diamond$2,
        vi: diamond$1
      }[options3.languageTag ?? languageTag()]();
    };
    spade = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: spade$2,
        vi: spade$1
      }[options3.languageTag ?? languageTag()]();
    };
    club = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: club$2,
        vi: club$1
      }[options3.languageTag ?? languageTag()]();
    };
    length_too_short = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: length_too_short$2,
        vi: length_too_short$1
      }[options3.languageTag ?? languageTag()]();
    };
    language = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: language$2,
        vi: language$1
      }[options3.languageTag ?? languageTag()]();
    };
    this_replaces = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: this_replaces$2,
        vi: this_replaces$1
      }[options3.languageTag ?? languageTag()]();
    };
    approve = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: approve$2,
        vi: approve$1
      }[options3.languageTag ?? languageTag()]();
    };
    reason = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: reason$2,
        vi: reason$1
      }[options3.languageTag ?? languageTag()]();
    };
    reject = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: reject$2,
        vi: reject$1
      }[options3.languageTag ?? languageTag()]();
    };
    author = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: author$2,
        vi: author$1
      }[options3.languageTag ?? languageTag()]();
    };
    tags = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: tags$2,
        vi: tags$1
      }[options3.languageTag ?? languageTag()]();
    };
    multiple_choice = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: multiple_choice$2,
        vi: multiple_choice$1
      }[options3.languageTag ?? languageTag()]();
    };
    answers = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: answers$2,
        vi: answers$1
      }[options3.languageTag ?? languageTag()]();
    };
    time_before_answers = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: time_before_answers$2,
        vi: time_before_answers$1
      }[options3.languageTag ?? languageTag()]();
    };
    publish_title = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: publish_title$2,
        vi: publish_title$1
      }[options3.languageTag ?? languageTag()]();
    };
    publish_desc = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: publish_desc$2,
        vi: publish_desc$1
      }[options3.languageTag ?? languageTag()]();
    };
    request_approved = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: request_approved$2,
        vi: request_approved$1
      }[options3.languageTag ?? languageTag()]();
    };
    request_denied = /* @__NO_SIDE_EFFECTS__ */
    (params, options3 = {}) => {
      return {
        en: request_denied$2,
        vi: request_denied$1
      }[options3.languageTag ?? languageTag()](params);
    };
    request_publish = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: request_publish$2,
        vi: request_publish$1
      }[options3.languageTag ?? languageTag()]();
    };
    request_update = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: request_update$2,
        vi: request_update$1
      }[options3.languageTag ?? languageTag()]();
    };
    add_tag = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: add_tag$2,
        vi: add_tag$1
      }[options3.languageTag ?? languageTag()]();
    };
    tag = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: tag$2,
        vi: tag$1
      }[options3.languageTag ?? languageTag()]();
    };
    unknown = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: unknown$2,
        vi: unknown$1
      }[options3.languageTag ?? languageTag()]();
    };
    author_played_times = /* @__NO_SIDE_EFFECTS__ */
    (params, options3 = {}) => {
      return {
        en: author_played_times$2,
        vi: author_played_times$1
      }[options3.languageTag ?? languageTag()](params);
    };
    fallback = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: fallback$2,
        vi: fallback$1
      }[options3.languageTag ?? languageTag()]();
    };
    played_times = /* @__NO_SIDE_EFFECTS__ */
    (params, options3 = {}) => {
      return {
        en: played_times$2,
        vi: played_times$1
      }[options3.languageTag ?? languageTag()](params);
    };
    import_fuiz = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: import_fuiz$2,
        vi: import_fuiz$1
      }[options3.languageTag ?? languageTag()]();
    };
    recently_published = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: recently_published$2,
        vi: recently_published$1
      }[options3.languageTag ?? languageTag()]();
    };
    most_published = /* @__NO_SIDE_EFFECTS__ */
    (params = {}, options3 = {}) => {
      return {
        en: most_published$2,
        vi: most_published$1
      }[options3.languageTag ?? languageTag()]();
    };
  }
});
var css2;
var FancyButtonBase;
var FancyButton;
var init_FancyButton = __esm({
  ".svelte-kit/output/server/chunks/FancyButton.js"() {
    init_ssr();
    css2 = {
      code: "button.svelte-e2dftg .front.svelte-e2dftg{transform:translateY(-0.15em);transition:transform 150ms, background-color 300ms linear, border-color 300ms linear}button.svelte-e2dftg:active:not(:disabled) .front.svelte-e2dftg{transform:translateY(0em)}button.svelte-e2dftg:where(:hover, :focus) .front.svelte-e2dftg{transform:translateY(-0.3em)}",
      map: null
    };
    FancyButtonBase = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { foregroundColor } = $$props;
      let { backgroundColor } = $$props;
      let { backgroundDeepColor } = $$props;
      let { disabled = false } = $$props;
      let { active = true } = $$props;
      let { type = void 0 } = $$props;
      let { action } = $$props;
      if ($$props.foregroundColor === void 0 && $$bindings.foregroundColor && foregroundColor !== void 0)
        $$bindings.foregroundColor(foregroundColor);
      if ($$props.backgroundColor === void 0 && $$bindings.backgroundColor && backgroundColor !== void 0)
        $$bindings.backgroundColor(backgroundColor);
      if ($$props.backgroundDeepColor === void 0 && $$bindings.backgroundDeepColor && backgroundDeepColor !== void 0)
        $$bindings.backgroundDeepColor(backgroundDeepColor);
      if ($$props.disabled === void 0 && $$bindings.disabled && disabled !== void 0)
        $$bindings.disabled(disabled);
      if ($$props.active === void 0 && $$bindings.active && active !== void 0)
        $$bindings.active(active);
      if ($$props.type === void 0 && $$bindings.type && type !== void 0)
        $$bindings.type(type);
      if ($$props.action === void 0 && $$bindings.action && action !== void 0)
        $$bindings.action(action);
      $$result.css.add(css2);
      return `<button${add_attribute("type", type, 0)} ${disabled || !active ? "disabled" : ""} class="svelte-e2dftg"${add_styles({
        "display": `flex`,
        "background": `none`,
        "border": `none`,
        "color": `inherit`,
        "box-sizing": `border-box`,
        "padding": `0.3em 0 0 0`,
        "width": `100%`,
        "height": `100%`,
        "font": `inherit`,
        "outline": `none`
      })}><div${add_styles({
        "background": disabled ? "#636363" : backgroundDeepColor,
        "transition": `background 300ms linear`,
        "border-radius": `0.7em`,
        "transform": `translateY(0)`,
        "width": `100%`,
        "height": `100%`
      })}><div class="front svelte-e2dftg"${add_styles({
        "background-color": disabled ? "#737373" : backgroundColor,
        "border": `0.1em solid ${disabled ? "#636363" : backgroundDeepColor}`,
        "border-radius": `0.7em`,
        "box-sizing": `border-box`,
        "color": foregroundColor,
        "width": `100%`,
        "height": `100%`
      })}>${slots.default ? slots.default({}) : ``}</div></div> </button>`;
    });
    FancyButton = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { foregroundColor = void 0 } = $$props;
      let { backgroundColor = void 0 } = $$props;
      let { backgroundDeepColor = void 0 } = $$props;
      let { disabled = false } = $$props;
      let { active = true } = $$props;
      let { type = void 0 } = $$props;
      let { action = void 0 } = $$props;
      if ($$props.foregroundColor === void 0 && $$bindings.foregroundColor && foregroundColor !== void 0)
        $$bindings.foregroundColor(foregroundColor);
      if ($$props.backgroundColor === void 0 && $$bindings.backgroundColor && backgroundColor !== void 0)
        $$bindings.backgroundColor(backgroundColor);
      if ($$props.backgroundDeepColor === void 0 && $$bindings.backgroundDeepColor && backgroundDeepColor !== void 0)
        $$bindings.backgroundDeepColor(backgroundDeepColor);
      if ($$props.disabled === void 0 && $$bindings.disabled && disabled !== void 0)
        $$bindings.disabled(disabled);
      if ($$props.active === void 0 && $$bindings.active && active !== void 0)
        $$bindings.active(active);
      if ($$props.type === void 0 && $$bindings.type && type !== void 0)
        $$bindings.type(type);
      if ($$props.action === void 0 && $$bindings.action && action !== void 0)
        $$bindings.action(action);
      return `${validate_component(FancyButtonBase, "FancyButtonBase").$$render(
        $$result,
        {
          action: action || (() => {
          }),
          // do nothing
          type,
          foregroundColor: foregroundColor ?? "#FFFFFF",
          backgroundColor: backgroundColor ?? "#D4131B",
          backgroundDeepColor: backgroundDeepColor ?? "#A40E13",
          disabled,
          active
        },
        {},
        {
          default: () => {
            return `<div${add_styles({
              "height": `100%`,
              "padding": `5px`,
              "font-weight": `bold`,
              "box-sizing": `border-box`
            })}>${slots.default ? slots.default({}) : ``}</div>`;
          }
        }
      )}`;
    });
  }
});
var css3;
var Textarea;
var init_Textarea = __esm({
  ".svelte-kit/output/server/chunks/Textarea.js"() {
    init_ssr();
    css3 = {
      code: "div.svelte-16xnuj4.svelte-16xnuj4{display:flex;flex-direction:column;align-items:center}label.svelte-16xnuj4.svelte-16xnuj4{text-align:center;color:color-mix(in srgb, currentColor 50%, transparent);padding:0 8px;position:absolute;pointer-events:none;border-radius:5px 5px 0 0;top:50%;transform:translateY(-50%);line-height:1em;transform-origin:top;transition:all 100ms linear}textarea.svelte-16xnuj4:where(:not(:placeholder-shown), :focus, :active)+label.svelte-16xnuj4{top:0;scale:0.75;background:var(--background-color);transform:translateY(-50%)}textarea.svelte-16xnuj4:focus+label.svelte-16xnuj4{color:var(--accent-color)}textarea.svelte-16xnuj4.svelte-16xnuj4{background:none;font:inherit;font-weight:bold;border:2px solid #a9a8aa;border-radius:10px;width:100%;color:inherit;box-sizing:border-box;text-align:center;padding:8px 5px;resize:none;transition:border-color 100ms linear}textarea.svelte-16xnuj4.svelte-16xnuj4:focus{outline:none;border-color:var(--accent-color)}",
      map: null
    };
    Textarea = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { id } = $$props;
      let { placeholder } = $$props;
      let { required } = $$props;
      let { disabled } = $$props;
      let { value } = $$props;
      let { maxHeight = "4em" } = $$props;
      let { maxLength = void 0 } = $$props;
      let editableElement;
      if ($$props.id === void 0 && $$bindings.id && id !== void 0)
        $$bindings.id(id);
      if ($$props.placeholder === void 0 && $$bindings.placeholder && placeholder !== void 0)
        $$bindings.placeholder(placeholder);
      if ($$props.required === void 0 && $$bindings.required && required !== void 0)
        $$bindings.required(required);
      if ($$props.disabled === void 0 && $$bindings.disabled && disabled !== void 0)
        $$bindings.disabled(disabled);
      if ($$props.value === void 0 && $$bindings.value && value !== void 0)
        $$bindings.value(value);
      if ($$props.maxHeight === void 0 && $$bindings.maxHeight && maxHeight !== void 0)
        $$bindings.maxHeight(maxHeight);
      if ($$props.maxLength === void 0 && $$bindings.maxLength && maxLength !== void 0)
        $$bindings.maxLength(maxLength);
      $$result.css.add(css3);
      return `<div class="svelte-16xnuj4"${add_styles({ "position": `relative` })}><textarea${add_attribute("id", id, 0)}${add_attribute("name", id, 0)} ${required ? "required" : ""} ${disabled ? "disabled" : ""} placeholder=""${add_attribute("maxlength", maxLength, 0)} class="svelte-16xnuj4"${add_styles({ "max-height": maxHeight })}${add_attribute("this", editableElement, 0)}>${escape(value || "")}</textarea> <label${add_attribute("for", id, 0)} class="svelte-16xnuj4">${escape(placeholder)}</label> </div>`;
    });
  }
});
var init_inline_svg_internal = __esm({
  "node_modules/@svelte-put/inline-svg/src/inline-svg.internal.js"() {
  }
});
var init_inline_svg_action = __esm({
  "node_modules/@svelte-put/inline-svg/src/inline-svg.action.js"() {
    init_inline_svg_internal();
  }
});
var init_src = __esm({
  "node_modules/@svelte-put/inline-svg/src/index.js"() {
    init_inline_svg_action();
  }
});
function noop2() {
}
function run2(fn) {
  return fn();
}
function run_all2(fns) {
  fns.forEach(run2);
}
function is_function(thing) {
  return typeof thing === "function";
}
function safe_not_equal2(a, b) {
  return a != a ? b == b : a !== b || a && typeof a === "object" || typeof a === "function";
}
function subscribe2(store, ...callbacks) {
  if (store == null) {
    for (const callback of callbacks) {
      callback(void 0);
    }
    return noop2;
  }
  const unsub = store.subscribe(...callbacks);
  return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
}
var init_utils2 = __esm({
  "node_modules/svelte/src/runtime/internal/utils.js"() {
  }
});
var init_environment = __esm({
  "node_modules/svelte/src/runtime/internal/environment.js"() {
    init_utils2();
  }
});
var init_loop = __esm({
  "node_modules/svelte/src/runtime/internal/loop.js"() {
    init_environment();
  }
});
var globals;
var init_globals = __esm({
  "node_modules/svelte/src/runtime/internal/globals.js"() {
    globals = typeof window !== "undefined" ? window : typeof globalThis !== "undefined" ? globalThis : (
      // @ts-ignore Node typings have this
      global
    );
  }
});
var ResizeObserverSingleton;
var init_ResizeObserverSingleton = __esm({
  "node_modules/svelte/src/runtime/internal/ResizeObserverSingleton.js"() {
    init_globals();
    ResizeObserverSingleton = class _ResizeObserverSingleton {
      /**
       * @private
       * @readonly
       * @type {WeakMap<Element, import('./private.js').Listener>}
       */
      _listeners = "WeakMap" in globals ? /* @__PURE__ */ new WeakMap() : void 0;
      /**
       * @private
       * @type {ResizeObserver}
       */
      _observer = void 0;
      /** @type {ResizeObserverOptions} */
      options;
      /** @param {ResizeObserverOptions} options */
      constructor(options3) {
        this.options = options3;
      }
      /**
       * @param {Element} element
       * @param {import('./private.js').Listener} listener
       * @returns {() => void}
       */
      observe(element2, listener) {
        this._listeners.set(element2, listener);
        this._getObserver().observe(element2, this.options);
        return () => {
          this._listeners.delete(element2);
          this._observer.unobserve(element2);
        };
      }
      /**
       * @private
       */
      _getObserver() {
        return this._observer ?? (this._observer = new ResizeObserver((entries) => {
          for (const entry of entries) {
            _ResizeObserverSingleton.entries.set(entry.target, entry);
            this._listeners.get(entry.target)?.(entry);
          }
        }));
      }
    };
    ResizeObserverSingleton.entries = "WeakMap" in globals ? /* @__PURE__ */ new WeakMap() : void 0;
  }
});
function insert(target, node, anchor) {
  target.insertBefore(node, anchor || null);
}
function detach(node) {
  if (node.parentNode) {
    node.parentNode.removeChild(node);
  }
}
function element(name) {
  return document.createElement(name);
}
function attr(node, attribute, value) {
  if (value == null)
    node.removeAttribute(attribute);
  else if (node.getAttribute(attribute) !== value)
    node.setAttribute(attribute, value);
}
function get_custom_elements_slots(element2) {
  const result = {};
  element2.childNodes.forEach(
    /** @param {Element} node */
    (node) => {
      result[node.slot || "default"] = true;
    }
  );
  return result;
}
var init_dom = __esm({
  "node_modules/svelte/src/runtime/internal/dom.js"() {
    init_utils2();
    init_ResizeObserverSingleton();
  }
});
var init_style_manager = __esm({
  "node_modules/svelte/src/runtime/internal/style_manager.js"() {
    init_dom();
    init_environment();
  }
});
var init_animations = __esm({
  "node_modules/svelte/src/runtime/internal/animations.js"() {
    init_utils2();
    init_environment();
    init_loop();
    init_style_manager();
  }
});
var init_lifecycle = __esm({
  "node_modules/svelte/src/runtime/internal/lifecycle.js"() {
    init_dom();
  }
});
var init_scheduler = __esm({
  "node_modules/svelte/src/runtime/internal/scheduler.js"() {
    init_utils2();
    init_lifecycle();
  }
});
var init_transitions = __esm({
  "node_modules/svelte/src/runtime/internal/transitions.js"() {
    init_utils2();
    init_environment();
    init_loop();
    init_style_manager();
    init_dom();
    init_scheduler();
  }
});
var init_await_block = __esm({
  "node_modules/svelte/src/runtime/internal/await_block.js"() {
    init_utils2();
    init_transitions();
    init_scheduler();
    init_lifecycle();
  }
});
var init_each = __esm({
  "node_modules/svelte/src/runtime/internal/each.js"() {
    init_transitions();
    init_utils2();
  }
});
var init_spread = __esm({
  "node_modules/svelte/src/runtime/internal/spread.js"() {
  }
});
var _boolean_attributes;
var boolean_attributes;
var init_boolean_attributes = __esm({
  "node_modules/svelte/src/shared/boolean_attributes.js"() {
    _boolean_attributes = /** @type {const} */
    [
      "allowfullscreen",
      "allowpaymentrequest",
      "async",
      "autofocus",
      "autoplay",
      "checked",
      "controls",
      "default",
      "defer",
      "disabled",
      "formnovalidate",
      "hidden",
      "inert",
      "ismap",
      "loop",
      "multiple",
      "muted",
      "nomodule",
      "novalidate",
      "open",
      "playsinline",
      "readonly",
      "required",
      "reversed",
      "selected"
    ];
    boolean_attributes = /* @__PURE__ */ new Set([..._boolean_attributes]);
  }
});
var init_names = __esm({
  "node_modules/svelte/src/shared/utils/names.js"() {
  }
});
var init_ssr2 = __esm({
  "node_modules/svelte/src/runtime/internal/ssr.js"() {
    init_lifecycle();
    init_utils2();
    init_boolean_attributes();
    init_each();
    init_names();
  }
});
function get_custom_element_value(prop, value, props_definition, transform) {
  const type = props_definition[prop]?.type;
  value = type === "Boolean" && typeof value !== "boolean" ? value != null : value;
  if (!transform || !props_definition[prop]) {
    return value;
  } else if (transform === "toAttribute") {
    switch (type) {
      case "Object":
      case "Array":
        return value == null ? null : JSON.stringify(value);
      case "Boolean":
        return value ? "" : null;
      case "Number":
        return value == null ? null : value;
      default:
        return value;
    }
  } else {
    switch (type) {
      case "Object":
      case "Array":
        return value && JSON.parse(value);
      case "Boolean":
        return value;
      case "Number":
        return value != null ? +value : value;
      default:
        return value;
    }
  }
}
var SvelteElement;
var init_Component = __esm({
  "node_modules/svelte/src/runtime/internal/Component.js"() {
    init_scheduler();
    init_lifecycle();
    init_utils2();
    init_dom();
    init_transitions();
    if (typeof HTMLElement === "function") {
      SvelteElement = class extends HTMLElement {
        /** The Svelte component constructor */
        $$ctor;
        /** Slots */
        $$s;
        /** The Svelte component instance */
        $$c;
        /** Whether or not the custom element is connected */
        $$cn = false;
        /** Component props data */
        $$d = {};
        /** `true` if currently in the process of reflecting component props back to attributes */
        $$r = false;
        /** @type {Record<string, CustomElementPropDefinition>} Props definition (name, reflected, type etc) */
        $$p_d = {};
        /** @type {Record<string, Function[]>} Event listeners */
        $$l = {};
        /** @type {Map<Function, Function>} Event listener unsubscribe functions */
        $$l_u = /* @__PURE__ */ new Map();
        constructor($$componentCtor, $$slots, use_shadow_dom) {
          super();
          this.$$ctor = $$componentCtor;
          this.$$s = $$slots;
          if (use_shadow_dom) {
            this.attachShadow({ mode: "open" });
          }
        }
        addEventListener(type, listener, options3) {
          this.$$l[type] = this.$$l[type] || [];
          this.$$l[type].push(listener);
          if (this.$$c) {
            const unsub = this.$$c.$on(type, listener);
            this.$$l_u.set(listener, unsub);
          }
          super.addEventListener(type, listener, options3);
        }
        removeEventListener(type, listener, options3) {
          super.removeEventListener(type, listener, options3);
          if (this.$$c) {
            const unsub = this.$$l_u.get(listener);
            if (unsub) {
              unsub();
              this.$$l_u.delete(listener);
            }
          }
        }
        async connectedCallback() {
          this.$$cn = true;
          if (!this.$$c) {
            let create_slot = function(name) {
              return () => {
                let node;
                const obj = {
                  c: function create3() {
                    node = element("slot");
                    if (name !== "default") {
                      attr(node, "name", name);
                    }
                  },
                  /**
                   * @param {HTMLElement} target
                   * @param {HTMLElement} [anchor]
                   */
                  m: function mount(target, anchor) {
                    insert(target, node, anchor);
                  },
                  d: function destroy(detaching) {
                    if (detaching) {
                      detach(node);
                    }
                  }
                };
                return obj;
              };
            };
            await Promise.resolve();
            if (!this.$$cn || this.$$c) {
              return;
            }
            const $$slots = {};
            const existing_slots = get_custom_elements_slots(this);
            for (const name of this.$$s) {
              if (name in existing_slots) {
                $$slots[name] = [create_slot(name)];
              }
            }
            for (const attribute of this.attributes) {
              const name = this.$$g_p(attribute.name);
              if (!(name in this.$$d)) {
                this.$$d[name] = get_custom_element_value(name, attribute.value, this.$$p_d, "toProp");
              }
            }
            for (const key2 in this.$$p_d) {
              if (!(key2 in this.$$d) && this[key2] !== void 0) {
                this.$$d[key2] = this[key2];
                delete this[key2];
              }
            }
            this.$$c = new this.$$ctor({
              target: this.shadowRoot || this,
              props: {
                ...this.$$d,
                $$slots,
                $$scope: {
                  ctx: []
                }
              }
            });
            const reflect_attributes = () => {
              this.$$r = true;
              for (const key2 in this.$$p_d) {
                this.$$d[key2] = this.$$c.$$.ctx[this.$$c.$$.props[key2]];
                if (this.$$p_d[key2].reflect) {
                  const attribute_value = get_custom_element_value(
                    key2,
                    this.$$d[key2],
                    this.$$p_d,
                    "toAttribute"
                  );
                  if (attribute_value == null) {
                    this.removeAttribute(this.$$p_d[key2].attribute || key2);
                  } else {
                    this.setAttribute(this.$$p_d[key2].attribute || key2, attribute_value);
                  }
                }
              }
              this.$$r = false;
            };
            this.$$c.$$.after_update.push(reflect_attributes);
            reflect_attributes();
            for (const type in this.$$l) {
              for (const listener of this.$$l[type]) {
                const unsub = this.$$c.$on(type, listener);
                this.$$l_u.set(listener, unsub);
              }
            }
            this.$$l = {};
          }
        }
        // We don't need this when working within Svelte code, but for compatibility of people using this outside of Svelte
        // and setting attributes through setAttribute etc, this is helpful
        attributeChangedCallback(attr2, _oldValue, newValue) {
          if (this.$$r)
            return;
          attr2 = this.$$g_p(attr2);
          this.$$d[attr2] = get_custom_element_value(attr2, newValue, this.$$p_d, "toProp");
          this.$$c?.$set({ [attr2]: this.$$d[attr2] });
        }
        disconnectedCallback() {
          this.$$cn = false;
          Promise.resolve().then(() => {
            if (!this.$$cn) {
              this.$$c.$destroy();
              this.$$c = void 0;
            }
          });
        }
        $$g_p(attribute_name) {
          return Object.keys(this.$$p_d).find(
            (key2) => this.$$p_d[key2].attribute === attribute_name || !this.$$p_d[key2].attribute && key2.toLowerCase() === attribute_name
          ) || attribute_name;
        }
      };
    }
  }
});
var init_version = __esm({
  "node_modules/svelte/src/shared/version.js"() {
  }
});
var init_dev = __esm({
  "node_modules/svelte/src/runtime/internal/dev.js"() {
    init_dom();
    init_Component();
    init_names();
    init_version();
    init_utils2();
    init_each();
  }
});
var init_internal = __esm({
  "node_modules/svelte/src/runtime/internal/index.js"() {
    init_animations();
    init_await_block();
    init_dom();
    init_environment();
    init_globals();
    init_each();
    init_lifecycle();
    init_loop();
    init_scheduler();
    init_spread();
    init_ssr2();
    init_transitions();
    init_utils2();
    init_Component();
    init_dev();
  }
});
function readable2(value, start) {
  return {
    subscribe: writable2(value, start).subscribe
  };
}
function writable2(value, start = noop2) {
  let stop;
  const subscribers = /* @__PURE__ */ new Set();
  function set2(new_value) {
    if (safe_not_equal2(value, new_value)) {
      value = new_value;
      if (stop) {
        const run_queue = !subscriber_queue2.length;
        for (const subscriber of subscribers) {
          subscriber[1]();
          subscriber_queue2.push(subscriber, value);
        }
        if (run_queue) {
          for (let i = 0; i < subscriber_queue2.length; i += 2) {
            subscriber_queue2[i][0](subscriber_queue2[i + 1]);
          }
          subscriber_queue2.length = 0;
        }
      }
    }
  }
  function update(fn) {
    set2(fn(value));
  }
  function subscribe3(run3, invalidate = noop2) {
    const subscriber = [run3, invalidate];
    subscribers.add(subscriber);
    if (subscribers.size === 1) {
      stop = start(set2, update) || noop2;
    }
    run3(value);
    return () => {
      subscribers.delete(subscriber);
      if (subscribers.size === 0 && stop) {
        stop();
        stop = null;
      }
    };
  }
  return { set: set2, update, subscribe: subscribe3 };
}
function derived(stores, fn, initial_value) {
  const single = !Array.isArray(stores);
  const stores_array = single ? [stores] : stores;
  if (!stores_array.every(Boolean)) {
    throw new Error("derived() expects stores as input, got a falsy value");
  }
  const auto = fn.length < 2;
  return readable2(initial_value, (set2, update) => {
    let started = false;
    const values = [];
    let pending = 0;
    let cleanup = noop2;
    const sync2 = () => {
      if (pending) {
        return;
      }
      cleanup();
      const result = fn(single ? values[0] : values, set2, update);
      if (auto) {
        set2(result);
      } else {
        cleanup = is_function(result) ? result : noop2;
      }
    };
    const unsubscribers = stores_array.map(
      (store, i) => subscribe2(
        store,
        (value) => {
          values[i] = value;
          pending &= ~(1 << i);
          if (started) {
            sync2();
          }
        },
        () => {
          pending |= 1 << i;
        }
      )
    );
    started = true;
    sync2();
    return function stop() {
      run_all2(unsubscribers);
      cleanup();
      started = false;
    };
  });
}
var subscriber_queue2;
var init_store = __esm({
  "node_modules/svelte/src/runtime/store/index.js"() {
    init_internal();
    subscriber_queue2 = [];
  }
});
var init_runtime2 = __esm({
  "node_modules/svelte/src/runtime/index.js"() {
    init_internal();
  }
});
function Re(t2) {
  let r3;
  return derived(t2, (e3, o2) => {
    e3 !== r3 && (r3 = e3, o2(e3));
  });
}
function _e(t2) {
  if (t2.key !== Te)
    return;
  let r3 = t2.currentTarget, e3 = t2.target;
  if (!r3.contains(e3))
    return;
  let o2 = [...r3.querySelectorAll(Ge.join(","))], i = o2[0], s3 = o2[o2.length - 1];
  e3 === i && t2.shiftKey && (s3.focus(), t2.preventDefault()), e3 === s3 && !t2.shiftKey && (i.focus(), t2.preventDefault());
}
function u(t2, r3) {
  let e3 = r3.map((o2) => o2(t2));
  return () => e3.forEach((o2) => o2());
}
function ie(t2) {
  return (r3) => () => t2(r3);
}
function le(t2) {
  if (t2.active === -1 || t2.items[t2.active].disabled)
    return {};
  let r3 = Z(t2);
  return { selected: t2.multi ? t2.selected.includes(r3) ? t2.selected.filter((o2) => o2 !== r3) : [...t2.selected, r3] : r3 };
}
function At(t2, r3) {
  let e3 = t2.textContent?.trim() ?? "";
  return { text: e3, value: r3?.value || e3, disabled: r3?.disabled ?? false };
}
function gt(t2, r3) {
  let e3 = t2.length;
  for (; e3--; )
    if (r3(t2[e3], e3, t2))
      return e3;
  return -1;
}
function kt() {
  return ++Tt;
}
function x2(t2, r3) {
  return t2.id || (t2.id = `${r3}:${kt()}`), t2;
}
function G(t2, r3) {
  return () => {
    let e3 = null;
    function o2(n2) {
      if (n2.pointerType === "" || !e3)
        return;
      let m = t2().filter((c2) => c2);
      for (let c2 of m)
        if (c2.contains(e3))
          return;
      r3(n2), e3 = null;
    }
    function i(n2) {
      n2.isPrimary && (e3 = n2.target);
    }
    let s3 = [ve(document.documentElement, "pointerdown", i, true), ve(document.documentElement, "click", o2, true)];
    return () => s3.forEach((n2) => n2());
  };
}
function k(...t2) {
  let r3 = (e3) => {
    for (let o2 of t2)
      o2(e3);
  };
  return (e3) => (e3.addEventListener("keydown", r3), () => e3.removeEventListener("keydown", r3));
}
function Ee(t2, r3) {
  return (e3) => {
    let o2 = (i) => {
      if (i.target !== e3) {
        let s3 = i.target.closest(t2);
        r3(s3);
      }
    };
    return e3.addEventListener("pointermove", o2), () => e3.removeEventListener("pointermove", o2);
  };
}
function Jo(t2) {
  let r3 = H("dialog"), e3 = { ...N, ...t2 }, o2 = writable2(e3), i = (v) => o2.set(e3 = { ...e3, ...v }), s3 = () => i({ expanded: true, opened: true }), n2 = () => i({ expanded: false });
  function m(v) {
    return x2(v, r3), { destroy: u(v, [h("dialog"), st(o2), C(o2), Je(o2), G(() => [v], n2), k(q(n2))]) };
  }
  let { subscribe: c2 } = derived(o2, (v) => {
    let { expanded: S } = v;
    return { expanded: S };
  });
  return { subscribe: c2, modal: m, open: s3, close: n2, set: i };
}
function zi(t2) {
  let r3 = H("listbox"), e3 = { ...oe(), ...N, ...re, ...t2 };
  e3.multi = Array.isArray(e3.selected);
  let o2 = writable2(e3), i = (a) => o2.set(e3 = { ...e3, ...a }), s3 = () => i({ expanded: true, opened: true, active: e3.items.findIndex((a) => a.value === e3.selected) }), n2 = () => i({ expanded: false }), m = () => e3.expanded ? n2() : s3(), c2 = (a) => {
    if (e3.active !== a) {
      i({ active: a });
      let y = e3.items[a];
      y && y.node.scrollIntoView({ block: "nearest" });
    }
  }, v = () => c2(ce(e3)), S = () => c2(be(e3)), d2 = () => c2(xe(e3)), f = () => c2(pe(e3)), F = Se(() => e3, c2), z = de(() => e3, c2), Q = (a) => i(ne(e3, a)), X = () => i(le(e3));
  function $(a) {
    return x2(a, r3), i({ button: a }), { destroy: u(a, [O("button"), h("button"), J(), L(0), C(o2), W(o2), j(o2), g(m), k(R(m), Le(m), Ae(m)), _(o2), se(o2)]) };
  }
  function ue(a) {
    return x2(a, r3), i({ controls: a.id }), { destroy: u(a, [h("listbox"), L(0), G(() => [e3.button, a], n2), g(ae('[role="option"]', z, X, e3.multi ? B : n2)), Ee('[role="option"]', z), k(R(X, e3.multi ? B : n2), q(n2), fe(v, S, d2, f), Be(B), Pe(F)), te(o2), ee(o2), Me(o2)]) };
  }
  function b(a, y) {
    x2(a, r3);
    let M = me(a, () => e3, i);
    M(y);
    let U = e3.items[e3.items.length - 1].value, p = u(a, [L(-1), h("option"), ye(o2), he(o2, U), ie(Q)]);
    return { update: M, destroy: p };
  }
  function D(a, y) {
    return { destroy: u(a, [g((U) => {
      i({ selected: e3.selected.filter((p) => p !== y) }), U.stopImmediatePropagation();
    })]) };
  }
  let { subscribe: Y } = derived(o2, (a) => {
    let { expanded: y, selected: M } = a;
    return { expanded: y, selected: M, active: Z(a) };
  });
  return { subscribe: Y, button: $, items: ue, item: b, deselect: D, open: s3, close: n2, set: i };
}
var V;
var K;
var bt;
var ee;
var we;
var j;
var xt;
var ye;
var De;
var He;
var Ke;
var Ie;
var Fe;
var Ue;
var Ne;
var qe;
var Ve;
var je;
var We;
var ze;
var Te;
var ke;
var Ge;
var Je;
var N;
var vt;
var W;
var te;
var _;
var yt;
var C;
var re;
var ht;
var Et;
var he;
var Me;
var Lt;
var Ce;
var A;
var q;
var Be;
var Qe;
var oe;
var ne;
var Z;
var ae;
var se;
var ce;
var be;
var xe;
var pe;
var me;
var de;
var Se;
var Tt;
var ve;
var g;
var B;
var J;
var h;
var L;
var O;
var H;
var et;
var tt;
var rt;
var ot;
var it;
var Le;
var Ae;
var fe;
var eo;
var nt;
var Mt;
var st;
var R;
var lt;
var Pe;
var Ct;
var Bt;
var St;
var init_dist = __esm({
  "node_modules/svelte-headlessui/dist/index.js"() {
    init_store();
    init_runtime2();
    V = (t2) => (r3) => (e3) => e3 ? r3.setAttribute(t2, e3) : r3.removeAttribute(t2);
    K = (t2) => (r3) => (e3) => e3 === void 0 ? r3.removeAttribute(t2) : r3.setAttribute(t2, e3.toString());
    bt = V("aria-activedescendant");
    ee = (t2) => (r3) => derived(t2, (e3) => e3.items[e3.active]?.id ?? "").subscribe(bt(r3));
    we = V("aria-controls");
    j = (t2) => (r3) => derived(t2, (e3) => e3.controls).subscribe(we(r3));
    xt = K("aria-disabled");
    ye = (t2) => (r3) => derived(t2, (e3) => e3.items.find((o2) => o2.id === r3.id)?.disabled).subscribe(xt(r3));
    De = " ";
    He = "Enter";
    Ke = "Escape";
    Ie = "Backspace";
    Fe = "ArrowLeft";
    Ue = "ArrowUp";
    Ne = "ArrowRight";
    qe = "ArrowDown";
    Ve = "Home";
    je = "End";
    We = "PageUp";
    ze = "PageDown";
    Te = "Tab";
    ke = (t2) => (r3) => {
      r3 && requestAnimationFrame(() => {
        t2.focus({ preventScroll: true });
      });
    };
    Ge = ["[contentEditable=true]", "[tabindex]", "a[href]", "area[href]", "button:not([disabled])", "iframe", "input:not([disabled])", "select:not([disabled])", "textarea:not([disabled])"];
    Je = (t2) => (r3) => derived(t2, (e3) => e3.expanded).subscribe((e3) => {
      if (e3) {
        let o2 = r3.querySelector(Ge.join(","));
        o2 && requestAnimationFrame(() => o2.focus()), r3.addEventListener("keydown", _e);
      } else
        r3.removeEventListener("keydown", _e);
    });
    N = { expanded: false, opened: false };
    vt = K("aria-expanded");
    W = (t2) => (r3) => derived(t2, (e3) => e3.expanded).subscribe(vt(r3));
    te = (t2) => (r3) => derived(t2, (e3) => e3.expanded).subscribe(ke(r3));
    _ = (t2) => (r3) => derived(t2, (e3) => e3.opened && !e3.expanded).subscribe(ke(r3));
    yt = V("aria-label");
    C = (t2) => (r3) => derived(t2, (e3) => e3.label).subscribe(yt(r3));
    re = { selected: null, multi: false };
    ht = K("aria-selected");
    Et = K("aria-multiselectable");
    he = (t2, r3) => (e3) => derived(t2, (o2) => o2.multi ? o2.selected.includes(r3) : o2.selected === r3).subscribe(ht(e3));
    Me = (t2) => (r3) => derived(t2, (e3) => e3.multi).subscribe(Et(r3));
    Lt = (t2) => {
      t2.preventDefault(), t2.stopPropagation(), t2.stopImmediatePropagation();
    };
    Ce = (t2) => {
    };
    A = (t2, r3 = Lt) => (...e3) => (o2) => {
      t2.includes(o2.key) && (e3.forEach((i) => i()), r3 && r3(o2));
    };
    q = A([Ke]);
    Be = A([Te]);
    Qe = A([Te], Ce);
    oe = () => ({ items: [], active: -1 });
    ne = (t2, r3) => ({ items: t2.items.filter((e3) => e3.id !== r3.id) });
    Z = (t2) => t2.active === -1 || t2.items.length === 0 ? void 0 : t2.active >= t2.items.length ? t2.items[t2.active] : t2.items[t2.active]?.value;
    ae = (t2, r3, ...e3) => (o2) => {
      let i = o2.target.closest(t2);
      r3(i), e3.forEach((s3) => s3());
    };
    se = (t2) => (r3) => Re(derived(t2, (e3) => e3.selected)).subscribe((e3) => {
      let o2 = new CustomEvent("select", { detail: { selected: e3 } });
      r3.dispatchEvent(o2);
    });
    ce = (t2) => t2.items.findIndex((r3) => !r3.disabled);
    be = (t2) => {
      let r3 = t2.active === -1 ? t2.items.length : t2.active;
      for (; --r3 > -1; )
        if (!t2.items[r3].disabled)
          return r3;
      return t2.active;
    };
    xe = (t2) => {
      let r3 = t2.active;
      for (; ++r3 < t2.items.length; )
        if (!t2.items[r3].disabled)
          return r3;
      return t2.active;
    };
    pe = (t2) => gt(t2.items, (r3) => !r3.disabled);
    me = (t2, r3, e3) => (o2) => {
      let i = r3(), s3 = At(t2, o2), n2 = i.items.find((m) => m.id === t2.id);
      if (n2) {
        if (n2.text === s3.text && n2.value === s3.value && n2.disabled === s3.disabled)
          return;
        Object.assign(n2, s3);
      } else
        i.items.push({ id: t2.id, node: t2, ...s3 });
      e3({ items: i.items });
    };
    de = (t2, r3) => (e3) => {
      let o2 = t2();
      r3(e3 ? o2.items.findIndex((i) => i.id === e3.id && !i.disabled) : -1);
    };
    Se = (t2, r3, e3 = false) => (o2) => {
      let i = t2(), s3 = i.active === -1 ? i.items : i.items.slice(i.active + 1).concat(i.items.slice(0, i.active + 1)), n2 = new RegExp(`${e3 ? "^" : ""}${o2}`, "i"), m = s3.findIndex((c2) => c2.text.match(n2) && !c2.disabled);
      if (m > -1) {
        let c2 = (m + i.active + 1) % i.items.length;
        r3(c2);
      }
    };
    Tt = 0;
    ve = (t2, r3, e3, o2 = false) => (t2.addEventListener(r3, e3, o2), () => t2.removeEventListener(r3, e3, o2));
    g = (t2) => (r3) => ve(r3, "click", t2);
    B = () => {
    };
    J = () => (t2) => (t2.setAttribute("aria-haspopup", "true"), B);
    h = (t2) => (r3) => (r3.setAttribute("role", t2), B);
    L = (t2 = -1) => (r3) => (r3.tabIndex = t2, B);
    O = (t2) => (r3) => (r3.setAttribute("type", t2), B);
    H = (t2) => "headlessui-" + t2;
    et = A([He]);
    tt = A([Ve, We]);
    rt = A([je, ze]);
    ot = A([Fe]);
    it = A([Ne]);
    Le = A([Ue]);
    Ae = A([qe]);
    fe = (t2, r3, e3, o2, i = "vertical") => {
      let s3 = tt(t2), n2 = i === "vertical" ? Le(r3) : ot(r3), m = i === "vertical" ? Ae(e3) : it(e3), c2 = rt(o2);
      return (v) => {
        s3(v), n2(v), m(v), c2(v);
      };
    };
    eo = A([Ie]);
    nt = A([Ie], Ce);
    Mt = K("aria-modal");
    st = (t2) => (r3) => derived(t2, (e3) => e3.expanded).subscribe(Mt(r3));
    R = A([De, He]);
    lt = (t2) => /^\S$/.test(t2);
    Pe = (t2) => {
      let r3, e3 = "";
      return (o2) => {
        let { key: i } = o2;
        lt(i) && (r3 && clearTimeout(r3), e3 += i, t2(e3), r3 = window.setTimeout(() => {
          r3 = 0, e3 = "";
        }, 350));
      };
    };
    Ct = V("aria-oriantation");
    Bt = K("aria-checked");
    St = K("aria-pressed");
  }
});
var Icon;
var css$2;
var IconButton;
var DarkModeSwitcher;
var ___ASSET___2;
var balance;
var css$1;
var Anchor;
var ___ASSET___0;
var css4;
var LanguageSwitcher;
var Footer;
var Logo;
var Header;
var init_Header = __esm({
  ".svelte-kit/output/server/chunks/Header.js"() {
    init_ssr();
    init_LanguageSwitcher_svelte_svelte_type_style_lang();
    init_runtime();
    init_stores();
    init_i18n_routing();
    init_dist();
    init_src();
    Icon = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let urlString;
      let { src } = $$props;
      let { alt } = $$props;
      let { size } = $$props;
      if ($$props.src === void 0 && $$bindings.src && src !== void 0)
        $$bindings.src(src);
      if ($$props.alt === void 0 && $$bindings.alt && alt !== void 0)
        $$bindings.alt(alt);
      if ($$props.size === void 0 && $$bindings.size && size !== void 0)
        $$bindings.size(size);
      urlString = `url("${src}")`;
      return `<div${add_attribute("title", alt, 0)}${add_styles({
        "width": size,
        "height": size,
        "aspect-ratio": `1/1`,
        "display": `flex`,
        "-webkit-mask-size": `cover`,
        "color": `inherit`,
        "background": `currentColor`,
        "mask-image": urlString,
        "mask-position": `center`,
        "mask-size": `contain`,
        "mask-repeat": `no-repeat`,
        "-webkit-mask-image": urlString
      })}></div>`;
    });
    css$2 = {
      code: "button.svelte-1dd8d21{background:none;border-radius:4px}button.svelte-1dd8d21:where(:hover, :focus){background:#00000040;outline:2px solid #ffffff40}",
      map: null
    };
    IconButton = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { src } = $$props;
      let { alt } = $$props;
      let { size } = $$props;
      let { padding = "0" } = $$props;
      if ($$props.src === void 0 && $$bindings.src && src !== void 0)
        $$bindings.src(src);
      if ($$props.alt === void 0 && $$bindings.alt && alt !== void 0)
        $$bindings.alt(alt);
      if ($$props.size === void 0 && $$bindings.size && size !== void 0)
        $$bindings.size(size);
      if ($$props.padding === void 0 && $$bindings.padding && padding !== void 0)
        $$bindings.padding(padding);
      $$result.css.add(css$2);
      return `<button class="svelte-1dd8d21"${add_styles({
        "font": `inherit`,
        "display": `flex`,
        "aspect-ratio": `1/1`,
        "appearance": `none`,
        padding,
        "box-sizing": `border-box`,
        "border": `none`,
        "cursor": `pointer`,
        "color": `inherit`
      })}>${validate_component(Icon, "Icon").$$render($$result, { alt, src, size }, {}, {})} </button>`;
    });
    DarkModeSwitcher = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let $$settled;
      let $$rendered;
      let previous_head = $$result.head;
      do {
        $$settled = true;
        $$result.head = previous_head;
        $$rendered = `${``}`;
      } while (!$$settled);
      return $$rendered;
    });
    ___ASSET___2 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20height='20'%20viewBox='0%20-960%20960%20960'%20width='20'%3e%3cpath%20d='M57-418q-28-45-42.5-90T0-600q0-108%2076.5-186T264-864q61.644%200%20117.322%2025.5Q437-813%20480-764q43-49%2098.678-74.5T696-864q111%200%20187.5%2078T960-600q0%2045-14.711%2090.991Q930.579-463.018%20903-419q-19-23-47-35.5T798-468q21.895-33%2032.947-67.5Q842-570%20842-600q0-59-42-102.5T696-746q-45%200-99%2023T480-611q-63-88-117-111.5T264-746q-62%200-104%2043.5T118-600q0%2033%2011.053%2066%2011.052%2033%2032.947%2066-27%201-54.5%2014T57-418ZM-16-16v-66q0-51.258%2049.45-80.629Q82.9-192%20168-192h8q-8%2017-14%2038.792-6%2021.791-6%2050.208v87H-16Zm240%200v-87q0-73.172%2069.895-117.086T479.862-264Q596-264%20666-220.086T736-103v87H224Zm580%200v-87q0-29.37-6-50.62-6-21.25-15-38.38h9q85.2%200%20134.6%2029.371Q976-133.258%20976-82v66H804ZM168.089-236Q134-236%20110-259.886t-24-57.939Q86-352%20109.886-376t57.939-24Q202-400%20226-376.15q24%2023.849%2024%2058.061Q250-284%20226.15-260q-23.849%2024-58.061%2024Zm624%200Q758-236%20734-259.886t-24-57.939Q710-352%20733.886-376t57.939-24Q826-400%20850-376.15q24%2023.849%2024%2058.061Q874-284%20850.15-260q-23.849%2024-58.061%2024ZM480-287q-56.667%200-96.333-39.667Q344-366.333%20344-423q0-56%2039.667-96%2039.666-40%2096.333-40%2056%200%2096%2040t40%2096q0%2056.667-40%2096.333Q536-287%20480-287Z'/%3e%3c/svg%3e";
    balance = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20height='20'%20viewBox='0%20-960%20960%20960'%20width='20'%3e%3cpath%20d='M62-110v-118h359v-421q-19-10-33.5-25T363-708h-99l108%20249q-1%2060.25-47.626%2097.625Q277.747-324%20217.374-324%20157-324%20110-361.375%2063-398.75%2062-459l109-249h-61v-118h252.649Q379-860%20410-879t70-19q39%200%2070%2019t47.351%2052.652H850V-708h-61l109%20248.758q0%2060.492-46.626%2097.867Q804.747-324%20743.874-324%20683-324%20636-361.375%20589-398.75%20588-459l108-249h-99q-10%2019-24.5%2034T539-649v421h359v118H62Zm616-360h130l-65-148-65%20148Zm-525%200h130l-66-148-64%20148Zm327.07-268q11.93%200%2019.93-8.789%208-8.79%208-20.422%200-11.631-7.868-19.71-7.869-8.079-19.5-8.079-11.632%200-20.132%207.868-8.5%207.869-8.5%2019.5Q452-756%20460.07-747q8.07%209%2020%209Z'/%3e%3c/svg%3e";
    css$1 = {
      code: "a.svelte-v8rzmp{text-decoration:none;--highlight:color-mix(in srgb, currentColor 20%, transparent);background:var(--highlight);color:inherit;padding:0.05em 0.15em;border-radius:0.15em;font-weight:bold;display:inline-flex;align-items:center}a.svelte-v8rzmp:focus,a.svelte-v8rzmp:hover{outline:0.15em solid var(--highlight);text-decoration:underline solid}",
      map: null
    };
    Anchor = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { href } = $$props;
      if ($$props.href === void 0 && $$bindings.href && href !== void 0)
        $$bindings.href(href);
      $$result.css.add(css$1);
      return `<a${add_attribute("href", href, 0)} target="_blank" class="svelte-v8rzmp">${slots.default ? slots.default({}) : ``} </a>`;
    });
    ___ASSET___0 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20height='24'%20viewBox='0%20-960%20960%20960'%20width='24'%3e%3cpath%20d='M480-80q-82%200-155-31.5t-127.5-86Q143-252%20111.5-325T80-480q0-83%2031.5-155.5t86-127Q252-817%20325-848.5T480-880q83%200%20155.5%2031.5t127%2086q54.5%2054.5%2086%20127T880-480q0%2082-31.5%20155t-86%20127.5q-54.5%2054.5-127%2086T480-80Zm0-82q26-36%2045-75t31-83H404q12%2044%2031%2083t45%2075Zm-104-16q-18-33-31.5-68.5T322-320H204q29%2050%2072.5%2087t99.5%2055Zm208%200q56-18%2099.5-55t72.5-87H638q-9%2038-22.5%2073.5T584-178ZM170-400h136q-3-20-4.5-39.5T300-480q0-21%201.5-40.5T306-560H170q-5%2020-7.5%2039.5T160-480q0%2021%202.5%2040.5T170-400Zm216%200h188q3-20%204.5-39.5T580-480q0-21-1.5-40.5T574-560H386q-3%2020-4.5%2039.5T380-480q0%2021%201.5%2040.5T386-400Zm268%200h136q5-20%207.5-39.5T800-480q0-21-2.5-40.5T790-560H654q3%2020%204.5%2039.5T660-480q0%2021-1.5%2040.5T654-400Zm-16-240h118q-29-50-72.5-87T584-782q18%2033%2031.5%2068.5T638-640Zm-234%200h152q-12-44-31-83t-45-75q-26%2036-45%2075t-31%2083Zm-200%200h118q9-38%2022.5-73.5T376-782q-56%2018-99.5%2055T204-640Z'/%3e%3c/svg%3e";
    css4 = {
      code: "div.svelte-16idx2j{position:relative}ul.svelte-16idx2j{position:absolute;background:var(--background-color);border:0.1em solid;border-radius:0.7em;transform-origin:top;padding:0.3em;transform:translateX(calc(-100% + 1.25em)) translateY(var(--y));z-index:100;margin:0.15em 0}ul.svelte-16idx2j:dir(rtl){transform:translateX(calc(100% - 1.25em)) translateY(var(--y))}li.svelte-16idx2j{display:block;text-transform:capitalize;padding:0.3em 0.3em;line-height:1.25;white-space:nowrap}a.svelte-16idx2j{color:inherit;text-decoration:none}",
      map: null
    };
    LanguageSwitcher = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let $dialog, $$unsubscribe_dialog;
      let $page, $$unsubscribe_page;
      $$unsubscribe_page = subscribe(page, (value) => $page = value);
      const dialog = Jo();
      $$unsubscribe_dialog = subscribe(dialog, (value) => $dialog = value);
      let { up = false } = $$props;
      if ($$props.up === void 0 && $$bindings.up && up !== void 0)
        $$bindings.up(up);
      $$result.css.add(css4);
      $$unsubscribe_dialog();
      $$unsubscribe_page();
      return `<div class="svelte-16idx2j">${validate_component(IconButton, "IconButton").$$render(
        $$result,
        {
          src: ___ASSET___0,
          alt: language(),
          size: "1em"
        },
        {},
        {}
      )} ${$dialog.expanded ? `<ul class="svelte-16idx2j"${add_styles({ "--y": up ? "calc(-100% - 1.25em)" : "0" })}>${each(availableLanguageTags, (lang) => {
        return `<li class="svelte-16idx2j"><a${add_attribute("href", route($page.url.pathname + $page.url.search, lang), 0)}${add_attribute("hreflang", lang, 0)} class="svelte-16idx2j">${escape(new Intl.DisplayNames([lang], { type: "language" }).of(lang))}</a> </li>`;
      })}</ul>` : ``} </div>`;
    });
    Footer = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      return `<div${add_styles({
        "border-top": `2px solid #00000080`,
        "text-align": `center`,
        "width": `100%`,
        "font-size": `0.75em`,
        "box-sizing": `border-box`,
        "font-family": `Poppins`,
        "padding": `8px`,
        "display": `flex`,
        "align-items": `center`,
        "gap": `10px`,
        "justify-content": `center`,
        "flex-wrap": `wrap`
      })}>${validate_component(Anchor, "Anchor").$$render($$result, { href: route("/credits", languageTag()) }, {}, {
        default: () => {
          return `<div${add_styles({
            "display": `inline-flex`,
            "align-items": `center`,
            "gap": `0.2em`
          })}>${validate_component(Icon, "Icon").$$render(
            $$result,
            {
              src: ___ASSET___2,
              alt: community(),
              size: "1em"
            },
            {},
            {}
          )} <div>${escape(community())}</div></div>`;
        }
      })} ${validate_component(Anchor, "Anchor").$$render(
        $$result,
        {
          href: "https://gitlab.com/opencode-mit/fuiz-website"
        },
        {},
        {
          default: () => {
            return `<div${add_styles({
              "display": `inline-flex`,
              "align-items": `center`,
              "gap": `0.2em`
            })}>${validate_component(Icon, "Icon").$$render(
              $$result,
              {
                src: balance,
                alt: open_source(),
                size: "1em"
              },
              {},
              {}
            )} <div data-svelte-h="svelte-1cel4d5">GNU AGPLv3</div></div>`;
          }
        }
      )} ${validate_component(LanguageSwitcher, "LanguageSwitcher").$$render($$result, { up: true }, {}, {})} ${validate_component(DarkModeSwitcher, "DarkModeSwitcher").$$render($$result, {}, {}, {})}</div>`;
    });
    Logo = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { height = 60 } = $$props;
      if ($$props.height === void 0 && $$bindings.height && height !== void 0)
        $$bindings.height(height);
      return `<div${add_styles({
        "height": `100%`,
        "display": `flex`,
        "aspect-ratio": `11/4`
      })}><div${add_styles({
        "margin-right": `auto`,
        "display": `flex`,
        "width": `auto`,
        "height": `${height}px`
      })}><svg height="100%"></svg></div></div>`;
    });
    Header = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      return `<div${add_styles({
        "display": `flex`,
        "justify-content": `center`
      })}><a${add_attribute("href", route("/", languageTag()), 0)}${add_styles({
        "height": `60px`,
        "overflow": `hidden`,
        "color": `inherit`
      })}>${validate_component(Logo, "Logo").$$render($$result, { height: 60 }, {}, {})}</a></div>`;
    });
  }
});
var ___ASSET___02;
var MediaFallback;
var MediaDisplay;
var MediaContainer;
var init_MediaContainer = __esm({
  ".svelte-kit/output/server/chunks/MediaContainer.js"() {
    init_ssr();
    init_public();
    init_src();
    init_Header();
    ___ASSET___02 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20height='24'%20viewBox='0%20-960%20960%20960'%20width='24'%3e%3cpath%20d='M200-120q-33%200-56.5-23.5T120-200v-560q0-33%2023.5-56.5T200-840h560q33%200%2056.5%2023.5T840-760v560q0%2033-23.5%2056.5T760-120H200Zm0-80h560v-560H200v560Zm40-80h480L570-480%20450-320l-90-120-120%20160Zm-40%2080v-560%20560Z'/%3e%3c/svg%3e";
    MediaFallback = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      return `<div${add_styles({
        "height": `100%`,
        "width": `100%`,
        "display": `flex`,
        "align-items": `center`,
        "justify-content": `center`,
        "opacity": `0.4`
      })}>${validate_component(Icon, "Icon").$$render($$result, { src: ___ASSET___02, alt: "fallback", size: "50%" }, {}, {})}</div>`;
    });
    MediaDisplay = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { media } = $$props;
      let { fit } = $$props;
      if ($$props.media === void 0 && $$bindings.media && media !== void 0)
        $$bindings.media(media);
      if ($$props.fit === void 0 && $$bindings.fit && fit !== void 0)
        $$bindings.fit(fit);
      return `${"Base64" in media.Image ? `${media.Image.Base64.data !== "" ? `<img${add_attribute("alt", media.Image.Base64.alt, 0)}${add_attribute("src", media.Image.Base64.data, 0)}${add_styles({
        "display": `flex`,
        "height": `100%`,
        "width": `100%`,
        "object-fit": fit
      })}>` : `${validate_component(MediaFallback, "MediaFallback").$$render($$result, {}, {}, {})}`}` : `${"Corkboard" in media.Image ? `<img${add_attribute("src", PUBLIC_CORKBOARD_URL + "/get/" + media.Image.Corkboard.id, 0)}${add_attribute("alt", media.Image.Corkboard.alt, 0)}${add_styles({
        "display": `flex`,
        "height": `100%`,
        "width": `100%`,
        "object-fit": fit
      })}>` : `${"Url" in media.Image ? `<div${add_styles({
        "display": `flex`,
        "height": `100%`,
        "width": `100%`,
        "align-items": `center`,
        "justify-content": `center`
      })}><svg></svg></div>` : ``}`}`}`;
    });
    MediaContainer = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { media } = $$props;
      let { align = "center" } = $$props;
      let { fit = "scale-down" } = $$props;
      let { showFallback = true } = $$props;
      if ($$props.media === void 0 && $$bindings.media && media !== void 0)
        $$bindings.media(media);
      if ($$props.align === void 0 && $$bindings.align && align !== void 0)
        $$bindings.align(align);
      if ($$props.fit === void 0 && $$bindings.fit && fit !== void 0)
        $$bindings.fit(fit);
      if ($$props.showFallback === void 0 && $$bindings.showFallback && showFallback !== void 0)
        $$bindings.showFallback(showFallback);
      return `<div${add_styles({
        "max-height": `100%`,
        "display": `flex`,
        "justify-content": align,
        "max-width": `100%`,
        "margin": `auto`,
        "position": `absolute`,
        "inset": `0`
      })}>${media ? `${validate_component(MediaDisplay, "MediaDisplay").$$render($$result, { media, fit }, {}, {})}` : `${showFallback ? `${validate_component(MediaFallback, "MediaFallback").$$render($$result, {}, {}, {})}` : ``}`}</div>`;
    });
  }
});
var heart2;
var club2;
var spade2;
var diamond2;
var css5;
var NiceBackground;
var init_NiceBackground = __esm({
  ".svelte-kit/output/server/chunks/NiceBackground.js"() {
    init_ssr();
    init_LanguageSwitcher_svelte_svelte_type_style_lang();
    heart2 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2024%2024'%3e%3cpath%20d='M12,21.35L10.55,20.03C5.4,15.36%202,12.27%202,8.5C2,5.41%204.42,3%207.5,3C9.24,3%2010.91,3.81%2012,5.08C13.09,3.81%2014.76,3%2016.5,3C19.58,3%2022,5.41%2022,8.5C22,12.27%2018.6,15.36%2013.45,20.03L12,21.35Z'%20/%3e%3c/svg%3e";
    club2 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2024%2024'%3e%3cpath%20d='M12,2C14.3,2%2016.3,4%2016.3,6.2C16.21,8.77%2014.34,9.83%2014.04,10C15.04,9.5%2016.5,9.5%2016.5,9.5C19,9.5%2021,11.3%2021,13.8C21,16.3%2019,18%2016.5,18C16.5,18%2015,18%2013,17C13,17%2012.7,19%2015,22H9C11.3,19%2011,17%2011,17C9,18%207.5,18%207.5,18C5,18%203,16.3%203,13.8C3,11.3%205,9.5%207.5,9.5C7.5,9.5%208.96,9.5%209.96,10C9.66,9.83%207.79,8.77%207.7,6.2C7.7,4%209.7,2%2012,2Z'%20/%3e%3c/svg%3e";
    spade2 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2024%2024'%3e%3cpath%20d='M12,2C9,7%204,9%204,14C4,16%206,18%208,18C9,18%2010,18%2011,17C11,17%2011.32,19%209,22H15C13,19%2013,17%2013,17C14,18%2015,18%2016,18C18,18%2020,16%2020,14C20,9%2015,7%2012,2Z'%20/%3e%3c/svg%3e";
    diamond2 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2024%2024'%3e%3cpath%20d='M19,12L12,22L5,12L12,2'%20/%3e%3c/svg%3e";
    css5 = {
      code: ".card-container.svelte-508roa.svelte-508roa{height:50%;width:50%}.card-container.svelte-508roa img.svelte-508roa{height:100%;width:100%}",
      map: null
    };
    NiceBackground = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      $$result.css.add(css5);
      return `<div${add_styles({ "height": `100%`, "position": `relative` })}><div${add_styles({
        "background-color": `var(--background-color)`,
        "inset": `0`,
        "position": `absolute`,
        "overflow": `hidden`,
        "z-index": `-1`
      })}><div${add_styles({ "height": `100%`, "width": `100%` })}><div${add_styles({
        "display": `flex`,
        "flex-wrap": `wrap`,
        "opacity": `20%`,
        "height": `100%`
      })}><div class="card-container svelte-508roa"${add_styles({
        "transform": `scale(1) translate(0%, 0%) rotate(0.0625turn)`
      })} data-svelte-h="svelte-16rol6c"><img${add_attribute("src", heart2, 0)}${add_attribute("alt", heart(), 0)} class="svelte-508roa"></div> <div class="card-container svelte-508roa"${add_styles({
        "transform": `scale(1) translate(0%, -25%) rotate(-0.0625turn)`
      })} data-svelte-h="svelte-131f983"><img${add_attribute("src", diamond2, 0)}${add_attribute("alt", diamond(), 0)} class="svelte-508roa"></div> <div class="card-container svelte-508roa"${add_styles({
        "transform": `scale(1) translate(-25%, 12.5%) rotate(-0.0625turn)`
      })} data-svelte-h="svelte-1oxc3sl"><img${add_attribute("src", spade2, 0)}${add_attribute("alt", spade(), 0)} class="svelte-508roa"></div> <div class="card-container svelte-508roa"${add_styles({
        "transform": `scale(1) translate(12.5%, -25%) rotate(0.0625turn)`
      })} data-svelte-h="svelte-11t433c"><img${add_attribute("src", club2, 0)}${add_attribute("alt", club(), 0)} class="svelte-508roa"></div></div></div></div> <div${add_styles({ "height": `100%` })}>${slots.default ? slots.default({}) : ``}</div> </div>`;
    });
  }
});
var TypicalPage;
var init_TypicalPage = __esm({
  ".svelte-kit/output/server/chunks/TypicalPage.js"() {
    init_ssr();
    init_NiceBackground();
    init_Header();
    TypicalPage = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      return `${validate_component(NiceBackground, "NiceBackground").$$render($$result, {}, {}, {
        default: () => {
          return `<div${add_styles({
            "height": `100%`,
            "display": `flex`,
            "flex-direction": `column`,
            "padding": `0.5em 0.5em 0`,
            "gap": `0.5em`,
            "align-items": `center`,
            "box-sizing": `border-box`
          })}><header>${validate_component(Header, "Header").$$render($$result, {}, {}, {})}</header> <section${add_styles({ "flex": `1`, "width": `100%` })}>${slots.default ? slots.default({}) : ``}</section> <footer>${validate_component(Footer, "Footer").$$render($$result, {}, {}, {})}</footer></div>`;
        }
      })}`;
    });
  }
});
var page_svelte_exports = {};
__export(page_svelte_exports, {
  default: () => Page
});
function altFromImage(media) {
  if ("Base64" in media.Image) {
    return media.Image.Base64.alt;
  } else if ("Corkboard" in media.Image) {
    return media.Image.Corkboard.alt;
  } else if ("Url" in media.Image) {
    return media.Image.Url.alt;
  } else {
    return "";
  }
}
var FuizDisplay;
var Page;
var init_page_svelte = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/admin/_page.svelte.js"() {
    init_ssr();
    init_LanguageSwitcher_svelte_svelte_type_style_lang();
    init_FancyButton();
    init_Textarea();
    init_MediaContainer();
    init_TypicalPage();
    FuizDisplay = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { fuiz } = $$props;
      if ($$props.fuiz === void 0 && $$bindings.fuiz && fuiz !== void 0)
        $$bindings.fuiz(fuiz);
      return `<dl><dt>${escape(author())}</dt><dd>${escape(fuiz.author)}</dd><dt>${escape(tags())}</dt><dd>${escape(fuiz.tags.join(", "))}</dd><dt>${escape(fuiz_title())}</dt><dd>${escape(fuiz.config.title)}</dd>${each(fuiz.config.slides, (slide) => {
        let { title, introduce_question, time_limit: time_limit$12, media, answers: answers$12 } = slide.MultipleChoice;
        return ` <dt>${escape(multiple_choice())}</dt> <dd><dl><dt>${escape(question_text())}</dt><dd>${escape(title)}</dd>${media ? `<dt>${escape(Object.getOwnPropertyNames(media.Image))} Image</dt> <dd${add_styles({ "height": `5em`, "position": `relative` })}>${validate_component(MediaContainer, "MediaContainer").$$render($$result, { media }, {}, {})}</dd> <dt>${escape(image_alt())}</dt> <dd>${escape(altFromImage(media))}</dd>` : ``}<dt>${escape(answers())}</dt><dd><ol>${each(answers$12, (answer) => {
          return `<li>${escape(answer.content.Text)}, ${escape(answer.correct ? correct() : wrong())}</li>`;
        })}</ol> </dd><dt>${escape(time_before_answers())}</dt><dd>${escape(introduce_question)}ms</dd><dt>${escape(time_limit())}</dt><dd>${escape(time_limit$12)}ms</dd></dl> </dd>`;
      })}</dl>`;
    });
    Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let previousFuiz;
      let fuiz;
      let { data } = $$props;
      if ($$props.data === void 0 && $$bindings.data && data !== void 0)
        $$bindings.data(data);
      previousFuiz = data.previous_fuiz;
      fuiz = data.fuiz;
      return `${validate_component(TypicalPage, "TypicalPage").$$render($$result, {}, {}, {
        default: () => {
          return `${fuiz ? `${validate_component(FuizDisplay, "FuizDisplay").$$render($$result, { fuiz }, {}, {})} ${previousFuiz ? `<p>${escape(this_replaces())}</p> ${validate_component(FuizDisplay, "FuizDisplay").$$render($$result, { fuiz: previousFuiz }, {}, {})}` : ``} <div${add_styles({
            "display": `flex`,
            "flex-direction": `column`,
            "gap": `0.5em`
          })}><form method="POST" action="?/approve">${validate_component(FancyButton, "FancyButton").$$render($$result, {}, {}, {
            default: () => {
              return `${escape(approve())}`;
            }
          })}</form> <form method="POST" action="?/reject">${validate_component(Textarea, "Textarea").$$render(
            $$result,
            {
              id: "reason",
              placeholder: reason(),
              required: true,
              disabled: false,
              value: ""
            },
            {},
            {}
          )} <div>${validate_component(FancyButton, "FancyButton").$$render($$result, {}, {}, {
            default: () => {
              return `${escape(reject())}`;
            }
          })}</div></form></div>` : `${escape(nothing())}`}`;
        }
      })}`;
    });
  }
});
var __exports5 = {};
__export(__exports5, {
  component: () => component5,
  fonts: () => fonts5,
  imports: () => imports5,
  index: () => index5,
  server: () => page_server_ts_exports,
  server_id: () => server_id2,
  stylesheets: () => stylesheets5,
  universal: () => page_ts_exports,
  universal_id: () => universal_id4
});
var index5;
var component_cache5;
var component5;
var universal_id4;
var server_id2;
var imports5;
var stylesheets5;
var fonts5;
var init__5 = __esm({
  ".svelte-kit/output/server/nodes/6.js"() {
    init_page_ts();
    init_page_server_ts();
    index5 = 6;
    component5 = async () => component_cache5 ??= (await Promise.resolve().then(() => (init_page_svelte(), page_svelte_exports))).default;
    universal_id4 = "src/routes/[[lang]]/admin/+page.ts";
    server_id2 = "src/routes/[[lang]]/admin/+page.server.ts";
    imports5 = ["_app/immutable/nodes/6.DTs2WEmN.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js", "_app/immutable/chunks/Header.lUXZAEz6.js", "_app/immutable/chunks/runtime.CM0qr2RF.js", "_app/immutable/chunks/i18n-routing.GhjsszfS.js", "_app/immutable/chunks/stores.C6La04jU.js", "_app/immutable/chunks/entry.Dv6jx6u8.js", "_app/immutable/chunks/MediaDisplay.BQa7FbJF.js", "_app/immutable/chunks/public.Bp7fU829.js", "_app/immutable/chunks/Textarea.DR5b5Gnl.js", "_app/immutable/chunks/MediaContainer.BEwKe0F-.js", "_app/immutable/chunks/TypicalPage._YIdHS8V.js", "_app/immutable/chunks/NiceBackground.CNQfe3-4.js"];
    stylesheets5 = ["_app/immutable/assets/Header.Cih96lxr.css", "_app/immutable/assets/MediaDisplay.DYPbcJCr.css", "_app/immutable/assets/Textarea.BgCR4nEh.css", "_app/immutable/assets/NiceBackground.D5oVBkOJ.css"];
    fonts5 = [];
  }
});
var page_server_ts_exports2 = {};
__export(page_server_ts_exports2, {
  load: () => load3
});
var load3;
var init_page_server_ts2 = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/library/_page.server.ts.js"() {
    init_serverOnlyUtils();
    load3 = async ({ platform }) => {
      console.log("load.....", platform);
      const mostPlayed = ((await platform?.env.DATABASE.prepare(
        "SELECT * FROM approved_submissions ORDER BY played_count DESC LIMIT 8"
      ).all())?.results || []).map(fixPublish);
      const recentlyPublished = ((await platform?.env.DATABASE.prepare(
        "SELECT * FROM approved_submissions ORDER BY last_updated DESC LIMIT 8"
      ).all())?.results || []).map(fixPublish);
      return {
        mostPlayed,
        recentlyPublished
      };
    };
  }
});
var page_svelte_exports2 = {};
__export(page_svelte_exports2, {
  default: () => Page2
});
var ___ASSET___03;
var css$12;
var OnlinePublised;
var css6;
var Listing;
var Page2;
var init_page_svelte2 = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/library/_page.svelte.js"() {
    init_ssr();
    init_TypicalPage();
    init_LanguageSwitcher_svelte_svelte_type_style_lang();
    init_Header();
    ___ASSET___03 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2024%2024'%3e%3cpath%20d='M12,2A9,9%200%200,0%203,11V22L6,19L9,22L12,19L15,22L18,19L21,22V11A9,9%200%200,0%2012,2M9,8A2,2%200%200,1%2011,10A2,2%200%200,1%209,12A2,2%200%200,1%207,10A2,2%200%200,1%209,8M15,8A2,2%200%200,1%2017,10A2,2%200%200,1%2015,12A2,2%200%200,1%2013,10A2,2%200%200,1%2015,8Z'%20/%3e%3c/svg%3e";
    css$12 = {
      code: ".container.svelte-1ye48dx{border:0.15em solid;border-radius:0.7em;max-width:20ch;display:flex;flex-direction:column;overflow:hidden;background:var(--background-color);aspect-ratio:4/3}.image-container.svelte-1ye48dx{flex:1;overflow:hidden;display:flex;border-bottom:0.15em solid}img.svelte-1ye48dx{width:100%;height:auto;flex:1;object-fit:cover}.info.svelte-1ye48dx{padding:0.15em}.little.svelte-1ye48dx{font-size:0.75em}",
      map: null
    };
    OnlinePublised = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { data } = $$props;
      if ($$props.data === void 0 && $$bindings.data && data !== void 0)
        $$bindings.data(data);
      $$result.css.add(css$12);
      return `<div class="container svelte-1ye48dx"><div class="image-container svelte-1ye48dx">${data.thumbnail ? `<img${add_attribute("src", data.thumbnail, 0)}${add_attribute("alt", data.alt, 0)} class="svelte-1ye48dx">` : ``}</div> <div class="info svelte-1ye48dx">${escape(data.title)} <div class="little svelte-1ye48dx">${escape(author_played_times({
        author: data.author,
        times: data.played_count
      }))}</div></div> </div>`;
    });
    css6 = {
      code: ".grid.svelte-l59aev{display:grid;grid-template-columns:repeat(auto-fit, minmax(15ch, 1fr));grid-auto-rows:1fr;grid-gap:0.4em}h2.svelte-l59aev{margin:0.2em 0;font-family:'Truculenta'}.section.svelte-l59aev{margin-bottom:0.8em}a.svelte-l59aev{color:inherit;text-decoration:inherit}",
      map: null
    };
    Listing = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { recentlyPublished, mostPlayed } = $$props;
      if ($$props.recentlyPublished === void 0 && $$bindings.recentlyPublished && recentlyPublished !== void 0)
        $$bindings.recentlyPublished(recentlyPublished);
      if ($$props.mostPlayed === void 0 && $$bindings.mostPlayed && mostPlayed !== void 0)
        $$bindings.mostPlayed(mostPlayed);
      $$result.css.add(css6);
      return `<div class="section svelte-l59aev"><h2 class="svelte-l59aev">${escape(recently_published())}</h2> <div class="grid svelte-l59aev">${recentlyPublished.length ? each(recentlyPublished, (fuiz) => {
        return `<a href="${"library/public/" + escape(fuiz.id, true)}" class="svelte-l59aev">${validate_component(OnlinePublised, "OnlinePublised").$$render($$result, { data: fuiz }, {}, {})} </a>`;
      }) : `<div${add_styles({
        "display": `flex`,
        "flex-direction": `column`,
        "opacity": `0.7`,
        "align-items": `center`
      })}>${validate_component(Icon, "Icon").$$render(
        $$result,
        {
          src: ___ASSET___03,
          size: "min(20vh, 60vw)",
          alt: nothing()
        },
        {},
        {}
      )} ${escape(nothing())} </div>`}</div></div> <div class="section svelte-l59aev"><h2 class="svelte-l59aev">${escape(most_published())}</h2> <div class="grid svelte-l59aev">${mostPlayed.length ? each(mostPlayed, (fuiz) => {
        return `<a href="${"library/public/" + escape(fuiz.id, true)}" class="svelte-l59aev">${validate_component(OnlinePublised, "OnlinePublised").$$render($$result, { data: fuiz }, {}, {})} </a>`;
      }) : `<div${add_styles({
        "display": `flex`,
        "flex-direction": `column`,
        "opacity": `0.7`,
        "align-items": `center`
      })}>${validate_component(Icon, "Icon").$$render(
        $$result,
        {
          src: ___ASSET___03,
          size: "min(20vh, 60vw)",
          alt: nothing()
        },
        {},
        {}
      )} ${escape(nothing())} </div>`}</div> </div>`;
    });
    Page2 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { data } = $$props;
      if ($$props.data === void 0 && $$bindings.data && data !== void 0)
        $$bindings.data(data);
      return `${validate_component(TypicalPage, "TypicalPage").$$render($$result, {}, {}, {
        default: () => {
          let { mostPlayed, recentlyPublished } = data;
          return `${validate_component(Listing, "Listing").$$render($$result, { mostPlayed, recentlyPublished }, {}, {})}`;
        }
      })}`;
    });
  }
});
var __exports6 = {};
__export(__exports6, {
  component: () => component6,
  fonts: () => fonts6,
  imports: () => imports6,
  index: () => index6,
  server: () => page_server_ts_exports2,
  server_id: () => server_id3,
  stylesheets: () => stylesheets6
});
var index6;
var component_cache6;
var component6;
var server_id3;
var imports6;
var stylesheets6;
var fonts6;
var init__6 = __esm({
  ".svelte-kit/output/server/nodes/10.js"() {
    init_page_server_ts2();
    index6 = 10;
    component6 = async () => component_cache6 ??= (await Promise.resolve().then(() => (init_page_svelte2(), page_svelte_exports2))).default;
    server_id3 = "src/routes/[[lang]]/library/+page.server.ts";
    imports6 = ["_app/immutable/nodes/10.BliigyP2.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js", "_app/immutable/chunks/TypicalPage._YIdHS8V.js", "_app/immutable/chunks/NiceBackground.CNQfe3-4.js", "_app/immutable/chunks/Header.lUXZAEz6.js", "_app/immutable/chunks/runtime.CM0qr2RF.js", "_app/immutable/chunks/i18n-routing.GhjsszfS.js", "_app/immutable/chunks/stores.C6La04jU.js", "_app/immutable/chunks/entry.Dv6jx6u8.js", "_app/immutable/chunks/ghost.Rtn9S435.js"];
    stylesheets6 = ["_app/immutable/assets/10.3iNKa_sD.css", "_app/immutable/assets/NiceBackground.D5oVBkOJ.css", "_app/immutable/assets/Header.Cih96lxr.css"];
    fonts6 = [];
  }
});
var page_server_ts_exports3 = {};
__export(page_server_ts_exports3, {
  load: () => load4
});
var load4;
var init_page_server_ts3 = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/library/public/_id_/_page.server.ts.js"() {
    init_chunks();
    init_serverOnlyUtils();
    init_j_toml();
    load4 = async ({ params, platform }) => {
      const published = await platform?.env.DATABASE.prepare("SELECT * FROM approved_submissions WHERE id = ?1").bind(parseInt(params.id)).first() || void 0;
      if (!published) {
        error(404, "fuiz was not found");
      }
      const fuiz = fixPublish(published);
      const { config } = parse$1(await (await fetch(fuiz.public_url)).text(), {
        bigint: false
      });
      return {
        fuiz,
        config: structuredClone(config)
      };
    };
  }
});
var require_object_hash = __commonJS({
  "node_modules/object-hash/dist/object_hash.js"(exports, module) {
    !function(e3) {
      var t2;
      "object" == typeof exports ? module.exports = e3() : "function" == typeof define && define.amd ? define(e3) : ("undefined" != typeof window ? t2 = window : "undefined" != typeof global ? t2 = global : "undefined" != typeof self && (t2 = self), t2.objectHash = e3());
    }(function() {
      return function r3(o2, i, u2) {
        function s3(n2, e4) {
          if (!i[n2]) {
            if (!o2[n2]) {
              var t2 = "function" == typeof __require22 && __require22;
              if (!e4 && t2)
                return t2(n2, true);
              if (a)
                return a(n2, true);
              throw new Error("Cannot find module '" + n2 + "'");
            }
            e4 = i[n2] = { exports: {} };
            o2[n2][0].call(e4.exports, function(e5) {
              var t3 = o2[n2][1][e5];
              return s3(t3 || e5);
            }, e4, e4.exports, r3, o2, i, u2);
          }
          return i[n2].exports;
        }
        for (var a = "function" == typeof __require22 && __require22, e3 = 0; e3 < u2.length; e3++)
          s3(u2[e3]);
        return s3;
      }({ 1: [function(w, b, m) {
        !function(e3, n2, s3, c2, d2, h2, p, g2, y) {
          "use strict";
          var r3 = w("crypto");
          function t2(e4, t3) {
            t3 = u2(e4, t3);
            var n3;
            return void 0 === (n3 = "passthrough" !== t3.algorithm ? r3.createHash(t3.algorithm) : new l()).write && (n3.write = n3.update, n3.end = n3.update), f(t3, n3).dispatch(e4), n3.update || n3.end(""), n3.digest ? n3.digest("buffer" === t3.encoding ? void 0 : t3.encoding) : (e4 = n3.read(), "buffer" !== t3.encoding ? e4.toString(t3.encoding) : e4);
          }
          (m = b.exports = t2).sha1 = function(e4) {
            return t2(e4);
          }, m.keys = function(e4) {
            return t2(e4, { excludeValues: true, algorithm: "sha1", encoding: "hex" });
          }, m.MD5 = function(e4) {
            return t2(e4, { algorithm: "md5", encoding: "hex" });
          }, m.keysMD5 = function(e4) {
            return t2(e4, { algorithm: "md5", encoding: "hex", excludeValues: true });
          };
          var o2 = r3.getHashes ? r3.getHashes().slice() : ["sha1", "md5"], i = (o2.push("passthrough"), ["buffer", "hex", "binary", "base64"]);
          function u2(e4, t3) {
            var n3 = {};
            if (n3.algorithm = (t3 = t3 || {}).algorithm || "sha1", n3.encoding = t3.encoding || "hex", n3.excludeValues = !!t3.excludeValues, n3.algorithm = n3.algorithm.toLowerCase(), n3.encoding = n3.encoding.toLowerCase(), n3.ignoreUnknown = true === t3.ignoreUnknown, n3.respectType = false !== t3.respectType, n3.respectFunctionNames = false !== t3.respectFunctionNames, n3.respectFunctionProperties = false !== t3.respectFunctionProperties, n3.unorderedArrays = true === t3.unorderedArrays, n3.unorderedSets = false !== t3.unorderedSets, n3.unorderedObjects = false !== t3.unorderedObjects, n3.replacer = t3.replacer || void 0, n3.excludeKeys = t3.excludeKeys || void 0, void 0 === e4)
              throw new Error("Object argument required.");
            for (var r4 = 0; r4 < o2.length; ++r4)
              o2[r4].toLowerCase() === n3.algorithm.toLowerCase() && (n3.algorithm = o2[r4]);
            if (-1 === o2.indexOf(n3.algorithm))
              throw new Error('Algorithm "' + n3.algorithm + '"  not supported. supported values: ' + o2.join(", "));
            if (-1 === i.indexOf(n3.encoding) && "passthrough" !== n3.algorithm)
              throw new Error('Encoding "' + n3.encoding + '"  not supported. supported values: ' + i.join(", "));
            return n3;
          }
          function a(e4) {
            if ("function" == typeof e4)
              return null != /^function\s+\w*\s*\(\s*\)\s*{\s+\[native code\]\s+}$/i.exec(Function.prototype.toString.call(e4));
          }
          function f(o3, t3, i2) {
            i2 = i2 || [];
            function u3(e4) {
              return t3.update ? t3.update(e4, "utf8") : t3.write(e4, "utf8");
            }
            return { dispatch: function(e4) {
              return this["_" + (null === (e4 = o3.replacer ? o3.replacer(e4) : e4) ? "null" : typeof e4)](e4);
            }, _object: function(t4) {
              var n3, e4 = Object.prototype.toString.call(t4), r4 = /\[object (.*)\]/i.exec(e4);
              r4 = (r4 = r4 ? r4[1] : "unknown:[" + e4 + "]").toLowerCase();
              if (0 <= (e4 = i2.indexOf(t4)))
                return this.dispatch("[CIRCULAR:" + e4 + "]");
              if (i2.push(t4), void 0 !== s3 && s3.isBuffer && s3.isBuffer(t4))
                return u3("buffer:"), u3(t4);
              if ("object" === r4 || "function" === r4 || "asyncfunction" === r4)
                return e4 = Object.keys(t4), o3.unorderedObjects && (e4 = e4.sort()), false === o3.respectType || a(t4) || e4.splice(0, 0, "prototype", "__proto__", "constructor"), o3.excludeKeys && (e4 = e4.filter(function(e5) {
                  return !o3.excludeKeys(e5);
                })), u3("object:" + e4.length + ":"), n3 = this, e4.forEach(function(e5) {
                  n3.dispatch(e5), u3(":"), o3.excludeValues || n3.dispatch(t4[e5]), u3(",");
                });
              if (!this["_" + r4]) {
                if (o3.ignoreUnknown)
                  return u3("[" + r4 + "]");
                throw new Error('Unknown object type "' + r4 + '"');
              }
              this["_" + r4](t4);
            }, _array: function(e4, t4) {
              t4 = void 0 !== t4 ? t4 : false !== o3.unorderedArrays;
              var n3 = this;
              if (u3("array:" + e4.length + ":"), !t4 || e4.length <= 1)
                return e4.forEach(function(e5) {
                  return n3.dispatch(e5);
                });
              var r4 = [], t4 = e4.map(function(e5) {
                var t5 = new l(), n4 = i2.slice();
                return f(o3, t5, n4).dispatch(e5), r4 = r4.concat(n4.slice(i2.length)), t5.read().toString();
              });
              return i2 = i2.concat(r4), t4.sort(), this._array(t4, false);
            }, _date: function(e4) {
              return u3("date:" + e4.toJSON());
            }, _symbol: function(e4) {
              return u3("symbol:" + e4.toString());
            }, _error: function(e4) {
              return u3("error:" + e4.toString());
            }, _boolean: function(e4) {
              return u3("bool:" + e4.toString());
            }, _string: function(e4) {
              u3("string:" + e4.length + ":"), u3(e4.toString());
            }, _function: function(e4) {
              u3("fn:"), a(e4) ? this.dispatch("[native]") : this.dispatch(e4.toString()), false !== o3.respectFunctionNames && this.dispatch("function-name:" + String(e4.name)), o3.respectFunctionProperties && this._object(e4);
            }, _number: function(e4) {
              return u3("number:" + e4.toString());
            }, _xml: function(e4) {
              return u3("xml:" + e4.toString());
            }, _null: function() {
              return u3("Null");
            }, _undefined: function() {
              return u3("Undefined");
            }, _regexp: function(e4) {
              return u3("regex:" + e4.toString());
            }, _uint8array: function(e4) {
              return u3("uint8array:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _uint8clampedarray: function(e4) {
              return u3("uint8clampedarray:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _int8array: function(e4) {
              return u3("int8array:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _uint16array: function(e4) {
              return u3("uint16array:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _int16array: function(e4) {
              return u3("int16array:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _uint32array: function(e4) {
              return u3("uint32array:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _int32array: function(e4) {
              return u3("int32array:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _float32array: function(e4) {
              return u3("float32array:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _float64array: function(e4) {
              return u3("float64array:"), this.dispatch(Array.prototype.slice.call(e4));
            }, _arraybuffer: function(e4) {
              return u3("arraybuffer:"), this.dispatch(new Uint8Array(e4));
            }, _url: function(e4) {
              return u3("url:" + e4.toString());
            }, _map: function(e4) {
              u3("map:");
              e4 = Array.from(e4);
              return this._array(e4, false !== o3.unorderedSets);
            }, _set: function(e4) {
              u3("set:");
              e4 = Array.from(e4);
              return this._array(e4, false !== o3.unorderedSets);
            }, _file: function(e4) {
              return u3("file:"), this.dispatch([e4.name, e4.size, e4.type, e4.lastModfied]);
            }, _blob: function() {
              if (o3.ignoreUnknown)
                return u3("[blob]");
              throw Error('Hashing Blob objects is currently not supported\n(see https://github.com/puleos/object-hash/issues/26)\nUse "options.replacer" or "options.ignoreUnknown"\n');
            }, _domwindow: function() {
              return u3("domwindow");
            }, _bigint: function(e4) {
              return u3("bigint:" + e4.toString());
            }, _process: function() {
              return u3("process");
            }, _timer: function() {
              return u3("timer");
            }, _pipe: function() {
              return u3("pipe");
            }, _tcp: function() {
              return u3("tcp");
            }, _udp: function() {
              return u3("udp");
            }, _tty: function() {
              return u3("tty");
            }, _statwatcher: function() {
              return u3("statwatcher");
            }, _securecontext: function() {
              return u3("securecontext");
            }, _connection: function() {
              return u3("connection");
            }, _zlib: function() {
              return u3("zlib");
            }, _context: function() {
              return u3("context");
            }, _nodescript: function() {
              return u3("nodescript");
            }, _httpparser: function() {
              return u3("httpparser");
            }, _dataview: function() {
              return u3("dataview");
            }, _signal: function() {
              return u3("signal");
            }, _fsevent: function() {
              return u3("fsevent");
            }, _tlswrap: function() {
              return u3("tlswrap");
            } };
          }
          function l() {
            return { buf: "", write: function(e4) {
              this.buf += e4;
            }, end: function(e4) {
              this.buf += e4;
            }, read: function() {
              return this.buf;
            } };
          }
          m.writeToStream = function(e4, t3, n3) {
            return void 0 === n3 && (n3 = t3, t3 = {}), f(t3 = u2(e4, t3), n3).dispatch(e4);
          };
        }.call(this, w("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, w("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/fake_9a5aa49d.js", "/");
      }, { buffer: 3, crypto: 5, lYpoI2: 11 }], 2: [function(e3, t2, f) {
        !function(e4, t3, n2, r3, o2, i, u2, s3, a) {
          !function(e5) {
            "use strict";
            var a2 = "undefined" != typeof Uint8Array ? Uint8Array : Array, t4 = "+".charCodeAt(0), n3 = "/".charCodeAt(0), r4 = "0".charCodeAt(0), o3 = "a".charCodeAt(0), i2 = "A".charCodeAt(0), u3 = "-".charCodeAt(0), s4 = "_".charCodeAt(0);
            function f2(e6) {
              e6 = e6.charCodeAt(0);
              return e6 === t4 || e6 === u3 ? 62 : e6 === n3 || e6 === s4 ? 63 : e6 < r4 ? -1 : e6 < r4 + 10 ? e6 - r4 + 26 + 26 : e6 < i2 + 26 ? e6 - i2 : e6 < o3 + 26 ? e6 - o3 + 26 : void 0;
            }
            e5.toByteArray = function(e6) {
              var t5, n4;
              if (0 < e6.length % 4)
                throw new Error("Invalid string. Length must be a multiple of 4");
              var r5 = e6.length, r5 = "=" === e6.charAt(r5 - 2) ? 2 : "=" === e6.charAt(r5 - 1) ? 1 : 0, o4 = new a2(3 * e6.length / 4 - r5), i3 = 0 < r5 ? e6.length - 4 : e6.length, u4 = 0;
              function s5(e7) {
                o4[u4++] = e7;
              }
              for (t5 = 0; t5 < i3; t5 += 4, 0)
                s5((16711680 & (n4 = f2(e6.charAt(t5)) << 18 | f2(e6.charAt(t5 + 1)) << 12 | f2(e6.charAt(t5 + 2)) << 6 | f2(e6.charAt(t5 + 3)))) >> 16), s5((65280 & n4) >> 8), s5(255 & n4);
              return 2 == r5 ? s5(255 & (n4 = f2(e6.charAt(t5)) << 2 | f2(e6.charAt(t5 + 1)) >> 4)) : 1 == r5 && (s5((n4 = f2(e6.charAt(t5)) << 10 | f2(e6.charAt(t5 + 1)) << 4 | f2(e6.charAt(t5 + 2)) >> 2) >> 8 & 255), s5(255 & n4)), o4;
            }, e5.fromByteArray = function(e6) {
              var t5, n4, r5, o4, i3 = e6.length % 3, u4 = "";
              function s5(e7) {
                return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(e7);
              }
              for (t5 = 0, r5 = e6.length - i3; t5 < r5; t5 += 3)
                n4 = (e6[t5] << 16) + (e6[t5 + 1] << 8) + e6[t5 + 2], u4 += s5((o4 = n4) >> 18 & 63) + s5(o4 >> 12 & 63) + s5(o4 >> 6 & 63) + s5(63 & o4);
              switch (i3) {
                case 1:
                  u4 = (u4 += s5((n4 = e6[e6.length - 1]) >> 2)) + s5(n4 << 4 & 63) + "==";
                  break;
                case 2:
                  u4 = (u4 = (u4 += s5((n4 = (e6[e6.length - 2] << 8) + e6[e6.length - 1]) >> 10)) + s5(n4 >> 4 & 63)) + s5(n4 << 2 & 63) + "=";
              }
              return u4;
            };
          }(void 0 === f ? this.base64js = {} : f);
        }.call(this, e3("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e3("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/base64-js/lib/b64.js", "/node_modules/gulp-browserify/node_modules/base64-js/lib");
      }, { buffer: 3, lYpoI2: 11 }], 3: [function(O2, e3, H2) {
        !function(e4, n2, f, r3, h2, p, g2, y, w) {
          var a = O2("base64-js"), i = O2("ieee754");
          function f(e5, t3, n3) {
            if (!(this instanceof f))
              return new f(e5, t3, n3);
            var r4, o3, i2, u3, s4 = typeof e5;
            if ("base64" === t3 && "string" == s4)
              for (e5 = (u3 = e5).trim ? u3.trim() : u3.replace(/^\s+|\s+$/g, ""); e5.length % 4 != 0; )
                e5 += "=";
            if ("number" == s4)
              r4 = j2(e5);
            else if ("string" == s4)
              r4 = f.byteLength(e5, t3);
            else {
              if ("object" != s4)
                throw new Error("First argument needs to be a number, array or string.");
              r4 = j2(e5.length);
            }
            if (f._useTypedArrays ? o3 = f._augment(new Uint8Array(r4)) : ((o3 = this).length = r4, o3._isBuffer = true), f._useTypedArrays && "number" == typeof e5.byteLength)
              o3._set(e5);
            else if (C2(u3 = e5) || f.isBuffer(u3) || u3 && "object" == typeof u3 && "number" == typeof u3.length)
              for (i2 = 0; i2 < r4; i2++)
                f.isBuffer(e5) ? o3[i2] = e5.readUInt8(i2) : o3[i2] = e5[i2];
            else if ("string" == s4)
              o3.write(e5, 0, t3);
            else if ("number" == s4 && !f._useTypedArrays && !n3)
              for (i2 = 0; i2 < r4; i2++)
                o3[i2] = 0;
            return o3;
          }
          function b(e5, t3, n3, r4) {
            return f._charsWritten = c2(function(e6) {
              for (var t4 = [], n4 = 0; n4 < e6.length; n4++)
                t4.push(255 & e6.charCodeAt(n4));
              return t4;
            }(t3), e5, n3, r4);
          }
          function m(e5, t3, n3, r4) {
            return f._charsWritten = c2(function(e6) {
              for (var t4, n4, r5 = [], o3 = 0; o3 < e6.length; o3++)
                n4 = e6.charCodeAt(o3), t4 = n4 >> 8, n4 = n4 % 256, r5.push(n4), r5.push(t4);
              return r5;
            }(t3), e5, n3, r4);
          }
          function v(e5, t3, n3) {
            var r4 = "";
            n3 = Math.min(e5.length, n3);
            for (var o3 = t3; o3 < n3; o3++)
              r4 += String.fromCharCode(e5[o3]);
            return r4;
          }
          function o2(e5, t3, n3, r4) {
            r4 || (d2("boolean" == typeof n3, "missing or invalid endian"), d2(null != t3, "missing offset"), d2(t3 + 1 < e5.length, "Trying to read beyond buffer length"));
            var o3, r4 = e5.length;
            if (!(r4 <= t3))
              return n3 ? (o3 = e5[t3], t3 + 1 < r4 && (o3 |= e5[t3 + 1] << 8)) : (o3 = e5[t3] << 8, t3 + 1 < r4 && (o3 |= e5[t3 + 1])), o3;
          }
          function u2(e5, t3, n3, r4) {
            r4 || (d2("boolean" == typeof n3, "missing or invalid endian"), d2(null != t3, "missing offset"), d2(t3 + 3 < e5.length, "Trying to read beyond buffer length"));
            var o3, r4 = e5.length;
            if (!(r4 <= t3))
              return n3 ? (t3 + 2 < r4 && (o3 = e5[t3 + 2] << 16), t3 + 1 < r4 && (o3 |= e5[t3 + 1] << 8), o3 |= e5[t3], t3 + 3 < r4 && (o3 += e5[t3 + 3] << 24 >>> 0)) : (t3 + 1 < r4 && (o3 = e5[t3 + 1] << 16), t3 + 2 < r4 && (o3 |= e5[t3 + 2] << 8), t3 + 3 < r4 && (o3 |= e5[t3 + 3]), o3 += e5[t3] << 24 >>> 0), o3;
          }
          function _2(e5, t3, n3, r4) {
            if (r4 || (d2("boolean" == typeof n3, "missing or invalid endian"), d2(null != t3, "missing offset"), d2(t3 + 1 < e5.length, "Trying to read beyond buffer length")), !(e5.length <= t3))
              return r4 = o2(e5, t3, n3, true), 32768 & r4 ? -1 * (65535 - r4 + 1) : r4;
          }
          function E(e5, t3, n3, r4) {
            if (r4 || (d2("boolean" == typeof n3, "missing or invalid endian"), d2(null != t3, "missing offset"), d2(t3 + 3 < e5.length, "Trying to read beyond buffer length")), !(e5.length <= t3))
              return r4 = u2(e5, t3, n3, true), 2147483648 & r4 ? -1 * (4294967295 - r4 + 1) : r4;
          }
          function I(e5, t3, n3, r4) {
            return r4 || (d2("boolean" == typeof n3, "missing or invalid endian"), d2(t3 + 3 < e5.length, "Trying to read beyond buffer length")), i.read(e5, t3, n3, 23, 4);
          }
          function A2(e5, t3, n3, r4) {
            return r4 || (d2("boolean" == typeof n3, "missing or invalid endian"), d2(t3 + 7 < e5.length, "Trying to read beyond buffer length")), i.read(e5, t3, n3, 52, 8);
          }
          function s3(e5, t3, n3, r4, o3) {
            o3 || (d2(null != t3, "missing value"), d2("boolean" == typeof r4, "missing or invalid endian"), d2(null != n3, "missing offset"), d2(n3 + 1 < e5.length, "trying to write beyond buffer length"), Y(t3, 65535));
            o3 = e5.length;
            if (!(o3 <= n3))
              for (var i2 = 0, u3 = Math.min(o3 - n3, 2); i2 < u3; i2++)
                e5[n3 + i2] = (t3 & 255 << 8 * (r4 ? i2 : 1 - i2)) >>> 8 * (r4 ? i2 : 1 - i2);
          }
          function l(e5, t3, n3, r4, o3) {
            o3 || (d2(null != t3, "missing value"), d2("boolean" == typeof r4, "missing or invalid endian"), d2(null != n3, "missing offset"), d2(n3 + 3 < e5.length, "trying to write beyond buffer length"), Y(t3, 4294967295));
            o3 = e5.length;
            if (!(o3 <= n3))
              for (var i2 = 0, u3 = Math.min(o3 - n3, 4); i2 < u3; i2++)
                e5[n3 + i2] = t3 >>> 8 * (r4 ? i2 : 3 - i2) & 255;
          }
          function B2(e5, t3, n3, r4, o3) {
            o3 || (d2(null != t3, "missing value"), d2("boolean" == typeof r4, "missing or invalid endian"), d2(null != n3, "missing offset"), d2(n3 + 1 < e5.length, "Trying to write beyond buffer length"), F(t3, 32767, -32768)), e5.length <= n3 || s3(e5, 0 <= t3 ? t3 : 65535 + t3 + 1, n3, r4, o3);
          }
          function L2(e5, t3, n3, r4, o3) {
            o3 || (d2(null != t3, "missing value"), d2("boolean" == typeof r4, "missing or invalid endian"), d2(null != n3, "missing offset"), d2(n3 + 3 < e5.length, "Trying to write beyond buffer length"), F(t3, 2147483647, -2147483648)), e5.length <= n3 || l(e5, 0 <= t3 ? t3 : 4294967295 + t3 + 1, n3, r4, o3);
          }
          function U(e5, t3, n3, r4, o3) {
            o3 || (d2(null != t3, "missing value"), d2("boolean" == typeof r4, "missing or invalid endian"), d2(null != n3, "missing offset"), d2(n3 + 3 < e5.length, "Trying to write beyond buffer length"), D(t3, 34028234663852886e22, -34028234663852886e22)), e5.length <= n3 || i.write(e5, t3, n3, r4, 23, 4);
          }
          function x3(e5, t3, n3, r4, o3) {
            o3 || (d2(null != t3, "missing value"), d2("boolean" == typeof r4, "missing or invalid endian"), d2(null != n3, "missing offset"), d2(n3 + 7 < e5.length, "Trying to write beyond buffer length"), D(t3, 17976931348623157e292, -17976931348623157e292)), e5.length <= n3 || i.write(e5, t3, n3, r4, 52, 8);
          }
          H2.Buffer = f, H2.SlowBuffer = f, H2.INSPECT_MAX_BYTES = 50, f.poolSize = 8192, f._useTypedArrays = function() {
            try {
              var e5 = new ArrayBuffer(0), t3 = new Uint8Array(e5);
              return t3.foo = function() {
                return 42;
              }, 42 === t3.foo() && "function" == typeof t3.subarray;
            } catch (e6) {
              return false;
            }
          }(), f.isEncoding = function(e5) {
            switch (String(e5).toLowerCase()) {
              case "hex":
              case "utf8":
              case "utf-8":
              case "ascii":
              case "binary":
              case "base64":
              case "raw":
              case "ucs2":
              case "ucs-2":
              case "utf16le":
              case "utf-16le":
                return true;
              default:
                return false;
            }
          }, f.isBuffer = function(e5) {
            return !(null == e5 || !e5._isBuffer);
          }, f.byteLength = function(e5, t3) {
            var n3;
            switch (e5 += "", t3 || "utf8") {
              case "hex":
                n3 = e5.length / 2;
                break;
              case "utf8":
              case "utf-8":
                n3 = T2(e5).length;
                break;
              case "ascii":
              case "binary":
              case "raw":
                n3 = e5.length;
                break;
              case "base64":
                n3 = M(e5).length;
                break;
              case "ucs2":
              case "ucs-2":
              case "utf16le":
              case "utf-16le":
                n3 = 2 * e5.length;
                break;
              default:
                throw new Error("Unknown encoding");
            }
            return n3;
          }, f.concat = function(e5, t3) {
            if (d2(C2(e5), "Usage: Buffer.concat(list, [totalLength])\nlist should be an Array."), 0 === e5.length)
              return new f(0);
            if (1 === e5.length)
              return e5[0];
            if ("number" != typeof t3)
              for (o3 = t3 = 0; o3 < e5.length; o3++)
                t3 += e5[o3].length;
            for (var n3 = new f(t3), r4 = 0, o3 = 0; o3 < e5.length; o3++) {
              var i2 = e5[o3];
              i2.copy(n3, r4), r4 += i2.length;
            }
            return n3;
          }, f.prototype.write = function(e5, t3, n3, r4) {
            isFinite(t3) ? isFinite(n3) || (r4 = n3, n3 = void 0) : (a2 = r4, r4 = t3, t3 = n3, n3 = a2), t3 = Number(t3) || 0;
            var o3, i2, u3, s4, a2 = this.length - t3;
            switch ((!n3 || a2 < (n3 = Number(n3))) && (n3 = a2), r4 = String(r4 || "utf8").toLowerCase()) {
              case "hex":
                o3 = function(e6, t4, n4, r5) {
                  n4 = Number(n4) || 0;
                  var o4 = e6.length - n4;
                  (!r5 || o4 < (r5 = Number(r5))) && (r5 = o4), d2((o4 = t4.length) % 2 == 0, "Invalid hex string"), o4 / 2 < r5 && (r5 = o4 / 2);
                  for (var i3 = 0; i3 < r5; i3++) {
                    var u4 = parseInt(t4.substr(2 * i3, 2), 16);
                    d2(!isNaN(u4), "Invalid hex string"), e6[n4 + i3] = u4;
                  }
                  return f._charsWritten = 2 * i3, i3;
                }(this, e5, t3, n3);
                break;
              case "utf8":
              case "utf-8":
                i2 = this, u3 = t3, s4 = n3, o3 = f._charsWritten = c2(T2(e5), i2, u3, s4);
                break;
              case "ascii":
              case "binary":
                o3 = b(this, e5, t3, n3);
                break;
              case "base64":
                i2 = this, u3 = t3, s4 = n3, o3 = f._charsWritten = c2(M(e5), i2, u3, s4);
                break;
              case "ucs2":
              case "ucs-2":
              case "utf16le":
              case "utf-16le":
                o3 = m(this, e5, t3, n3);
                break;
              default:
                throw new Error("Unknown encoding");
            }
            return o3;
          }, f.prototype.toString = function(e5, t3, n3) {
            var r4, o3, i2, u3, s4 = this;
            if (e5 = String(e5 || "utf8").toLowerCase(), t3 = Number(t3) || 0, (n3 = void 0 !== n3 ? Number(n3) : s4.length) === t3)
              return "";
            switch (e5) {
              case "hex":
                r4 = function(e6, t4, n4) {
                  var r5 = e6.length;
                  (!t4 || t4 < 0) && (t4 = 0);
                  (!n4 || n4 < 0 || r5 < n4) && (n4 = r5);
                  for (var o4 = "", i3 = t4; i3 < n4; i3++)
                    o4 += k2(e6[i3]);
                  return o4;
                }(s4, t3, n3);
                break;
              case "utf8":
              case "utf-8":
                r4 = function(e6, t4, n4) {
                  var r5 = "", o4 = "";
                  n4 = Math.min(e6.length, n4);
                  for (var i3 = t4; i3 < n4; i3++)
                    e6[i3] <= 127 ? (r5 += N2(o4) + String.fromCharCode(e6[i3]), o4 = "") : o4 += "%" + e6[i3].toString(16);
                  return r5 + N2(o4);
                }(s4, t3, n3);
                break;
              case "ascii":
              case "binary":
                r4 = v(s4, t3, n3);
                break;
              case "base64":
                o3 = s4, u3 = n3, r4 = 0 === (i2 = t3) && u3 === o3.length ? a.fromByteArray(o3) : a.fromByteArray(o3.slice(i2, u3));
                break;
              case "ucs2":
              case "ucs-2":
              case "utf16le":
              case "utf-16le":
                r4 = function(e6, t4, n4) {
                  for (var r5 = e6.slice(t4, n4), o4 = "", i3 = 0; i3 < r5.length; i3 += 2)
                    o4 += String.fromCharCode(r5[i3] + 256 * r5[i3 + 1]);
                  return o4;
                }(s4, t3, n3);
                break;
              default:
                throw new Error("Unknown encoding");
            }
            return r4;
          }, f.prototype.toJSON = function() {
            return { type: "Buffer", data: Array.prototype.slice.call(this._arr || this, 0) };
          }, f.prototype.copy = function(e5, t3, n3, r4) {
            if (t3 = t3 || 0, (r4 = r4 || 0 === r4 ? r4 : this.length) !== (n3 = n3 || 0) && 0 !== e5.length && 0 !== this.length) {
              d2(n3 <= r4, "sourceEnd < sourceStart"), d2(0 <= t3 && t3 < e5.length, "targetStart out of bounds"), d2(0 <= n3 && n3 < this.length, "sourceStart out of bounds"), d2(0 <= r4 && r4 <= this.length, "sourceEnd out of bounds"), r4 > this.length && (r4 = this.length);
              var o3 = (r4 = e5.length - t3 < r4 - n3 ? e5.length - t3 + n3 : r4) - n3;
              if (o3 < 100 || !f._useTypedArrays)
                for (var i2 = 0; i2 < o3; i2++)
                  e5[i2 + t3] = this[i2 + n3];
              else
                e5._set(this.subarray(n3, n3 + o3), t3);
            }
          }, f.prototype.slice = function(e5, t3) {
            var n3 = this.length;
            if (e5 = S(e5, n3, 0), t3 = S(t3, n3, n3), f._useTypedArrays)
              return f._augment(this.subarray(e5, t3));
            for (var r4 = t3 - e5, o3 = new f(r4, void 0, true), i2 = 0; i2 < r4; i2++)
              o3[i2] = this[i2 + e5];
            return o3;
          }, f.prototype.get = function(e5) {
            return console.log(".get() is deprecated. Access using array indexes instead."), this.readUInt8(e5);
          }, f.prototype.set = function(e5, t3) {
            return console.log(".set() is deprecated. Access using array indexes instead."), this.writeUInt8(e5, t3);
          }, f.prototype.readUInt8 = function(e5, t3) {
            if (t3 || (d2(null != e5, "missing offset"), d2(e5 < this.length, "Trying to read beyond buffer length")), !(e5 >= this.length))
              return this[e5];
          }, f.prototype.readUInt16LE = function(e5, t3) {
            return o2(this, e5, true, t3);
          }, f.prototype.readUInt16BE = function(e5, t3) {
            return o2(this, e5, false, t3);
          }, f.prototype.readUInt32LE = function(e5, t3) {
            return u2(this, e5, true, t3);
          }, f.prototype.readUInt32BE = function(e5, t3) {
            return u2(this, e5, false, t3);
          }, f.prototype.readInt8 = function(e5, t3) {
            if (t3 || (d2(null != e5, "missing offset"), d2(e5 < this.length, "Trying to read beyond buffer length")), !(e5 >= this.length))
              return 128 & this[e5] ? -1 * (255 - this[e5] + 1) : this[e5];
          }, f.prototype.readInt16LE = function(e5, t3) {
            return _2(this, e5, true, t3);
          }, f.prototype.readInt16BE = function(e5, t3) {
            return _2(this, e5, false, t3);
          }, f.prototype.readInt32LE = function(e5, t3) {
            return E(this, e5, true, t3);
          }, f.prototype.readInt32BE = function(e5, t3) {
            return E(this, e5, false, t3);
          }, f.prototype.readFloatLE = function(e5, t3) {
            return I(this, e5, true, t3);
          }, f.prototype.readFloatBE = function(e5, t3) {
            return I(this, e5, false, t3);
          }, f.prototype.readDoubleLE = function(e5, t3) {
            return A2(this, e5, true, t3);
          }, f.prototype.readDoubleBE = function(e5, t3) {
            return A2(this, e5, false, t3);
          }, f.prototype.writeUInt8 = function(e5, t3, n3) {
            n3 || (d2(null != e5, "missing value"), d2(null != t3, "missing offset"), d2(t3 < this.length, "trying to write beyond buffer length"), Y(e5, 255)), t3 >= this.length || (this[t3] = e5);
          }, f.prototype.writeUInt16LE = function(e5, t3, n3) {
            s3(this, e5, t3, true, n3);
          }, f.prototype.writeUInt16BE = function(e5, t3, n3) {
            s3(this, e5, t3, false, n3);
          }, f.prototype.writeUInt32LE = function(e5, t3, n3) {
            l(this, e5, t3, true, n3);
          }, f.prototype.writeUInt32BE = function(e5, t3, n3) {
            l(this, e5, t3, false, n3);
          }, f.prototype.writeInt8 = function(e5, t3, n3) {
            n3 || (d2(null != e5, "missing value"), d2(null != t3, "missing offset"), d2(t3 < this.length, "Trying to write beyond buffer length"), F(e5, 127, -128)), t3 >= this.length || (0 <= e5 ? this.writeUInt8(e5, t3, n3) : this.writeUInt8(255 + e5 + 1, t3, n3));
          }, f.prototype.writeInt16LE = function(e5, t3, n3) {
            B2(this, e5, t3, true, n3);
          }, f.prototype.writeInt16BE = function(e5, t3, n3) {
            B2(this, e5, t3, false, n3);
          }, f.prototype.writeInt32LE = function(e5, t3, n3) {
            L2(this, e5, t3, true, n3);
          }, f.prototype.writeInt32BE = function(e5, t3, n3) {
            L2(this, e5, t3, false, n3);
          }, f.prototype.writeFloatLE = function(e5, t3, n3) {
            U(this, e5, t3, true, n3);
          }, f.prototype.writeFloatBE = function(e5, t3, n3) {
            U(this, e5, t3, false, n3);
          }, f.prototype.writeDoubleLE = function(e5, t3, n3) {
            x3(this, e5, t3, true, n3);
          }, f.prototype.writeDoubleBE = function(e5, t3, n3) {
            x3(this, e5, t3, false, n3);
          }, f.prototype.fill = function(e5, t3, n3) {
            if (t3 = t3 || 0, n3 = n3 || this.length, d2("number" == typeof (e5 = "string" == typeof (e5 = e5 || 0) ? e5.charCodeAt(0) : e5) && !isNaN(e5), "value is not a number"), d2(t3 <= n3, "end < start"), n3 !== t3 && 0 !== this.length) {
              d2(0 <= t3 && t3 < this.length, "start out of bounds"), d2(0 <= n3 && n3 <= this.length, "end out of bounds");
              for (var r4 = t3; r4 < n3; r4++)
                this[r4] = e5;
            }
          }, f.prototype.inspect = function() {
            for (var e5 = [], t3 = this.length, n3 = 0; n3 < t3; n3++)
              if (e5[n3] = k2(this[n3]), n3 === H2.INSPECT_MAX_BYTES) {
                e5[n3 + 1] = "...";
                break;
              }
            return "<Buffer " + e5.join(" ") + ">";
          }, f.prototype.toArrayBuffer = function() {
            if ("undefined" == typeof Uint8Array)
              throw new Error("Buffer.toArrayBuffer not supported in this browser");
            if (f._useTypedArrays)
              return new f(this).buffer;
            for (var e5 = new Uint8Array(this.length), t3 = 0, n3 = e5.length; t3 < n3; t3 += 1)
              e5[t3] = this[t3];
            return e5.buffer;
          };
          var t2 = f.prototype;
          function S(e5, t3, n3) {
            return "number" != typeof e5 ? n3 : t3 <= (e5 = ~~e5) ? t3 : 0 <= e5 || 0 <= (e5 += t3) ? e5 : 0;
          }
          function j2(e5) {
            return (e5 = ~~Math.ceil(+e5)) < 0 ? 0 : e5;
          }
          function C2(e5) {
            return (Array.isArray || function(e6) {
              return "[object Array]" === Object.prototype.toString.call(e6);
            })(e5);
          }
          function k2(e5) {
            return e5 < 16 ? "0" + e5.toString(16) : e5.toString(16);
          }
          function T2(e5) {
            for (var t3 = [], n3 = 0; n3 < e5.length; n3++) {
              var r4 = e5.charCodeAt(n3);
              if (r4 <= 127)
                t3.push(e5.charCodeAt(n3));
              else
                for (var o3 = n3, i2 = (55296 <= r4 && r4 <= 57343 && n3++, encodeURIComponent(e5.slice(o3, n3 + 1)).substr(1).split("%")), u3 = 0; u3 < i2.length; u3++)
                  t3.push(parseInt(i2[u3], 16));
            }
            return t3;
          }
          function M(e5) {
            return a.toByteArray(e5);
          }
          function c2(e5, t3, n3, r4) {
            for (var o3 = 0; o3 < r4 && !(o3 + n3 >= t3.length || o3 >= e5.length); o3++)
              t3[o3 + n3] = e5[o3];
            return o3;
          }
          function N2(e5) {
            try {
              return decodeURIComponent(e5);
            } catch (e6) {
              return String.fromCharCode(65533);
            }
          }
          function Y(e5, t3) {
            d2("number" == typeof e5, "cannot write a non-number as a number"), d2(0 <= e5, "specified a negative value for writing an unsigned value"), d2(e5 <= t3, "value is larger than maximum value for type"), d2(Math.floor(e5) === e5, "value has a fractional component");
          }
          function F(e5, t3, n3) {
            d2("number" == typeof e5, "cannot write a non-number as a number"), d2(e5 <= t3, "value larger than maximum allowed value"), d2(n3 <= e5, "value smaller than minimum allowed value"), d2(Math.floor(e5) === e5, "value has a fractional component");
          }
          function D(e5, t3, n3) {
            d2("number" == typeof e5, "cannot write a non-number as a number"), d2(e5 <= t3, "value larger than maximum allowed value"), d2(n3 <= e5, "value smaller than minimum allowed value");
          }
          function d2(e5, t3) {
            if (!e5)
              throw new Error(t3 || "Failed assertion");
          }
          f._augment = function(e5) {
            return e5._isBuffer = true, e5._get = e5.get, e5._set = e5.set, e5.get = t2.get, e5.set = t2.set, e5.write = t2.write, e5.toString = t2.toString, e5.toLocaleString = t2.toString, e5.toJSON = t2.toJSON, e5.copy = t2.copy, e5.slice = t2.slice, e5.readUInt8 = t2.readUInt8, e5.readUInt16LE = t2.readUInt16LE, e5.readUInt16BE = t2.readUInt16BE, e5.readUInt32LE = t2.readUInt32LE, e5.readUInt32BE = t2.readUInt32BE, e5.readInt8 = t2.readInt8, e5.readInt16LE = t2.readInt16LE, e5.readInt16BE = t2.readInt16BE, e5.readInt32LE = t2.readInt32LE, e5.readInt32BE = t2.readInt32BE, e5.readFloatLE = t2.readFloatLE, e5.readFloatBE = t2.readFloatBE, e5.readDoubleLE = t2.readDoubleLE, e5.readDoubleBE = t2.readDoubleBE, e5.writeUInt8 = t2.writeUInt8, e5.writeUInt16LE = t2.writeUInt16LE, e5.writeUInt16BE = t2.writeUInt16BE, e5.writeUInt32LE = t2.writeUInt32LE, e5.writeUInt32BE = t2.writeUInt32BE, e5.writeInt8 = t2.writeInt8, e5.writeInt16LE = t2.writeInt16LE, e5.writeInt16BE = t2.writeInt16BE, e5.writeInt32LE = t2.writeInt32LE, e5.writeInt32BE = t2.writeInt32BE, e5.writeFloatLE = t2.writeFloatLE, e5.writeFloatBE = t2.writeFloatBE, e5.writeDoubleLE = t2.writeDoubleLE, e5.writeDoubleBE = t2.writeDoubleBE, e5.fill = t2.fill, e5.inspect = t2.inspect, e5.toArrayBuffer = t2.toArrayBuffer, e5;
          };
        }.call(this, O2("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, O2("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/buffer/index.js", "/node_modules/gulp-browserify/node_modules/buffer");
      }, { "base64-js": 2, buffer: 3, ieee754: 10, lYpoI2: 11 }], 4: [function(c2, d2, e3) {
        !function(e4, t2, a, n2, r3, o2, i, u2, s3) {
          var a = c2("buffer").Buffer, f = 4, l = new a(f);
          l.fill(0);
          d2.exports = { hash: function(e5, t3, n3, r4) {
            for (var o3 = t3(function(e6, t4) {
              e6.length % f != 0 && (n4 = e6.length + (f - e6.length % f), e6 = a.concat([e6, l], n4));
              for (var n4, r5 = [], o4 = t4 ? e6.readInt32BE : e6.readInt32LE, i3 = 0; i3 < e6.length; i3 += f)
                r5.push(o4.call(e6, i3));
              return r5;
            }(e5 = a.isBuffer(e5) ? e5 : new a(e5), r4), 8 * e5.length), t3 = r4, i2 = new a(n3), u3 = t3 ? i2.writeInt32BE : i2.writeInt32LE, s4 = 0; s4 < o3.length; s4++)
              u3.call(i2, o3[s4], 4 * s4, true);
            return i2;
          } };
        }.call(this, c2("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, c2("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/helpers.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify");
      }, { buffer: 3, lYpoI2: 11 }], 5: [function(v, e3, _2) {
        !function(l, c2, u2, d2, h2, p, g2, y, w) {
          var u2 = v("buffer").Buffer, e4 = v("./sha"), t2 = v("./sha256"), n2 = v("./rng"), b = { sha1: e4, sha256: t2, md5: v("./md5") }, s3 = 64, a = new u2(s3);
          function r3(e5, n3) {
            var r4 = b[e5 = e5 || "sha1"], o3 = [];
            return r4 || i("algorithm:", e5, "is not yet supported"), { update: function(e6) {
              return u2.isBuffer(e6) || (e6 = new u2(e6)), o3.push(e6), e6.length, this;
            }, digest: function(e6) {
              var t3 = u2.concat(o3), t3 = n3 ? function(e7, t4, n4) {
                u2.isBuffer(t4) || (t4 = new u2(t4)), u2.isBuffer(n4) || (n4 = new u2(n4)), t4.length > s3 ? t4 = e7(t4) : t4.length < s3 && (t4 = u2.concat([t4, a], s3));
                for (var r5 = new u2(s3), o4 = new u2(s3), i2 = 0; i2 < s3; i2++)
                  r5[i2] = 54 ^ t4[i2], o4[i2] = 92 ^ t4[i2];
                return n4 = e7(u2.concat([r5, n4])), e7(u2.concat([o4, n4]));
              }(r4, n3, t3) : r4(t3);
              return o3 = null, e6 ? t3.toString(e6) : t3;
            } };
          }
          function i() {
            var e5 = [].slice.call(arguments).join(" ");
            throw new Error([e5, "we accept pull requests", "http://github.com/dominictarr/crypto-browserify"].join("\n"));
          }
          a.fill(0), _2.createHash = function(e5) {
            return r3(e5);
          }, _2.createHmac = r3, _2.randomBytes = function(e5, t3) {
            if (!t3 || !t3.call)
              return new u2(n2(e5));
            try {
              t3.call(this, void 0, new u2(n2(e5)));
            } catch (e6) {
              t3(e6);
            }
          };
          var o2, f = ["createCredentials", "createCipher", "createCipheriv", "createDecipher", "createDecipheriv", "createSign", "createVerify", "createDiffieHellman", "pbkdf2"], m = function(e5) {
            _2[e5] = function() {
              i("sorry,", e5, "is not implemented yet");
            };
          };
          for (o2 in f)
            m(f[o2], o2);
        }.call(this, v("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, v("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/index.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify");
      }, { "./md5": 6, "./rng": 7, "./sha": 8, "./sha256": 9, buffer: 3, lYpoI2: 11 }], 6: [function(w, b, e3) {
        !function(e4, r3, o2, i, u2, a, f, l, y) {
          var t2 = w("./helpers");
          function n2(e5, t3) {
            e5[t3 >> 5] |= 128 << t3 % 32, e5[14 + (t3 + 64 >>> 9 << 4)] = t3;
            for (var n3 = 1732584193, r4 = -271733879, o3 = -1732584194, i2 = 271733878, u3 = 0; u3 < e5.length; u3 += 16) {
              var s4 = n3, a2 = r4, f2 = o3, l2 = i2, n3 = c2(n3, r4, o3, i2, e5[u3 + 0], 7, -680876936), i2 = c2(i2, n3, r4, o3, e5[u3 + 1], 12, -389564586), o3 = c2(o3, i2, n3, r4, e5[u3 + 2], 17, 606105819), r4 = c2(r4, o3, i2, n3, e5[u3 + 3], 22, -1044525330);
              n3 = c2(n3, r4, o3, i2, e5[u3 + 4], 7, -176418897), i2 = c2(i2, n3, r4, o3, e5[u3 + 5], 12, 1200080426), o3 = c2(o3, i2, n3, r4, e5[u3 + 6], 17, -1473231341), r4 = c2(r4, o3, i2, n3, e5[u3 + 7], 22, -45705983), n3 = c2(n3, r4, o3, i2, e5[u3 + 8], 7, 1770035416), i2 = c2(i2, n3, r4, o3, e5[u3 + 9], 12, -1958414417), o3 = c2(o3, i2, n3, r4, e5[u3 + 10], 17, -42063), r4 = c2(r4, o3, i2, n3, e5[u3 + 11], 22, -1990404162), n3 = c2(n3, r4, o3, i2, e5[u3 + 12], 7, 1804603682), i2 = c2(i2, n3, r4, o3, e5[u3 + 13], 12, -40341101), o3 = c2(o3, i2, n3, r4, e5[u3 + 14], 17, -1502002290), n3 = d2(n3, r4 = c2(r4, o3, i2, n3, e5[u3 + 15], 22, 1236535329), o3, i2, e5[u3 + 1], 5, -165796510), i2 = d2(i2, n3, r4, o3, e5[u3 + 6], 9, -1069501632), o3 = d2(o3, i2, n3, r4, e5[u3 + 11], 14, 643717713), r4 = d2(r4, o3, i2, n3, e5[u3 + 0], 20, -373897302), n3 = d2(n3, r4, o3, i2, e5[u3 + 5], 5, -701558691), i2 = d2(i2, n3, r4, o3, e5[u3 + 10], 9, 38016083), o3 = d2(o3, i2, n3, r4, e5[u3 + 15], 14, -660478335), r4 = d2(r4, o3, i2, n3, e5[u3 + 4], 20, -405537848), n3 = d2(n3, r4, o3, i2, e5[u3 + 9], 5, 568446438), i2 = d2(i2, n3, r4, o3, e5[u3 + 14], 9, -1019803690), o3 = d2(o3, i2, n3, r4, e5[u3 + 3], 14, -187363961), r4 = d2(r4, o3, i2, n3, e5[u3 + 8], 20, 1163531501), n3 = d2(n3, r4, o3, i2, e5[u3 + 13], 5, -1444681467), i2 = d2(i2, n3, r4, o3, e5[u3 + 2], 9, -51403784), o3 = d2(o3, i2, n3, r4, e5[u3 + 7], 14, 1735328473), n3 = h2(n3, r4 = d2(r4, o3, i2, n3, e5[u3 + 12], 20, -1926607734), o3, i2, e5[u3 + 5], 4, -378558), i2 = h2(i2, n3, r4, o3, e5[u3 + 8], 11, -2022574463), o3 = h2(o3, i2, n3, r4, e5[u3 + 11], 16, 1839030562), r4 = h2(r4, o3, i2, n3, e5[u3 + 14], 23, -35309556), n3 = h2(n3, r4, o3, i2, e5[u3 + 1], 4, -1530992060), i2 = h2(i2, n3, r4, o3, e5[u3 + 4], 11, 1272893353), o3 = h2(o3, i2, n3, r4, e5[u3 + 7], 16, -155497632), r4 = h2(r4, o3, i2, n3, e5[u3 + 10], 23, -1094730640), n3 = h2(n3, r4, o3, i2, e5[u3 + 13], 4, 681279174), i2 = h2(i2, n3, r4, o3, e5[u3 + 0], 11, -358537222), o3 = h2(o3, i2, n3, r4, e5[u3 + 3], 16, -722521979), r4 = h2(r4, o3, i2, n3, e5[u3 + 6], 23, 76029189), n3 = h2(n3, r4, o3, i2, e5[u3 + 9], 4, -640364487), i2 = h2(i2, n3, r4, o3, e5[u3 + 12], 11, -421815835), o3 = h2(o3, i2, n3, r4, e5[u3 + 15], 16, 530742520), n3 = p(n3, r4 = h2(r4, o3, i2, n3, e5[u3 + 2], 23, -995338651), o3, i2, e5[u3 + 0], 6, -198630844), i2 = p(i2, n3, r4, o3, e5[u3 + 7], 10, 1126891415), o3 = p(o3, i2, n3, r4, e5[u3 + 14], 15, -1416354905), r4 = p(r4, o3, i2, n3, e5[u3 + 5], 21, -57434055), n3 = p(n3, r4, o3, i2, e5[u3 + 12], 6, 1700485571), i2 = p(i2, n3, r4, o3, e5[u3 + 3], 10, -1894986606), o3 = p(o3, i2, n3, r4, e5[u3 + 10], 15, -1051523), r4 = p(r4, o3, i2, n3, e5[u3 + 1], 21, -2054922799), n3 = p(n3, r4, o3, i2, e5[u3 + 8], 6, 1873313359), i2 = p(i2, n3, r4, o3, e5[u3 + 15], 10, -30611744), o3 = p(o3, i2, n3, r4, e5[u3 + 6], 15, -1560198380), r4 = p(r4, o3, i2, n3, e5[u3 + 13], 21, 1309151649), n3 = p(n3, r4, o3, i2, e5[u3 + 4], 6, -145523070), i2 = p(i2, n3, r4, o3, e5[u3 + 11], 10, -1120210379), o3 = p(o3, i2, n3, r4, e5[u3 + 2], 15, 718787259), r4 = p(r4, o3, i2, n3, e5[u3 + 9], 21, -343485551), n3 = g2(n3, s4), r4 = g2(r4, a2), o3 = g2(o3, f2), i2 = g2(i2, l2);
            }
            return Array(n3, r4, o3, i2);
          }
          function s3(e5, t3, n3, r4, o3, i2) {
            return g2((t3 = g2(g2(t3, e5), g2(r4, i2))) << o3 | t3 >>> 32 - o3, n3);
          }
          function c2(e5, t3, n3, r4, o3, i2, u3) {
            return s3(t3 & n3 | ~t3 & r4, e5, t3, o3, i2, u3);
          }
          function d2(e5, t3, n3, r4, o3, i2, u3) {
            return s3(t3 & r4 | n3 & ~r4, e5, t3, o3, i2, u3);
          }
          function h2(e5, t3, n3, r4, o3, i2, u3) {
            return s3(t3 ^ n3 ^ r4, e5, t3, o3, i2, u3);
          }
          function p(e5, t3, n3, r4, o3, i2, u3) {
            return s3(n3 ^ (t3 | ~r4), e5, t3, o3, i2, u3);
          }
          function g2(e5, t3) {
            var n3 = (65535 & e5) + (65535 & t3);
            return (e5 >> 16) + (t3 >> 16) + (n3 >> 16) << 16 | 65535 & n3;
          }
          b.exports = function(e5) {
            return t2.hash(e5, n2, 16);
          };
        }.call(this, w("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, w("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/md5.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify");
      }, { "./helpers": 4, buffer: 3, lYpoI2: 11 }], 7: [function(e3, l, t2) {
        !function(e4, t3, n2, r3, o2, i, u2, s3, f) {
          var a;
          l.exports = a || function(e5) {
            for (var t4, n3 = new Array(e5), r4 = 0; r4 < e5; r4++)
              0 == (3 & r4) && (t4 = 4294967296 * Math.random()), n3[r4] = t4 >>> ((3 & r4) << 3) & 255;
            return n3;
          };
        }.call(this, e3("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e3("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/rng.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify");
      }, { buffer: 3, lYpoI2: 11 }], 8: [function(c2, d2, e3) {
        !function(e4, t2, n2, r3, o2, s3, a, f, l) {
          var i = c2("./helpers");
          function u2(l2, c3) {
            l2[c3 >> 5] |= 128 << 24 - c3 % 32, l2[15 + (c3 + 64 >> 9 << 4)] = c3;
            for (var e5, t3, n3, r4 = Array(80), o3 = 1732584193, i2 = -271733879, u3 = -1732584194, s4 = 271733878, d3 = -1009589776, h2 = 0; h2 < l2.length; h2 += 16) {
              for (var p = o3, g2 = i2, y = u3, w = s4, b = d3, a2 = 0; a2 < 80; a2++) {
                r4[a2] = a2 < 16 ? l2[h2 + a2] : v(r4[a2 - 3] ^ r4[a2 - 8] ^ r4[a2 - 14] ^ r4[a2 - 16], 1);
                var f2 = m(m(v(o3, 5), (f2 = i2, t3 = u3, n3 = s4, (e5 = a2) < 20 ? f2 & t3 | ~f2 & n3 : !(e5 < 40) && e5 < 60 ? f2 & t3 | f2 & n3 | t3 & n3 : f2 ^ t3 ^ n3)), m(m(d3, r4[a2]), (e5 = a2) < 20 ? 1518500249 : e5 < 40 ? 1859775393 : e5 < 60 ? -1894007588 : -899497514)), d3 = s4, s4 = u3, u3 = v(i2, 30), i2 = o3, o3 = f2;
              }
              o3 = m(o3, p), i2 = m(i2, g2), u3 = m(u3, y), s4 = m(s4, w), d3 = m(d3, b);
            }
            return Array(o3, i2, u3, s4, d3);
          }
          function m(e5, t3) {
            var n3 = (65535 & e5) + (65535 & t3);
            return (e5 >> 16) + (t3 >> 16) + (n3 >> 16) << 16 | 65535 & n3;
          }
          function v(e5, t3) {
            return e5 << t3 | e5 >>> 32 - t3;
          }
          d2.exports = function(e5) {
            return i.hash(e5, u2, 20, true);
          };
        }.call(this, c2("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, c2("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/sha.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify");
      }, { "./helpers": 4, buffer: 3, lYpoI2: 11 }], 9: [function(c2, d2, e3) {
        !function(e4, t2, n2, r3, u2, s3, a, f, l) {
          function b(e5, t3) {
            var n3 = (65535 & e5) + (65535 & t3);
            return (e5 >> 16) + (t3 >> 16) + (n3 >> 16) << 16 | 65535 & n3;
          }
          function o2(e5, l2) {
            var c3, d3 = new Array(1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298), t3 = new Array(1779033703, 3144134277, 1013904242, 2773480762, 1359893119, 2600822924, 528734635, 1541459225), n3 = new Array(64);
            e5[l2 >> 5] |= 128 << 24 - l2 % 32, e5[15 + (l2 + 64 >> 9 << 4)] = l2;
            for (var r4, o3, h2 = 0; h2 < e5.length; h2 += 16) {
              for (var i2 = t3[0], u3 = t3[1], s4 = t3[2], p = t3[3], a2 = t3[4], g2 = t3[5], y = t3[6], w = t3[7], f2 = 0; f2 < 64; f2++)
                n3[f2] = f2 < 16 ? e5[f2 + h2] : b(b(b((o3 = n3[f2 - 2], m(o3, 17) ^ m(o3, 19) ^ v(o3, 10)), n3[f2 - 7]), (o3 = n3[f2 - 15], m(o3, 7) ^ m(o3, 18) ^ v(o3, 3))), n3[f2 - 16]), c3 = b(b(b(b(w, m(o3 = a2, 6) ^ m(o3, 11) ^ m(o3, 25)), a2 & g2 ^ ~a2 & y), d3[f2]), n3[f2]), r4 = b(m(r4 = i2, 2) ^ m(r4, 13) ^ m(r4, 22), i2 & u3 ^ i2 & s4 ^ u3 & s4), w = y, y = g2, g2 = a2, a2 = b(p, c3), p = s4, s4 = u3, u3 = i2, i2 = b(c3, r4);
              t3[0] = b(i2, t3[0]), t3[1] = b(u3, t3[1]), t3[2] = b(s4, t3[2]), t3[3] = b(p, t3[3]), t3[4] = b(a2, t3[4]), t3[5] = b(g2, t3[5]), t3[6] = b(y, t3[6]), t3[7] = b(w, t3[7]);
            }
            return t3;
          }
          var i = c2("./helpers"), m = function(e5, t3) {
            return e5 >>> t3 | e5 << 32 - t3;
          }, v = function(e5, t3) {
            return e5 >>> t3;
          };
          d2.exports = function(e5) {
            return i.hash(e5, o2, 32, true);
          };
        }.call(this, c2("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, c2("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/sha256.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify");
      }, { "./helpers": 4, buffer: 3, lYpoI2: 11 }], 10: [function(e3, t2, f) {
        !function(e4, t3, n2, r3, o2, i, u2, s3, a) {
          f.read = function(e5, t4, n3, r4, o3) {
            var i2, u3, l = 8 * o3 - r4 - 1, c2 = (1 << l) - 1, d2 = c2 >> 1, s4 = -7, a2 = n3 ? o3 - 1 : 0, f2 = n3 ? -1 : 1, o3 = e5[t4 + a2];
            for (a2 += f2, i2 = o3 & (1 << -s4) - 1, o3 >>= -s4, s4 += l; 0 < s4; i2 = 256 * i2 + e5[t4 + a2], a2 += f2, s4 -= 8)
              ;
            for (u3 = i2 & (1 << -s4) - 1, i2 >>= -s4, s4 += r4; 0 < s4; u3 = 256 * u3 + e5[t4 + a2], a2 += f2, s4 -= 8)
              ;
            if (0 === i2)
              i2 = 1 - d2;
            else {
              if (i2 === c2)
                return u3 ? NaN : 1 / 0 * (o3 ? -1 : 1);
              u3 += Math.pow(2, r4), i2 -= d2;
            }
            return (o3 ? -1 : 1) * u3 * Math.pow(2, i2 - r4);
          }, f.write = function(e5, t4, l, n3, r4, c2) {
            var o3, i2, u3 = 8 * c2 - r4 - 1, s4 = (1 << u3) - 1, a2 = s4 >> 1, d2 = 23 === r4 ? Math.pow(2, -24) - Math.pow(2, -77) : 0, f2 = n3 ? 0 : c2 - 1, h2 = n3 ? 1 : -1, c2 = t4 < 0 || 0 === t4 && 1 / t4 < 0 ? 1 : 0;
            for (t4 = Math.abs(t4), isNaN(t4) || t4 === 1 / 0 ? (i2 = isNaN(t4) ? 1 : 0, o3 = s4) : (o3 = Math.floor(Math.log(t4) / Math.LN2), t4 * (n3 = Math.pow(2, -o3)) < 1 && (o3--, n3 *= 2), 2 <= (t4 += 1 <= o3 + a2 ? d2 / n3 : d2 * Math.pow(2, 1 - a2)) * n3 && (o3++, n3 /= 2), s4 <= o3 + a2 ? (i2 = 0, o3 = s4) : 1 <= o3 + a2 ? (i2 = (t4 * n3 - 1) * Math.pow(2, r4), o3 += a2) : (i2 = t4 * Math.pow(2, a2 - 1) * Math.pow(2, r4), o3 = 0)); 8 <= r4; e5[l + f2] = 255 & i2, f2 += h2, i2 /= 256, r4 -= 8)
              ;
            for (o3 = o3 << r4 | i2, u3 += r4; 0 < u3; e5[l + f2] = 255 & o3, f2 += h2, o3 /= 256, u3 -= 8)
              ;
            e5[l + f2 - h2] |= 128 * c2;
          };
        }.call(this, e3("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e3("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/ieee754/index.js", "/node_modules/gulp-browserify/node_modules/ieee754");
      }, { buffer: 3, lYpoI2: 11 }], 11: [function(e3, h2, t2) {
        !function(e4, t3, n2, r3, o2, f, l, c2, d2) {
          var i, u2, s3;
          function a() {
          }
          (e4 = h2.exports = {}).nextTick = (u2 = "undefined" != typeof window && window.setImmediate, s3 = "undefined" != typeof window && window.postMessage && window.addEventListener, u2 ? function(e5) {
            return window.setImmediate(e5);
          } : s3 ? (i = [], window.addEventListener("message", function(e5) {
            var t4 = e5.source;
            t4 !== window && null !== t4 || "process-tick" !== e5.data || (e5.stopPropagation(), 0 < i.length && i.shift()());
          }, true), function(e5) {
            i.push(e5), window.postMessage("process-tick", "*");
          }) : function(e5) {
            setTimeout(e5, 0);
          }), e4.title = "browser", e4.browser = true, e4.env = {}, e4.argv = [], e4.on = a, e4.addListener = a, e4.once = a, e4.off = a, e4.removeListener = a, e4.removeAllListeners = a, e4.emit = a, e4.binding = function(e5) {
            throw new Error("process.binding is not supported");
          }, e4.cwd = function() {
            return "/";
          }, e4.chdir = function(e5) {
            throw new Error("process.chdir is not supported");
          };
        }.call(this, e3("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e3("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/process/browser.js", "/node_modules/gulp-browserify/node_modules/process");
      }, { buffer: 3, lYpoI2: 11 }] }, {}, [1])(1);
    });
  }
});
var page_svelte_exports3 = {};
__export(page_svelte_exports3, {
  default: () => Page3
});
var import_object_hash;
var css7;
var Page3;
var init_page_svelte3 = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/library/public/_id_/_page.svelte.js"() {
    init_ssr();
    init_MediaContainer();
    init_LanguageSwitcher_svelte_svelte_type_style_lang();
    init_TypicalPage();
    init_runtime();
    init_Header();
    init_FancyButton();
    init_stores();
    import_object_hash = __toESM(require_object_hash(), 1);
    css7 = {
      code: ".image-container.svelte-1jze9wh{flex:1;overflow:hidden;display:flex;min-height:6em;border-bottom:0.15em solid}img.svelte-1jze9wh{width:100%;height:auto;flex:1;object-fit:cover}ul.svelte-1jze9wh{margin:0}.title.svelte-1jze9wh{font-weight:bold}#start-pane.svelte-1jze9wh{display:flex;flex-direction:column;max-width:30ch;flex:1;gap:0.5em;min-width:fit-content;height:fit-content}#summary.svelte-1jze9wh{display:flex;flex-direction:column;max-width:30ch;flex:1;border:0.15em solid;border-radius:0.7em;overflow:hidden;min-width:fit-content;height:fit-content}.info.svelte-1jze9wh{padding:0.5em}#page.svelte-1jze9wh{display:flex;flex-wrap:wrap;justify-content:center;gap:0.5em}",
      map: null
    };
    Page3 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let fuiz;
      let config;
      let { data } = $$props;
      if ($$props.data === void 0 && $$bindings.data && data !== void 0)
        $$bindings.data(data);
      $$result.css.add(css7);
      fuiz = data.fuiz;
      config = data.config;
      return `${validate_component(TypicalPage, "TypicalPage").$$render($$result, {}, {}, {
        default: () => {
          return `<div id="page" class="svelte-1jze9wh"><div id="start-pane" class="svelte-1jze9wh"><div id="summary" class="svelte-1jze9wh"><div class="image-container svelte-1jze9wh">${fuiz.thumbnail ? `<img${add_attribute("src", fuiz.thumbnail, 0)}${add_attribute("alt", fuiz.alt, 0)} class="svelte-1jze9wh">` : `<div${add_styles({
            "width": `100%`,
            "display": `flex`,
            "align-items": `center`,
            "justify-content": `center`
          })}>${validate_component(Icon, "Icon").$$render(
            $$result,
            {
              src: ___ASSET___02,
              size: "2em",
              alt: fallback()
            },
            {},
            {}
          )}</div>`}</div> <div class="info svelte-1jze9wh"><div class="title svelte-1jze9wh">${escape(fuiz.title)}</div> <div>${escape(author())}: ${escape(fuiz.author)}</div> <div>${escape(tags())}: ${escape(new Intl.ListFormat("vi", { style: "narrow", type: "conjunction" }).format(fuiz.tags))}</div> <div>${escape(played_times({ times: fuiz.played_count }))}</div> <div>${escape(language())}: ${escape(new Intl.DisplayNames([languageTag()], { type: "language" }).of(fuiz.language))}</div></div></div> ${validate_component(FancyButton, "FancyButton").$$render($$result, {}, {}, {
            default: () => {
              return `<div${add_styles({ "font-family": `Poppins` })}>${escape(import_fuiz())}</div>`;
            }
          })}</div> <div${add_styles({
            "flex": `3`,
            "display": `flex`,
            "flex-direction": `column`,
            "gap": `0.5em`,
            "height": `fit-content`
          })}>${each(config.slides, (slide) => {
            let { title, answers: answers2, media } = slide.MultipleChoice;
            return ` <div${add_styles({
              "border": `0.15em solid`,
              "border-radius": `0.7em`,
              "overflow": `hidden`,
              "flex-wrap": `wrap`,
              "display": `flex`,
              "justify-content": `center`
            })}>${media ? `<div${add_styles({
              "position": `relative`,
              "width": `6em`,
              "min-height": `4em`
            })}>${validate_component(MediaContainer, "MediaContainer").$$render($$result, { media, fit: "cover" }, {}, {})} </div>` : ``} <div${add_styles({
              "min-width": `fit-content`,
              "padding": `0.2em`,
              "flex": `1`
            })}>${escape(title)} <ul class="svelte-1jze9wh">${each(answers2, (answer) => {
              return `<li>${escape(answer.content.Text)}, ${escape(answer.correct ? correct() : wrong())}</li>`;
            })} </ul></div> </div>`;
          })}</div></div>`;
        }
      })}`;
    });
  }
});
var __exports7 = {};
__export(__exports7, {
  component: () => component7,
  fonts: () => fonts7,
  imports: () => imports7,
  index: () => index7,
  server: () => page_server_ts_exports3,
  server_id: () => server_id4,
  stylesheets: () => stylesheets7
});
var index7;
var component_cache7;
var component7;
var server_id4;
var imports7;
var stylesheets7;
var fonts7;
var init__7 = __esm({
  ".svelte-kit/output/server/nodes/11.js"() {
    init_page_server_ts3();
    index7 = 11;
    component7 = async () => component_cache7 ??= (await Promise.resolve().then(() => (init_page_svelte3(), page_svelte_exports3))).default;
    server_id4 = "src/routes/[[lang]]/library/public/[id]/+page.server.ts";
    imports7 = ["_app/immutable/nodes/11.Bp_9TytC.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js", "_app/immutable/chunks/i18n-routing.GhjsszfS.js", "_app/immutable/chunks/runtime.CM0qr2RF.js", "_app/immutable/chunks/MediaDisplay.BQa7FbJF.js", "_app/immutable/chunks/public.Bp7fU829.js", "_app/immutable/chunks/Header.lUXZAEz6.js", "_app/immutable/chunks/stores.C6La04jU.js", "_app/immutable/chunks/entry.Dv6jx6u8.js", "_app/immutable/chunks/TypicalPage._YIdHS8V.js", "_app/immutable/chunks/NiceBackground.CNQfe3-4.js", "_app/immutable/chunks/MediaContainer.BEwKe0F-.js", "_app/immutable/chunks/storage.kpBeg3J0.js", "_app/immutable/chunks/util.BeUAJj1L.js"];
    stylesheets7 = ["_app/immutable/assets/11.BR1Z81wc.css", "_app/immutable/assets/MediaDisplay.DYPbcJCr.css", "_app/immutable/assets/Header.Cih96lxr.css", "_app/immutable/assets/NiceBackground.D5oVBkOJ.css"];
    fonts7 = [];
  }
});
var page_server_ts_exports4 = {};
__export(page_server_ts_exports4, {
  actions: () => actions2,
  prerender: () => prerender5
});
var prerender5;
var actions2;
var init_page_server_ts4 = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/publish/_page.server.ts.js"() {
    init_chunks();
    prerender5 = false;
    actions2 = {
      request_publish: async ({ request, platform }) => {
        const data = await request.formData();
        const config = data.get("config");
        if (!config)
          return fail(400, { missing: true });
        const id = crypto.randomUUID();
        await platform?.env.BUCKET.put(id, config.toString());
        await platform?.env.DATABASE.prepare(
          "INSERT INTO pending_submissions (id, assigned, desired_id, r2_key) VALUES (NULL, NULL, ?1, ?1)"
        ).bind(id).run();
        return { r2_key: id };
      },
      check_publish: async ({ request, platform }) => {
        const data = await request.formData();
        const key2 = data.get("r2_key");
        if (!key2)
          return fail(400, { missing: true });
        const pendingRes = await platform?.env.DATABASE.prepare(
          "SELECT * FROM pending_submissions WHERE r2_key = ?1"
        ).bind(key2).first();
        if (pendingRes)
          return { status: "pending" };
        const deniedRes = await platform?.env.DATABASE.prepare(
          "SELECT reason FROM denied_submissions WHERE r2_key = ?1"
        ).bind(key2).first();
        if (deniedRes)
          return { status: "denied", reason: deniedRes.reason };
        return { status: "approved" };
      },
      request_update: async ({ request, platform }) => {
        const data = await request.formData();
        const config = data.get("config");
        const desiredId = data.get("id");
        if (!config || !desiredId)
          return fail(400, { missing: true });
        const id = crypto.randomUUID();
        await platform?.env.BUCKET.put(id, config.toString());
        await platform?.env.DATABASE.prepare(
          "INSERT INTO pending_submissions (id, assigned, desired_id, r2_key) VALUES (NULL, NULL, ?1, ?2)"
        ).bind(desiredId, id).run();
        return { r2_key: id };
      }
    };
  }
});
var css8;
var LoadingCircle;
var Loading;
var init_Loading = __esm({
  ".svelte-kit/output/server/chunks/Loading.js"() {
    init_ssr();
    init_NiceBackground();
    css8 = {
      code: "div.svelte-1whpmvz{box-sizing:border-box;border:var(--border-width) solid;border-radius:50%;border-top:var(--border-width) solid transparent;width:100%;height:100%;animation:svelte-1whpmvz-spin 1s linear infinite}@keyframes svelte-1whpmvz-spin{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}",
      map: null
    };
    LoadingCircle = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { borderWidth = 4 } = $$props;
      if ($$props.borderWidth === void 0 && $$bindings.borderWidth && borderWidth !== void 0)
        $$bindings.borderWidth(borderWidth);
      $$result.css.add(css8);
      return `<div class="svelte-1whpmvz"${add_styles({ "--border-width": `${borderWidth}px` })}></div>`;
    });
    Loading = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      return `<div${add_styles({
        "height": `100%`,
        "display": `flex`,
        "flex-direction": `column`
      })}><div${add_styles({ "flex": `1` })}>${validate_component(NiceBackground, "NiceBackground").$$render($$result, {}, {}, {
        default: () => {
          return `<div${add_styles({
            "height": `100%`,
            "display": `flex`,
            "justify-content": `center`,
            "align-items": `center`
          })}><div${add_styles({ "height": `40px`, "width": `40px` })}>${validate_component(LoadingCircle, "LoadingCircle").$$render($$result, { borderWidth: 8 }, {}, {})}</div></div>`;
        }
      })}</div></div>`;
    });
  }
});
var css9;
var FancyAnchorButtonBase;
var FancyAnchorButton;
var init_FancyAnchorButton = __esm({
  ".svelte-kit/output/server/chunks/FancyAnchorButton.js"() {
    init_ssr();
    css9 = {
      code: "a.svelte-5r2z69 .front.svelte-5r2z69{transform:translateY(-0.15em);transition:transform 150ms}a.svelte-5r2z69:active:not(:disabled) .front.svelte-5r2z69{transform:translateY(0em)}a.svelte-5r2z69:where(:hover, :focus) .front.svelte-5r2z69{transform:translateY(-0.3em)}",
      map: null
    };
    FancyAnchorButtonBase = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { foregroundColor } = $$props;
      let { backgroundColor } = $$props;
      let { backgroundDeepColor } = $$props;
      let { href } = $$props;
      if ($$props.foregroundColor === void 0 && $$bindings.foregroundColor && foregroundColor !== void 0)
        $$bindings.foregroundColor(foregroundColor);
      if ($$props.backgroundColor === void 0 && $$bindings.backgroundColor && backgroundColor !== void 0)
        $$bindings.backgroundColor(backgroundColor);
      if ($$props.backgroundDeepColor === void 0 && $$bindings.backgroundDeepColor && backgroundDeepColor !== void 0)
        $$bindings.backgroundDeepColor(backgroundDeepColor);
      if ($$props.href === void 0 && $$bindings.href && href !== void 0)
        $$bindings.href(href);
      $$result.css.add(css9);
      return `<a${add_attribute("href", href, 0)} class="svelte-5r2z69"${add_styles({
        "display": `flex`,
        "background": `none`,
        "border": `none`,
        "text-decoration": `none`,
        "box-sizing": `border-box`,
        "padding": `0.3em 0 0 0`,
        "width": `100%`,
        "height": `100%`,
        "font": `inherit`,
        "outline": `none`
      })}><div${add_styles({
        "background": backgroundDeepColor,
        "border-radius": `0.7em`,
        "transform": `translateY(0)`,
        "width": `100%`,
        "height": `100%`
      })}><div class="front svelte-5r2z69"${add_styles({
        "background": backgroundColor,
        "border": `0.15em solid ${backgroundDeepColor}`,
        "border-radius": `0.7em`,
        "box-sizing": `border-box`,
        "color": foregroundColor,
        "width": `100%`,
        "height": `100%`
      })}>${slots.default ? slots.default({}) : ``}</div></div> </a>`;
    });
    FancyAnchorButton = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { foregroundColor = void 0 } = $$props;
      let { backgroundColor = void 0 } = $$props;
      let { backgroundDeepColor = void 0 } = $$props;
      let { href } = $$props;
      if ($$props.foregroundColor === void 0 && $$bindings.foregroundColor && foregroundColor !== void 0)
        $$bindings.foregroundColor(foregroundColor);
      if ($$props.backgroundColor === void 0 && $$bindings.backgroundColor && backgroundColor !== void 0)
        $$bindings.backgroundColor(backgroundColor);
      if ($$props.backgroundDeepColor === void 0 && $$bindings.backgroundDeepColor && backgroundDeepColor !== void 0)
        $$bindings.backgroundDeepColor(backgroundDeepColor);
      if ($$props.href === void 0 && $$bindings.href && href !== void 0)
        $$bindings.href(href);
      return `${validate_component(FancyAnchorButtonBase, "FancyAnchorButtonBase").$$render(
        $$result,
        {
          foregroundColor: foregroundColor ?? "#FFFFFF",
          backgroundColor: backgroundColor ?? "#D4131B",
          backgroundDeepColor: backgroundDeepColor ?? "#A40E13",
          href
        },
        {},
        {
          default: () => {
            return `<div${add_styles({
              "height": `100%`,
              "padding": `5px`,
              "font-weight": `bold`,
              "box-sizing": `border-box`
            })}>${slots.default ? slots.default({}) : ``}</div>`;
          }
        }
      )}`;
    });
  }
});
var ___ASSET___04;
var init_error = __esm({
  ".svelte-kit/output/server/chunks/error.js"() {
    ___ASSET___04 = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20height='48'%20viewBox='0%20-960%20960%20960'%20width='48'%3e%3cpath%20d='M479.754-255Q504-255%20519.5-269.754q15.5-14.755%2015.5-39Q535-333%20519.746-349q-15.255-16-39.5-16Q456-365%20440.5-349.018%20425-333.035%20425-308.789q0%2024.245%2015.254%2039.017Q455.509-255%20479.754-255ZM436-432h94v-270h-94v270Zm44.404%20377q-88.872%200-166.125-33.084-77.254-33.083-135.183-91.012-57.929-57.929-91.012-135.119Q55-391.406%2055-480.362q0-88.957%2033.084-166.285%2033.083-77.328%2090.855-134.809%2057.772-57.482%20135.036-91.013Q391.238-906%20480.279-906q89.04%200%20166.486%2033.454%2077.446%2033.453%20134.853%2090.802%2057.407%2057.349%2090.895%20134.877Q906-569.34%20906-480.266q0%2089.01-33.531%20166.247-33.531%2077.237-91.013%20134.86-57.481%2057.623-134.831%2090.891Q569.276-55%20480.404-55Zm.096-94q137.5%200%20234-96.372T811-480.5q0-137.5-96.312-234Q618.375-811%20479.5-811q-137.5%200-234%2096.312Q149-618.375%20149-479.5q0%20137.5%2096.372%20234T480.5-149Zm-.5-331Z'/%3e%3c/svg%3e";
  }
});
var css10;
var Textfield;
var init_Textfield = __esm({
  ".svelte-kit/output/server/chunks/Textfield.js"() {
    init_ssr();
    init_error();
    init_LanguageSwitcher_svelte_svelte_type_style_lang();
    init_Header();
    css10 = {
      code: "div.svelte-1iaxln7.svelte-1iaxln7{display:flex;flex-direction:column;align-items:center}label.svelte-1iaxln7.svelte-1iaxln7{text-align:center;color:color-mix(in srgb, currentColor 50%, transparent);padding:0 8px;position:absolute;pointer-events:none;border-radius:5px 5px 0 0;top:50%;transform:translateY(-50%);line-height:10px;transform-origin:top;transition:all 100ms linear}input.svelte-1iaxln7:is(:not(:placeholder-shown), :focus, :active)+label.svelte-1iaxln7{top:0;scale:0.75;background:var(--background-color);transform:translateY(-50%)}#error.svelte-1iaxln7.svelte-1iaxln7{display:none}input.show-invalid.svelte-1iaxln7.svelte-1iaxln7:invalid{padding-right:1.8em;& ~ #error {\n			color: var(--accent-color);\n			display: flex;\n			justify-content: center;\n			align-items: center;\n			position: absolute;\n			bottom: 0;\n			top: 0;\n			right: 0.4em;\n		}}input.svelte-1iaxln7:focus+label.svelte-1iaxln7{color:var(--accent-color)}input.svelte-1iaxln7.svelte-1iaxln7{background:none;border:2px solid #a9a8aa;border-radius:10px;width:100%;color:inherit;box-sizing:border-box;font:inherit;box-sizing:border-box;text-align:center;padding:8px 5px;resize:none;transition:border-color 100ms linear}input.svelte-1iaxln7.svelte-1iaxln7:focus{outline:none;border-color:var(--accent-color)}",
      map: null
    };
    Textfield = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { id } = $$props;
      let { placeholder } = $$props;
      let { required } = $$props;
      let { disabled } = $$props;
      let { value } = $$props;
      let { textTransform = "" } = $$props;
      let { autocomplete = "off" } = $$props;
      let { maxLength = void 0 } = $$props;
      let { minLength = void 0 } = $$props;
      let { showInvalid = true } = $$props;
      let { inputmode = void 0 } = $$props;
      if ($$props.id === void 0 && $$bindings.id && id !== void 0)
        $$bindings.id(id);
      if ($$props.placeholder === void 0 && $$bindings.placeholder && placeholder !== void 0)
        $$bindings.placeholder(placeholder);
      if ($$props.required === void 0 && $$bindings.required && required !== void 0)
        $$bindings.required(required);
      if ($$props.disabled === void 0 && $$bindings.disabled && disabled !== void 0)
        $$bindings.disabled(disabled);
      if ($$props.value === void 0 && $$bindings.value && value !== void 0)
        $$bindings.value(value);
      if ($$props.textTransform === void 0 && $$bindings.textTransform && textTransform !== void 0)
        $$bindings.textTransform(textTransform);
      if ($$props.autocomplete === void 0 && $$bindings.autocomplete && autocomplete !== void 0)
        $$bindings.autocomplete(autocomplete);
      if ($$props.maxLength === void 0 && $$bindings.maxLength && maxLength !== void 0)
        $$bindings.maxLength(maxLength);
      if ($$props.minLength === void 0 && $$bindings.minLength && minLength !== void 0)
        $$bindings.minLength(minLength);
      if ($$props.showInvalid === void 0 && $$bindings.showInvalid && showInvalid !== void 0)
        $$bindings.showInvalid(showInvalid);
      if ($$props.inputmode === void 0 && $$bindings.inputmode && inputmode !== void 0)
        $$bindings.inputmode(inputmode);
      $$result.css.add(css10);
      return `<div class="svelte-1iaxln7"${add_styles({ "position": `relative` })}><input class="${escape(null_to_empty(showInvalid ? "show-invalid" : ""), true) + " svelte-1iaxln7"}"${add_attribute("id", id, 0)}${add_attribute("name", id, 0)}${add_attribute("autocomplete", autocomplete, 0)} type="text" ${required ? "required" : ""} ${disabled ? "disabled" : ""} placeholder=""${add_attribute("maxlength", maxLength, 0)}${add_attribute("minlength", minLength, 0)}${add_attribute("inputmode", inputmode, 0)}${add_styles({ "text-transform": textTransform })}${add_attribute("value", value, 0)}> <label${add_attribute("for", id, 0)} class="svelte-1iaxln7">${escape(placeholder)}</label> <div id="error" class="svelte-1iaxln7">${validate_component(Icon, "Icon").$$render(
        $$result,
        {
          src: ___ASSET___04,
          alt: length_too_short(),
          size: "1.2em"
        },
        {},
        {}
      )}</div> </div>`;
    });
  }
});
function retrieveRemoteSync(provider) {
  if (provider === "google")
    return new GoogleDriveSync();
  throw `got unexpected '${provider}' as provider`;
}
async function reconcile(remoteDatabase, localDatabase, onRemote, hashOnRemote, onLocal) {
  const getRemote = (() => {
    const ids = new Map(
      onRemote.map(
        (c2) => c2.uniqueId ? [c2.uniqueId, c2] : void 0
      ).filter(isNotUndefined)
    );
    return (id) => {
      return ids.get(id);
    };
  })();
  const getLocal = (() => {
    const ids = new Map(
      onLocal.map(
        ([key2, c2]) => c2.uniqueId ? [c2.uniqueId, [key2, c2]] : void 0
      ).filter(isNotUndefined)
    );
    return (id) => {
      return ids.get(id);
    };
  })();
  const onlyInRemote = onRemote.filter((c2) => !c2.uniqueId || getLocal(c2.uniqueId) === void 0);
  const onlyInExisting = onLocal.filter(
    ([, c2]) => !c2.uniqueId || getRemote(c2.uniqueId) === void 0
  );
  const remoteNewer = onRemote.map((c2) => {
    if (!c2.uniqueId)
      return;
    const local = getLocal(c2.uniqueId);
    if (!local)
      return;
    const [localKey, localInternal] = local;
    const localVersion = localInternal.versionId ?? 0;
    const remoteVersion = c2.versionId ?? 0;
    return localVersion < remoteVersion ? [c2, localKey] : void 0;
  }).filter(isNotUndefined);
  const localNewer = onLocal.filter(([, c2]) => {
    if (!c2.uniqueId)
      return false;
    const remote = getRemote(c2.uniqueId);
    return remote && (remote.versionId ?? 0) < (c2.versionId ?? 0);
  });
  async function updateLocalImages(internal2) {
    const references = (await Promise.all(
      internal2?.config.slides.map(async (s3) => {
        if (!s3.MultipleChoice.media)
          return void 0;
        const mediaReference = s3.MultipleChoice.media;
        if (typeof mediaReference === "string") {
          const media = await remoteDatabase.getImage(mediaReference);
          if (!media)
            return void 0;
          return [mediaReference, media];
        }
        if ("HashReference" in mediaReference.Image) {
          const media = await remoteDatabase.getImage(mediaReference.Image.HashReference.hash);
          if (!media)
            return void 0;
          return [mediaReference.Image.HashReference.hash, media];
        }
        return void 0;
      }) ?? []
    )).filter(isNotUndefined);
    references.map(([hash2, media]) => {
      updateLocalImagesDatabse(media, hash2, localDatabase);
    });
  }
  async function images(internal2) {
    const references = (await Promise.all(
      internal2?.config.slides.map(async (s3) => {
        if (!s3.MultipleChoice.media)
          return void 0;
        const mediaReference = s3.MultipleChoice.media;
        if (typeof mediaReference === "string") {
          const media = await retrieveMediaFromLocal(mediaReference, localDatabase);
          if (!media)
            return void 0;
          return [mediaReference, media];
        }
        if ("HashReference" in mediaReference.Image) {
          const media = await retrieveMediaFromLocal(
            mediaReference.Image.HashReference.hash,
            localDatabase,
            mediaReference.Image.HashReference.alt
          );
          if (!media)
            return void 0;
          return [mediaReference.Image.HashReference.hash, media];
        }
        return void 0;
      }) ?? []
    )).filter(isNotUndefined);
    return references.map(([hash2, media]) => {
      return [hash2, "Base64" in media.Image ? media.Image.Base64.data : media];
    });
  }
  return await Promise.all([
    ...onlyInRemote.map(async (c2) => {
      if (!c2.uniqueId)
        return;
      const config = await remoteDatabase.get(c2.uniqueId);
      if (!config)
        return;
      const internal2 = {
        ...c2,
        config
      };
      await updateLocalImages(internal2);
      await addCreationLocal(internal2, localDatabase);
    }),
    ...onlyInExisting.map(async ([key2]) => {
      const internal2 = await getCreationLocal(key2, localDatabase);
      if (!internal2)
        return;
      const uniqueId = internal2.uniqueId ?? generateUuid();
      await updateCreationLocal(
        key2,
        {
          ...internal2,
          uniqueId
        },
        localDatabase
      );
      await Promise.all(
        (await images(internal2)).filter(([hash2]) => !hashOnRemote(hash2)).map(async ([hash2, media]) => await remoteDatabase.createImage(hash2, media))
      );
      await remoteDatabase.create(uniqueId, internal2);
    }),
    ...remoteNewer.map(async ([c2, localKey]) => {
      if (!c2.uniqueId)
        return;
      const config = await remoteDatabase.get(c2.uniqueId);
      if (!config)
        return;
      const internal2 = {
        ...c2,
        config
      };
      await updateLocalImages(internal2);
      await updateCreationLocal(localKey, internal2, localDatabase);
    }),
    ...localNewer.map(async ([key2]) => {
      const internal2 = await getCreationLocal(key2, localDatabase);
      if (!internal2)
        return;
      const uniqueId = internal2.uniqueId ?? generateUuid();
      await updateCreationLocal(
        key2,
        {
          ...internal2,
          uniqueId
        },
        localDatabase
      );
      internal2 && await Promise.all(
        (await images(internal2)).filter(([hash2]) => !hashOnRemote(hash2)).map(async ([hash2, media]) => await remoteDatabase.createImage(hash2, media))
      );
      internal2 && await remoteDatabase.update(uniqueId, internal2);
    })
  ]);
}
function generateUuid() {
  function generateRandomHex(length) {
    function convertToHex(value, length2) {
      return value.toString(16).padStart(length2, "0");
    }
    function convertUint8ArrayToHex(values2) {
      return [...values2].map((v) => convertToHex(v, 2)).join("");
    }
    const values = new Uint8Array(length / 2);
    crypto.getRandomValues(values);
    return convertUint8ArrayToHex(values);
  }
  function splitString(value, lengths) {
    return lengths.reduce(
      ({ value: value2, res }, length) => {
        return {
          res: [...res, value2.slice(0, length)],
          value: value2.slice(length)
        };
      },
      { value, res: [] }
    ).res;
  }
  try {
    return crypto.randomUUID();
  } catch (e3) {
    return splitString(generateRandomHex(32), [8, 4, 4, 4, 12]).join("-");
  }
}
function hashMedia(media) {
  if ("Base64" in media.Image) {
    const hash2 = media.Image.Base64.hash ?? (0, import_object_hash2.default)(media.Image.Base64.data);
    return [hash2, media.Image.Base64.alt, media.Image.Base64.data];
  } else if ("Corkboard" in media.Image) {
    return [media.Image.Corkboard.id, media.Image.Corkboard.alt, media];
  } else {
    return [media.Image.Url.url, media.Image.Url.alt, media];
  }
}
async function hashExists(hash2, database) {
  const imagesStore = database.transaction(["images"], "readwrite").objectStore("images");
  const request = imagesStore.count(hash2);
  return await new Promise((resolve2) => {
    request.addEventListener("success", () => {
      resolve2(request.result > 0);
    });
  });
}
async function updateLocalImagesDatabse(media, hash2, database) {
  if (!await hashExists(hash2, database)) {
    const imagesStore = database.transaction(["images"], "readwrite").objectStore("images");
    imagesStore.add(media, hash2);
    return true;
  }
  return false;
}
async function updateImagesDatabse(media, hash2, database) {
  if (await updateLocalImagesDatabse(media, hash2, database.local)) {
    database.remotes.forEach((db) => {
      db.createImage(hash2, media);
    });
  }
}
function internalizeMedia(media, database) {
  if (media == void 0)
    return void 0;
  const [hash2, alt, newMedia] = hashMedia(media);
  updateImagesDatabse(newMedia, hash2, database);
  return {
    Image: {
      HashReference: {
        hash: hash2,
        alt
      }
    }
  };
}
function internalizeFuiz(fuiz, database) {
  return {
    ...fuiz,
    config: {
      ...fuiz.config,
      slides: fuiz.config.slides.map((slide) => ({
        MultipleChoice: {
          ...slide.MultipleChoice,
          media: internalizeMedia(slide.MultipleChoice.media, database)
        }
      }))
    }
  };
}
async function retrieveMediaFromLocal(hash2, database, alt) {
  const imagesStore = database.transaction(["images"], "readonly").objectStore("images");
  const transaction = imagesStore.get(hash2);
  return await new Promise((resolve2) => {
    transaction.addEventListener("success", () => {
      const value = transaction.result ?? void 0;
      if (!value)
        resolve2(void 0);
      else if (typeof value === "string") {
        resolve2({
          Image: {
            Base64: {
              data: value,
              alt: alt ?? ""
            }
          }
        });
      } else {
        resolve2(value);
      }
    });
  });
}
async function collectMedia(media, database) {
  if (media == void 0)
    return void 0;
  if (typeof media === "string") {
    return await retrieveMediaFromLocal(media, database);
  } else if ("HashReference" in media.Image) {
    return await retrieveMediaFromLocal(
      media.Image.HashReference.hash,
      database,
      media.Image.HashReference.alt
    );
  } else {
    return {
      Image: media.Image
    };
  }
}
async function collectFuiz(fuiz, database) {
  return {
    ...fuiz,
    uniqueId: fuiz.uniqueId ?? generateUuid(),
    versionId: fuiz.versionId ?? 0,
    config: {
      ...fuiz.config,
      slides: await Promise.all(
        fuiz.config.slides.map(
          async (slide) => slide.MultipleChoice.media ? {
            MultipleChoice: {
              ...slide.MultipleChoice,
              media: await collectMedia(slide.MultipleChoice.media, database)
            }
          } : {
            MultipleChoice: {
              introduce_question: slide.MultipleChoice.introduce_question,
              time_limit: slide.MultipleChoice.time_limit,
              points_awarded: slide.MultipleChoice.points_awarded,
              answers: slide.MultipleChoice.answers,
              title: slide.MultipleChoice.title
            }
          }
        )
      )
    }
  };
}
async function loadDatabase(remote) {
  const request = indexedDB.open("FuizDB", 2);
  request.addEventListener("upgradeneeded", (event) => {
    const db = request.result;
    if (event.oldVersion != 1) {
      db.createObjectStore("creations", { autoIncrement: true });
    }
    db.createObjectStore("images");
  });
  return await new Promise((resolve2, reject2) => {
    request.addEventListener("success", () => {
      const remotes = [];
      if (remote.google) {
        remotes.push(retrieveRemoteSync("google"));
      }
      resolve2({
        local: request.result,
        remotes
      });
    });
    request.addEventListener("error", reject2);
  });
}
async function sync(database) {
  for (const remote of database.remotes) {
    await remote.sync(
      database.local,
      (await getAllCreationsLocal(database.local)).map(([k2, v]) => [parseInt(k2.toString()), v])
    );
  }
}
async function getAllCreationsLocal(database) {
  const creationsStore = database.transaction(["creations"], "readwrite").objectStore("creations");
  const creationsTransaction = creationsStore.openCursor();
  return await new Promise((resolve2) => {
    const internals = [];
    creationsTransaction.addEventListener("success", async () => {
      const cursor = creationsTransaction.result;
      if (cursor) {
        const value = cursor.value;
        const strictValue = {
          config: value.config,
          lastEdited: value.lastEdited,
          uniqueId: value.uniqueId ?? generateUuid(),
          versionId: value.versionId ?? 0,
          publish: value.publish
        };
        cursor.update(strictValue);
        internals.push([cursor.key, strictValue]);
        cursor.continue();
      } else {
        resolve2(internals);
      }
    });
  });
}
async function getAllCreations(database) {
  await sync(database);
  const internals = await getAllCreationsLocal(database.local);
  return await Promise.all(
    internals.map(async ([key2, f]) => {
      const value = await collectFuiz(f, database.local);
      return {
        id: parseInt(key2.toString()),
        lastEdited: value.lastEdited,
        title: value.config.title,
        slidesCount: value.config.slides.length,
        media: value.config.slides.reduce(
          (p, c2) => p || c2.MultipleChoice.media,
          void 0
        )
      };
    })
  );
}
async function getCreationLocal(id, database) {
  const creationsStore = database.transaction(["creations"], "readonly").objectStore("creations");
  const creationsTransaction = creationsStore.get(id);
  return await new Promise((resolve2) => {
    creationsTransaction.addEventListener("success", async () => {
      resolve2(creationsTransaction.result ?? void 0);
    });
  });
}
async function getCreation(id, database) {
  const internal2 = await getCreationLocal(id, database.local);
  return internal2 ? await collectFuiz(internal2, database.local) : void 0;
}
async function addCreationLocal(internalFuiz, database) {
  const creationsStore = database.transaction(["creations"], "readwrite").objectStore("creations");
  const request = creationsStore.put(internalFuiz);
  return await new Promise((resolve2) => {
    request.addEventListener("success", () => {
      resolve2(parseInt(request.result.toString()));
    });
  });
}
async function updateCreationLocal(id, internalFuiz, database) {
  const creationsStore = database.transaction(["creations"], "readwrite").objectStore("creations");
  const request = creationsStore.put(internalFuiz, id);
  return await new Promise((resolve2) => {
    request.addEventListener("success", () => {
      resolve2(void 0);
    });
  });
}
async function updateCreation(id, newSlide, database) {
  const internalFuiz = internalizeFuiz(newSlide, database);
  await updateCreationLocal(id, internalFuiz, database.local);
  database.remotes.forEach((db) => db.update(newSlide.uniqueId, internalFuiz));
}
var import_object_hash2;
var GoogleDriveSync;
var warning;
var Message;
var ErrorMessage;
var init_ErrorMessage = __esm({
  ".svelte-kit/output/server/chunks/ErrorMessage.js"() {
    import_object_hash2 = __toESM(require_object_hash(), 1);
    init_util();
    init_ssr();
    init_LanguageSwitcher_svelte_svelte_type_style_lang();
    init_Header();
    GoogleDriveSync = class {
      static async connect() {
        window.open("/google/login", "fuizWindow", "popup");
      }
      static async disconnect() {
        await bring("/google/logout");
        window.location.reload();
      }
      async sync(localDatabase, existing) {
        const res = await fetch(`/google/list`);
        const imageList = await bring("/google/listImage");
        const imageSet = new Set(await imageList?.json() ?? []);
        await reconcile(this, localDatabase, await res.json(), (hash2) => imageSet.has(hash2), existing);
      }
      async get(uuid) {
        const res = await bring(`/google/get/${uuid}`);
        return res?.ok ? await res.json() : void 0;
      }
      async create(uuid, internalFuiz) {
        await fetch(`/google/create/${uuid}`, {
          method: "POST",
          body: JSON.stringify(internalFuiz)
        });
      }
      async update(uuid, internalFuiz) {
        await fetch(`/google/update/${uuid}`, {
          method: "POST",
          body: JSON.stringify(internalFuiz)
        });
      }
      async delete(uuid) {
        await fetch(`/google/delete/${uuid}`);
      }
      async createImage(hash2, value) {
        const reqExists = await bring(`/google/existImage/${hash2}`);
        if (!reqExists?.ok || await reqExists.json())
          return void 0;
        const serialized = typeof value === "string" ? value : JSON.stringify(value);
        await fetch(`/google/createImage/${hash2}`, {
          method: "POST",
          body: serialized
        });
      }
      async getImage(hash2) {
        const res = await bring(`/google/getImage/${hash2}`);
        if (!res?.ok)
          return void 0;
        const media = await res.text();
        try {
          return JSON.parse(media);
        } catch (e3) {
          return media;
        }
      }
    };
    warning = "data:image/svg+xml,%3csvg%20xmlns='http://www.w3.org/2000/svg'%20height='24'%20viewBox='0%20-960%20960%20960'%20width='24'%3e%3cpath%20d='M120-103q-18%200-32.087-8.8Q73.826-120.6%2066-135q-8-14-8.5-30.597T66-198l359-622q9-16%2024.103-23.5%2015.102-7.5%2031-7.5Q496-851%20511-843.5q15%207.5%2024%2023.5l359%20622q9%2015.806%208.5%2032.403T894-135q-8%2014-22%2023t-32%209H120Zm83-111h554L480-692%20203-214Zm277-29q18%200%2031.5-13.5T525-288q0-18-13.5-31T480-332q-18%200-31.5%2013T435-288q0%2018%2013.5%2031.5T480-243Zm0-117q17%200%2028.5-11.5T520-400v-109q0-17-11.5-28.5T480-549q-17%200-28.5%2011.5T440-509v109q0%2017%2011.5%2028.5T480-360Zm0-93Z'/%3e%3c/svg%3e";
    Message = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { message } = $$props;
      let { image } = $$props;
      let { background } = $$props;
      let { color } = $$props;
      if ($$props.message === void 0 && $$bindings.message && message !== void 0)
        $$bindings.message(message);
      if ($$props.image === void 0 && $$bindings.image && image !== void 0)
        $$bindings.image(image);
      if ($$props.background === void 0 && $$bindings.background && background !== void 0)
        $$bindings.background(background);
      if ($$props.color === void 0 && $$bindings.color && color !== void 0)
        $$bindings.color(color);
      return `${message ? `<div${add_styles({
        "width": `100%`,
        "display": `flex`,
        "align-items": `center`,
        background,
        "padding": `5px 10px`,
        "box-sizing": `border-box`,
        "border-radius": `5px`,
        "gap": `10px`,
        color,
        "font-weight": `bold`,
        "border": `1px solid ${color}`,
        "word-wrap": `anywhere`
      })}>${validate_component(Icon, "Icon").$$render(
        $$result,
        {
          size: "1em",
          src: image.src,
          alt: image.alt
        },
        {},
        {}
      )} <div${add_styles({ "flex": `1`, "text-align": `center` })}>${escape(message)}</div></div>` : ``}`;
    });
    ErrorMessage = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { errorMessage } = $$props;
      if ($$props.errorMessage === void 0 && $$bindings.errorMessage && errorMessage !== void 0)
        $$bindings.errorMessage(errorMessage);
      return `${validate_component(Message, "Message").$$render(
        $$result,
        {
          message: errorMessage,
          background: "#e01b2430",
          color: "#e01b24",
          image: { src: warning, alt: error_alt() }
        },
        {},
        {}
      )}`;
    });
  }
});
var page_svelte_exports4 = {};
__export(page_svelte_exports4, {
  default: () => Page4
});
function deserialize(result) {
  const parsed = JSON.parse(result);
  if (parsed.data) {
    parsed.data = parse(parsed.data);
  }
  return parsed;
}
function map(lang2) {
  return new Intl.DisplayNames([lang2], { type: "language" }).of(lang2) || lang2;
}
function parseInt2(str) {
  if (str === null) {
    return null;
  }
  try {
    return Number.parseInt(str);
  } catch {
    return null;
  }
}
var css$22;
var SelectTime;
var Tags;
var css$13;
var Publish;
var css11;
var Page4;
var init_page_svelte4 = __esm({
  ".svelte-kit/output/server/entries/pages/__lang__/publish/_page.svelte.js"() {
    init_ssr();
    init_LanguageSwitcher_svelte_svelte_type_style_lang();
    init_stores();
    init_Loading();
    init_NiceBackground();
    init_Header();
    init_FancyAnchorButton();
    init_i18n_routing();
    init_runtime();
    init_public();
    init_j_toml();
    init_Textfield();
    init_TypicalPage();
    init_FancyButton();
    init_dist();
    init_MediaContainer();
    init_devalue();
    init_ErrorMessage();
    css$22 = {
      code: "#container.svelte-1eqi93c{position:fixed;z-index:1;top:0;left:0;height:100%;width:100%;background-color:#ffffffa0;display:flex;align-items:center;justify-content:center;& ul {\n			margin: 0;\n			padding: 10px;\n			display: grid;\n			grid-gap: 10px;\n			font-size: max(7vmin, 1em);\n			grid-template-columns: repeat(auto-fit, minmax(6ch, 1fr));\n			width: min(80vw, 90vh);\n			flex-wrap: wrap;\n\n			& li {\n				display: block;\n			}\n		}}",
      map: null
    };
    SelectTime = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let $listbox, $$unsubscribe_listbox;
      let { options: options3 } = $$props;
      let { selected } = $$props;
      let { map: map2 = (a) => a } = $$props;
      const listbox = zi({ label: "Actions", selected });
      $$unsubscribe_listbox = subscribe(listbox, (value) => $listbox = value);
      if ($$props.options === void 0 && $$bindings.options && options3 !== void 0)
        $$bindings.options(options3);
      if ($$props.selected === void 0 && $$bindings.selected && selected !== void 0)
        $$bindings.selected(selected);
      if ($$props.map === void 0 && $$bindings.map && map2 !== void 0)
        $$bindings.map(map2);
      $$result.css.add(css$22);
      $$unsubscribe_listbox();
      return `${validate_component(FancyButton, "FancyButton").$$render($$result, { action: listbox.button }, {}, {
        default: () => {
          return `<div${add_styles({
            "display": `flex`,
            "align-items": `center`,
            "justify-content": `center`
          })}>${slots.default ? slots.default({}) : ``} <div${add_styles({
            "padding": `0 5px`,
            "text-transform": `capitalize`
          })}>${escape(map2(selected.toString()))}</div></div>`;
        }
      })} ${$listbox.expanded ? `<div id="container" class="svelte-1eqi93c"><ul>${each(options3, (value) => {
        return `<li>${validate_component(FancyButton, "FancyButton").$$render($$result, { action: (n2) => listbox.item(n2, { value }) }, {}, {
          default: () => {
            return `<div${add_styles({
              "padding": `0.25em 0.5em`,
              "text-transform": `capitalize`
            })}>${escape(map2(value.toString()))}</div> `;
          }
        })} </li>`;
      })}</ul></div>` : ``}`;
    });
    Tags = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let { tags: tags2 } = $$props;
      if ($$props.tags === void 0 && $$bindings.tags && tags2 !== void 0)
        $$bindings.tags(tags2);
      let $$settled;
      let $$rendered;
      let previous_head = $$result.head;
      do {
        $$settled = true;
        $$result.head = previous_head;
        $$rendered = `${each(tags2, (tag$12, index10) => {
          return `${validate_component(Textfield, "Textfield").$$render(
            $$result,
            {
              id: "tag" + index10,
              placeholder: tag(),
              required: false,
              disabled: false,
              value: tag$12
            },
            {
              value: ($$value) => {
                tag$12 = $$value;
                $$settled = false;
              }
            },
            {}
          )}`;
        })} ${tags2.length == 0 || tags2.at(-1) ? `<div>${validate_component(FancyButton, "FancyButton").$$render($$result, { type: "button" }, {}, {
          default: () => {
            return `${escape(add_tag())}`;
          }
        })}</div>` : ``}`;
      } while (!$$settled);
      return $$rendered;
    });
    css$13 = {
      code: "p.svelte-e6j7vk{margin:0}",
      map: null
    };
    Publish = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let media;
      let requestStatus;
      let rememberStatus;
      let { creation } = $$props;
      let { id } = $$props;
      let { db } = $$props;
      let author$12 = "";
      let tags2 = [];
      let lang = languageTag();
      let reasonState = void 0;
      async function checkRequest(pending_r2_key) {
        if (!pending_r2_key)
          return void 0;
        const formdata = new FormData();
        formdata.append("r2_key", pending_r2_key);
        const res = await fetch("?/check_publish", {
          method: "POST",
          mode: "same-origin",
          body: formdata,
          headers: { "x-sveltekit-action": "true" }
        });
        const resjson = deserialize(await res.text());
        if (resjson.type !== "success")
          return void 0;
        const { status, reason: reason2 } = (
          /* eslint-disable */
          resjson.data || { status: void 0 }
        );
        if (!status)
          return void 0;
        if (reason2) {
          reasonState = reason2;
        }
        if (status === "approved" || status === "denied") {
          creation = {
            ...creation,
            publish: {
              ...creation.publish,
              ...status === "approved" && { released_r2_key: pending_r2_key },
              pending_r2_key: void 0
            }
          };
          await updateCreation(id, creation, db);
        }
        return status;
      }
      async function memorize(requestStatus2) {
        return await rememberStatus || await requestStatus2;
      }
      if ($$props.creation === void 0 && $$bindings.creation && creation !== void 0)
        $$bindings.creation(creation);
      if ($$props.id === void 0 && $$bindings.id && id !== void 0)
        $$bindings.id(id);
      if ($$props.db === void 0 && $$bindings.db && db !== void 0)
        $$bindings.db(db);
      $$result.css.add(css$13);
      let $$settled;
      let $$rendered;
      let previous_head = $$result.head;
      do {
        $$settled = true;
        $$result.head = previous_head;
        media = creation.config.slides.reduce((m2, s3) => m2 || s3.MultipleChoice.media, void 0);
        requestStatus = checkRequest(creation.publish?.pending_r2_key);
        rememberStatus = memorize(requestStatus);
        $$rendered = `${validate_component(TypicalPage, "TypicalPage").$$render($$result, {}, {}, {
          default: () => {
            return `<section${add_styles({ "max-width": `20ch`, "margin": `auto` })}>${!creation?.publish?.pending_r2_key ? `<form${add_styles({
              "height": `100%`,
              "display": `flex`,
              "justify-content": `center`,
              "flex-direction": `column`,
              "gap": `0.5em`
            })}>${function(__value) {
              if (is_promise(__value)) {
                __value.then(null, noop);
                return ``;
              }
              return function(res) {
                return ` ${res === "approved" || creation?.publish?.released_r2_key ? `<p class="svelte-e6j7vk">${escape(request_approved())}</p>` : `${res === "denied" ? `<p class="svelte-e6j7vk">${escape(request_denied({ reasonState: reasonState || unknown() }))}</p>` : ``}`} `;
              }(__value);
            }(rememberStatus)} <div${add_styles({
              "border": `0.15em solid`,
              "border-radius": `0.5em`,
              "overflow": `hidden`
            })}><div${add_styles({
              "aspect-ratio": `3 / 2`,
              "height": `auto`,
              "position": `relative`,
              "border-bottom": `0.15em solid`
            })}>${validate_component(MediaContainer, "MediaContainer").$$render($$result, { media, fit: "cover" }, {}, {})}</div> <div${add_styles({
              "padding": `0.15em 0.3em`,
              "word-break": `break-word`
            })}>${escape(creation.config.title)}</div></div> ${validate_component(Textfield, "Textfield").$$render(
              $$result,
              {
                id: "author",
                placeholder: author(),
                required: true,
                disabled: false,
                showInvalid: false,
                value: author$12
              },
              {
                value: ($$value) => {
                  author$12 = $$value;
                  $$settled = false;
                }
              },
              {}
            )} ${validate_component(Tags, "Tags").$$render(
              $$result,
              { tags: tags2 },
              {
                tags: ($$value) => {
                  tags2 = $$value;
                  $$settled = false;
                }
              },
              {}
            )} <div>${validate_component(SelectTime, "SelectTime").$$render(
              $$result,
              {
                options: [...availableLanguageTags],
                map,
                selected: lang
              },
              {
                selected: ($$value) => {
                  lang = $$value;
                  $$settled = false;
                }
              },
              {
                default: () => {
                  return `${validate_component(Icon, "Icon").$$render(
                    $$result,
                    {
                      src: ___ASSET___0,
                      alt: language(),
                      size: "1em"
                    },
                    {},
                    {}
                  )}`;
                }
              }
            )}</div> <div>${validate_component(FancyButton, "FancyButton").$$render($$result, {}, {}, {
              default: () => {
                return `<div${add_styles({ "font-family": `Poppins` })}>${creation.publish?.released_r2_key ? `${escape(request_update())}` : `${escape(request_publish())}`}</div>`;
              }
            })}</div></form>` : `Request is pending`}</section>`;
          }
        })}`;
      } while (!$$settled);
      return $$rendered;
    });
    css11 = {
      code: "section.svelte-1ifie66.svelte-1ifie66{flex:1;display:flex;max-width:25ch;flex-direction:column;justify-content:center;padding:0.5em;width:100%}#creations-list.svelte-1ifie66.svelte-1ifie66{display:flex;flex-direction:column;border:0.2em solid;border-radius:0.5em;padding:0;margin:0}.creation.svelte-1ifie66.svelte-1ifie66{display:flex}hr.svelte-1ifie66.svelte-1ifie66{appearance:none;width:100%;color:inherit;border-top:0.2em solid;margin:0}h2.svelte-1ifie66.svelte-1ifie66{margin:0 0 0.5em;font-size:1.25em}.creation.svelte-1ifie66 a.svelte-1ifie66{flex:1;text-decoration:inherit;color:inherit;padding:0.4em;margin:0}.create.svelte-1ifie66.svelte-1ifie66{font-family:Poppins;text-align:center}",
      map: null
    };
    Page4 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      let id;
      let $page, $$unsubscribe_page;
      $$unsubscribe_page = subscribe(page, (value) => $page = value);
      let { data } = $$props;
      const title = publish_title();
      const description = publish_desc();
      if ($$props.data === void 0 && $$bindings.data && data !== void 0)
        $$bindings.data(data);
      $$result.css.add(css11);
      id = parseInt2($page.url.searchParams.get("id"));
      $$unsubscribe_page();
      return `${$$result.head += `<!-- HEAD_svelte-dgwf6m_START -->${$$result.title = `<title>${escape(title)}</title>`, ""}<meta property="og:title"${add_attribute("content", title, 0)}><meta name="description"${add_attribute("content", description, 0)}><meta property="og:description"${add_attribute("content", description, 0)}><link rel="canonical" href="${escape(PUBLIC_PLAY_URL, true) + escape(route("/publish", languageTag()), true)}"><!-- HEAD_svelte-dgwf6m_END -->`, ""} ${id ? (() => {
        let filteredId = id;
        return ` ${function(__value) {
          if (is_promise(__value)) {
            __value.then(null, noop);
            return ` ${validate_component(Loading, "Loading").$$render($$result, {}, {}, {})} `;
          }
          return function({ db, creation }) {
            return ` ${creation ? `${validate_component(Publish, "Publish").$$render($$result, { creation, id, db }, {}, {})}` : `${validate_component(ErrorMessage, "ErrorMessage").$$render($$result, { errorMessage: missing_fuiz() }, {}, {})}`} `;
          }(__value);
        }(loadDatabase({ google: data.google }).then(async (db) => ({
          db,
          creation: await getCreation(filteredId, db)
        })))}`;
      })() : `${function(__value) {
        if (is_promise(__value)) {
          __value.then(null, noop);
          return ` ${validate_component(Loading, "Loading").$$render($$result, {}, {}, {})} `;
        }
        return function(creations) {
          let sortedCreations = creations.sort((a, b) => -b.lastEdited - a.lastEdited);
          return `  ${validate_component(NiceBackground, "NiceBackground").$$render($$result, {}, {}, {
            default: () => {
              return `<div${add_styles({
                "height": `100%`,
                "display": `flex`,
                "flex-direction": `column`,
                "align-items": `center`,
                "padding": `0.5em`,
                "box-sizing": `border-box`
              })}><header${add_styles({ "margin": `0.5em 0` })}>${validate_component(Header, "Header").$$render($$result, {}, {}, {})}</header> <section class="svelte-1ifie66">${creations.length > 0 ? `<h2 class="svelte-1ifie66">${escape(choose_local())}</h2> <ul id="creations-list" class="svelte-1ifie66">${each(sortedCreations, ({ title: title2, id: id2, slidesCount }, index10) => {
                return `<li class="creation svelte-1ifie66"><a${add_attribute("href", "?id=" + id2, 0)} class="svelte-1ifie66">${escape(title2)} \xB7 ${escape(slides_count({ count: slidesCount }))}</a></li> ${index10 + 1 != creations.length ? `<hr class="svelte-1ifie66">` : ``}`;
              })}</ul>` : `<div>${validate_component(FancyAnchorButton, "FancyAnchorButton").$$render($$result, { href: route("/create", languageTag()) }, {}, {
                default: () => {
                  return `<div class="create svelte-1ifie66">${escape(create2())}</div>`;
                }
              })}</div>`}</section> ${validate_component(Footer, "Footer").$$render($$result, {}, {}, {})}</div>`;
            }
          })} `;
        }(__value);
      }(loadDatabase({ google: data.google }).then((db) => getAllCreations(db)))}`}`;
    });
  }
});
var __exports8 = {};
__export(__exports8, {
  component: () => component8,
  fonts: () => fonts8,
  imports: () => imports8,
  index: () => index8,
  server: () => page_server_ts_exports4,
  server_id: () => server_id5,
  stylesheets: () => stylesheets8
});
var index8;
var component_cache8;
var component8;
var server_id5;
var imports8;
var stylesheets8;
var fonts8;
var init__8 = __esm({
  ".svelte-kit/output/server/nodes/15.js"() {
    init_page_server_ts4();
    index8 = 15;
    component8 = async () => component_cache8 ??= (await Promise.resolve().then(() => (init_page_svelte4(), page_svelte_exports4))).default;
    server_id5 = "src/routes/[[lang]]/publish/+page.server.ts";
    imports8 = ["_app/immutable/nodes/15.B2k0dW7R.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/await_block.Dykn38up.js", "_app/immutable/chunks/index.C2sGjX4M.js", "_app/immutable/chunks/i18n-routing.GhjsszfS.js", "_app/immutable/chunks/runtime.CM0qr2RF.js", "_app/immutable/chunks/Header.lUXZAEz6.js", "_app/immutable/chunks/stores.C6La04jU.js", "_app/immutable/chunks/entry.Dv6jx6u8.js", "_app/immutable/chunks/ErrorMessage.D0FRmlGo.js", "_app/immutable/chunks/NiceBackground.CNQfe3-4.js", "_app/immutable/chunks/FancyAnchorButton.B-JF2KxB.js", "_app/immutable/chunks/public.Bp7fU829.js", "_app/immutable/chunks/index.DPnII_k3.js", "_app/immutable/chunks/util.BeUAJj1L.js", "_app/immutable/chunks/Textfield.Yf4omwMO.js", "_app/immutable/chunks/error.D7oIHJjY.js", "_app/immutable/chunks/TypicalPage._YIdHS8V.js", "_app/immutable/chunks/MediaDisplay.BQa7FbJF.js", "_app/immutable/chunks/SelectTime.BxMDbB9Q.js", "_app/immutable/chunks/MediaContainer.BEwKe0F-.js", "_app/immutable/chunks/storage.kpBeg3J0.js"];
    stylesheets8 = ["_app/immutable/assets/15.Da9GuPfP.css", "_app/immutable/assets/Header.Cih96lxr.css", "_app/immutable/assets/ErrorMessage.DHtK-L-z.css", "_app/immutable/assets/NiceBackground.D5oVBkOJ.css", "_app/immutable/assets/FancyAnchorButton.BW2Askik.css", "_app/immutable/assets/Textfield.BbvOhDA7.css", "_app/immutable/assets/MediaDisplay.DYPbcJCr.css", "_app/immutable/assets/SelectTime.8wAem7V4.css"];
    fonts8 = [];
  }
});
var page_server_ts_exports5 = {};
__export(page_server_ts_exports5, {
  load: () => load5
});
async function getTokensFromCode(code) {
  const { clientId, clientSecret, redirectUri } = options();
  const response = await fetch("https://oauth2.googleapis.com/token", {
    method: "POST",
    headers: {
      "content-type": "application/json",
      accept: "application/json"
    },
    body: JSON.stringify({
      client_id: clientId,
      client_secret: clientSecret,
      redirect_uri: redirectUri,
      scope,
      code,
      grant_type: "authorization_code"
    })
  });
  const result = await response.json();
  if (result.error) {
    throw new Error(result.error_description);
  }
  return result;
}
var load5;
var init_page_server_ts5 = __esm({
  ".svelte-kit/output/server/entries/pages/google/callback/_page.server.ts.js"() {
    init_chunks();
    init_index2();
    init_googleUtil();
    load5 = async ({ url, cookies }) => {
      const code = url.searchParams.get("code");
      if (!code)
        error(504, "no");
      cookies.set("google", JSON.stringify(await getTokensFromCode(code)), {
        path: "/",
        httpOnly: true,
        sameSite: "strict",
        secure: !dev,
        maxAge: 60 * 60 * 24 * 30
      });
      return {};
    };
  }
});
var page_svelte_exports5 = {};
__export(page_svelte_exports5, {
  default: () => Page5
});
var Page5;
var init_page_svelte5 = __esm({
  ".svelte-kit/output/server/entries/pages/google/callback/_page.svelte.js"() {
    init_ssr();
    Page5 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
      return ``;
    });
  }
});
var __exports9 = {};
__export(__exports9, {
  component: () => component9,
  fonts: () => fonts9,
  imports: () => imports9,
  index: () => index9,
  server: () => page_server_ts_exports5,
  server_id: () => server_id6,
  stylesheets: () => stylesheets9
});
var index9;
var component_cache9;
var component9;
var server_id6;
var imports9;
var stylesheets9;
var fonts9;
var init__9 = __esm({
  ".svelte-kit/output/server/nodes/16.js"() {
    init_page_server_ts5();
    index9 = 16;
    component9 = async () => component_cache9 ??= (await Promise.resolve().then(() => (init_page_svelte5(), page_svelte_exports5))).default;
    server_id6 = "src/routes/google/callback/+page.server.ts";
    imports9 = ["_app/immutable/nodes/16.CNwstNk-.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js"];
    stylesheets9 = [];
    fonts9 = [];
  }
});
var server_ts_exports = {};
__export(server_ts_exports, {
  GET: () => GET,
  prerender: () => prerender6
});
var prerender6;
var GET;
var init_server_ts = __esm({
  ".svelte-kit/output/server/entries/endpoints/__lang__/admin/thumbnail/_server.ts.js"() {
    init_j_toml();
    init_chunks();
    init_serverOnlyUtils();
    prerender6 = false;
    GET = async ({ request, platform }) => {
      const email = request.headers.get("Cf-Access-Authenticated-User-Email");
      if (!email) {
        return error(400, "not allowed");
      }
      const { r2_key: r2Key } = await platform?.env.DATABASE.prepare(
        `SELECT * FROM pending_submissions WHERE assigned = ?1 LIMIT 1`
      ).bind(email).first() || { r2_key: void 0 };
      if (!r2Key)
        error(400, "not allowed");
      const fuizBody = await platform?.env.BUCKET.get(r2Key) || void 0;
      if (!fuizBody)
        error(401, "not allowed");
      const res = await fuizBody.text();
      const { config } = parse$1(res, { bigint: false });
      return new Response((await getThumbnail(config))?.thumbnail);
    };
  }
});
var server_ts_exports2 = {};
__export(server_ts_exports2, {
  GET: () => GET2
});
var GET2;
var init_server_ts2 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/clearImage/_server.ts.js"() {
    init_googleUtil();
    GET2 = async ({ cookies }) => {
      const service = getDrive(cookies);
      const files = await getImages(service, async (f) => {
        await delay(10);
        await service.deleteFile(f);
      });
      return new Response(JSON.stringify(files.length), {
        headers: {
          "Content-Type": "application/json"
        }
      });
    };
  }
});
var server_ts_exports3 = {};
__export(server_ts_exports3, {
  GET: () => GET3
});
var GET3;
var init_server_ts3 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/clear/_server.ts.js"() {
    init_googleUtil();
    GET3 = async ({ cookies }) => {
      const service = getDrive(cookies);
      const files = await getCreations(service, async (f) => {
        await delay(10);
        await service.deleteFile(f);
      });
      return new Response(JSON.stringify(files.length), {
        headers: {
          "Content-Type": "application/json"
        }
      });
    };
  }
});
var server_ts_exports4 = {};
__export(server_ts_exports4, {
  POST: () => POST
});
var POST;
var init_server_ts4 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/createImage/_hash_/_server.ts.js"() {
    init_googleUtil();
    POST = async ({ request, cookies, params: { hash: hash2 } }) => {
      const service = getDrive(cookies);
      if (await getFileIdFromName(service, `${hash2}.txt`) !== void 0) {
        return new Response();
      }
      const data = await request.text();
      await service.create(
        { name: `${hash2}.txt` },
        {
          type: "text/plain",
          data
        }
      );
      return new Response();
    };
  }
});
var server_ts_exports5 = {};
__export(server_ts_exports5, {
  POST: () => POST2
});
var POST2;
var init_server_ts5 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/create/_uuid_/_server.ts.js"() {
    init_googleUtil();
    POST2 = async ({ request, cookies, params: { uuid } }) => {
      const service = getDrive(cookies);
      const data = await request.json();
      const { config: content, ...metadata } = data;
      await service.create(
        {
          name: `${uuid}.json`,
          properties: {
            ...metadata
          }
        },
        {
          type: "application/json",
          data: JSON.stringify(content)
        }
      );
      return new Response();
    };
  }
});
var server_ts_exports6 = {};
__export(server_ts_exports6, {
  GET: () => GET4
});
var GET4;
var init_server_ts6 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/delete/_uuid_/_server.ts.js"() {
    init_chunks();
    init_googleUtil();
    GET4 = async ({ cookies, params: { uuid } }) => {
      const service = getDrive(cookies);
      const file = await getFileIdFromName(service, uuid + ".json");
      if (!file)
        error(401, "file doesnt exist");
      await service.deleteFile(file);
      return new Response();
    };
  }
});
var server_ts_exports7 = {};
__export(server_ts_exports7, {
  GET: () => GET5
});
var GET5;
var init_server_ts7 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/existImage/_hash_/_server.ts.js"() {
    init_googleUtil();
    GET5 = async ({ cookies, params: { hash: hash2 } }) => {
      const service = getDrive(cookies);
      return new Response(
        JSON.stringify(await getFileIdFromName(service, hash2 + ".txt") !== void 0)
      );
    };
  }
});
var server_ts_exports8 = {};
__export(server_ts_exports8, {
  GET: () => GET6
});
var GET6;
var init_server_ts8 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/getImage/_hash_/_server.ts.js"() {
    init_chunks();
    init_googleUtil();
    GET6 = async ({ cookies, params: { hash: hash2 } }) => {
      const service = getDrive(cookies);
      const file = await getFileIdFromName(service, hash2 + ".txt");
      if (!file)
        error(401, "file doesnt exist");
      return new Response(await service.content(file));
    };
  }
});
var server_ts_exports9 = {};
__export(server_ts_exports9, {
  GET: () => GET7
});
var GET7;
var init_server_ts9 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/get/_uuid_/_server.ts.js"() {
    init_chunks();
    init_googleUtil();
    GET7 = async ({ cookies, params: { uuid } }) => {
      const service = getDrive(cookies);
      const file = await getFileIdFromName(service, uuid + ".json");
      if (!file)
        error(401, "file doesnt exist");
      return new Response(await service.content(file), {
        headers: {
          "Content-Type": "application/json"
        }
      });
    };
  }
});
var server_ts_exports10 = {};
__export(server_ts_exports10, {
  GET: () => GET8
});
var GET8;
var init_server_ts10 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/listImage/_server.ts.js"() {
    init_googleUtil();
    init_util();
    GET8 = async ({ cookies }) => {
      const service = getDrive(cookies);
      const files = await getImages(
        service,
        (f) => f.name?.split(".")?.at(0)
      );
      return new Response(JSON.stringify(files.filter(isNotUndefined)), {
        headers: {
          "Content-Type": "application/json"
        }
      });
    };
  }
});
var server_ts_exports11 = {};
__export(server_ts_exports11, {
  GET: () => GET9
});
var GET9;
var init_server_ts11 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/list/_server.ts.js"() {
    init_googleUtil();
    init_util();
    GET9 = async ({ cookies }) => {
      const service = getDrive(cookies);
      const files = await getCreations(service, (f) => {
        const { lastEdited, versionId, uniqueId } = f.properties ?? {
          lastEdited: void 0,
          versionId: void 0,
          uniqueId: void 0
        };
        if (!lastEdited || !versionId || !uniqueId)
          return void 0;
        return {
          lastEdited: parseInt(lastEdited),
          versionId: parseInt(versionId),
          uniqueId
        };
      });
      return new Response(JSON.stringify(files.filter(isNotUndefined)), {
        headers: {
          "Content-Type": "application/json"
        }
      });
    };
  }
});
function decodeComponents(components, split) {
  try {
    return [decodeURIComponent(components.join(""))];
  } catch {
  }
  if (components.length === 1) {
    return components;
  }
  split = split || 1;
  const left = components.slice(0, split);
  const right = components.slice(split);
  return Array.prototype.concat.call([], decodeComponents(left), decodeComponents(right));
}
function decode(input) {
  try {
    return decodeURIComponent(input);
  } catch {
    let tokens = input.match(singleMatcher) || [];
    for (let i = 1; i < tokens.length; i++) {
      input = decodeComponents(tokens, i).join("");
      tokens = input.match(singleMatcher) || [];
    }
    return input;
  }
}
function customDecodeURIComponent(input) {
  const replaceMap = {
    "%FE%FF": "\uFFFD\uFFFD",
    "%FF%FE": "\uFFFD\uFFFD"
  };
  let match = multiMatcher.exec(input);
  while (match) {
    try {
      replaceMap[match[0]] = decodeURIComponent(match[0]);
    } catch {
      const result = decode(match[0]);
      if (result !== match[0]) {
        replaceMap[match[0]] = result;
      }
    }
    match = multiMatcher.exec(input);
  }
  replaceMap["%C2"] = "\uFFFD";
  const entries = Object.keys(replaceMap);
  for (const key2 of entries) {
    input = input.replace(new RegExp(key2, "g"), replaceMap[key2]);
  }
  return input;
}
function decodeUriComponent(encodedURI) {
  if (typeof encodedURI !== "string") {
    throw new TypeError("Expected `encodedURI` to be of type `string`, got `" + typeof encodedURI + "`");
  }
  try {
    return decodeURIComponent(encodedURI);
  } catch {
    return customDecodeURIComponent(encodedURI);
  }
}
var token;
var singleMatcher;
var multiMatcher;
var init_decode_uri_component = __esm({
  "node_modules/decode-uri-component/index.js"() {
    token = "%[a-f0-9]{2}";
    singleMatcher = new RegExp("(" + token + ")|([^%]+?)", "gi");
    multiMatcher = new RegExp("(" + token + ")+", "gi");
  }
});
function splitOnFirst(string, separator) {
  if (!(typeof string === "string" && typeof separator === "string")) {
    throw new TypeError("Expected the arguments to be of type `string`");
  }
  if (string === "" || separator === "") {
    return [];
  }
  const separatorIndex = string.indexOf(separator);
  if (separatorIndex === -1) {
    return [];
  }
  return [
    string.slice(0, separatorIndex),
    string.slice(separatorIndex + separator.length)
  ];
}
var init_split_on_first = __esm({
  "node_modules/split-on-first/index.js"() {
  }
});
function includeKeys(object, predicate) {
  const result = {};
  if (Array.isArray(predicate)) {
    for (const key2 of predicate) {
      const descriptor = Object.getOwnPropertyDescriptor(object, key2);
      if (descriptor?.enumerable) {
        Object.defineProperty(result, key2, descriptor);
      }
    }
  } else {
    for (const key2 of Reflect.ownKeys(object)) {
      const descriptor = Object.getOwnPropertyDescriptor(object, key2);
      if (descriptor.enumerable) {
        const value = object[key2];
        if (predicate(key2, value, object)) {
          Object.defineProperty(result, key2, descriptor);
        }
      }
    }
  }
  return result;
}
var init_filter_obj = __esm({
  "node_modules/filter-obj/index.js"() {
  }
});
var base_exports = {};
__export(base_exports, {
  exclude: () => exclude,
  extract: () => extract,
  parse: () => parse4,
  parseUrl: () => parseUrl,
  pick: () => pick,
  stringify: () => stringify2,
  stringifyUrl: () => stringifyUrl
});
function encoderForArrayFormat(options3) {
  switch (options3.arrayFormat) {
    case "index": {
      return (key2) => (result, value) => {
        const index10 = result.length;
        if (value === void 0 || options3.skipNull && value === null || options3.skipEmptyString && value === "") {
          return result;
        }
        if (value === null) {
          return [
            ...result,
            [encode2(key2, options3), "[", index10, "]"].join("")
          ];
        }
        return [
          ...result,
          [encode2(key2, options3), "[", encode2(index10, options3), "]=", encode2(value, options3)].join("")
        ];
      };
    }
    case "bracket": {
      return (key2) => (result, value) => {
        if (value === void 0 || options3.skipNull && value === null || options3.skipEmptyString && value === "") {
          return result;
        }
        if (value === null) {
          return [
            ...result,
            [encode2(key2, options3), "[]"].join("")
          ];
        }
        return [
          ...result,
          [encode2(key2, options3), "[]=", encode2(value, options3)].join("")
        ];
      };
    }
    case "colon-list-separator": {
      return (key2) => (result, value) => {
        if (value === void 0 || options3.skipNull && value === null || options3.skipEmptyString && value === "") {
          return result;
        }
        if (value === null) {
          return [
            ...result,
            [encode2(key2, options3), ":list="].join("")
          ];
        }
        return [
          ...result,
          [encode2(key2, options3), ":list=", encode2(value, options3)].join("")
        ];
      };
    }
    case "comma":
    case "separator":
    case "bracket-separator": {
      const keyValueSep = options3.arrayFormat === "bracket-separator" ? "[]=" : "=";
      return (key2) => (result, value) => {
        if (value === void 0 || options3.skipNull && value === null || options3.skipEmptyString && value === "") {
          return result;
        }
        value = value === null ? "" : value;
        if (result.length === 0) {
          return [[encode2(key2, options3), keyValueSep, encode2(value, options3)].join("")];
        }
        return [[result, encode2(value, options3)].join(options3.arrayFormatSeparator)];
      };
    }
    default: {
      return (key2) => (result, value) => {
        if (value === void 0 || options3.skipNull && value === null || options3.skipEmptyString && value === "") {
          return result;
        }
        if (value === null) {
          return [
            ...result,
            encode2(key2, options3)
          ];
        }
        return [
          ...result,
          [encode2(key2, options3), "=", encode2(value, options3)].join("")
        ];
      };
    }
  }
}
function parserForArrayFormat(options3) {
  let result;
  switch (options3.arrayFormat) {
    case "index": {
      return (key2, value, accumulator) => {
        result = /\[(\d*)]$/.exec(key2);
        key2 = key2.replace(/\[\d*]$/, "");
        if (!result) {
          accumulator[key2] = value;
          return;
        }
        if (accumulator[key2] === void 0) {
          accumulator[key2] = {};
        }
        accumulator[key2][result[1]] = value;
      };
    }
    case "bracket": {
      return (key2, value, accumulator) => {
        result = /(\[])$/.exec(key2);
        key2 = key2.replace(/\[]$/, "");
        if (!result) {
          accumulator[key2] = value;
          return;
        }
        if (accumulator[key2] === void 0) {
          accumulator[key2] = [value];
          return;
        }
        accumulator[key2] = [...accumulator[key2], value];
      };
    }
    case "colon-list-separator": {
      return (key2, value, accumulator) => {
        result = /(:list)$/.exec(key2);
        key2 = key2.replace(/:list$/, "");
        if (!result) {
          accumulator[key2] = value;
          return;
        }
        if (accumulator[key2] === void 0) {
          accumulator[key2] = [value];
          return;
        }
        accumulator[key2] = [...accumulator[key2], value];
      };
    }
    case "comma":
    case "separator": {
      return (key2, value, accumulator) => {
        const isArray2 = typeof value === "string" && value.includes(options3.arrayFormatSeparator);
        const isEncodedArray = typeof value === "string" && !isArray2 && decode2(value, options3).includes(options3.arrayFormatSeparator);
        value = isEncodedArray ? decode2(value, options3) : value;
        const newValue = isArray2 || isEncodedArray ? value.split(options3.arrayFormatSeparator).map((item) => decode2(item, options3)) : value === null ? value : decode2(value, options3);
        accumulator[key2] = newValue;
      };
    }
    case "bracket-separator": {
      return (key2, value, accumulator) => {
        const isArray2 = /(\[])$/.test(key2);
        key2 = key2.replace(/\[]$/, "");
        if (!isArray2) {
          accumulator[key2] = value ? decode2(value, options3) : value;
          return;
        }
        const arrayValue = value === null ? [] : value.split(options3.arrayFormatSeparator).map((item) => decode2(item, options3));
        if (accumulator[key2] === void 0) {
          accumulator[key2] = arrayValue;
          return;
        }
        accumulator[key2] = [...accumulator[key2], ...arrayValue];
      };
    }
    default: {
      return (key2, value, accumulator) => {
        if (accumulator[key2] === void 0) {
          accumulator[key2] = value;
          return;
        }
        accumulator[key2] = [...[accumulator[key2]].flat(), value];
      };
    }
  }
}
function validateArrayFormatSeparator(value) {
  if (typeof value !== "string" || value.length !== 1) {
    throw new TypeError("arrayFormatSeparator must be single character string");
  }
}
function encode2(value, options3) {
  if (options3.encode) {
    return options3.strict ? strictUriEncode(value) : encodeURIComponent(value);
  }
  return value;
}
function decode2(value, options3) {
  if (options3.decode) {
    return decodeUriComponent(value);
  }
  return value;
}
function keysSorter(input) {
  if (Array.isArray(input)) {
    return input.sort();
  }
  if (typeof input === "object") {
    return keysSorter(Object.keys(input)).sort((a, b) => Number(a) - Number(b)).map((key2) => input[key2]);
  }
  return input;
}
function removeHash(input) {
  const hashStart = input.indexOf("#");
  if (hashStart !== -1) {
    input = input.slice(0, hashStart);
  }
  return input;
}
function getHash(url) {
  let hash2 = "";
  const hashStart = url.indexOf("#");
  if (hashStart !== -1) {
    hash2 = url.slice(hashStart);
  }
  return hash2;
}
function parseValue(value, options3) {
  if (options3.parseNumbers && !Number.isNaN(Number(value)) && (typeof value === "string" && value.trim() !== "")) {
    value = Number(value);
  } else if (options3.parseBooleans && value !== null && (value.toLowerCase() === "true" || value.toLowerCase() === "false")) {
    value = value.toLowerCase() === "true";
  }
  return value;
}
function extract(input) {
  input = removeHash(input);
  const queryStart = input.indexOf("?");
  if (queryStart === -1) {
    return "";
  }
  return input.slice(queryStart + 1);
}
function parse4(query, options3) {
  options3 = {
    decode: true,
    sort: true,
    arrayFormat: "none",
    arrayFormatSeparator: ",",
    parseNumbers: false,
    parseBooleans: false,
    ...options3
  };
  validateArrayFormatSeparator(options3.arrayFormatSeparator);
  const formatter = parserForArrayFormat(options3);
  const returnValue = /* @__PURE__ */ Object.create(null);
  if (typeof query !== "string") {
    return returnValue;
  }
  query = query.trim().replace(/^[?#&]/, "");
  if (!query) {
    return returnValue;
  }
  for (const parameter of query.split("&")) {
    if (parameter === "") {
      continue;
    }
    const parameter_ = options3.decode ? parameter.replace(/\+/g, " ") : parameter;
    let [key2, value] = splitOnFirst(parameter_, "=");
    if (key2 === void 0) {
      key2 = parameter_;
    }
    value = value === void 0 ? null : ["comma", "separator", "bracket-separator"].includes(options3.arrayFormat) ? value : decode2(value, options3);
    formatter(decode2(key2, options3), value, returnValue);
  }
  for (const [key2, value] of Object.entries(returnValue)) {
    if (typeof value === "object" && value !== null) {
      for (const [key22, value2] of Object.entries(value)) {
        value[key22] = parseValue(value2, options3);
      }
    } else {
      returnValue[key2] = parseValue(value, options3);
    }
  }
  if (options3.sort === false) {
    return returnValue;
  }
  return (options3.sort === true ? Object.keys(returnValue).sort() : Object.keys(returnValue).sort(options3.sort)).reduce((result, key2) => {
    const value = returnValue[key2];
    result[key2] = Boolean(value) && typeof value === "object" && !Array.isArray(value) ? keysSorter(value) : value;
    return result;
  }, /* @__PURE__ */ Object.create(null));
}
function stringify2(object, options3) {
  if (!object) {
    return "";
  }
  options3 = {
    encode: true,
    strict: true,
    arrayFormat: "none",
    arrayFormatSeparator: ",",
    ...options3
  };
  validateArrayFormatSeparator(options3.arrayFormatSeparator);
  const shouldFilter = (key2) => options3.skipNull && isNullOrUndefined(object[key2]) || options3.skipEmptyString && object[key2] === "";
  const formatter = encoderForArrayFormat(options3);
  const objectCopy = {};
  for (const [key2, value] of Object.entries(object)) {
    if (!shouldFilter(key2)) {
      objectCopy[key2] = value;
    }
  }
  const keys2 = Object.keys(objectCopy);
  if (options3.sort !== false) {
    keys2.sort(options3.sort);
  }
  return keys2.map((key2) => {
    const value = object[key2];
    if (value === void 0) {
      return "";
    }
    if (value === null) {
      return encode2(key2, options3);
    }
    if (Array.isArray(value)) {
      if (value.length === 0 && options3.arrayFormat === "bracket-separator") {
        return encode2(key2, options3) + "[]";
      }
      return value.reduce(formatter(key2), []).join("&");
    }
    return encode2(key2, options3) + "=" + encode2(value, options3);
  }).filter((x3) => x3.length > 0).join("&");
}
function parseUrl(url, options3) {
  options3 = {
    decode: true,
    ...options3
  };
  let [url_, hash2] = splitOnFirst(url, "#");
  if (url_ === void 0) {
    url_ = url;
  }
  return {
    url: url_?.split("?")?.[0] ?? "",
    query: parse4(extract(url), options3),
    ...options3 && options3.parseFragmentIdentifier && hash2 ? { fragmentIdentifier: decode2(hash2, options3) } : {}
  };
}
function stringifyUrl(object, options3) {
  options3 = {
    encode: true,
    strict: true,
    [encodeFragmentIdentifier]: true,
    ...options3
  };
  const url = removeHash(object.url).split("?")[0] || "";
  const queryFromUrl = extract(object.url);
  const query = {
    ...parse4(queryFromUrl, { sort: false }),
    ...object.query
  };
  let queryString = stringify2(query, options3);
  if (queryString) {
    queryString = `?${queryString}`;
  }
  let hash2 = getHash(object.url);
  if (object.fragmentIdentifier) {
    const urlObjectForFragmentEncode = new URL(url);
    urlObjectForFragmentEncode.hash = object.fragmentIdentifier;
    hash2 = options3[encodeFragmentIdentifier] ? urlObjectForFragmentEncode.hash : `#${object.fragmentIdentifier}`;
  }
  return `${url}${queryString}${hash2}`;
}
function pick(input, filter, options3) {
  options3 = {
    parseFragmentIdentifier: true,
    [encodeFragmentIdentifier]: false,
    ...options3
  };
  const { url, query, fragmentIdentifier } = parseUrl(input, options3);
  return stringifyUrl({
    url,
    query: includeKeys(query, filter),
    fragmentIdentifier
  }, options3);
}
function exclude(input, filter, options3) {
  const exclusionFilter = Array.isArray(filter) ? (key2) => !filter.includes(key2) : (key2, value) => !filter(key2, value);
  return pick(input, exclusionFilter, options3);
}
var isNullOrUndefined;
var strictUriEncode;
var encodeFragmentIdentifier;
var init_base = __esm({
  "node_modules/query-string/base.js"() {
    init_decode_uri_component();
    init_split_on_first();
    init_filter_obj();
    isNullOrUndefined = (value) => value === null || value === void 0;
    strictUriEncode = (string) => encodeURIComponent(string).replace(/[!'()*]/g, (x3) => `%${x3.charCodeAt(0).toString(16).toUpperCase()}`);
    encodeFragmentIdentifier = Symbol("encodeFragmentIdentifier");
  }
});
var query_string_default;
var init_query_string = __esm({
  "node_modules/query-string/index.js"() {
    init_base();
    query_string_default = base_exports;
  }
});
var server_ts_exports12 = {};
__export(server_ts_exports12, {
  GET: () => GET10
});
var GET10;
var init_server_ts12 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/login/_server.ts.js"() {
    init_chunks();
    init_googleUtil();
    init_query_string();
    GET10 = async () => {
      const { clientId, redirectUri } = options();
      redirect(
        302,
        `https://accounts.google.com/o/oauth2/v2/auth?${query_string_default.stringify({
          client_id: clientId,
          redirect_uri: redirectUri,
          response_type: "code",
          scope,
          include_granted_scopes: "true",
          state: "pass-through value",
          access_type: "offline",
          prompt: "consent"
        })}`
      );
    };
  }
});
var server_ts_exports13 = {};
__export(server_ts_exports13, {
  GET: () => GET11
});
var GET11;
var init_server_ts13 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/logout/_server.ts.js"() {
    GET11 = async ({ cookies }) => {
      cookies.delete("google", { path: "/" });
      return new Response();
    };
  }
});
var server_ts_exports14 = {};
__export(server_ts_exports14, {
  POST: () => POST3
});
var POST3;
var init_server_ts14 = __esm({
  ".svelte-kit/output/server/entries/endpoints/google/update/_uuid_/_server.ts.js"() {
    init_chunks();
    init_googleUtil();
    POST3 = async ({ request, cookies, params: { uuid } }) => {
      const service = getDrive(cookies);
      const file = await getFileIdFromName(service, uuid + ".json");
      if (!file)
        error(401, "file doesnt exist");
      const data = await request.json();
      const { config: content, ...metadata } = data;
      service.update(
        {
          id: file.id,
          name: `${uuid}.json`,
          properties: {
            ...metadata
          }
        },
        {
          data: JSON.stringify(content),
          type: "application/json"
        }
      );
      return new Response();
    };
  }
});
init_prod_ssr();
init_ssr();
init_shared_server();
var base = "";
var assets = base;
var initial = { base, assets };
function reset() {
  base = initial.base;
  assets = initial.assets;
}
function afterUpdate() {
}
var prerendering = false;
var Root = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { stores } = $$props;
  let { page: page2 } = $$props;
  let { constructors } = $$props;
  let { components = [] } = $$props;
  let { form } = $$props;
  let { data_0 = null } = $$props;
  let { data_1 = null } = $$props;
  let { data_2 = null } = $$props;
  {
    setContext("__svelte__", stores);
  }
  afterUpdate(stores.page.notify);
  if ($$props.stores === void 0 && $$bindings.stores && stores !== void 0)
    $$bindings.stores(stores);
  if ($$props.page === void 0 && $$bindings.page && page2 !== void 0)
    $$bindings.page(page2);
  if ($$props.constructors === void 0 && $$bindings.constructors && constructors !== void 0)
    $$bindings.constructors(constructors);
  if ($$props.components === void 0 && $$bindings.components && components !== void 0)
    $$bindings.components(components);
  if ($$props.form === void 0 && $$bindings.form && form !== void 0)
    $$bindings.form(form);
  if ($$props.data_0 === void 0 && $$bindings.data_0 && data_0 !== void 0)
    $$bindings.data_0(data_0);
  if ($$props.data_1 === void 0 && $$bindings.data_1 && data_1 !== void 0)
    $$bindings.data_1(data_1);
  if ($$props.data_2 === void 0 && $$bindings.data_2 && data_2 !== void 0)
    $$bindings.data_2(data_2);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    {
      stores.page.set(page2);
    }
    $$rendered = `  ${constructors[1] ? `${validate_component(constructors[0] || missing_component, "svelte:component").$$render(
      $$result,
      { data: data_0, this: components[0] },
      {
        this: ($$value) => {
          components[0] = $$value;
          $$settled = false;
        }
      },
      {
        default: () => {
          return `${constructors[2] ? `${validate_component(constructors[1] || missing_component, "svelte:component").$$render(
            $$result,
            { data: data_1, this: components[1] },
            {
              this: ($$value) => {
                components[1] = $$value;
                $$settled = false;
              }
            },
            {
              default: () => {
                return `${validate_component(constructors[2] || missing_component, "svelte:component").$$render(
                  $$result,
                  { data: data_2, form, this: components[2] },
                  {
                    this: ($$value) => {
                      components[2] = $$value;
                      $$settled = false;
                    }
                  },
                  {}
                )}`;
              }
            }
          )}` : `${validate_component(constructors[1] || missing_component, "svelte:component").$$render(
            $$result,
            { data: data_1, form, this: components[1] },
            {
              this: ($$value) => {
                components[1] = $$value;
                $$settled = false;
              }
            },
            {}
          )}`}`;
        }
      }
    )}` : `${validate_component(constructors[0] || missing_component, "svelte:component").$$render(
      $$result,
      { data: data_0, form, this: components[0] },
      {
        this: ($$value) => {
          components[0] = $$value;
          $$settled = false;
        }
      },
      {}
    )}`} ${``}`;
  } while (!$$settled);
  return $$rendered;
});
var options2 = {
  app_dir: "_app",
  app_template_contains_nonce: false,
  csp: { "mode": "auto", "directives": { "upgrade-insecure-requests": false, "block-all-mixed-content": false }, "reportOnly": { "upgrade-insecure-requests": false, "block-all-mixed-content": false } },
  csrf_check_origin: true,
  embedded: false,
  env_public_prefix: "PUBLIC_",
  env_private_prefix: "",
  hooks: null,
  // added lazily, via `get_hooks`
  preload_strategy: "modulepreload",
  root: Root,
  service_worker: false,
  templates: {
    app: ({ head, body: body2, assets: assets2, nonce, env }) => '<!DOCTYPE html>\n<html lang="%lang%" style="min-height: 100%; display: flex">\n	<head>\n		<meta charset="utf-8" />\n		<link rel="icon" href="' + assets2 + '/favicon.svg" />\n		<meta name="viewport" content="width=device-width" />\n		' + head + '\n		<title>Fuiz | Host Live Quizzes Freely</title>\n		<meta name="description" content="A free online platform for host and playing live quizzes" />\n		<meta property="og:title" content="Fuiz" />\n		<meta property="og:url" content="https://fuiz.us/" />\n		<meta property="og:type" content="website" />\n		<meta property="og:image" content="' + assets2 + '/image.png" />\n		<meta property="og:image:alt" content="Fuiz: Host Live Quizzes Freely" />\n		<meta\n			property="og:description"\n			content="A free online platform for host and playing live quizzes"\n		/>\n		<meta property="og:site_name" content="Fuiz" />\n		<meta name="twitter:card" content="summary_large_image" />\n	</head>\n\n	<body data-sveltekit-preload-data="hover" style="display: contents">\n		<div style="flex: 1">' + body2 + "</div>\n	</body>\n</html>\n",
    error: ({ status, message }) => '<!doctype html>\n<html lang="en">\n	<head>\n		<meta charset="utf-8" />\n		<title>' + message + `</title>

		<style>
			body {
				--bg: white;
				--fg: #222;
				--divider: #ccc;
				background: var(--bg);
				color: var(--fg);
				font-family:
					system-ui,
					-apple-system,
					BlinkMacSystemFont,
					'Segoe UI',
					Roboto,
					Oxygen,
					Ubuntu,
					Cantarell,
					'Open Sans',
					'Helvetica Neue',
					sans-serif;
				display: flex;
				align-items: center;
				justify-content: center;
				height: 100vh;
				margin: 0;
			}

			.error {
				display: flex;
				align-items: center;
				max-width: 32rem;
				margin: 0 1rem;
			}

			.status {
				font-weight: 200;
				font-size: 3rem;
				line-height: 1;
				position: relative;
				top: -0.05rem;
			}

			.message {
				border-left: 1px solid var(--divider);
				padding: 0 0 0 1rem;
				margin: 0 0 0 1rem;
				min-height: 2.5rem;
				display: flex;
				align-items: center;
			}

			.message h1 {
				font-weight: 400;
				font-size: 1em;
				margin: 0;
			}

			@media (prefers-color-scheme: dark) {
				body {
					--bg: #222;
					--fg: #ddd;
					--divider: #666;
				}
			}
		</style>
	</head>
	<body>
		<div class="error">
			<span class="status">` + status + '</span>\n			<div class="message">\n				<h1>' + message + "</h1>\n			</div>\n		</div>\n	</body>\n</html>\n"
  },
  version_hash: "1suh1o7"
};
async function get_hooks() {
  return {
    ...await Promise.resolve().then(() => (init_hooks_server(), hooks_server_exports))
  };
}
init_chunks();
init_exports();
init_devalue();
init_ssr();
init_shared_server();
var import_cookie = __toESM(require_cookie(), 1);
var set_cookie_parser = __toESM(require_set_cookie(), 1);
var ENDPOINT_METHODS = ["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS", "HEAD"];
var PAGE_METHODS = ["GET", "POST", "HEAD"];
function negotiate(accept, types) {
  const parts = [];
  accept.split(",").forEach((str, i) => {
    const match = /([^/]+)\/([^;]+)(?:;q=([0-9.]+))?/.exec(str);
    if (match) {
      const [, type, subtype, q2 = "1"] = match;
      parts.push({ type, subtype, q: +q2, i });
    }
  });
  parts.sort((a, b) => {
    if (a.q !== b.q) {
      return b.q - a.q;
    }
    if (a.subtype === "*" !== (b.subtype === "*")) {
      return a.subtype === "*" ? 1 : -1;
    }
    if (a.type === "*" !== (b.type === "*")) {
      return a.type === "*" ? 1 : -1;
    }
    return a.i - b.i;
  });
  let accepted;
  let min_priority = Infinity;
  for (const mimetype of types) {
    const [type, subtype] = mimetype.split("/");
    const priority = parts.findIndex(
      (part) => (part.type === type || part.type === "*") && (part.subtype === subtype || part.subtype === "*")
    );
    if (priority !== -1 && priority < min_priority) {
      accepted = mimetype;
      min_priority = priority;
    }
  }
  return accepted;
}
function is_content_type(request, ...types) {
  const type = request.headers.get("content-type")?.split(";", 1)[0].trim() ?? "";
  return types.includes(type.toLowerCase());
}
function is_form_content_type(request) {
  return is_content_type(
    request,
    "application/x-www-form-urlencoded",
    "multipart/form-data",
    "text/plain"
  );
}
function coalesce_to_error(err) {
  return err instanceof Error || err && /** @type {any} */
  err.name && /** @type {any} */
  err.message ? (
    /** @type {Error} */
    err
  ) : new Error(JSON.stringify(err));
}
function normalize_error(error2) {
  return (
    /** @type {import('../runtime/control.js').Redirect | HttpError | SvelteKitError | Error} */
    error2
  );
}
function get_status(error2) {
  return error2 instanceof HttpError || error2 instanceof SvelteKitError ? error2.status : 500;
}
function get_message(error2) {
  return error2 instanceof SvelteKitError ? error2.text : "Internal Error";
}
function method_not_allowed(mod, method) {
  return text(`${method} method not allowed`, {
    status: 405,
    headers: {
      // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/405
      // "The server must generate an Allow header field in a 405 status code response"
      allow: allowed_methods(mod).join(", ")
    }
  });
}
function allowed_methods(mod) {
  const allowed = ENDPOINT_METHODS.filter((method) => method in mod);
  if ("GET" in mod || "HEAD" in mod)
    allowed.push("HEAD");
  return allowed;
}
function static_error_page(options22, status, message) {
  let page2 = options22.templates.error({ status, message });
  return text(page2, {
    headers: { "content-type": "text/html; charset=utf-8" },
    status
  });
}
async function handle_fatal_error(event, options22, error2) {
  error2 = error2 instanceof HttpError ? error2 : coalesce_to_error(error2);
  const status = get_status(error2);
  const body2 = await handle_error_and_jsonify(event, options22, error2);
  const type = negotiate(event.request.headers.get("accept") || "text/html", [
    "application/json",
    "text/html"
  ]);
  if (event.isDataRequest || type === "application/json") {
    return json(body2, {
      status
    });
  }
  return static_error_page(options22, status, body2.message);
}
async function handle_error_and_jsonify(event, options22, error2) {
  if (error2 instanceof HttpError) {
    return error2.body;
  }
  const status = get_status(error2);
  const message = get_message(error2);
  return await options22.hooks.handleError({ error: error2, event, status, message }) ?? { message };
}
function redirect_response(status, location) {
  const response = new Response(void 0, {
    status,
    headers: { location }
  });
  return response;
}
function clarify_devalue_error(event, error2) {
  if (error2.path) {
    return `Data returned from \`load\` while rendering ${event.route.id} is not serializable: ${error2.message} (data${error2.path})`;
  }
  if (error2.path === "") {
    return `Data returned from \`load\` while rendering ${event.route.id} is not a plain object`;
  }
  return error2.message;
}
function stringify_uses(node) {
  const uses = [];
  if (node.uses && node.uses.dependencies.size > 0) {
    uses.push(`"dependencies":${JSON.stringify(Array.from(node.uses.dependencies))}`);
  }
  if (node.uses && node.uses.search_params.size > 0) {
    uses.push(`"search_params":${JSON.stringify(Array.from(node.uses.search_params))}`);
  }
  if (node.uses && node.uses.params.size > 0) {
    uses.push(`"params":${JSON.stringify(Array.from(node.uses.params))}`);
  }
  if (node.uses?.parent)
    uses.push('"parent":1');
  if (node.uses?.route)
    uses.push('"route":1');
  if (node.uses?.url)
    uses.push('"url":1');
  return `"uses":{${uses.join(",")}}`;
}
async function render_endpoint(event, mod, state) {
  const method = (
    /** @type {import('types').HttpMethod} */
    event.request.method
  );
  let handler = mod[method] || mod.fallback;
  if (method === "HEAD" && mod.GET && !mod.HEAD) {
    handler = mod.GET;
  }
  if (!handler) {
    return method_not_allowed(mod, method);
  }
  const prerender7 = mod.prerender ?? state.prerender_default;
  if (prerender7 && (mod.POST || mod.PATCH || mod.PUT || mod.DELETE)) {
    throw new Error("Cannot prerender endpoints that have mutative methods");
  }
  if (state.prerendering && !prerender7) {
    if (state.depth > 0) {
      throw new Error(`${event.route.id} is not prerenderable`);
    } else {
      return new Response(void 0, { status: 204 });
    }
  }
  try {
    let response = await handler(
      /** @type {import('@sveltejs/kit').RequestEvent<Record<string, any>>} */
      event
    );
    if (!(response instanceof Response)) {
      throw new Error(
        `Invalid response from route ${event.url.pathname}: handler should return a Response object`
      );
    }
    if (state.prerendering) {
      response = new Response(response.body, {
        status: response.status,
        statusText: response.statusText,
        headers: new Headers(response.headers)
      });
      response.headers.set("x-sveltekit-prerender", String(prerender7));
    }
    return response;
  } catch (e3) {
    if (e3 instanceof Redirect) {
      return new Response(void 0, {
        status: e3.status,
        headers: { location: e3.location }
      });
    }
    throw e3;
  }
}
function is_endpoint_request(event) {
  const { method, headers: headers2 } = event.request;
  if (ENDPOINT_METHODS.includes(method) && !PAGE_METHODS.includes(method)) {
    return true;
  }
  if (method === "POST" && headers2.get("x-sveltekit-action") === "true")
    return false;
  const accept = event.request.headers.get("accept") ?? "*/*";
  return negotiate(accept, ["*", "text/html"]) !== "text/html";
}
function compact(arr) {
  return arr.filter(
    /** @returns {val is NonNullable<T>} */
    (val) => val != null
  );
}
function is_action_json_request(event) {
  const accept = negotiate(event.request.headers.get("accept") ?? "*/*", [
    "application/json",
    "text/html"
  ]);
  return accept === "application/json" && event.request.method === "POST";
}
async function handle_action_json_request(event, options22, server2) {
  const actions3 = server2?.actions;
  if (!actions3) {
    const no_actions_error = new SvelteKitError(
      405,
      "Method Not Allowed",
      "POST method not allowed. No actions exist for this page"
    );
    return action_json(
      {
        type: "error",
        error: await handle_error_and_jsonify(event, options22, no_actions_error)
      },
      {
        status: no_actions_error.status,
        headers: {
          // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/405
          // "The server must generate an Allow header field in a 405 status code response"
          allow: "GET"
        }
      }
    );
  }
  check_named_default_separate(actions3);
  try {
    const data = await call_action(event, actions3);
    if (false)
      ;
    if (data instanceof ActionFailure) {
      return action_json({
        type: "failure",
        status: data.status,
        // @ts-expect-error we assign a string to what is supposed to be an object. That's ok
        // because we don't use the object outside, and this way we have better code navigation
        // through knowing where the related interface is used.
        data: stringify_action_response(
          data.data,
          /** @type {string} */
          event.route.id
        )
      });
    } else {
      return action_json({
        type: "success",
        status: data ? 200 : 204,
        // @ts-expect-error see comment above
        data: stringify_action_response(
          data,
          /** @type {string} */
          event.route.id
        )
      });
    }
  } catch (e3) {
    const err = normalize_error(e3);
    if (err instanceof Redirect) {
      return action_json_redirect(err);
    }
    return action_json(
      {
        type: "error",
        error: await handle_error_and_jsonify(event, options22, check_incorrect_fail_use(err))
      },
      {
        status: get_status(err)
      }
    );
  }
}
function check_incorrect_fail_use(error2) {
  return error2 instanceof ActionFailure ? new Error('Cannot "throw fail()". Use "return fail()"') : error2;
}
function action_json_redirect(redirect2) {
  return action_json({
    type: "redirect",
    status: redirect2.status,
    location: redirect2.location
  });
}
function action_json(data, init2) {
  return json(data, init2);
}
function is_action_request(event) {
  return event.request.method === "POST";
}
async function handle_action_request(event, server2) {
  const actions3 = server2?.actions;
  if (!actions3) {
    event.setHeaders({
      // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/405
      // "The server must generate an Allow header field in a 405 status code response"
      allow: "GET"
    });
    return {
      type: "error",
      error: new SvelteKitError(
        405,
        "Method Not Allowed",
        "POST method not allowed. No actions exist for this page"
      )
    };
  }
  check_named_default_separate(actions3);
  try {
    const data = await call_action(event, actions3);
    if (false)
      ;
    if (data instanceof ActionFailure) {
      return {
        type: "failure",
        status: data.status,
        data: data.data
      };
    } else {
      return {
        type: "success",
        status: 200,
        // @ts-expect-error this will be removed upon serialization, so `undefined` is the same as omission
        data
      };
    }
  } catch (e3) {
    const err = normalize_error(e3);
    if (err instanceof Redirect) {
      return {
        type: "redirect",
        status: err.status,
        location: err.location
      };
    }
    return {
      type: "error",
      error: check_incorrect_fail_use(err)
    };
  }
}
function check_named_default_separate(actions3) {
  if (actions3.default && Object.keys(actions3).length > 1) {
    throw new Error(
      "When using named actions, the default action cannot be used. See the docs for more info: https://kit.svelte.dev/docs/form-actions#named-actions"
    );
  }
}
async function call_action(event, actions3) {
  const url = new URL(event.request.url);
  let name = "default";
  for (const param of url.searchParams) {
    if (param[0].startsWith("/")) {
      name = param[0].slice(1);
      if (name === "default") {
        throw new Error('Cannot use reserved action name "default"');
      }
      break;
    }
  }
  const action = actions3[name];
  if (!action) {
    throw new SvelteKitError(404, "Not Found", `No action with name '${name}' found`);
  }
  if (!is_form_content_type(event.request)) {
    throw new SvelteKitError(
      415,
      "Unsupported Media Type",
      `Form actions expect form-encoded data \u2014 received ${event.request.headers.get(
        "content-type"
      )}`
    );
  }
  return action(event);
}
function uneval_action_response(data, route_id) {
  return try_deserialize(data, uneval, route_id);
}
function stringify_action_response(data, route_id) {
  return try_deserialize(data, stringify, route_id);
}
function try_deserialize(data, fn, route_id) {
  try {
    return fn(data);
  } catch (e3) {
    const error2 = (
      /** @type {any} */
      e3
    );
    if ("path" in error2) {
      let message = `Data returned from action inside ${route_id} is not serializable: ${error2.message}`;
      if (error2.path !== "")
        message += ` (data.${error2.path})`;
      throw new Error(message);
    }
    throw error2;
  }
}
var INVALIDATED_PARAM = "x-sveltekit-invalidated";
var TRAILING_SLASH_PARAM = "x-sveltekit-trailing-slash";
function b64_encode(buffer) {
  if (globalThis.Buffer) {
    return Buffer.from(buffer).toString("base64");
  }
  const little_endian = new Uint8Array(new Uint16Array([1]).buffer)[0] > 0;
  return btoa(
    new TextDecoder(little_endian ? "utf-16le" : "utf-16be").decode(
      new Uint16Array(new Uint8Array(buffer))
    )
  );
}
async function load_server_data({ event, state, node, parent }) {
  if (!node?.server)
    return null;
  let is_tracking = true;
  const uses = {
    dependencies: /* @__PURE__ */ new Set(),
    params: /* @__PURE__ */ new Set(),
    parent: false,
    route: false,
    url: false,
    search_params: /* @__PURE__ */ new Set()
  };
  const url = make_trackable(
    event.url,
    () => {
      if (is_tracking) {
        uses.url = true;
      }
    },
    (param) => {
      if (is_tracking) {
        uses.search_params.add(param);
      }
    }
  );
  if (state.prerendering) {
    disable_search(url);
  }
  const result = await node.server.load?.call(null, {
    ...event,
    fetch: (info, init2) => {
      new URL(info instanceof Request ? info.url : info, event.url);
      return event.fetch(info, init2);
    },
    /** @param {string[]} deps */
    depends: (...deps) => {
      for (const dep of deps) {
        const { href } = new URL(dep, event.url);
        uses.dependencies.add(href);
      }
    },
    params: new Proxy(event.params, {
      get: (target, key2) => {
        if (is_tracking) {
          uses.params.add(key2);
        }
        return target[
          /** @type {string} */
          key2
        ];
      }
    }),
    parent: async () => {
      if (is_tracking) {
        uses.parent = true;
      }
      return parent();
    },
    route: new Proxy(event.route, {
      get: (target, key2) => {
        if (is_tracking) {
          uses.route = true;
        }
        return target[
          /** @type {'id'} */
          key2
        ];
      }
    }),
    url,
    untrack(fn) {
      is_tracking = false;
      try {
        return fn();
      } finally {
        is_tracking = true;
      }
    }
  });
  return {
    type: "data",
    data: result ?? null,
    uses,
    slash: node.server.trailingSlash
  };
}
async function load_data({
  event,
  fetched,
  node,
  parent,
  server_data_promise,
  state,
  resolve_opts,
  csr
}) {
  const server_data_node = await server_data_promise;
  if (!node?.universal?.load) {
    return server_data_node?.data ?? null;
  }
  const result = await node.universal.load.call(null, {
    url: event.url,
    params: event.params,
    data: server_data_node?.data ?? null,
    route: event.route,
    fetch: create_universal_fetch(event, state, fetched, csr, resolve_opts),
    setHeaders: event.setHeaders,
    depends: () => {
    },
    parent,
    untrack: (fn) => fn()
  });
  return result ?? null;
}
function create_universal_fetch(event, state, fetched, csr, resolve_opts) {
  const universal_fetch = async (input, init2) => {
    const cloned_body = input instanceof Request && input.body ? input.clone().body : null;
    const cloned_headers = input instanceof Request && [...input.headers].length ? new Headers(input.headers) : init2?.headers;
    let response = await event.fetch(input, init2);
    const url = new URL(input instanceof Request ? input.url : input, event.url);
    const same_origin = url.origin === event.url.origin;
    let dependency;
    if (same_origin) {
      if (state.prerendering) {
        dependency = { response, body: null };
        state.prerendering.dependencies.set(url.pathname, dependency);
      }
    } else {
      const mode = input instanceof Request ? input.mode : init2?.mode ?? "cors";
      if (mode === "no-cors") {
        response = new Response("", {
          status: response.status,
          statusText: response.statusText,
          headers: response.headers
        });
      } else {
        const acao = response.headers.get("access-control-allow-origin");
        if (!acao || acao !== event.url.origin && acao !== "*") {
          throw new Error(
            `CORS error: ${acao ? "Incorrect" : "No"} 'Access-Control-Allow-Origin' header is present on the requested resource`
          );
        }
      }
    }
    const proxy = new Proxy(response, {
      get(response2, key2, _receiver) {
        async function push_fetched(body2, is_b64) {
          const status_number = Number(response2.status);
          if (isNaN(status_number)) {
            throw new Error(
              `response.status is not a number. value: "${response2.status}" type: ${typeof response2.status}`
            );
          }
          fetched.push({
            url: same_origin ? url.href.slice(event.url.origin.length) : url.href,
            method: event.request.method,
            request_body: (
              /** @type {string | ArrayBufferView | undefined} */
              input instanceof Request && cloned_body ? await stream_to_string(cloned_body) : init2?.body
            ),
            request_headers: cloned_headers,
            response_body: body2,
            response: response2,
            is_b64
          });
        }
        if (key2 === "arrayBuffer") {
          return async () => {
            const buffer = await response2.arrayBuffer();
            if (dependency) {
              dependency.body = new Uint8Array(buffer);
            }
            if (buffer instanceof ArrayBuffer) {
              await push_fetched(b64_encode(buffer), true);
            }
            return buffer;
          };
        }
        async function text2() {
          const body2 = await response2.text();
          if (!body2 || typeof body2 === "string") {
            await push_fetched(body2, false);
          }
          if (dependency) {
            dependency.body = body2;
          }
          return body2;
        }
        if (key2 === "text") {
          return text2;
        }
        if (key2 === "json") {
          return async () => {
            return JSON.parse(await text2());
          };
        }
        return Reflect.get(response2, key2, response2);
      }
    });
    if (csr) {
      const get3 = response.headers.get;
      response.headers.get = (key2) => {
        const lower = key2.toLowerCase();
        const value = get3.call(response.headers, lower);
        if (value && !lower.startsWith("x-sveltekit-")) {
          const included = resolve_opts.filterSerializedResponseHeaders(lower, value);
          if (!included) {
            throw new Error(
              `Failed to get response header "${lower}" \u2014 it must be included by the \`filterSerializedResponseHeaders\` option: https://kit.svelte.dev/docs/hooks#server-hooks-handle (at ${event.route.id})`
            );
          }
        }
        return value;
      };
    }
    return proxy;
  };
  return (input, init2) => {
    const response = universal_fetch(input, init2);
    response.catch(() => {
    });
    return response;
  };
}
async function stream_to_string(stream) {
  let result = "";
  const reader = stream.getReader();
  const decoder = new TextDecoder();
  while (true) {
    const { done: done2, value } = await reader.read();
    if (done2) {
      break;
    }
    result += decoder.decode(value);
  }
  return result;
}
var subscriber_queue = [];
function readable(value, start) {
  return {
    subscribe: writable(value, start).subscribe
  };
}
function writable(value, start = noop) {
  let stop;
  const subscribers = /* @__PURE__ */ new Set();
  function set2(new_value) {
    if (safe_not_equal(value, new_value)) {
      value = new_value;
      if (stop) {
        const run_queue = !subscriber_queue.length;
        for (const subscriber of subscribers) {
          subscriber[1]();
          subscriber_queue.push(subscriber, value);
        }
        if (run_queue) {
          for (let i = 0; i < subscriber_queue.length; i += 2) {
            subscriber_queue[i][0](subscriber_queue[i + 1]);
          }
          subscriber_queue.length = 0;
        }
      }
    }
  }
  function update(fn) {
    set2(fn(value));
  }
  function subscribe3(run3, invalidate = noop) {
    const subscriber = [run3, invalidate];
    subscribers.add(subscriber);
    if (subscribers.size === 1) {
      stop = start(set2, update) || noop;
    }
    run3(value);
    return () => {
      subscribers.delete(subscriber);
      if (subscribers.size === 0 && stop) {
        stop();
        stop = null;
      }
    };
  }
  return { set: set2, update, subscribe: subscribe3 };
}
function hash(...values) {
  let hash2 = 5381;
  for (const value of values) {
    if (typeof value === "string") {
      let i = value.length;
      while (i)
        hash2 = hash2 * 33 ^ value.charCodeAt(--i);
    } else if (ArrayBuffer.isView(value)) {
      const buffer = new Uint8Array(value.buffer, value.byteOffset, value.byteLength);
      let i = buffer.length;
      while (i)
        hash2 = hash2 * 33 ^ buffer[--i];
    } else {
      throw new TypeError("value must be a string or TypedArray");
    }
  }
  return (hash2 >>> 0).toString(36);
}
var escape_html_attr_dict = {
  "&": "&amp;",
  '"': "&quot;"
};
var escape_html_attr_regex = new RegExp(
  // special characters
  `[${Object.keys(escape_html_attr_dict).join("")}]|[\\ud800-\\udbff](?![\\udc00-\\udfff])|[\\ud800-\\udbff][\\udc00-\\udfff]|[\\udc00-\\udfff]`,
  "g"
);
function escape_html_attr(str) {
  const escaped_str = str.replace(escape_html_attr_regex, (match) => {
    if (match.length === 2) {
      return match;
    }
    return escape_html_attr_dict[match] ?? `&#${match.charCodeAt(0)};`;
  });
  return `"${escaped_str}"`;
}
var replacements = {
  "<": "\\u003C",
  "\u2028": "\\u2028",
  "\u2029": "\\u2029"
};
var pattern = new RegExp(`[${Object.keys(replacements).join("")}]`, "g");
function serialize_data(fetched, filter, prerendering2 = false) {
  const headers2 = {};
  let cache_control = null;
  let age = null;
  let varyAny = false;
  for (const [key2, value] of fetched.response.headers) {
    if (filter(key2, value)) {
      headers2[key2] = value;
    }
    if (key2 === "cache-control")
      cache_control = value;
    else if (key2 === "age")
      age = value;
    else if (key2 === "vary" && value.trim() === "*")
      varyAny = true;
  }
  const payload = {
    status: fetched.response.status,
    statusText: fetched.response.statusText,
    headers: headers2,
    body: fetched.response_body
  };
  const safe_payload = JSON.stringify(payload).replace(pattern, (match) => replacements[match]);
  const attrs = [
    'type="application/json"',
    "data-sveltekit-fetched",
    `data-url=${escape_html_attr(fetched.url)}`
  ];
  if (fetched.is_b64) {
    attrs.push("data-b64");
  }
  if (fetched.request_headers || fetched.request_body) {
    const values = [];
    if (fetched.request_headers) {
      values.push([...new Headers(fetched.request_headers)].join(","));
    }
    if (fetched.request_body) {
      values.push(fetched.request_body);
    }
    attrs.push(`data-hash="${hash(...values)}"`);
  }
  if (!prerendering2 && fetched.method === "GET" && cache_control && !varyAny) {
    const match = /s-maxage=(\d+)/g.exec(cache_control) ?? /max-age=(\d+)/g.exec(cache_control);
    if (match) {
      const ttl = +match[1] - +(age ?? "0");
      attrs.push(`data-ttl="${ttl}"`);
    }
  }
  return `<script ${attrs.join(" ")}>${safe_payload}<\/script>`;
}
var s = JSON.stringify;
var encoder$2 = new TextEncoder();
function sha256(data) {
  if (!key[0])
    precompute();
  const out = init.slice(0);
  const array2 = encode(data);
  for (let i = 0; i < array2.length; i += 16) {
    const w = array2.subarray(i, i + 16);
    let tmp;
    let a;
    let b;
    let out0 = out[0];
    let out1 = out[1];
    let out2 = out[2];
    let out3 = out[3];
    let out4 = out[4];
    let out5 = out[5];
    let out6 = out[6];
    let out7 = out[7];
    for (let i2 = 0; i2 < 64; i2++) {
      if (i2 < 16) {
        tmp = w[i2];
      } else {
        a = w[i2 + 1 & 15];
        b = w[i2 + 14 & 15];
        tmp = w[i2 & 15] = (a >>> 7 ^ a >>> 18 ^ a >>> 3 ^ a << 25 ^ a << 14) + (b >>> 17 ^ b >>> 19 ^ b >>> 10 ^ b << 15 ^ b << 13) + w[i2 & 15] + w[i2 + 9 & 15] | 0;
      }
      tmp = tmp + out7 + (out4 >>> 6 ^ out4 >>> 11 ^ out4 >>> 25 ^ out4 << 26 ^ out4 << 21 ^ out4 << 7) + (out6 ^ out4 & (out5 ^ out6)) + key[i2];
      out7 = out6;
      out6 = out5;
      out5 = out4;
      out4 = out3 + tmp | 0;
      out3 = out2;
      out2 = out1;
      out1 = out0;
      out0 = tmp + (out1 & out2 ^ out3 & (out1 ^ out2)) + (out1 >>> 2 ^ out1 >>> 13 ^ out1 >>> 22 ^ out1 << 30 ^ out1 << 19 ^ out1 << 10) | 0;
    }
    out[0] = out[0] + out0 | 0;
    out[1] = out[1] + out1 | 0;
    out[2] = out[2] + out2 | 0;
    out[3] = out[3] + out3 | 0;
    out[4] = out[4] + out4 | 0;
    out[5] = out[5] + out5 | 0;
    out[6] = out[6] + out6 | 0;
    out[7] = out[7] + out7 | 0;
  }
  const bytes = new Uint8Array(out.buffer);
  reverse_endianness(bytes);
  return base64(bytes);
}
var init = new Uint32Array(8);
var key = new Uint32Array(64);
function precompute() {
  function frac(x3) {
    return (x3 - Math.floor(x3)) * 4294967296;
  }
  let prime = 2;
  for (let i = 0; i < 64; prime++) {
    let is_prime = true;
    for (let factor = 2; factor * factor <= prime; factor++) {
      if (prime % factor === 0) {
        is_prime = false;
        break;
      }
    }
    if (is_prime) {
      if (i < 8) {
        init[i] = frac(prime ** (1 / 2));
      }
      key[i] = frac(prime ** (1 / 3));
      i++;
    }
  }
}
function reverse_endianness(bytes) {
  for (let i = 0; i < bytes.length; i += 4) {
    const a = bytes[i + 0];
    const b = bytes[i + 1];
    const c2 = bytes[i + 2];
    const d2 = bytes[i + 3];
    bytes[i + 0] = d2;
    bytes[i + 1] = c2;
    bytes[i + 2] = b;
    bytes[i + 3] = a;
  }
}
function encode(str) {
  const encoded = encoder$2.encode(str);
  const length = encoded.length * 8;
  const size = 512 * Math.ceil((length + 65) / 512);
  const bytes = new Uint8Array(size / 8);
  bytes.set(encoded);
  bytes[encoded.length] = 128;
  reverse_endianness(bytes);
  const words = new Uint32Array(bytes.buffer);
  words[words.length - 2] = Math.floor(length / 4294967296);
  words[words.length - 1] = length;
  return words;
}
var chars2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".split("");
function base64(bytes) {
  const l = bytes.length;
  let result = "";
  let i;
  for (i = 2; i < l; i += 3) {
    result += chars2[bytes[i - 2] >> 2];
    result += chars2[(bytes[i - 2] & 3) << 4 | bytes[i - 1] >> 4];
    result += chars2[(bytes[i - 1] & 15) << 2 | bytes[i] >> 6];
    result += chars2[bytes[i] & 63];
  }
  if (i === l + 1) {
    result += chars2[bytes[i - 2] >> 2];
    result += chars2[(bytes[i - 2] & 3) << 4];
    result += "==";
  }
  if (i === l) {
    result += chars2[bytes[i - 2] >> 2];
    result += chars2[(bytes[i - 2] & 3) << 4 | bytes[i - 1] >> 4];
    result += chars2[(bytes[i - 1] & 15) << 2];
    result += "=";
  }
  return result;
}
var array = new Uint8Array(16);
function generate_nonce() {
  crypto.getRandomValues(array);
  return base64(array);
}
var quoted = /* @__PURE__ */ new Set([
  "self",
  "unsafe-eval",
  "unsafe-hashes",
  "unsafe-inline",
  "none",
  "strict-dynamic",
  "report-sample",
  "wasm-unsafe-eval",
  "script"
]);
var crypto_pattern = /^(nonce|sha\d\d\d)-/;
var BaseProvider = class {
  /** @type {boolean} */
  #use_hashes;
  /** @type {boolean} */
  #script_needs_csp;
  /** @type {boolean} */
  #style_needs_csp;
  /** @type {import('types').CspDirectives} */
  #directives;
  /** @type {import('types').Csp.Source[]} */
  #script_src;
  /** @type {import('types').Csp.Source[]} */
  #script_src_elem;
  /** @type {import('types').Csp.Source[]} */
  #style_src;
  /** @type {import('types').Csp.Source[]} */
  #style_src_attr;
  /** @type {import('types').Csp.Source[]} */
  #style_src_elem;
  /** @type {string} */
  #nonce;
  /**
   * @param {boolean} use_hashes
   * @param {import('types').CspDirectives} directives
   * @param {string} nonce
   */
  constructor(use_hashes, directives, nonce) {
    this.#use_hashes = use_hashes;
    this.#directives = directives;
    const d2 = this.#directives;
    this.#script_src = [];
    this.#script_src_elem = [];
    this.#style_src = [];
    this.#style_src_attr = [];
    this.#style_src_elem = [];
    const effective_script_src = d2["script-src"] || d2["default-src"];
    const script_src_elem = d2["script-src-elem"];
    const effective_style_src = d2["style-src"] || d2["default-src"];
    const style_src_attr = d2["style-src-attr"];
    const style_src_elem = d2["style-src-elem"];
    this.#script_needs_csp = !!effective_script_src && effective_script_src.filter((value) => value !== "unsafe-inline").length > 0 || !!script_src_elem && script_src_elem.filter((value) => value !== "unsafe-inline").length > 0;
    this.#style_needs_csp = !!effective_style_src && effective_style_src.filter((value) => value !== "unsafe-inline").length > 0 || !!style_src_attr && style_src_attr.filter((value) => value !== "unsafe-inline").length > 0 || !!style_src_elem && style_src_elem.filter((value) => value !== "unsafe-inline").length > 0;
    this.script_needs_nonce = this.#script_needs_csp && !this.#use_hashes;
    this.style_needs_nonce = this.#style_needs_csp && !this.#use_hashes;
    this.#nonce = nonce;
  }
  /** @param {string} content */
  add_script(content) {
    if (this.#script_needs_csp) {
      const d2 = this.#directives;
      if (this.#use_hashes) {
        const hash2 = sha256(content);
        this.#script_src.push(`sha256-${hash2}`);
        if (d2["script-src-elem"]?.length) {
          this.#script_src_elem.push(`sha256-${hash2}`);
        }
      } else {
        if (this.#script_src.length === 0) {
          this.#script_src.push(`nonce-${this.#nonce}`);
        }
        if (d2["script-src-elem"]?.length) {
          this.#script_src_elem.push(`nonce-${this.#nonce}`);
        }
      }
    }
  }
  /** @param {string} content */
  add_style(content) {
    if (this.#style_needs_csp) {
      const empty_comment_hash = "9OlNO0DNEeaVzHL4RZwCLsBHA8WBQ8toBp/4F5XV2nc=";
      const d2 = this.#directives;
      if (this.#use_hashes) {
        const hash2 = sha256(content);
        this.#style_src.push(`sha256-${hash2}`);
        if (d2["style-src-attr"]?.length) {
          this.#style_src_attr.push(`sha256-${hash2}`);
        }
        if (d2["style-src-elem"]?.length) {
          if (hash2 !== empty_comment_hash && !d2["style-src-elem"].includes(`sha256-${empty_comment_hash}`)) {
            this.#style_src_elem.push(`sha256-${empty_comment_hash}`);
          }
          this.#style_src_elem.push(`sha256-${hash2}`);
        }
      } else {
        if (this.#style_src.length === 0 && !d2["style-src"]?.includes("unsafe-inline")) {
          this.#style_src.push(`nonce-${this.#nonce}`);
        }
        if (d2["style-src-attr"]?.length) {
          this.#style_src_attr.push(`nonce-${this.#nonce}`);
        }
        if (d2["style-src-elem"]?.length) {
          if (!d2["style-src-elem"].includes(`sha256-${empty_comment_hash}`)) {
            this.#style_src_elem.push(`sha256-${empty_comment_hash}`);
          }
          this.#style_src_elem.push(`nonce-${this.#nonce}`);
        }
      }
    }
  }
  /**
   * @param {boolean} [is_meta]
   */
  get_header(is_meta = false) {
    const header = [];
    const directives = { ...this.#directives };
    if (this.#style_src.length > 0) {
      directives["style-src"] = [
        ...directives["style-src"] || directives["default-src"] || [],
        ...this.#style_src
      ];
    }
    if (this.#style_src_attr.length > 0) {
      directives["style-src-attr"] = [
        ...directives["style-src-attr"] || [],
        ...this.#style_src_attr
      ];
    }
    if (this.#style_src_elem.length > 0) {
      directives["style-src-elem"] = [
        ...directives["style-src-elem"] || [],
        ...this.#style_src_elem
      ];
    }
    if (this.#script_src.length > 0) {
      directives["script-src"] = [
        ...directives["script-src"] || directives["default-src"] || [],
        ...this.#script_src
      ];
    }
    if (this.#script_src_elem.length > 0) {
      directives["script-src-elem"] = [
        ...directives["script-src-elem"] || [],
        ...this.#script_src_elem
      ];
    }
    for (const key2 in directives) {
      if (is_meta && (key2 === "frame-ancestors" || key2 === "report-uri" || key2 === "sandbox")) {
        continue;
      }
      const value = (
        /** @type {string[] | true} */
        directives[key2]
      );
      if (!value)
        continue;
      const directive = [key2];
      if (Array.isArray(value)) {
        value.forEach((value2) => {
          if (quoted.has(value2) || crypto_pattern.test(value2)) {
            directive.push(`'${value2}'`);
          } else {
            directive.push(value2);
          }
        });
      }
      header.push(directive.join(" "));
    }
    return header.join("; ");
  }
};
var CspProvider = class extends BaseProvider {
  get_meta() {
    const content = this.get_header(true);
    if (!content) {
      return;
    }
    return `<meta http-equiv="content-security-policy" content=${escape_html_attr(content)}>`;
  }
};
var CspReportOnlyProvider = class extends BaseProvider {
  /**
   * @param {boolean} use_hashes
   * @param {import('types').CspDirectives} directives
   * @param {string} nonce
   */
  constructor(use_hashes, directives, nonce) {
    super(use_hashes, directives, nonce);
    if (Object.values(directives).filter((v) => !!v).length > 0) {
      const has_report_to = directives["report-to"]?.length ?? 0 > 0;
      const has_report_uri = directives["report-uri"]?.length ?? 0 > 0;
      if (!has_report_to && !has_report_uri) {
        throw Error(
          "`content-security-policy-report-only` must be specified with either the `report-to` or `report-uri` directives, or both"
        );
      }
    }
  }
};
var Csp = class {
  /** @readonly */
  nonce = generate_nonce();
  /** @type {CspProvider} */
  csp_provider;
  /** @type {CspReportOnlyProvider} */
  report_only_provider;
  /**
   * @param {import('./types.js').CspConfig} config
   * @param {import('./types.js').CspOpts} opts
   */
  constructor({ mode, directives, reportOnly }, { prerender: prerender7 }) {
    const use_hashes = mode === "hash" || mode === "auto" && prerender7;
    this.csp_provider = new CspProvider(use_hashes, directives, this.nonce);
    this.report_only_provider = new CspReportOnlyProvider(use_hashes, reportOnly, this.nonce);
  }
  get script_needs_nonce() {
    return this.csp_provider.script_needs_nonce || this.report_only_provider.script_needs_nonce;
  }
  get style_needs_nonce() {
    return this.csp_provider.style_needs_nonce || this.report_only_provider.style_needs_nonce;
  }
  /** @param {string} content */
  add_script(content) {
    this.csp_provider.add_script(content);
    this.report_only_provider.add_script(content);
  }
  /** @param {string} content */
  add_style(content) {
    this.csp_provider.add_style(content);
    this.report_only_provider.add_style(content);
  }
};
function defer() {
  let fulfil;
  let reject2;
  const promise = new Promise((f, r3) => {
    fulfil = f;
    reject2 = r3;
  });
  return { promise, fulfil, reject: reject2 };
}
function create_async_iterator() {
  const deferred = [defer()];
  return {
    iterator: {
      [Symbol.asyncIterator]() {
        return {
          next: async () => {
            const next2 = await deferred[0].promise;
            if (!next2.done)
              deferred.shift();
            return next2;
          }
        };
      }
    },
    push: (value) => {
      deferred[deferred.length - 1].fulfil({
        value,
        done: false
      });
      deferred.push(defer());
    },
    done: () => {
      deferred[deferred.length - 1].fulfil({ done: true });
    }
  };
}
var updated = {
  ...readable(false),
  check: () => false
};
var encoder$1 = new TextEncoder();
async function render_response({
  branch,
  fetched,
  options: options22,
  manifest: manifest2,
  state,
  page_config,
  status,
  error: error2 = null,
  event,
  resolve_opts,
  action_result
}) {
  if (state.prerendering) {
    if (options22.csp.mode === "nonce") {
      throw new Error('Cannot use prerendering if config.kit.csp.mode === "nonce"');
    }
    if (options22.app_template_contains_nonce) {
      throw new Error("Cannot use prerendering if page template contains %sveltekit.nonce%");
    }
  }
  const { client } = manifest2._;
  const modulepreloads = new Set(client.imports);
  const stylesheets10 = new Set(client.stylesheets);
  const fonts10 = new Set(client.fonts);
  const link_header_preloads = /* @__PURE__ */ new Set();
  const inline_styles = /* @__PURE__ */ new Map();
  let rendered;
  const form_value = action_result?.type === "success" || action_result?.type === "failure" ? action_result.data ?? null : null;
  let base$1 = base;
  let assets$1 = assets;
  let base_expression = s(base);
  if (page_config.ssr) {
    const props = {
      stores: {
        page: writable(null),
        navigating: writable(null),
        updated
      },
      constructors: await Promise.all(branch.map(({ node }) => node.component())),
      form: form_value
    };
    let data2 = {};
    for (let i = 0; i < branch.length; i += 1) {
      data2 = { ...data2, ...branch[i].data };
      props[`data_${i}`] = data2;
    }
    props.page = {
      error: error2,
      params: (
        /** @type {Record<string, any>} */
        event.params
      ),
      route: event.route,
      status,
      url: event.url,
      data: data2,
      form: form_value,
      state: {}
    };
    {
      try {
        rendered = options22.root.render(props);
      } finally {
        reset();
      }
    }
    for (const { node } of branch) {
      for (const url of node.imports)
        modulepreloads.add(url);
      for (const url of node.stylesheets)
        stylesheets10.add(url);
      for (const url of node.fonts)
        fonts10.add(url);
      if (node.inline_styles) {
        Object.entries(await node.inline_styles()).forEach(([k2, v]) => inline_styles.set(k2, v));
      }
    }
  } else {
    rendered = { head: "", html: "", css: { code: "", map: null } };
  }
  let head = "";
  let body2 = rendered.html;
  const csp = new Csp(options22.csp, {
    prerender: !!state.prerendering
  });
  const prefixed = (path) => {
    if (path.startsWith("/")) {
      return base + path;
    }
    return `${assets$1}/${path}`;
  };
  if (inline_styles.size > 0) {
    const content = Array.from(inline_styles.values()).join("\n");
    const attributes = [];
    if (csp.style_needs_nonce)
      attributes.push(` nonce="${csp.nonce}"`);
    csp.add_style(content);
    head += `
	<style${attributes.join("")}>${content}</style>`;
  }
  for (const dep of stylesheets10) {
    const path = prefixed(dep);
    const attributes = ['rel="stylesheet"'];
    if (inline_styles.has(dep)) {
      attributes.push("disabled", 'media="(max-width: 0)"');
    } else {
      if (resolve_opts.preload({ type: "css", path })) {
        const preload_atts = ['rel="preload"', 'as="style"'];
        link_header_preloads.add(`<${encodeURI(path)}>; ${preload_atts.join(";")}; nopush`);
      }
    }
    head += `
		<link href="${path}" ${attributes.join(" ")}>`;
  }
  for (const dep of fonts10) {
    const path = prefixed(dep);
    if (resolve_opts.preload({ type: "font", path })) {
      const ext = dep.slice(dep.lastIndexOf(".") + 1);
      const attributes = [
        'rel="preload"',
        'as="font"',
        `type="font/${ext}"`,
        `href="${path}"`,
        "crossorigin"
      ];
      head += `
		<link ${attributes.join(" ")}>`;
    }
  }
  const global2 = `__sveltekit_${options22.version_hash}`;
  const { data, chunks } = get_data(
    event,
    options22,
    branch.map((b) => b.server_data),
    global2
  );
  if (page_config.ssr && page_config.csr) {
    body2 += `
			${fetched.map(
      (item) => serialize_data(item, resolve_opts.filterSerializedResponseHeaders, !!state.prerendering)
    ).join("\n			")}`;
  }
  if (page_config.csr) {
    if (client.uses_env_dynamic_public && state.prerendering) {
      modulepreloads.add(`${options22.app_dir}/env.js`);
    }
    const included_modulepreloads = Array.from(modulepreloads, (dep) => prefixed(dep)).filter(
      (path) => resolve_opts.preload({ type: "js", path })
    );
    for (const path of included_modulepreloads) {
      link_header_preloads.add(`<${encodeURI(path)}>; rel="modulepreload"; nopush`);
      if (options22.preload_strategy !== "modulepreload") {
        head += `
		<link rel="preload" as="script" crossorigin="anonymous" href="${path}">`;
      } else if (state.prerendering) {
        head += `
		<link rel="modulepreload" href="${path}">`;
      }
    }
    const blocks = [];
    const load_env_eagerly = client.uses_env_dynamic_public && state.prerendering;
    const properties = [`base: ${base_expression}`];
    if (assets) {
      properties.push(`assets: ${s(assets)}`);
    }
    if (client.uses_env_dynamic_public) {
      properties.push(`env: ${load_env_eagerly ? "null" : s(public_env)}`);
    }
    if (chunks) {
      blocks.push("const deferred = new Map();");
      properties.push(`defer: (id) => new Promise((fulfil, reject) => {
							deferred.set(id, { fulfil, reject });
						})`);
      properties.push(`resolve: ({ id, data, error }) => {
							const { fulfil, reject } = deferred.get(id);
							deferred.delete(id);

							if (error) reject(error);
							else fulfil(data);
						}`);
    }
    blocks.push(`${global2} = {
						${properties.join(",\n						")}
					};`);
    const args = ["app", "element"];
    blocks.push("const element = document.currentScript.parentElement;");
    if (page_config.ssr) {
      const serialized = { form: "null", error: "null" };
      blocks.push(`const data = ${data};`);
      if (form_value) {
        serialized.form = uneval_action_response(
          form_value,
          /** @type {string} */
          event.route.id
        );
      }
      if (error2) {
        serialized.error = uneval(error2);
      }
      const hydrate = [
        `node_ids: [${branch.map(({ node }) => node.index).join(", ")}]`,
        "data",
        `form: ${serialized.form}`,
        `error: ${serialized.error}`
      ];
      if (status !== 200) {
        hydrate.push(`status: ${status}`);
      }
      if (options22.embedded) {
        hydrate.push(`params: ${uneval(event.params)}`, `route: ${s(event.route)}`);
      }
      const indent = "	".repeat(load_env_eagerly ? 7 : 6);
      args.push(`{
${indent}	${hydrate.join(`,
${indent}	`)}
${indent}}`);
    }
    if (load_env_eagerly) {
      blocks.push(`import(${s(`${base$1}/${options22.app_dir}/env.js`)}).then(({ env }) => {
						${global2}.env = env;

						Promise.all([
							import(${s(prefixed(client.start))}),
							import(${s(prefixed(client.app))})
						]).then(([kit, app]) => {
							kit.start(${args.join(", ")});
						});
					});`);
    } else {
      blocks.push(`Promise.all([
						import(${s(prefixed(client.start))}),
						import(${s(prefixed(client.app))})
					]).then(([kit, app]) => {
						kit.start(${args.join(", ")});
					});`);
    }
    if (options22.service_worker) {
      const opts = "";
      blocks.push(`if ('serviceWorker' in navigator) {
						addEventListener('load', function () {
							navigator.serviceWorker.register('${prefixed("service-worker.js")}'${opts});
						});
					}`);
    }
    const init_app = `
				{
					${blocks.join("\n\n					")}
				}
			`;
    csp.add_script(init_app);
    body2 += `
			<script${csp.script_needs_nonce ? ` nonce="${csp.nonce}"` : ""}>${init_app}<\/script>
		`;
  }
  const headers2 = new Headers({
    "x-sveltekit-page": "true",
    "content-type": "text/html"
  });
  if (state.prerendering) {
    const http_equiv = [];
    const csp_headers = csp.csp_provider.get_meta();
    if (csp_headers) {
      http_equiv.push(csp_headers);
    }
    if (state.prerendering.cache) {
      http_equiv.push(`<meta http-equiv="cache-control" content="${state.prerendering.cache}">`);
    }
    if (http_equiv.length > 0) {
      head = http_equiv.join("\n") + head;
    }
  } else {
    const csp_header = csp.csp_provider.get_header();
    if (csp_header) {
      headers2.set("content-security-policy", csp_header);
    }
    const report_only_header = csp.report_only_provider.get_header();
    if (report_only_header) {
      headers2.set("content-security-policy-report-only", report_only_header);
    }
    if (link_header_preloads.size) {
      headers2.set("link", Array.from(link_header_preloads).join(", "));
    }
  }
  head += rendered.head;
  const html = options22.templates.app({
    head,
    body: body2,
    assets: assets$1,
    nonce: (
      /** @type {string} */
      csp.nonce
    ),
    env: safe_public_env
  });
  const transformed = await resolve_opts.transformPageChunk({
    html,
    done: true
  }) || "";
  if (!chunks) {
    headers2.set("etag", `"${hash(transformed)}"`);
  }
  return !chunks ? text(transformed, {
    status,
    headers: headers2
  }) : new Response(
    new ReadableStream({
      async start(controller) {
        controller.enqueue(encoder$1.encode(transformed + "\n"));
        for await (const chunk of chunks) {
          controller.enqueue(encoder$1.encode(chunk));
        }
        controller.close();
      },
      type: "bytes"
    }),
    {
      headers: {
        "content-type": "text/html"
      }
    }
  );
}
function get_data(event, options22, nodes, global2) {
  let promise_id = 1;
  let count = 0;
  const { iterator, push: push2, done: done2 } = create_async_iterator();
  function replacer(thing) {
    if (typeof thing?.then === "function") {
      const id = promise_id++;
      count += 1;
      thing.then(
        /** @param {any} data */
        (data) => ({ data })
      ).catch(
        /** @param {any} error */
        async (error2) => ({
          error: await handle_error_and_jsonify(event, options22, error2)
        })
      ).then(
        /**
         * @param {{data: any; error: any}} result
         */
        async ({ data, error: error2 }) => {
          count -= 1;
          let str;
          try {
            str = uneval({ id, data, error: error2 }, replacer);
          } catch (e3) {
            error2 = await handle_error_and_jsonify(
              event,
              options22,
              new Error(`Failed to serialize promise while rendering ${event.route.id}`)
            );
            data = void 0;
            str = uneval({ id, data, error: error2 }, replacer);
          }
          push2(`<script>${global2}.resolve(${str})<\/script>
`);
          if (count === 0)
            done2();
        }
      );
      return `${global2}.defer(${id})`;
    }
  }
  try {
    const strings = nodes.map((node) => {
      if (!node)
        return "null";
      return `{"type":"data","data":${uneval(node.data, replacer)},${stringify_uses(node)}${node.slash ? `,"slash":${JSON.stringify(node.slash)}` : ""}}`;
    });
    return {
      data: `[${strings.join(",")}]`,
      chunks: count > 0 ? iterator : null
    };
  } catch (e3) {
    throw new Error(clarify_devalue_error(
      event,
      /** @type {any} */
      e3
    ));
  }
}
function get_option(nodes, option) {
  return nodes.reduce(
    (value, node) => {
      return (
        /** @type {Value} TypeScript's too dumb to understand this */
        node?.universal?.[option] ?? node?.server?.[option] ?? value
      );
    },
    /** @type {Value | undefined} */
    void 0
  );
}
async function respond_with_error({
  event,
  options: options22,
  manifest: manifest2,
  state,
  status,
  error: error2,
  resolve_opts
}) {
  if (event.request.headers.get("x-sveltekit-error")) {
    return static_error_page(
      options22,
      status,
      /** @type {Error} */
      error2.message
    );
  }
  const fetched = [];
  try {
    const branch = [];
    const default_layout = await manifest2._.nodes[0]();
    const ssr = get_option([default_layout], "ssr") ?? true;
    const csr = get_option([default_layout], "csr") ?? true;
    if (ssr) {
      state.error = true;
      const server_data_promise = load_server_data({
        event,
        state,
        node: default_layout,
        parent: async () => ({})
      });
      const server_data = await server_data_promise;
      const data = await load_data({
        event,
        fetched,
        node: default_layout,
        parent: async () => ({}),
        resolve_opts,
        server_data_promise,
        state,
        csr
      });
      branch.push(
        {
          node: default_layout,
          server_data,
          data
        },
        {
          node: await manifest2._.nodes[1](),
          // 1 is always the root error
          data: null,
          server_data: null
        }
      );
    }
    return await render_response({
      options: options22,
      manifest: manifest2,
      state,
      page_config: {
        ssr,
        csr
      },
      status,
      error: await handle_error_and_jsonify(event, options22, error2),
      branch,
      fetched,
      event,
      resolve_opts
    });
  } catch (e3) {
    if (e3 instanceof Redirect) {
      return redirect_response(e3.status, e3.location);
    }
    return static_error_page(
      options22,
      get_status(e3),
      (await handle_error_and_jsonify(event, options22, e3)).message
    );
  }
}
function once(fn) {
  let done2 = false;
  let result;
  return () => {
    if (done2)
      return result;
    done2 = true;
    return result = fn();
  };
}
var encoder2 = new TextEncoder();
async function render_data(event, route2, options22, manifest2, state, invalidated_data_nodes, trailing_slash) {
  if (!route2.page) {
    return new Response(void 0, {
      status: 404
    });
  }
  try {
    const node_ids = [...route2.page.layouts, route2.page.leaf];
    const invalidated = invalidated_data_nodes ?? node_ids.map(() => true);
    let aborted = false;
    const url = new URL(event.url);
    url.pathname = normalize_path(url.pathname, trailing_slash);
    const new_event = { ...event, url };
    const functions = node_ids.map((n2, i) => {
      return once(async () => {
        try {
          if (aborted) {
            return (
              /** @type {import('types').ServerDataSkippedNode} */
              {
                type: "skip"
              }
            );
          }
          const node = n2 == void 0 ? n2 : await manifest2._.nodes[n2]();
          return load_server_data({
            event: new_event,
            state,
            node,
            parent: async () => {
              const data2 = {};
              for (let j2 = 0; j2 < i; j2 += 1) {
                const parent = (
                  /** @type {import('types').ServerDataNode | null} */
                  await functions[j2]()
                );
                if (parent) {
                  Object.assign(data2, parent.data);
                }
              }
              return data2;
            }
          });
        } catch (e3) {
          aborted = true;
          throw e3;
        }
      });
    });
    const promises = functions.map(async (fn, i) => {
      if (!invalidated[i]) {
        return (
          /** @type {import('types').ServerDataSkippedNode} */
          {
            type: "skip"
          }
        );
      }
      return fn();
    });
    let length = promises.length;
    const nodes = await Promise.all(
      promises.map(
        (p, i) => p.catch(async (error2) => {
          if (error2 instanceof Redirect) {
            throw error2;
          }
          length = Math.min(length, i + 1);
          return (
            /** @type {import('types').ServerErrorNode} */
            {
              type: "error",
              error: await handle_error_and_jsonify(event, options22, error2),
              status: error2 instanceof HttpError || error2 instanceof SvelteKitError ? error2.status : void 0
            }
          );
        })
      )
    );
    const { data, chunks } = get_data_json(event, options22, nodes);
    if (!chunks) {
      return json_response(data);
    }
    return new Response(
      new ReadableStream({
        async start(controller) {
          controller.enqueue(encoder2.encode(data));
          for await (const chunk of chunks) {
            controller.enqueue(encoder2.encode(chunk));
          }
          controller.close();
        },
        type: "bytes"
      }),
      {
        headers: {
          // we use a proprietary content type to prevent buffering.
          // the `text` prefix makes it inspectable
          "content-type": "text/sveltekit-data",
          "cache-control": "private, no-store"
        }
      }
    );
  } catch (e3) {
    const error2 = normalize_error(e3);
    if (error2 instanceof Redirect) {
      return redirect_json_response(error2);
    } else {
      return json_response(await handle_error_and_jsonify(event, options22, error2), 500);
    }
  }
}
function json_response(json2, status = 200) {
  return text(typeof json2 === "string" ? json2 : JSON.stringify(json2), {
    status,
    headers: {
      "content-type": "application/json",
      "cache-control": "private, no-store"
    }
  });
}
function redirect_json_response(redirect2) {
  return json_response({
    type: "redirect",
    location: redirect2.location
  });
}
function get_data_json(event, options22, nodes) {
  let promise_id = 1;
  let count = 0;
  const { iterator, push: push2, done: done2 } = create_async_iterator();
  const reducers = {
    /** @param {any} thing */
    Promise: (thing) => {
      if (typeof thing?.then === "function") {
        const id = promise_id++;
        count += 1;
        let key2 = "data";
        thing.catch(
          /** @param {any} e */
          async (e3) => {
            key2 = "error";
            return handle_error_and_jsonify(
              event,
              options22,
              /** @type {any} */
              e3
            );
          }
        ).then(
          /** @param {any} value */
          async (value) => {
            let str;
            try {
              str = stringify(value, reducers);
            } catch (e3) {
              const error2 = await handle_error_and_jsonify(
                event,
                options22,
                new Error(`Failed to serialize promise while rendering ${event.route.id}`)
              );
              key2 = "error";
              str = stringify(error2, reducers);
            }
            count -= 1;
            push2(`{"type":"chunk","id":${id},"${key2}":${str}}
`);
            if (count === 0)
              done2();
          }
        );
        return id;
      }
    }
  };
  try {
    const strings = nodes.map((node) => {
      if (!node)
        return "null";
      if (node.type === "error" || node.type === "skip") {
        return JSON.stringify(node);
      }
      return `{"type":"data","data":${stringify(node.data, reducers)},${stringify_uses(
        node
      )}${node.slash ? `,"slash":${JSON.stringify(node.slash)}` : ""}}`;
    });
    return {
      data: `{"type":"data","nodes":[${strings.join(",")}]}
`,
      chunks: count > 0 ? iterator : null
    };
  } catch (e3) {
    throw new Error(clarify_devalue_error(
      event,
      /** @type {any} */
      e3
    ));
  }
}
function load_page_nodes(page2, manifest2) {
  return Promise.all([
    // we use == here rather than === because [undefined] serializes as "[null]"
    ...page2.layouts.map((n2) => n2 == void 0 ? n2 : manifest2._.nodes[n2]()),
    manifest2._.nodes[page2.leaf]()
  ]);
}
var MAX_DEPTH = 10;
async function render_page(event, page2, options22, manifest2, state, resolve_opts) {
  if (state.depth > MAX_DEPTH) {
    return text(`Not found: ${event.url.pathname}`, {
      status: 404
      // TODO in some cases this should be 500. not sure how to differentiate
    });
  }
  if (is_action_json_request(event)) {
    const node = await manifest2._.nodes[page2.leaf]();
    return handle_action_json_request(event, options22, node?.server);
  }
  try {
    const nodes = await load_page_nodes(page2, manifest2);
    const leaf_node = (
      /** @type {import('types').SSRNode} */
      nodes.at(-1)
    );
    let status = 200;
    let action_result = void 0;
    if (is_action_request(event)) {
      action_result = await handle_action_request(event, leaf_node.server);
      if (action_result?.type === "redirect") {
        return redirect_response(action_result.status, action_result.location);
      }
      if (action_result?.type === "error") {
        status = get_status(action_result.error);
      }
      if (action_result?.type === "failure") {
        status = action_result.status;
      }
    }
    const should_prerender_data = nodes.some((node) => node?.server?.load);
    const data_pathname = add_data_suffix(event.url.pathname);
    const should_prerender = get_option(nodes, "prerender") ?? false;
    if (should_prerender) {
      const mod = leaf_node.server;
      if (mod?.actions) {
        throw new Error("Cannot prerender pages with actions");
      }
    } else if (state.prerendering) {
      return new Response(void 0, {
        status: 204
      });
    }
    state.prerender_default = should_prerender;
    const fetched = [];
    if (get_option(nodes, "ssr") === false && !(state.prerendering && should_prerender_data)) {
      return await render_response({
        branch: [],
        fetched,
        page_config: {
          ssr: false,
          csr: get_option(nodes, "csr") ?? true
        },
        status,
        error: null,
        event,
        options: options22,
        manifest: manifest2,
        state,
        resolve_opts
      });
    }
    const branch = [];
    let load_error = null;
    const server_promises = nodes.map((node, i) => {
      if (load_error) {
        throw load_error;
      }
      return Promise.resolve().then(async () => {
        try {
          if (node === leaf_node && action_result?.type === "error") {
            throw action_result.error;
          }
          return await load_server_data({
            event,
            state,
            node,
            parent: async () => {
              const data = {};
              for (let j2 = 0; j2 < i; j2 += 1) {
                const parent = await server_promises[j2];
                if (parent)
                  Object.assign(data, await parent.data);
              }
              return data;
            }
          });
        } catch (e3) {
          load_error = /** @type {Error} */
          e3;
          throw load_error;
        }
      });
    });
    const csr = get_option(nodes, "csr") ?? true;
    const load_promises = nodes.map((node, i) => {
      if (load_error)
        throw load_error;
      return Promise.resolve().then(async () => {
        try {
          return await load_data({
            event,
            fetched,
            node,
            parent: async () => {
              const data = {};
              for (let j2 = 0; j2 < i; j2 += 1) {
                Object.assign(data, await load_promises[j2]);
              }
              return data;
            },
            resolve_opts,
            server_data_promise: server_promises[i],
            state,
            csr
          });
        } catch (e3) {
          load_error = /** @type {Error} */
          e3;
          throw load_error;
        }
      });
    });
    for (const p of server_promises)
      p.catch(() => {
      });
    for (const p of load_promises)
      p.catch(() => {
      });
    for (let i = 0; i < nodes.length; i += 1) {
      const node = nodes[i];
      if (node) {
        try {
          const server_data = await server_promises[i];
          const data = await load_promises[i];
          branch.push({ node, server_data, data });
        } catch (e3) {
          const err = normalize_error(e3);
          if (err instanceof Redirect) {
            if (state.prerendering && should_prerender_data) {
              const body2 = JSON.stringify({
                type: "redirect",
                location: err.location
              });
              state.prerendering.dependencies.set(data_pathname, {
                response: text(body2),
                body: body2
              });
            }
            return redirect_response(err.status, err.location);
          }
          const status2 = get_status(err);
          const error2 = await handle_error_and_jsonify(event, options22, err);
          while (i--) {
            if (page2.errors[i]) {
              const index10 = (
                /** @type {number} */
                page2.errors[i]
              );
              const node2 = await manifest2._.nodes[index10]();
              let j2 = i;
              while (!branch[j2])
                j2 -= 1;
              return await render_response({
                event,
                options: options22,
                manifest: manifest2,
                state,
                resolve_opts,
                page_config: { ssr: true, csr: true },
                status: status2,
                error: error2,
                branch: compact(branch.slice(0, j2 + 1)).concat({
                  node: node2,
                  data: null,
                  server_data: null
                }),
                fetched
              });
            }
          }
          return static_error_page(options22, status2, error2.message);
        }
      } else {
        branch.push(null);
      }
    }
    if (state.prerendering && should_prerender_data) {
      let { data, chunks } = get_data_json(
        event,
        options22,
        branch.map((node) => node?.server_data)
      );
      if (chunks) {
        for await (const chunk of chunks) {
          data += chunk;
        }
      }
      state.prerendering.dependencies.set(data_pathname, {
        response: text(data),
        body: data
      });
    }
    const ssr = get_option(nodes, "ssr") ?? true;
    return await render_response({
      event,
      options: options22,
      manifest: manifest2,
      state,
      resolve_opts,
      page_config: {
        csr: get_option(nodes, "csr") ?? true,
        ssr
      },
      status,
      error: null,
      branch: ssr === false ? [] : compact(branch),
      action_result,
      fetched
    });
  } catch (e3) {
    return await respond_with_error({
      event,
      options: options22,
      manifest: manifest2,
      state,
      status: 500,
      error: e3,
      resolve_opts
    });
  }
}
function exec(match, params, matchers) {
  const result = {};
  const values = match.slice(1);
  const values_needing_match = values.filter((value) => value !== void 0);
  let buffered = 0;
  for (let i = 0; i < params.length; i += 1) {
    const param = params[i];
    let value = values[i - buffered];
    if (param.chained && param.rest && buffered) {
      value = values.slice(i - buffered, i + 1).filter((s22) => s22).join("/");
      buffered = 0;
    }
    if (value === void 0) {
      if (param.rest)
        result[param.name] = "";
      continue;
    }
    if (!param.matcher || matchers[param.matcher](value)) {
      result[param.name] = value;
      const next_param = params[i + 1];
      const next_value = values[i + 1];
      if (next_param && !next_param.rest && next_param.optional && next_value && param.chained) {
        buffered = 0;
      }
      if (!next_param && !next_value && Object.keys(result).length === values_needing_match.length) {
        buffered = 0;
      }
      continue;
    }
    if (param.optional && param.chained) {
      buffered++;
      continue;
    }
    return;
  }
  if (buffered)
    return;
  return result;
}
function validate_options(options22) {
  if (options22?.path === void 0) {
    throw new Error("You must specify a `path` when setting, deleting or serializing cookies");
  }
}
function get_cookies(request, url, trailing_slash) {
  const header = request.headers.get("cookie") ?? "";
  const initial_cookies = (0, import_cookie.parse)(header, { decode: (value) => value });
  const normalized_url = normalize_path(url.pathname, trailing_slash);
  const new_cookies = {};
  const defaults = {
    httpOnly: true,
    sameSite: "lax",
    secure: url.hostname === "localhost" && url.protocol === "http:" ? false : true
  };
  const cookies = {
    // The JSDoc param annotations appearing below for get, set and delete
    // are necessary to expose the `cookie` library types to
    // typescript users. `@type {import('@sveltejs/kit').Cookies}` above is not
    // sufficient to do so.
    /**
     * @param {string} name
     * @param {import('cookie').CookieParseOptions} opts
     */
    get(name, opts) {
      const c2 = new_cookies[name];
      if (c2 && domain_matches(url.hostname, c2.options.domain) && path_matches(url.pathname, c2.options.path)) {
        return c2.value;
      }
      const decoder = opts?.decode || decodeURIComponent;
      const req_cookies = (0, import_cookie.parse)(header, { decode: decoder });
      const cookie = req_cookies[name];
      return cookie;
    },
    /**
     * @param {import('cookie').CookieParseOptions} opts
     */
    getAll(opts) {
      const decoder = opts?.decode || decodeURIComponent;
      const cookies2 = (0, import_cookie.parse)(header, { decode: decoder });
      for (const c2 of Object.values(new_cookies)) {
        if (domain_matches(url.hostname, c2.options.domain) && path_matches(url.pathname, c2.options.path)) {
          cookies2[c2.name] = c2.value;
        }
      }
      return Object.entries(cookies2).map(([name, value]) => ({ name, value }));
    },
    /**
     * @param {string} name
     * @param {string} value
     * @param {import('./page/types.js').Cookie['options']} options
     */
    set(name, value, options22) {
      validate_options(options22);
      set_internal(name, value, { ...defaults, ...options22 });
    },
    /**
     * @param {string} name
     *  @param {import('./page/types.js').Cookie['options']} options
     */
    delete(name, options22) {
      validate_options(options22);
      cookies.set(name, "", { ...options22, maxAge: 0 });
    },
    /**
     * @param {string} name
     * @param {string} value
     *  @param {import('./page/types.js').Cookie['options']} options
     */
    serialize(name, value, options22) {
      validate_options(options22);
      let path = options22.path;
      if (!options22.domain || options22.domain === url.hostname) {
        path = resolve(normalized_url, path);
      }
      return (0, import_cookie.serialize)(name, value, { ...defaults, ...options22, path });
    }
  };
  function get_cookie_header(destination, header2) {
    const combined_cookies = {
      // cookies sent by the user agent have lowest precedence
      ...initial_cookies
    };
    for (const key2 in new_cookies) {
      const cookie = new_cookies[key2];
      if (!domain_matches(destination.hostname, cookie.options.domain))
        continue;
      if (!path_matches(destination.pathname, cookie.options.path))
        continue;
      const encoder22 = cookie.options.encode || encodeURIComponent;
      combined_cookies[cookie.name] = encoder22(cookie.value);
    }
    if (header2) {
      const parsed = (0, import_cookie.parse)(header2, { decode: (value) => value });
      for (const name in parsed) {
        combined_cookies[name] = parsed[name];
      }
    }
    return Object.entries(combined_cookies).map(([name, value]) => `${name}=${value}`).join("; ");
  }
  function set_internal(name, value, options22) {
    let path = options22.path;
    if (!options22.domain || options22.domain === url.hostname) {
      path = resolve(normalized_url, path);
    }
    new_cookies[name] = { name, value, options: { ...options22, path } };
  }
  return { cookies, new_cookies, get_cookie_header, set_internal };
}
function domain_matches(hostname, constraint) {
  if (!constraint)
    return true;
  const normalized = constraint[0] === "." ? constraint.slice(1) : constraint;
  if (hostname === normalized)
    return true;
  return hostname.endsWith("." + normalized);
}
function path_matches(path, constraint) {
  if (!constraint)
    return true;
  const normalized = constraint.endsWith("/") ? constraint.slice(0, -1) : constraint;
  if (path === normalized)
    return true;
  return path.startsWith(normalized + "/");
}
function add_cookies_to_headers(headers2, cookies) {
  for (const new_cookie of cookies) {
    const { name, value, options: options22 } = new_cookie;
    headers2.append("set-cookie", (0, import_cookie.serialize)(name, value, options22));
    if (options22.path.endsWith(".html")) {
      const path = add_data_suffix(options22.path);
      headers2.append("set-cookie", (0, import_cookie.serialize)(name, value, { ...options22, path }));
    }
  }
}
function create_fetch({ event, options: options22, manifest: manifest2, state, get_cookie_header, set_internal }) {
  const server_fetch = async (info, init2) => {
    const original_request = normalize_fetch_input(info, init2, event.url);
    let mode = (info instanceof Request ? info.mode : init2?.mode) ?? "cors";
    let credentials = (info instanceof Request ? info.credentials : init2?.credentials) ?? "same-origin";
    return options22.hooks.handleFetch({
      event,
      request: original_request,
      fetch: async (info2, init3) => {
        const request = normalize_fetch_input(info2, init3, event.url);
        const url = new URL(request.url);
        if (!request.headers.has("origin")) {
          request.headers.set("origin", event.url.origin);
        }
        if (info2 !== original_request) {
          mode = (info2 instanceof Request ? info2.mode : init3?.mode) ?? "cors";
          credentials = (info2 instanceof Request ? info2.credentials : init3?.credentials) ?? "same-origin";
        }
        if ((request.method === "GET" || request.method === "HEAD") && (mode === "no-cors" && url.origin !== event.url.origin || url.origin === event.url.origin)) {
          request.headers.delete("origin");
        }
        if (url.origin !== event.url.origin) {
          if (`.${url.hostname}`.endsWith(`.${event.url.hostname}`) && credentials !== "omit") {
            const cookie = get_cookie_header(url, request.headers.get("cookie"));
            if (cookie)
              request.headers.set("cookie", cookie);
          }
          return fetch(request);
        }
        const prefix = assets || base;
        const decoded = decodeURIComponent(url.pathname);
        const filename = (decoded.startsWith(prefix) ? decoded.slice(prefix.length) : decoded).slice(1);
        const filename_html = `${filename}/index.html`;
        const is_asset = manifest2.assets.has(filename);
        const is_asset_html = manifest2.assets.has(filename_html);
        if (is_asset || is_asset_html) {
          const file = is_asset ? filename : filename_html;
          if (state.read) {
            const type = is_asset ? manifest2.mimeTypes[filename.slice(filename.lastIndexOf("."))] : "text/html";
            return new Response(state.read(file), {
              headers: type ? { "content-type": type } : {}
            });
          }
          return await fetch(request);
        }
        if (credentials !== "omit") {
          const cookie = get_cookie_header(url, request.headers.get("cookie"));
          if (cookie) {
            request.headers.set("cookie", cookie);
          }
          const authorization = event.request.headers.get("authorization");
          if (authorization && !request.headers.has("authorization")) {
            request.headers.set("authorization", authorization);
          }
        }
        if (!request.headers.has("accept")) {
          request.headers.set("accept", "*/*");
        }
        if (!request.headers.has("accept-language")) {
          request.headers.set(
            "accept-language",
            /** @type {string} */
            event.request.headers.get("accept-language")
          );
        }
        const response = await respond(request, options22, manifest2, {
          ...state,
          depth: state.depth + 1
        });
        const set_cookie = response.headers.get("set-cookie");
        if (set_cookie) {
          for (const str of set_cookie_parser.splitCookiesString(set_cookie)) {
            const { name, value, ...options3 } = set_cookie_parser.parseString(str);
            const path = options3.path ?? (url.pathname.split("/").slice(0, -1).join("/") || "/");
            set_internal(name, value, {
              path,
              .../** @type {import('cookie').CookieSerializeOptions} */
              options3
            });
          }
        }
        return response;
      }
    });
  };
  return (input, init2) => {
    const response = server_fetch(input, init2);
    response.catch(() => {
    });
    return response;
  };
}
function normalize_fetch_input(info, init2, url) {
  if (info instanceof Request) {
    return info;
  }
  return new Request(typeof info === "string" ? new URL(info, url) : info, init2);
}
var body;
var etag;
var headers;
function get_public_env(request) {
  body ??= `export const env=${JSON.stringify(public_env)}`;
  etag ??= `W/${Date.now()}`;
  headers ??= new Headers({
    "content-type": "application/javascript; charset=utf-8",
    etag
  });
  if (request.headers.get("if-none-match") === etag) {
    return new Response(void 0, { status: 304, headers });
  }
  return new Response(body, { headers });
}
function get_page_config(nodes) {
  let current = {};
  for (const node of nodes) {
    if (!node?.universal?.config && !node?.server?.config)
      continue;
    current = {
      ...current,
      ...node?.universal?.config,
      ...node?.server?.config
    };
  }
  return Object.keys(current).length ? current : void 0;
}
var default_transform = ({ html }) => html;
var default_filter = () => false;
var default_preload = ({ type }) => type === "js" || type === "css";
var page_methods = /* @__PURE__ */ new Set(["GET", "HEAD", "POST"]);
var allowed_page_methods = /* @__PURE__ */ new Set(["GET", "HEAD", "OPTIONS"]);
async function respond(request, options22, manifest2, state) {
  const url = new URL(request.url);
  if (options22.csrf_check_origin) {
    const forbidden = is_form_content_type(request) && (request.method === "POST" || request.method === "PUT" || request.method === "PATCH" || request.method === "DELETE") && request.headers.get("origin") !== url.origin;
    if (forbidden) {
      const csrf_error = new HttpError(
        403,
        `Cross-site ${request.method} form submissions are forbidden`
      );
      if (request.headers.get("accept") === "application/json") {
        return json(csrf_error.body, { status: csrf_error.status });
      }
      return text(csrf_error.body.message, { status: csrf_error.status });
    }
  }
  let rerouted_path;
  try {
    rerouted_path = options22.hooks.reroute({ url: new URL(url) }) ?? url.pathname;
  } catch (e3) {
    return text("Internal Server Error", {
      status: 500
    });
  }
  let decoded;
  try {
    decoded = decode_pathname(rerouted_path);
  } catch {
    return text("Malformed URI", { status: 400 });
  }
  let route2 = null;
  let params = {};
  if (base && !state.prerendering?.fallback) {
    if (!decoded.startsWith(base)) {
      return text("Not found", { status: 404 });
    }
    decoded = decoded.slice(base.length) || "/";
  }
  if (decoded === `/${options22.app_dir}/env.js`) {
    return get_public_env(request);
  }
  if (decoded.startsWith(`/${options22.app_dir}`)) {
    return text("Not found", { status: 404 });
  }
  const is_data_request = has_data_suffix(decoded);
  let invalidated_data_nodes;
  if (is_data_request) {
    decoded = strip_data_suffix(decoded) || "/";
    url.pathname = strip_data_suffix(url.pathname) + (url.searchParams.get(TRAILING_SLASH_PARAM) === "1" ? "/" : "") || "/";
    url.searchParams.delete(TRAILING_SLASH_PARAM);
    invalidated_data_nodes = url.searchParams.get(INVALIDATED_PARAM)?.split("").map((node) => node === "1");
    url.searchParams.delete(INVALIDATED_PARAM);
  }
  if (!state.prerendering?.fallback) {
    const matchers = await manifest2._.matchers();
    for (const candidate of manifest2._.routes) {
      const match = candidate.pattern.exec(decoded);
      if (!match)
        continue;
      const matched = exec(match, candidate.params, matchers);
      if (matched) {
        route2 = candidate;
        params = decode_params(matched);
        break;
      }
    }
  }
  let trailing_slash = void 0;
  const headers2 = {};
  let cookies_to_add = {};
  const event = {
    // @ts-expect-error `cookies` and `fetch` need to be created after the `event` itself
    cookies: null,
    // @ts-expect-error
    fetch: null,
    getClientAddress: state.getClientAddress || (() => {
      throw new Error(
        `${"@sveltejs/adapter-cloudflare"} does not specify getClientAddress. Please raise an issue`
      );
    }),
    locals: {},
    params,
    platform: state.platform,
    request,
    route: { id: route2?.id ?? null },
    setHeaders: (new_headers) => {
      for (const key2 in new_headers) {
        const lower = key2.toLowerCase();
        const value = new_headers[key2];
        if (lower === "set-cookie") {
          throw new Error(
            "Use `event.cookies.set(name, value, options)` instead of `event.setHeaders` to set cookies"
          );
        } else if (lower in headers2) {
          throw new Error(`"${key2}" header is already set`);
        } else {
          headers2[lower] = value;
          if (state.prerendering && lower === "cache-control") {
            state.prerendering.cache = /** @type {string} */
            value;
          }
        }
      }
    },
    url,
    isDataRequest: is_data_request,
    isSubRequest: state.depth > 0
  };
  let resolve_opts = {
    transformPageChunk: default_transform,
    filterSerializedResponseHeaders: default_filter,
    preload: default_preload
  };
  try {
    if (route2) {
      if (url.pathname === base || url.pathname === base + "/") {
        trailing_slash = "always";
      } else if (route2.page) {
        const nodes = await load_page_nodes(route2.page, manifest2);
        if (DEV)
          ;
        trailing_slash = get_option(nodes, "trailingSlash");
      } else if (route2.endpoint) {
        const node = await route2.endpoint();
        trailing_slash = node.trailingSlash;
        if (DEV)
          ;
      }
      if (!is_data_request) {
        const normalized = normalize_path(url.pathname, trailing_slash ?? "never");
        if (normalized !== url.pathname && !state.prerendering?.fallback) {
          return new Response(void 0, {
            status: 308,
            headers: {
              "x-sveltekit-normalize": "1",
              location: (
                // ensure paths starting with '//' are not treated as protocol-relative
                (normalized.startsWith("//") ? url.origin + normalized : normalized) + (url.search === "?" ? "" : url.search)
              )
            }
          });
        }
      }
      if (state.before_handle || state.emulator?.platform) {
        let config = {};
        let prerender7 = false;
        if (route2.endpoint) {
          const node = await route2.endpoint();
          config = node.config ?? config;
          prerender7 = node.prerender ?? prerender7;
        } else if (route2.page) {
          const nodes = await load_page_nodes(route2.page, manifest2);
          config = get_page_config(nodes) ?? config;
          prerender7 = get_option(nodes, "prerender") ?? false;
        }
        if (state.before_handle) {
          state.before_handle(event, config, prerender7);
        }
        if (state.emulator?.platform) {
          event.platform = await state.emulator.platform({ config, prerender: prerender7 });
        }
      }
    }
    const { cookies, new_cookies, get_cookie_header, set_internal } = get_cookies(
      request,
      url,
      trailing_slash ?? "never"
    );
    cookies_to_add = new_cookies;
    event.cookies = cookies;
    event.fetch = create_fetch({
      event,
      options: options22,
      manifest: manifest2,
      state,
      get_cookie_header,
      set_internal
    });
    if (state.prerendering && !state.prerendering.fallback)
      disable_search(url);
    const response = await options22.hooks.handle({
      event,
      resolve: (event2, opts) => resolve2(event2, opts).then((response2) => {
        for (const key2 in headers2) {
          const value = headers2[key2];
          response2.headers.set(
            key2,
            /** @type {string} */
            value
          );
        }
        add_cookies_to_headers(response2.headers, Object.values(cookies_to_add));
        if (state.prerendering && event2.route.id !== null) {
          response2.headers.set("x-sveltekit-routeid", encodeURI(event2.route.id));
        }
        return response2;
      })
    });
    if (response.status === 200 && response.headers.has("etag")) {
      let if_none_match_value = request.headers.get("if-none-match");
      if (if_none_match_value?.startsWith('W/"')) {
        if_none_match_value = if_none_match_value.substring(2);
      }
      const etag2 = (
        /** @type {string} */
        response.headers.get("etag")
      );
      if (if_none_match_value === etag2) {
        const headers22 = new Headers({ etag: etag2 });
        for (const key2 of [
          "cache-control",
          "content-location",
          "date",
          "expires",
          "vary",
          "set-cookie"
        ]) {
          const value = response.headers.get(key2);
          if (value)
            headers22.set(key2, value);
        }
        return new Response(void 0, {
          status: 304,
          headers: headers22
        });
      }
    }
    if (is_data_request && response.status >= 300 && response.status <= 308) {
      const location = response.headers.get("location");
      if (location) {
        return redirect_json_response(new Redirect(
          /** @type {any} */
          response.status,
          location
        ));
      }
    }
    return response;
  } catch (e3) {
    if (e3 instanceof Redirect) {
      const response = is_data_request ? redirect_json_response(e3) : route2?.page && is_action_json_request(event) ? action_json_redirect(e3) : redirect_response(e3.status, e3.location);
      add_cookies_to_headers(response.headers, Object.values(cookies_to_add));
      return response;
    }
    return await handle_fatal_error(event, options22, e3);
  }
  async function resolve2(event2, opts) {
    try {
      if (opts) {
        resolve_opts = {
          transformPageChunk: opts.transformPageChunk || default_transform,
          filterSerializedResponseHeaders: opts.filterSerializedResponseHeaders || default_filter,
          preload: opts.preload || default_preload
        };
      }
      if (state.prerendering?.fallback) {
        return await render_response({
          event: event2,
          options: options22,
          manifest: manifest2,
          state,
          page_config: { ssr: false, csr: true },
          status: 200,
          error: null,
          branch: [],
          fetched: [],
          resolve_opts
        });
      }
      if (route2) {
        const method = (
          /** @type {import('types').HttpMethod} */
          event2.request.method
        );
        let response;
        if (is_data_request) {
          response = await render_data(
            event2,
            route2,
            options22,
            manifest2,
            state,
            invalidated_data_nodes,
            trailing_slash ?? "never"
          );
        } else if (route2.endpoint && (!route2.page || is_endpoint_request(event2))) {
          response = await render_endpoint(event2, await route2.endpoint(), state);
        } else if (route2.page) {
          if (page_methods.has(method)) {
            response = await render_page(event2, route2.page, options22, manifest2, state, resolve_opts);
          } else {
            const allowed_methods2 = new Set(allowed_page_methods);
            const node = await manifest2._.nodes[route2.page.leaf]();
            if (node?.server?.actions) {
              allowed_methods2.add("POST");
            }
            if (method === "OPTIONS") {
              response = new Response(null, {
                status: 204,
                headers: {
                  allow: Array.from(allowed_methods2.values()).join(", ")
                }
              });
            } else {
              const mod = [...allowed_methods2].reduce(
                (acc, curr) => {
                  acc[curr] = true;
                  return acc;
                },
                /** @type {Record<string, any>} */
                {}
              );
              response = method_not_allowed(mod, method);
            }
          }
        } else {
          throw new Error("This should never happen");
        }
        if (request.method === "GET" && route2.page && route2.endpoint) {
          const vary = response.headers.get("vary")?.split(",")?.map((v) => v.trim().toLowerCase());
          if (!(vary?.includes("accept") || vary?.includes("*"))) {
            response = new Response(response.body, {
              status: response.status,
              statusText: response.statusText,
              headers: new Headers(response.headers)
            });
            response.headers.append("Vary", "Accept");
          }
        }
        return response;
      }
      if (state.error && event2.isSubRequest) {
        return await fetch(request, {
          headers: {
            "x-sveltekit-error": "true"
          }
        });
      }
      if (state.error) {
        return text("Internal Server Error", {
          status: 500
        });
      }
      if (state.depth === 0) {
        return await respond_with_error({
          event: event2,
          options: options22,
          manifest: manifest2,
          state,
          status: 404,
          error: new SvelteKitError(404, "Not Found", `Not found: ${event2.url.pathname}`),
          resolve_opts
        });
      }
      if (state.prerendering) {
        return text("not found", { status: 404 });
      }
      return await fetch(request);
    } catch (e3) {
      return await handle_fatal_error(event2, options22, e3);
    } finally {
      event2.cookies.set = () => {
        throw new Error("Cannot use `cookies.set(...)` after the response has been generated");
      };
      event2.setHeaders = () => {
        throw new Error("Cannot use `setHeaders(...)` after the response has been generated");
      };
    }
  }
}
function filter_private_env(env, { public_prefix, private_prefix }) {
  return Object.fromEntries(
    Object.entries(env).filter(
      ([k2]) => k2.startsWith(private_prefix) && (public_prefix === "" || !k2.startsWith(public_prefix))
    )
  );
}
function filter_public_env(env, { public_prefix, private_prefix }) {
  return Object.fromEntries(
    Object.entries(env).filter(
      ([k2]) => k2.startsWith(public_prefix) && (private_prefix === "" || !k2.startsWith(private_prefix))
    )
  );
}
var prerender_env_handler = {
  get({ type }, prop) {
    throw new Error(
      `Cannot read values from $env/dynamic/${type} while prerendering (attempted to read env.${prop.toString()}). Use $env/static/${type} instead`
    );
  }
};
var Server = class {
  /** @type {import('types').SSROptions} */
  #options;
  /** @type {import('@sveltejs/kit').SSRManifest} */
  #manifest;
  /** @param {import('@sveltejs/kit').SSRManifest} manifest */
  constructor(manifest2) {
    this.#options = options2;
    this.#manifest = manifest2;
  }
  /**
   * @param {{
   *   env: Record<string, string>;
   *   read?: (file: string) => ReadableStream;
   * }} opts
   */
  async init({ env, read }) {
    const prefixes = {
      public_prefix: this.#options.env_public_prefix,
      private_prefix: this.#options.env_private_prefix
    };
    const private_env2 = filter_private_env(env, prefixes);
    const public_env2 = filter_public_env(env, prefixes);
    set_private_env(
      prerendering ? new Proxy({ type: "private" }, prerender_env_handler) : private_env2
    );
    set_public_env(
      prerendering ? new Proxy({ type: "public" }, prerender_env_handler) : public_env2
    );
    set_safe_public_env(public_env2);
    if (!this.#options.hooks) {
      try {
        const module = await get_hooks();
        this.#options.hooks = {
          handle: module.handle || (({ event, resolve: resolve2 }) => resolve2(event)),
          handleError: module.handleError || (({ error: error2 }) => console.error(error2)),
          handleFetch: module.handleFetch || (({ request, fetch: fetch2 }) => fetch2(request)),
          reroute: module.reroute || (() => {
          })
        };
      } catch (error2) {
        {
          throw error2;
        }
      }
    }
  }
  /**
   * @param {Request} request
   * @param {import('types').RequestOptions} options
   */
  async respond(request, options22) {
    return respond(request, this.#options, this.#manifest, {
      ...options22,
      error: false,
      depth: 0
    });
  }
};
var manifest = (() => {
  function __memo(fn) {
    let value;
    return () => value ??= value = fn();
  }
  return {
    appDir: "_app",
    appPath: "_app",
    assets: /* @__PURE__ */ new Set(["favicon.ico", "favicon.svg", "image.png", "robots.txt"]),
    mimeTypes: { ".svg": "image/svg+xml", ".png": "image/png", ".txt": "text/plain" },
    _: {
      client: { "start": "_app/immutable/entry/start.D7ZO3Tdj.js", "app": "_app/immutable/entry/app.Cg1xkwmi.js", "imports": ["_app/immutable/entry/start.D7ZO3Tdj.js", "_app/immutable/chunks/entry.Dv6jx6u8.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/entry/app.Cg1xkwmi.js", "_app/immutable/chunks/runtime.CM0qr2RF.js", "_app/immutable/chunks/scheduler.CM4zAhdD.js", "_app/immutable/chunks/index.C2sGjX4M.js"], "stylesheets": [], "fonts": [], "uses_env_dynamic_public": true },
      nodes: [
        __memo(() => Promise.resolve().then(() => (init__(), __exports))),
        __memo(() => Promise.resolve().then(() => (init__2(), __exports2))),
        __memo(() => Promise.resolve().then(() => (init__3(), __exports3))),
        __memo(() => Promise.resolve().then(() => (init__4(), __exports4))),
        __memo(() => Promise.resolve().then(() => (init__5(), __exports5))),
        __memo(() => Promise.resolve().then(() => (init__6(), __exports6))),
        __memo(() => Promise.resolve().then(() => (init__7(), __exports7))),
        __memo(() => Promise.resolve().then(() => (init__8(), __exports8))),
        __memo(() => Promise.resolve().then(() => (init__9(), __exports9)))
      ],
      routes: [
        {
          id: "/[[lang]]/admin",
          pattern: /^(?:\/([^/]+))?\/admin\/?$/,
          params: [{ "name": "lang", "optional": true, "rest": false, "chained": true }],
          page: { layouts: [0], errors: [1], leaf: 4 },
          endpoint: null
        },
        {
          id: "/[[lang]]/admin/thumbnail",
          pattern: /^(?:\/([^/]+))?\/admin\/thumbnail\/?$/,
          params: [{ "name": "lang", "optional": true, "rest": false, "chained": true }],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts(), server_ts_exports)))
        },
        {
          id: "/google/callback",
          pattern: /^\/google\/callback\/?$/,
          params: [],
          page: { layouts: [0, 3], errors: [1, ,], leaf: 8 },
          endpoint: null
        },
        {
          id: "/google/clearImage",
          pattern: /^\/google\/clearImage\/?$/,
          params: [],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts2(), server_ts_exports2)))
        },
        {
          id: "/google/clear",
          pattern: /^\/google\/clear\/?$/,
          params: [],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts3(), server_ts_exports3)))
        },
        {
          id: "/google/createImage/[hash]",
          pattern: /^\/google\/createImage\/([^/]+?)\/?$/,
          params: [{ "name": "hash", "optional": false, "rest": false, "chained": false }],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts4(), server_ts_exports4)))
        },
        {
          id: "/google/create/[uuid]",
          pattern: /^\/google\/create\/([^/]+?)\/?$/,
          params: [{ "name": "uuid", "optional": false, "rest": false, "chained": false }],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts5(), server_ts_exports5)))
        },
        {
          id: "/google/delete/[uuid]",
          pattern: /^\/google\/delete\/([^/]+?)\/?$/,
          params: [{ "name": "uuid", "optional": false, "rest": false, "chained": false }],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts6(), server_ts_exports6)))
        },
        {
          id: "/google/existImage/[hash]",
          pattern: /^\/google\/existImage\/([^/]+?)\/?$/,
          params: [{ "name": "hash", "optional": false, "rest": false, "chained": false }],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts7(), server_ts_exports7)))
        },
        {
          id: "/google/getImage/[hash]",
          pattern: /^\/google\/getImage\/([^/]+?)\/?$/,
          params: [{ "name": "hash", "optional": false, "rest": false, "chained": false }],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts8(), server_ts_exports8)))
        },
        {
          id: "/google/get/[uuid]",
          pattern: /^\/google\/get\/([^/]+?)\/?$/,
          params: [{ "name": "uuid", "optional": false, "rest": false, "chained": false }],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts9(), server_ts_exports9)))
        },
        {
          id: "/google/listImage",
          pattern: /^\/google\/listImage\/?$/,
          params: [],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts10(), server_ts_exports10)))
        },
        {
          id: "/google/list",
          pattern: /^\/google\/list\/?$/,
          params: [],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts11(), server_ts_exports11)))
        },
        {
          id: "/google/login",
          pattern: /^\/google\/login\/?$/,
          params: [],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts12(), server_ts_exports12)))
        },
        {
          id: "/google/logout",
          pattern: /^\/google\/logout\/?$/,
          params: [],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts13(), server_ts_exports13)))
        },
        {
          id: "/google/update/[uuid]",
          pattern: /^\/google\/update\/([^/]+?)\/?$/,
          params: [{ "name": "uuid", "optional": false, "rest": false, "chained": false }],
          page: null,
          endpoint: __memo(() => Promise.resolve().then(() => (init_server_ts14(), server_ts_exports14)))
        },
        {
          id: "/[[lang]]/library",
          pattern: /^(?:\/([^/]+))?\/library\/?$/,
          params: [{ "name": "lang", "optional": true, "rest": false, "chained": true }],
          page: { layouts: [0, 2], errors: [1, ,], leaf: 5 },
          endpoint: null
        },
        {
          id: "/[[lang]]/library/public/[id]",
          pattern: /^(?:\/([^/]+))?\/library\/public\/([^/]+?)\/?$/,
          params: [{ "name": "lang", "optional": true, "rest": false, "chained": true }, { "name": "id", "optional": false, "rest": false, "chained": false }],
          page: { layouts: [0, 2], errors: [1, ,], leaf: 6 },
          endpoint: null
        },
        {
          id: "/[[lang]]/publish",
          pattern: /^(?:\/([^/]+))?\/publish\/?$/,
          params: [{ "name": "lang", "optional": true, "rest": false, "chained": true }],
          page: { layouts: [0], errors: [1], leaf: 7 },
          endpoint: null
        }
      ],
      matchers: async () => {
        return {};
      },
      server_assets: {}
    }
  };
})();
var prerendered = /* @__PURE__ */ new Set(["/create", "/create/__data.json", "/credits", "/credits/__data.json", "/host", "/host/__data.json", "/play", "/play/__data.json", "/posts", "/posts/__data.json", "/posts/introducing-fuiz", "/posts/introducing-fuiz/__data.json", "/privacy", "/privacy/__data.json", "/sitemap.xml", "/", "/__data.json"]);
var app_path = "_app";
async function e(e3, t2) {
  let n2 = "string" != typeof t2 && "HEAD" === t2.method;
  n2 && (t2 = new Request(t2, { method: "GET" }));
  let r3 = await e3.match(t2);
  return n2 && r3 && (r3 = new Response(null, r3)), r3;
}
function t(e3, t2, n2, o2) {
  return ("string" == typeof t2 || "GET" === t2.method) && r(n2) && (n2.headers.has("Set-Cookie") && (n2 = new Response(n2.body, n2)).headers.append("Cache-Control", "private=Set-Cookie"), o2.waitUntil(e3.put(t2, n2.clone()))), n2;
}
var n = /* @__PURE__ */ new Set([200, 203, 204, 300, 301, 404, 405, 410, 414, 501]);
function r(e3) {
  if (!n.has(e3.status))
    return false;
  if (~(e3.headers.get("Vary") || "").indexOf("*"))
    return false;
  let t2 = e3.headers.get("Cache-Control") || "";
  return !/(private|no-cache|no-store)/i.test(t2);
}
function o(n2) {
  return async function(r3, o2) {
    let a = await e(n2, r3);
    if (a)
      return a;
    o2.defer((e3) => {
      t(n2, r3, e3, o2);
    });
  };
}
var s2 = caches.default;
var c = t.bind(0, s2);
var r2 = e.bind(0, s2);
var e2 = o.bind(0, s2);
var server = new Server(manifest);
var immutable = `/${app_path}/immutable/`;
var version_file = `/${app_path}/version.json`;
var worker = {
  async fetch(req, env, context) {
    await server.init({ env });
    let pragma = req.headers.get("cache-control") || "";
    let res = !pragma.includes("no-cache") && await r2(req);
    if (res)
      return res;
    let { pathname, search } = new URL(req.url);
    try {
      pathname = decodeURIComponent(pathname);
    } catch {
    }
    const stripped_pathname = pathname.replace(/\/$/, "");
    let is_static_asset = false;
    const filename = stripped_pathname.substring(1);
    if (filename) {
      is_static_asset = manifest.assets.has(filename) || manifest.assets.has(filename + "/index.html");
    }
    let location = pathname.at(-1) === "/" ? stripped_pathname : pathname + "/";
    if (is_static_asset || prerendered.has(pathname) || pathname === version_file || pathname.startsWith(immutable)) {
      res = await env.ASSETS.fetch(req);
    } else if (location && prerendered.has(location)) {
      if (search)
        location += search;
      res = new Response("", {
        status: 308,
        headers: {
          location
        }
      });
    } else {
      res = await server.respond(req, {
        // @ts-ignore
        platform: { env, context, caches, cf: req.cf },
        getClientAddress() {
          return req.headers.get("cf-connecting-ip");
        }
      });
    }
    pragma = res.headers.get("cache-control") || "";
    return pragma && res.status < 400 ? c(req, res, context) : res;
  }
};
var worker_default = worker;
var drainBody = async (request, env, _ctx, middlewareCtx) => {
  try {
    return await middlewareCtx.next(request, env);
  } finally {
    try {
      if (request.body !== null && !request.bodyUsed) {
        const reader = request.body.getReader();
        while (!(await reader.read()).done) {
        }
      }
    } catch (e3) {
      console.error("Failed to drain the unused request body.", e3);
    }
  }
};
var middleware_ensure_req_body_drained_default = drainBody;
var wrap = void 0;
function reduceError(e3) {
  return {
    name: e3?.name,
    message: e3?.message ?? String(e3),
    stack: e3?.stack,
    cause: e3?.cause === void 0 ? void 0 : reduceError(e3.cause)
  };
}
var jsonError = async (request, env, _ctx, middlewareCtx) => {
  try {
    return await middlewareCtx.next(request, env);
  } catch (e3) {
    const error2 = reduceError(e3);
    return Response.json(error2, {
      status: 500,
      headers: { "MF-Experimental-Error-Stack": "true" }
    });
  }
};
var middleware_miniflare3_json_error_default = jsonError;
var wrap2 = void 0;
var envWrappers = [wrap, wrap2].filter(Boolean);
var facade = {
  ...worker_default,
  envWrappers,
  middleware: [
    middleware_ensure_req_body_drained_default,
    middleware_miniflare3_json_error_default,
    ...worker_default.middleware ? worker_default.middleware : []
  ].filter(Boolean)
};
var middleware_insertion_facade_default = facade;
var __facade_middleware__ = [];
function __facade_register__(...args) {
  __facade_middleware__.push(...args.flat());
}
function __facade_invokeChain__(request, env, ctx, dispatch, middlewareChain) {
  const [head, ...tail] = middlewareChain;
  const middlewareCtx = {
    dispatch,
    next(newRequest, newEnv) {
      return __facade_invokeChain__(newRequest, newEnv, ctx, dispatch, tail);
    }
  };
  return head(request, env, ctx, middlewareCtx);
}
function __facade_invoke__(request, env, ctx, dispatch, finalMiddleware) {
  return __facade_invokeChain__(request, env, ctx, dispatch, [
    ...__facade_middleware__,
    finalMiddleware
  ]);
}
var __Facade_ScheduledController__ = class {
  constructor(scheduledTime, cron, noRetry) {
    this.scheduledTime = scheduledTime;
    this.cron = cron;
    this.#noRetry = noRetry;
  }
  #noRetry;
  noRetry() {
    if (!(this instanceof __Facade_ScheduledController__)) {
      throw new TypeError("Illegal invocation");
    }
    this.#noRetry();
  }
};
var __facade_modules_fetch__ = function(request, env, ctx) {
  if (middleware_insertion_facade_default.fetch === void 0)
    throw new Error("Handler does not export a fetch() function.");
  return middleware_insertion_facade_default.fetch(request, env, ctx);
};
function getMaskedEnv(rawEnv) {
  let env = rawEnv;
  if (middleware_insertion_facade_default.envWrappers && middleware_insertion_facade_default.envWrappers.length > 0) {
    for (const wrapFn of middleware_insertion_facade_default.envWrappers) {
      env = wrapFn(env);
    }
  }
  return env;
}
var registeredMiddleware = false;
var facade2 = {
  ...middleware_insertion_facade_default.tail && {
    tail: maskHandlerEnv(middleware_insertion_facade_default.tail)
  },
  ...middleware_insertion_facade_default.trace && {
    trace: maskHandlerEnv(middleware_insertion_facade_default.trace)
  },
  ...middleware_insertion_facade_default.scheduled && {
    scheduled: maskHandlerEnv(middleware_insertion_facade_default.scheduled)
  },
  ...middleware_insertion_facade_default.queue && {
    queue: maskHandlerEnv(middleware_insertion_facade_default.queue)
  },
  ...middleware_insertion_facade_default.test && {
    test: maskHandlerEnv(middleware_insertion_facade_default.test)
  },
  ...middleware_insertion_facade_default.email && {
    email: maskHandlerEnv(middleware_insertion_facade_default.email)
  },
  fetch(request, rawEnv, ctx) {
    const env = getMaskedEnv(rawEnv);
    if (middleware_insertion_facade_default.middleware && middleware_insertion_facade_default.middleware.length > 0) {
      if (!registeredMiddleware) {
        registeredMiddleware = true;
        for (const middleware of middleware_insertion_facade_default.middleware) {
          __facade_register__(middleware);
        }
      }
      const __facade_modules_dispatch__ = function(type, init2) {
        if (type === "scheduled" && middleware_insertion_facade_default.scheduled !== void 0) {
          const controller = new __Facade_ScheduledController__(
            Date.now(),
            init2.cron ?? "",
            () => {
            }
          );
          return middleware_insertion_facade_default.scheduled(controller, env, ctx);
        }
      };
      return __facade_invoke__(
        request,
        env,
        ctx,
        __facade_modules_dispatch__,
        __facade_modules_fetch__
      );
    } else {
      return __facade_modules_fetch__(request, env, ctx);
    }
  }
};
function maskHandlerEnv(handler) {
  return (data, env, ctx) => handler(data, getMaskedEnv(env), ctx);
}
var middleware_loader_entry_default = facade2;

// ../../../.nvm/versions/node/v20.11.1/lib/node_modules/wrangler/templates/pages-dev-util.ts
function isRoutingRuleMatch(pathname, routingRule) {
  if (!pathname) {
    throw new Error("Pathname is undefined.");
  }
  if (!routingRule) {
    throw new Error("Routing rule is undefined.");
  }
  const ruleRegExp = transformRoutingRuleToRegExp(routingRule);
  return pathname.match(ruleRegExp) !== null;
}
function transformRoutingRuleToRegExp(rule) {
  let transformedRule;
  if (rule === "/" || rule === "/*") {
    transformedRule = rule;
  } else if (rule.endsWith("/*")) {
    transformedRule = `${rule.substring(0, rule.length - 2)}(/*)?`;
  } else if (rule.endsWith("/")) {
    transformedRule = `${rule.substring(0, rule.length - 1)}(/)?`;
  } else if (rule.endsWith("*")) {
    transformedRule = rule;
  } else {
    transformedRule = `${rule}(/)?`;
  }
  transformedRule = `^${transformedRule.replaceAll(/\./g, "\\.").replaceAll(/\*/g, ".*")}$`;
  return new RegExp(transformedRule);
}

// .wrangler/tmp/pages-ebF2m8/3g4881okzma.js
var define_ROUTES_default = {
  version: 1,
  description: "Generated by @sveltejs/adapter-cloudflare",
  include: [
    "/*"
  ],
  exclude: [
    "/_app/*",
    "/favicon.ico",
    "/favicon.svg",
    "/image.png",
    "/robots.txt",
    "/create",
    "/create/__data.json",
    "/credits",
    "/credits/__data.json",
    "/host",
    "/host/__data.json",
    "/play",
    "/play/__data.json",
    "/posts",
    "/posts/__data.json",
    "/posts/introducing-fuiz",
    "/posts/introducing-fuiz/__data.json",
    "/privacy",
    "/privacy/__data.json",
    "/sitemap.xml",
    "/",
    "/__data.json"
  ]
};
var routes = define_ROUTES_default;
var pages_dev_pipeline_default = {
  fetch(request, env, context) {
    const { pathname } = new URL(request.url);
    for (const exclude2 of routes.exclude) {
      if (isRoutingRuleMatch(pathname, exclude2)) {
        return env.ASSETS.fetch(request);
      }
    }
    for (const include of routes.include) {
      if (isRoutingRuleMatch(pathname, include)) {
        if (middleware_loader_entry_default.fetch === void 0) {
          throw new TypeError("Entry point missing `fetch` handler");
        }
        return middleware_loader_entry_default.fetch(request, env, context);
      }
    }
    return env.ASSETS.fetch(request);
  }
};

// ../../../.nvm/versions/node/v20.11.1/lib/node_modules/wrangler/templates/middleware/middleware-ensure-req-body-drained.ts
var drainBody2 = async (request, env, _ctx, middlewareCtx) => {
  try {
    return await middlewareCtx.next(request, env);
  } finally {
    try {
      if (request.body !== null && !request.bodyUsed) {
        const reader = request.body.getReader();
        while (!(await reader.read()).done) {
        }
      }
    } catch (e3) {
      console.error("Failed to drain the unused request body.", e3);
    }
  }
};
var middleware_ensure_req_body_drained_default2 = drainBody2;
var wrap3 = void 0;

// ../../../.nvm/versions/node/v20.11.1/lib/node_modules/wrangler/templates/middleware/middleware-miniflare3-json-error.ts
function reduceError2(e3) {
  return {
    name: e3?.name,
    message: e3?.message ?? String(e3),
    stack: e3?.stack,
    cause: e3?.cause === void 0 ? void 0 : reduceError2(e3.cause)
  };
}
var jsonError2 = async (request, env, _ctx, middlewareCtx) => {
  try {
    return await middlewareCtx.next(request, env);
  } catch (e3) {
    const error2 = reduceError2(e3);
    return Response.json(error2, {
      status: 500,
      headers: { "MF-Experimental-Error-Stack": "true" }
    });
  }
};
var middleware_miniflare3_json_error_default2 = jsonError2;
var wrap4 = void 0;

// .wrangler/tmp/bundle-F0QOhY/middleware-insertion-facade.js
var envWrappers2 = [wrap3, wrap4].filter(Boolean);
var facade3 = {
  ...pages_dev_pipeline_default,
  envWrappers: envWrappers2,
  middleware: [
    middleware_ensure_req_body_drained_default2,
    middleware_miniflare3_json_error_default2,
    ...pages_dev_pipeline_default.middleware ? pages_dev_pipeline_default.middleware : []
  ].filter(Boolean)
};
var middleware_insertion_facade_default2 = facade3;

// ../../../.nvm/versions/node/v20.11.1/lib/node_modules/wrangler/templates/middleware/common.ts
var __facade_middleware__2 = [];
function __facade_register__2(...args) {
  __facade_middleware__2.push(...args.flat());
}
function __facade_invokeChain__2(request, env, ctx, dispatch, middlewareChain) {
  const [head, ...tail] = middlewareChain;
  const middlewareCtx = {
    dispatch,
    next(newRequest, newEnv) {
      return __facade_invokeChain__2(newRequest, newEnv, ctx, dispatch, tail);
    }
  };
  return head(request, env, ctx, middlewareCtx);
}
function __facade_invoke__2(request, env, ctx, dispatch, finalMiddleware) {
  return __facade_invokeChain__2(request, env, ctx, dispatch, [
    ...__facade_middleware__2,
    finalMiddleware
  ]);
}

// .wrangler/tmp/bundle-F0QOhY/middleware-loader.entry.ts
var __Facade_ScheduledController__2 = class {
  constructor(scheduledTime, cron, noRetry) {
    this.scheduledTime = scheduledTime;
    this.cron = cron;
    this.#noRetry = noRetry;
  }
  #noRetry;
  noRetry() {
    if (!(this instanceof __Facade_ScheduledController__2)) {
      throw new TypeError("Illegal invocation");
    }
    this.#noRetry();
  }
};
var __facade_modules_fetch__2 = function(request, env, ctx) {
  if (middleware_insertion_facade_default2.fetch === void 0)
    throw new Error("Handler does not export a fetch() function.");
  return middleware_insertion_facade_default2.fetch(request, env, ctx);
};
function getMaskedEnv2(rawEnv) {
  let env = rawEnv;
  if (middleware_insertion_facade_default2.envWrappers && middleware_insertion_facade_default2.envWrappers.length > 0) {
    for (const wrapFn of middleware_insertion_facade_default2.envWrappers) {
      env = wrapFn(env);
    }
  }
  return env;
}
var registeredMiddleware2 = false;
var facade4 = {
  ...middleware_insertion_facade_default2.tail && {
    tail: maskHandlerEnv2(middleware_insertion_facade_default2.tail)
  },
  ...middleware_insertion_facade_default2.trace && {
    trace: maskHandlerEnv2(middleware_insertion_facade_default2.trace)
  },
  ...middleware_insertion_facade_default2.scheduled && {
    scheduled: maskHandlerEnv2(middleware_insertion_facade_default2.scheduled)
  },
  ...middleware_insertion_facade_default2.queue && {
    queue: maskHandlerEnv2(middleware_insertion_facade_default2.queue)
  },
  ...middleware_insertion_facade_default2.test && {
    test: maskHandlerEnv2(middleware_insertion_facade_default2.test)
  },
  ...middleware_insertion_facade_default2.email && {
    email: maskHandlerEnv2(middleware_insertion_facade_default2.email)
  },
  fetch(request, rawEnv, ctx) {
    const env = getMaskedEnv2(rawEnv);
    if (middleware_insertion_facade_default2.middleware && middleware_insertion_facade_default2.middleware.length > 0) {
      if (!registeredMiddleware2) {
        registeredMiddleware2 = true;
        for (const middleware of middleware_insertion_facade_default2.middleware) {
          __facade_register__2(middleware);
        }
      }
      const __facade_modules_dispatch__ = function(type, init2) {
        if (type === "scheduled" && middleware_insertion_facade_default2.scheduled !== void 0) {
          const controller = new __Facade_ScheduledController__2(
            Date.now(),
            init2.cron ?? "",
            () => {
            }
          );
          return middleware_insertion_facade_default2.scheduled(controller, env, ctx);
        }
      };
      return __facade_invoke__2(
        request,
        env,
        ctx,
        __facade_modules_dispatch__,
        __facade_modules_fetch__2
      );
    } else {
      return __facade_modules_fetch__2(request, env, ctx);
    }
  }
};
function maskHandlerEnv2(handler) {
  return (data, env, ctx) => handler(data, getMaskedEnv2(env), ctx);
}
var middleware_loader_entry_default2 = facade4;
export {
  middleware_loader_entry_default2 as default
};
/*! Bundled license information:

cookie/index.js:
  (*!
   * cookie
   * Copyright(c) 2012-2014 Roman Shtylman
   * Copyright(c) 2015 Douglas Christopher Wilson
   * MIT Licensed
   *)

@ltd/j-toml/index.mjs:
  (*!@preserve@license
   * 模块名称：j-regexp
   * 模块功能：可读性更好的正则表达式创建方式。从属于“简计划”。
     　　　　　More readable way for creating RegExp. Belong to "Plan J".
   * 模块版本：8.2.0
   * 许可条款：LGPL-3.0
   * 所属作者：龙腾道 <LongTengDao@LongTengDao.com> (www.LongTengDao.com)
   * 问题反馈：https://GitHub.com/LongTengDao/j-regexp/issues
   * 项目主页：https://GitHub.com/LongTengDao/j-regexp/
   *)
  (*!@preserve@license
   * 模块名称：j-orderify
   * 模块功能：返回一个能保证给定对象的属性按此后添加顺序排列的 proxy，即使键名是 symbol，或整数 string。从属于“简计划”。
     　　　　　Return a proxy for given object, which can guarantee own keys are in setting order, even if the key name is symbol or int string. Belong to "Plan J".
   * 模块版本：7.0.1
   * 许可条款：LGPL-3.0
   * 所属作者：龙腾道 <LongTengDao@LongTengDao.com> (www.LongTengDao.com)
   * 问题反馈：https://GitHub.com/LongTengDao/j-orderify/issues
   * 项目主页：https://GitHub.com/LongTengDao/j-orderify/
   *)
*/
//# sourceMappingURL=3g4881okzma.js.map
